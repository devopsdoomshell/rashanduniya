// JavaScript Document
$('.slider_owl').owlCarousel({
    loop:true,
    responsiveClass:true,
	 nav:false,
	autoplay:true,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:false,
            loop:true
        }
    }
})



$('.client_owl').owlCarousel({
    loop:true,
    responsiveClass:true,
	 nav:false,
	autoplay:true,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:false,
            loop:true
        }
    }
})


$('.recommonded_owl').owlCarousel({
    loop:true,
    responsiveClass:true,
	 nav:false,
	autoplay:true,
    responsive:{
        0:{
            items:1,
            nav:false
        },
		  480:{
            items:2,
            nav:false
        },
        643:{
            items:3,
            nav:false
        },
        1000:{
            items:4,
            nav:false,
            loop:true
        }
    }
})


$(document).ready(function() {
  $('.contact_form input, .contact_form textarea').each(function() {
    $(this).on('focus', function() {
      $(this).parent('.f-style').addClass('active');
    });

    $(this).on('blur', function() {
      if ($(this).val().length == 0) {
        $(this).parent('.f-style').removeClass('active');
      }
    });

    if ($(this).val() != '') $(this).parent('.f-style').addClass('active');
  });
});


$(document).ready(function() {
		$(document).delegate('.open', 'click', function(event){
			$(this).addClass('oppenned');
			event.stopPropagation();
		})
		$(document).delegate('body', 'click', function(event) {
			$('.open').removeClass('oppenned');
		})
		$(document).delegate('.cls', 'click', function(event){
			$('.open').removeClass('oppenned');
			event.stopPropagation();
		});
	});