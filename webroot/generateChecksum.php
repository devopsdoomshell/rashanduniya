<?php
header("Pragma: no-cache");
header("Cache-Control: no-cache");
header("Expires: 0");
// following files need to be included
//require_once("./lib/config_paytm.php");
require_once("Paytm/lib/encdec_paytm.php");
$checkSum = "";

// below code snippet is mandatory, so that no one can use your checksumgeneration url for other purpose .
$findme   = 'REFUND';
$findmepipe = '|';

$paramList = array();

$paramList["MID"] = $_POST["MID"];
$paramList["ORDER_ID"] = $_POST["ORDER_ID"];
$paramList["CUST_ID"] = $_POST["CUST_ID"];
$paramList["INDUSTRY_TYPE_ID"] = "Retail";
$paramList["CHANNEL_ID"] = "WAP";
$paramList["TXN_AMOUNT"] = $_POST["TXN_AMOUNT"];
$paramList["WEBSITE"] = $_POST["WEBSITE"];
$paramList["CALLBACK_URL"] = $_POST["CALLBACK_URL"];
/*
foreach($_POST as $key=>$value)
{  
  $pos = strpos($value, $findme);
  $pospipe = strpos($value, $findmepipe);
  if ($pos === false || $pospipe === false) 
    {
        $paramList[$key] = $value;
    }
}

*/
  
//Here checksum string will return by getChecksumFromArray() function.
$checkSum = getRefundChecksumFromArray($paramList,"KZyJf4XJrc@4uiCs");

 echo json_encode(array("CHECKSUMHASH" => $checkSum,"ORDER_ID" => $_POST["ORDER_ID"], "payt_STATUS" => "1"));


 
?>
