<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'cakephp/app',
  ),
  'versions' => 
  array (
    'admad/cakephp-jwt-auth' => 
    array (
      'pretty_version' => '2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1b600beea022679b52bf67bde3858ab6e078b322',
    ),
    'ajgl/breakpoint-twig-extension' => 
    array (
      'pretty_version' => '0.3.1',
      'version' => '0.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '360ec6351ad7e1968ee78abb31430046c2e04fc5',
    ),
    'alexpechkarev/geometry-library' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '85a0859e87343e871020f0bc7530309d75ab4a3c',
    ),
    'aptoma/twig-markdown' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '64a9c5c7418c08faf91c4410b34bdb65fb25c23d',
    ),
    'asm89/twig-cache-extension' => 
    array (
      'pretty_version' => '1.3.2',
      'version' => '1.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '630ea7abdc3fc62ba6786c02590a1560e449cf55',
    ),
    'aura/intl' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7fce228980b19bf4dee2d7bbd6202a69b0dde926',
    ),
    'cakephp/app' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'cakephp/bake' => 
    array (
      'pretty_version' => '1.7.3',
      'version' => '1.7.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '68db6894e7b572f073b0c7654796b020890b7906',
    ),
    'cakephp/cache' => 
    array (
      'replaced' => 
      array (
        0 => '3.6.3',
      ),
    ),
    'cakephp/cakephp' => 
    array (
      'pretty_version' => '3.6.3',
      'version' => '3.6.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '1cfebf6c6719a2a743d9a3e59ee091a726f18844',
    ),
    'cakephp/cakephp-codesniffer' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd77ac81199f2f1e5a8d8ebf96a5d6d7cd4e0542b',
    ),
    'cakephp/chronos' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '85bcaea6a832684b32ef54b2487b0c14a172e9e6',
    ),
    'cakephp/collection' => 
    array (
      'replaced' => 
      array (
        0 => '3.6.3',
      ),
    ),
    'cakephp/core' => 
    array (
      'replaced' => 
      array (
        0 => '3.6.3',
      ),
    ),
    'cakephp/database' => 
    array (
      'replaced' => 
      array (
        0 => '3.6.3',
      ),
    ),
    'cakephp/datasource' => 
    array (
      'replaced' => 
      array (
        0 => '3.6.3',
      ),
    ),
    'cakephp/debug_kit' => 
    array (
      'pretty_version' => '3.16.0',
      'version' => '3.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eaaf2f6bf7be23c249e2924c1fe36be013bde84a',
    ),
    'cakephp/event' => 
    array (
      'replaced' => 
      array (
        0 => '3.6.3',
      ),
    ),
    'cakephp/filesystem' => 
    array (
      'replaced' => 
      array (
        0 => '3.6.3',
      ),
    ),
    'cakephp/form' => 
    array (
      'replaced' => 
      array (
        0 => '3.6.3',
      ),
    ),
    'cakephp/i18n' => 
    array (
      'replaced' => 
      array (
        0 => '3.6.3',
      ),
    ),
    'cakephp/log' => 
    array (
      'replaced' => 
      array (
        0 => '3.6.3',
      ),
    ),
    'cakephp/migrations' => 
    array (
      'pretty_version' => '1.8.1',
      'version' => '1.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd65daa9fae933bc0ccc69d5b5d92460375da9e2',
    ),
    'cakephp/orm' => 
    array (
      'replaced' => 
      array (
        0 => '3.6.3',
      ),
    ),
    'cakephp/plugin-installer' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '41373d0678490502f45adc7be88aa22d24ac1843',
    ),
    'cakephp/utility' => 
    array (
      'replaced' => 
      array (
        0 => '3.6.3',
      ),
    ),
    'cakephp/validation' => 
    array (
      'replaced' => 
      array (
        0 => '3.6.3',
      ),
    ),
    'composer/ca-bundle' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd2c0a83b7533d6912e8d516756ebd34f893e9169',
    ),
    'composer/composer' => 
    array (
      'pretty_version' => '1.6.5',
      'version' => '1.6.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b184a92419cc9a9c4c6a09db555a94d441cb11c9',
    ),
    'composer/semver' => 
    array (
      'pretty_version' => '1.4.2',
      'version' => '1.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c7cb9a2095a074d131b65a8a0cd294479d785573',
    ),
    'composer/spdx-licenses' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb17687e9f936acd7e7245ad3890f953770dec1b',
    ),
    'dnoegel/php-xdg-base-dir' => 
    array (
      'pretty_version' => '0.1',
      'version' => '0.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '265b8593498b997dc2d31e75b89f053b5cc9621a',
    ),
    'firebase/php-jwt' => 
    array (
      'pretty_version' => 'v5.2.1',
      'version' => '5.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f42c9110abe98dd6cfe9053c49bc86acc70b2d23',
    ),
    'jakub-onderka/php-console-color' => 
    array (
      'pretty_version' => '0.1',
      'version' => '0.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e0b393dacf7703fc36a4efc3df1435485197e6c1',
    ),
    'jakub-onderka/php-console-highlighter' => 
    array (
      'pretty_version' => 'v0.3.2',
      'version' => '0.3.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '7daa75df45242c8d5b75a22c00a201e7954e4fb5',
    ),
    'jasny/twig-extensions' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '30bdf3a3903c021544f36332c9d5d4d563527da4',
    ),
    'jdorn/sql-formatter' => 
    array (
      'pretty_version' => 'v1.2.17',
      'version' => '1.2.17.0',
      'aliases' => 
      array (
      ),
      'reference' => '64990d96e0959dff8e059dfcdc1af130728d92bc',
    ),
    'josegonzalez/dotenv' => 
    array (
      'pretty_version' => '3.2.0',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f19174d9d7213a6c20e8e5e268aa7dd042d821ca',
    ),
    'justinrainbow/json-schema' => 
    array (
      'pretty_version' => '5.2.7',
      'version' => '5.2.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '8560d4314577199ba51bf2032f02cd1315587c23',
    ),
    'm1/env' => 
    array (
      'pretty_version' => '2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3589eae8e40d40be96de39222a6ca19c3af8eae4',
    ),
    'mobiledetect/mobiledetectlib' => 
    array (
      'pretty_version' => '2.8.31',
      'version' => '2.8.31.0',
      'aliases' => 
      array (
      ),
      'reference' => 'adb882ea3b9d154f087ecb2c333180dad6f4dd37',
    ),
    'muffin/slug' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e36fe293fe4fe65db9e10528dfd54beb0edfa226',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.0.1',
      'version' => '4.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e4a54fa90a5cd8e8dd3fb4099942681731c5cdd3',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.0.2',
      'version' => '1.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '4ebe3a8bf773a19edfe0a84b6585ba3d401b724d',
    ),
    'psy/psysh' => 
    array (
      'pretty_version' => 'v0.9.3',
      'version' => '0.9.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '79c280013cf0b30fa23f3ba8bd3649218075adf4',
    ),
    'robmorgan/phinx' => 
    array (
      'pretty_version' => 'v0.8.1',
      'version' => '0.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a19de5bebc59321edd9613bc2a667e7f96224ec',
    ),
    'seld/cli-prompt' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a19a7376a4689d4d94cab66ab4f3c816019ba8dd',
    ),
    'seld/jsonlint' => 
    array (
      'pretty_version' => '1.7.1',
      'version' => '1.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd15f59a67ff805a44c50ea0516d2341740f81a38',
    ),
    'seld/phar-utils' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7009b5139491975ef6486545a39f3e6dad5ac30a',
    ),
    'squizlabs/php_codesniffer' => 
    array (
      'pretty_version' => '3.2.3',
      'version' => '3.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '4842476c434e375f9d3182ff7b89059583aa8b27',
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v3.4.9',
      'version' => '3.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '7c2a9d44f4433863e9bca682e7f03609234657f9',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v3.4.9',
      'version' => '3.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b1fdfa8eb93464bcc36c34da39cedffef822cdf',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v3.4.9',
      'version' => '3.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '1b95888cfd996484527cb41e8952d9a5eaf7454f',
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v3.4.9',
      'version' => '3.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '253a4490b528597aa14d2bf5aeded6f5e5e4a541',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v3.4.9',
      'version' => '3.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bd14efe8b1fabc4de82bf50dce62f05f9a102433',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3296adf6a6454a050679cde90f95350ad604b171',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v3.4.9',
      'version' => '3.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '4b7d64e852886319e93ddfdecff0d744ab87658b',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v3.4.9',
      'version' => '3.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '0e6545672d8c9ce70dd472adc2f8b03155a46f73',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v3.4.9',
      'version' => '3.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '033cfa61ef06ee0847e056e530201842b6e926c3',
    ),
    'tecnickcom/tcpdf' => 
    array (
      'pretty_version' => '6.4.1',
      'version' => '6.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5ba838befdb37ef06a16d9f716f35eb03cb1b329',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v1.35.3',
      'version' => '1.35.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b48680b6eb7d16b5025b9bfc4108d86f6b8af86f',
    ),
    'umpirsky/twig-php-function' => 
    array (
      'pretty_version' => 'v0.1',
      'version' => '0.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '53b4b1eb0c5eacbd7d66c504b7d809c79b4bedbc',
    ),
    'wyrihaximus/twig-view' => 
    array (
      'pretty_version' => '4.3.4',
      'version' => '4.3.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '95977a9e22745fa8a5226a7be5850c6ca2e3ebe3',
    ),
    'zendframework/zend-diactoros' => 
    array (
      'pretty_version' => '1.7.1',
      'version' => '1.7.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bf26aff803a11c5cc8eb7c4878a702c403ec67f1',
    ),
  ),
);
