<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property int $customer_id
 * @property int $customer_address_id
 * @property int $coupon_id
 * @property float $sub_total
 * @property float $coupon_discount
 * @property float $product_discount
 * @property int $delivery_charge
 * @property float $total_amount
 * @property int|null $payment_type_id
 * @property string|null $status
 * @property string|null $transaction_no
 * @property string $payment_status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int $paymentstatus
 *
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\CustomerAddress $customer_address
 * @property \App\Model\Entity\Coupon $coupon
 * @property \App\Model\Entity\PaymentType $payment_type
 * @property \App\Model\Entity\OrderDetail[] $order_details
 * @property \App\Model\Entity\TblWallet[] $tbl_wallet
 */
class Order extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'customer_id' => true,
        'customer_address_id' => true,
        'coupon_id' => true,
        'sub_total' => true,
        'coupon_discount' => true,
        'product_discount' => true,
        'delivery_charge' => true,
        'total_amount' => true,
        'payment_type_id' => true,
        'status' => true,
        'transaction_no' => true,
        'payment_status' => true,
        'created' => true,
        'modified' => true,
        'paymentstatus' => true,
        'customer' => true,
        'customer_address' => true,
        'coupon' => true,
        'payment_type' => true,
        'order_details' => true,
        'tbl_wallet' => true,
    ];
}
