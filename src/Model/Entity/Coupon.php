<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Coupon Entity
 *
 * @property int $id
 * @property int $vendor_id
 * @property string $code
 * @property string $applicable_to
 * @property string $applicable_type
 * @property string $discount_type
 * @property float $minimum_order_value
 * @property float $maximum_discount
 * @property float $discount_rate
 * @property \Cake\I18n\FrozenTime $valid_from
 * @property \Cake\I18n\FrozenTime $valid_to
 * @property string $max_redeem_type
 * @property float $max_redeem_rate
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Vendor $vendor
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Category[] $categories
 * @property \App\Model\Entity\Product[] $products
 */
class Coupon extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'vendor_id' => true,
        'code' => true,
        'applicable_to' => true,
        'applicable_type' => true,
        'discount_type' => true,
        'minimum_order_value' => true,
        'maximum_discount' => true,
        'discount_rate' => true,
        'valid_from' => true,
        'valid_to' => true,
        'max_redeem_type' => true,
        'max_redeem_rate' => true,
        'created' => true,
        'modified' => true,
        'vendor' => true,
        'orders' => true,
        'categories' => true,
        'products' => true,
    ];
}
