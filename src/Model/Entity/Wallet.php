<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Wallet Entity
 *
 * @property int $id
 * @property int $payment_id
 * @property int $user_id
 * @property float $amount
 * @property bool $is_expiry
 * @property \Cake\I18n\FrozenTime $expiry_date
 * @property string $amount_type
 * @property bool $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Payment $payment
 * @property \App\Model\Entity\User $user
 */
class Wallet extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'payment_id' => true,
        'user_id' => true,
        'amount' => true,
        'is_expiry' => true,
        'expiry_date' => true,
        'amount_type' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'payment' => true,
        'user' => true,
    ];
}
