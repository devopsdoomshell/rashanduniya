<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DeliveryBoy Entity
 *
 * @property int $id
 * @property int $vendor_id
 * @property string $name
 * @property int $mobile
 * @property bool $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Vendor $vendor
 */
class DeliveryBoy extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'vendor_id' => true,
        'name' => true,
        'mobile' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'vendor' => true,
    ];

    public function _getNameMobile()
    {
        return $this->name . '(' . $this->mobile . ')';
    }
}
