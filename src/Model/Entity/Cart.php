<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cart Entity
 *
 * @property int $id
 * @property int $user_id
 * @property int $vendor_id
 * @property int $product_id
 * @property int $attribute_id
 * @property int $attribute_type
 * @property float $quantity
 * @property bool $status
 * @property \Cake\I18n\FrozenTime $created
 * @property int|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Vendor $vendor
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Attribute $attribute
 */
class Cart extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'vendor_id' => true,
        'product_id' => true,
        'attribute_id' => true,
        'attribute_type' => true,
        'quantity' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'vendor' => true,
        'product' => true,
        'attribute' => true,
    ];
}
