<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Payment Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $transaction_id
 * @property float $amount
 * @property string $status
 * @property int $payment_type_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Transaction $transaction
 * @property \App\Model\Entity\PaymentType $payment_type
 * @property \App\Model\Entity\Wallet[] $wallets
 */
class Payment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'user_id' => true,
        'transaction_id' => true,
        'amount' => true,
        'status' => true,
        'payment_type_id' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'transaction' => true,
        'payment_type' => true,
        'wallets' => true,
    ];
}
