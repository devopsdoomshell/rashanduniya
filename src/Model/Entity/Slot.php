<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Slot Entity
 *
 * @property int $id
 * @property int $vendor_id
 * @property string $weekday
 * @property \Cake\I18n\FrozenTime|null $mintime
 * @property \Cake\I18n\FrozenTime|null $maxtime
 * @property string $status
 *
 * @property \App\Model\Entity\Vendor $vendor
 * @property \App\Model\Entity\Order[] $orders
 */
class Slot extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'vendor_id' => true,
        'weekday' => true,
        'mintime' => true,
        'maxtime' => true,
        'status' => true,
        'vendor' => true,
        'orders' => true,
    ];
    public function _getSlotName()
    {
        return date('h A', strtotime($this->mintime)) . '-' . date('h A', strtotime($this->maxtime));
    }
}
