<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class RolesTable extends Table {

    public $name = 'Roles';
    
    public function initialize(array $config)
    {
	    $this->table('tbl_roles');
	    $this->primaryKey('id');

	}

}
?>
