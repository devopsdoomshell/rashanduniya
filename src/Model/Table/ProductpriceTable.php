<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class ProductpriceTable extends Table {

    public $name = 'Productprice';
    
    public function initialize(array $config)
    {
	    $this->table('tbl_product_pricehistory');
	    $this->primaryKey('id');
	    
	    $this->belongsTo('Products', [
            'foreignKey' => 'p_id',
            'joinType' => 'INNER',
            ]);

        $this->belongsTo('Pricerequest', [
            'foreignKey' => 'r_id',
            'joinType' => 'INNER',
            ]);

	}


}
?>
