<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class WalletsTable extends Table
{

    public $name = 'Wallet';

    public function initialize(array $config)
    {
        $this->table('wallets');
        $this->primaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Payments', [
            'foreignKey' => 'payment_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
        ]);

        $this->hasMany('WalletExpenses', [
            'className' => 'Wallets',
            'foreignKey' => 'wallet_id',
        ])->setConditions(['WalletExpenses.amount_type' => 'expense']);

        $this->hasMany('WalletIncomes', [
            'className' => 'Wallets',
            'foreignKey' => 'wallet_id',
        ])->setConditions(['WalletIncomes.amount_type IN' => ['payment', 'admin'], 'is_expiry' => 0]);

        $this->hasMany('CashBacks', [
            'className' => 'Wallets',
            'foreignKey' => 'wallet_id',
        ])->setConditions(['CashBacks.amount_type IN' => ['promotional', 'admin'], 'is_expiry' => 1]);

    }

}
