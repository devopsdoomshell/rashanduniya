<?php
namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Orders Model
 *
 * @property \App\Model\Table\CustomersTable&\Cake\ORM\Association\BelongsTo $Customers
 * @property \App\Model\Table\CustomerAddressesTable&\Cake\ORM\Association\BelongsTo $CustomerAddresses
 * @property \App\Model\Table\CouponsTable&\Cake\ORM\Association\BelongsTo $Coupons
 * @property \App\Model\Table\PaymentTypesTable&\Cake\ORM\Association\BelongsTo $PaymentTypes
 * @property \App\Model\Table\OrderDetailsTable&\Cake\ORM\Association\HasMany $OrderDetails
 * @property \App\Model\Table\TblWalletTable&\Cake\ORM\Association\HasMany $TblWallet
 *
 * @method \App\Model\Entity\Order get($primaryKey, $options = [])
 * @method \App\Model\Entity\Order newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Order[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Order|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Order[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Order findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrdersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('UserAddresses', [
            'foreignKey' => 'user_address_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Coupons', [
            'foreignKey' => 'coupon_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('PaymentTypes', [
            'foreignKey' => 'payment_type_id',
        ]);
        $this->hasMany('OrderDetails', [
            'foreignKey' => 'order_id',
        ]);
        $this->belongsTo('Vendors', [
            'foreignKey' => 'vendor_id',
        ]);
        $this->hasMany('TblWallet', [
            'foreignKey' => 'order_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->numeric('sub_total')
            ->requirePresence('sub_total', 'create')
            ->notEmptyString('sub_total');

        $validator
            ->numeric('coupon_discount')
            ->requirePresence('coupon_discount', 'create')
            ->notEmptyString('coupon_discount');

        $validator
            ->numeric('product_discount')
            ->requirePresence('product_discount', 'create')
            ->notEmptyString('product_discount');

        $validator
            ->integer('delivery_charge')
            ->requirePresence('delivery_charge', 'create')
            ->notEmptyString('delivery_charge');

        $validator
            ->numeric('total_amount')
            ->requirePresence('total_amount', 'create')
            ->notEmptyString('total_amount');

        $validator
            ->scalar('status')
            ->allowEmptyString('status');

        $validator
            ->scalar('transaction_no')
            ->maxLength('transaction_no', 200)
            ->allowEmptyString('transaction_no');

        $validator
            ->scalar('payment_status')
            ->maxLength('payment_status', 20)
            ->requirePresence('payment_status', 'create')
            ->notEmptyString('payment_status');

        $validator
            ->integer('paymentstatus')
            ->requirePresence('paymentstatus', 'create')
            ->notEmptyString('paymentstatus');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_address_id'], 'UserAddresses'));
        // $rules->add($rules->existsIn(['coupon_id'], 'Coupons'));
        $rules->add($rules->existsIn(['payment_type_id'], 'PaymentTypes'));
        return $rules;
    }
}
