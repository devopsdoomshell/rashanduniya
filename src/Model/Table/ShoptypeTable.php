<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class ShoptypeTable extends Table {

    public $name = 'Shoptype';
    
    public function initialize(array $config)
    {
	    $this->table('tbl_shoptype');
	    $this->primaryKey('id');

	}

}
?>
