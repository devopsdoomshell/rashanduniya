<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class TokensTable extends Table {

    public $name = 'Tokens';
    
    public function initialize(array $config)
    {
	    $this->table('tokens');
	    $this->primaryKey('id');

	}

}
?>