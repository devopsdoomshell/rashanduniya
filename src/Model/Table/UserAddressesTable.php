<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class UserAddressesTable extends Table
{

    public function initialize(array $config)
    {
        $this->belongsTo('Users');
    }
    public function updateDefault($userId)
    {
        $this->updateAll(
            [ // fields
                'is_default' => 0,
            ],
            [ // conditions
                'is_default' => 1,
                'user_id' => $userId,
            ]
        );
    }

}
