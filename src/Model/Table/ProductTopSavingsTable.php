<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class ProductTopSavingsTable extends Table
{

    public $name = 'ProductTopSavings';

    public function initialize(array $config)
    {
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Vendors');

    }

}
