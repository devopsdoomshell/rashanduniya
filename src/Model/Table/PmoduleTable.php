<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class PmoduleTable extends Table {

    public function initialize(array $config)
    {
	    $this->table('permission_module');
	    $this->primaryKey('id');

	}

}
?>
