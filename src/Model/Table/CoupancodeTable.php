<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class CoupancodeTable extends Table {

    public $name = 'Coupancode';
    
    public function initialize(array $config)
    {
	    $this->table('tbl_coupancode');
	    $this->primaryKey('id');

	}

}
?>
