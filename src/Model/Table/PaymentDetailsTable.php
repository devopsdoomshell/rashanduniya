<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class PaymentDetailsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payment_details');
        $this->setPrimaryKey('id');

        $this->belongsTo('Payments');

    }

}
