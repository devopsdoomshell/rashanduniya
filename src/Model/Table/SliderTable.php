<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class SliderTable extends Table {

    public $name = 'Slider';
    
    public function initialize(array $config)
    {
	    $this->table('tbl_slider');
	    $this->primaryKey('id');

	}

}
?>
