<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class PaymentsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payments');
        $this->setPrimaryKey('id');

        $this->hasMany('PaymentDetails');

    }

}
