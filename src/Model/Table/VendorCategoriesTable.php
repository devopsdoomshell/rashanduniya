<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class VendorCategoriesTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Timestamp');

        $this->belongsTo('Categories');
        $this->belongsTo('Vendors');

    }

}
