<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class StockdetailTable extends Table {

    public $name = 'Stockdetail';
    
    public function initialize(array $config)
    {
	    $this->table('tbl_stockhistrory');
	    $this->primaryKey('id');

	    $this->belongsTo('Products', [
            'foreignKey' => 'p_id',
            'joinType' => 'INNER',
            ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT',
            ]);

	}

}
?>
