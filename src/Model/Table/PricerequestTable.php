<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class PricerequestTable extends Table {

    public $name = 'Pricerequest';
    
    public function initialize(array $config)
    {
	    $this->table('tbl_price_request');
	    $this->primaryKey('id');
	    
	    $this->belongsTo('Products', [
            'foreignKey' => 'p_id',
            'joinType' => 'INNER',
            ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            ]);

       

	}


	  protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }

}
?>
