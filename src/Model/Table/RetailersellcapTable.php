<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class RetailersellcapTable extends Table {

    public $name = 'Retailersellcap';
    
    public function initialize(array $config)
    {
	    $this->table('retailer_selling_capacity');
	    $this->primaryKey('id');
	    
	      $this->belongsTo('Users', [
            'foreignKey' => 'retailer_id',
            'joinType' => 'INNER',
            ]);
            $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
            ]);
	    
	    
	}


	  protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }

}
?>
