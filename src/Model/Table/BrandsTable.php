<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class BrandsTable extends Table
{

    public $name = 'Brands';
    public function initialize(array $config)
    {
        $this->table('brands');
        $this->setDisplayField('name');
    }

}
