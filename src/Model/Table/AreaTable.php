<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class AreaTable extends Table {

    public $name = 'Area';
    
    public function initialize(array $config)
    {
	    $this->table('tbl_area');
	    $this->primaryKey('id');

	}

}
?>
