<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class CartTable extends Table {

    public $name = 'Cart';
    
    public function initialize(array $config)
    {
	    $this->table('cart');
	    $this->primaryKey('id');
	    
	      $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            ]);
            $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
            ]);
	    
	    
	}


	  protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }

}
?>
