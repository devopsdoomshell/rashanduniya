<?php
namespace App\Model\Table;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Table;

class productsTable extends Table
{

    public $name = 'Products';

    public function initialize(array $config)
    {
        $this->displayField('productnameen');
        $this->table('products');
        $this->primaryKey('id');

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Vendors');
        $this->belongsTo('Categories', [
            'foreignKey' => 'subcategory_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('ParentCategory', [
            'className' => 'Categories',
            'foreignKey' => 'category_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('ProductTopSavings', [
            'className' => 'ProductTopSavings',
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsToMany('Attributes', [
            'through' => 'ProductsAttributes',
        ]);

        $this->hasMany('Ordersdetail', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsToMany('Coupons', [
            'foreignKey' => 'product_id',
            'targetForeignKey' => 'coupon_id',
            'joinTable' => 'coupons_products',
        ]);

    }
    public function isOwnedBy($productId, $vendorId)
    {
        return $this->exists(['id' => $productId, 'vendor_id' => $vendorId]);
    }

    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }

}
