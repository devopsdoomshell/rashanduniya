<?php
namespace App\Model\Table;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Table;

class UsersTable extends Table
{

    public $name = 'Users';

    public function initialize(array $config)
    {
        $this->table('users');
        $this->primaryKey('id');

        $this->belongsTo('Cart', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Area', [
            'foreignKey' => 'area_id',
            'joinType' => 'INNER',
        ]);
        $this->hasOne('vendor', [
            'className' => 'Vendors',
        ])
            ->setConditions(['Users.role_id' => 2]);

        $this->belongsTo('Shoptype', [
            'foreignKey' => 'Shop_type',
            'joinType' => 'LEFT',
        ]);

        $this->hasMany('Retailersellcap', [
            'foreignKey' => 'retailer_id',
            'joinType' => 'LEFT',
        ]);
        $this->hasMany('UserAddresses', [
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Roles');

    }
 

    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }

}
