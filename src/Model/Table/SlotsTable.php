<?php
namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Slots Model
 *
 * @property \App\Model\Table\VendorsTable&\Cake\ORM\Association\BelongsTo $Vendors
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\Slot get($primaryKey, $options = [])
 * @method \App\Model\Entity\Slot newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Slot[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Slot|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Slot saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Slot patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Slot[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Slot findOrCreate($search, callable $callback = null, $options = [])
 */
class SlotsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('slots');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Vendors', [
            'foreignKey' => 'vendor_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'slot_id',
        ]);
    }

    public function updateSlots($vendorId)
    {
        $this->updateAll(
            [ // fields
                'status' => 'N',
            ],
            [ // conditions
                'status' => 'Y',
                'vendor_id' => $vendorId,
            ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('weekday')
            ->maxLength('weekday', 20)
            ->requirePresence('weekday', 'create')
            ->notEmptyString('weekday');

        $validator
            ->time('mintime')
            ->allowEmptyTime('mintime');

        $validator
            ->time('maxtime')
            ->allowEmptyTime('maxtime');

        $validator
            ->scalar('status')
            ->notEmptyString('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['vendor_id'], 'Vendors'));

        return $rules;
    }
}
