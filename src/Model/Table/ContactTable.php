<?php
namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class ContactTable extends Table {

    public $name = 'Contact';
    
    public function initialize(array $config)
    {
	    $this->table('contact_list');
	    $this->primaryKey('id');

	}

}
?>
