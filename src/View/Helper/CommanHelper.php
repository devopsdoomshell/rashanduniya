<?php
namespace App\View\Helper;

use Cake\ORM\TableRegistry;
use Cake\View\Helper;
use Cake\View\View;

class CommanHelper extends Helper
{
    // initialize() hook is available since 3.2. For prior versions you can
    // override the constructor if required.
    public function initialize(array $config)
    {

    }
    public function orders($id = null)
    {
        $articles = TableRegistry::get('Orders');
        return $articles->find('all')->where(['Orders.vendor_id' => $id])->count();

    }
    public function totalVendorPayment($id = null)
    {
        $articles = TableRegistry::get('Vendorpayments');
        return $articles->find('all')->select(['sum' => 'SUM(amount)'])->where(['vendor_id' => $id])->first()->sum;

    }

    public function getVendorCompletedOrders($id = null)
    {
        $articles = TableRegistry::get('Orders');
        return $articles->find('all')->where(['vendor_id' => $id, 'order_status' => 'delivered'])->count();
    }

    public function getVendorPendingOrders($id = null)
    {
        $articles = TableRegistry::get('Orders');
        return $articles->find('all')->where(['vendor_id' => $id, 'AND' => [['order_status <>' => 'delivered'], ['order_status <>' => 'cancelled']]])->count();
    }
}
