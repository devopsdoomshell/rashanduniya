<?php 
namespace App\Controller;
use App\Controller;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
	include(ROOT .DS. "vendor" . DS  . "PHPExcel/Classes/" . DS . "PHPExcel.php");
		include(ROOT .DS. "vendor" . DS  . "PHPExcel/Classes/PHPExcel/" . DS . "IOFactory.php");
class UsersController extends AppController
{
	
		
	public function initialize()
	{
        parent::initialize();
       	$this->loadModel('Users');
        $this->Auth->allow(['excelorder','forgetcpass','forgotcpassword','findUsername','updateprofile','_setPassword']);
    }

	public  function _setPassword($password)
	{
		return (new DefaultPasswordHasher)->hash($password);
	}

	public function setting()
    {

    }


  public function excelorder($id,$name)
    {
        $this->loadModel('Users');
        $this->loadModel('Orders');
        $this->loadModel('Ordersdetail');
        $user =$this->Users->find('all')->order(['Users.id' => 'DESC'])->toarray();        
        $this->set(compact('user'));
    }


    // function used for dupicate email check:
	public function findUsername($username = null)
	{
		$username=$this->request->data['username'];
		$user = $this->Users->find('all')->where(['Users.email' =>$username])->toArray();
		echo $user[0]['id'];
		die;
	}
	
	
	public function forgotcpassword(){
		$this->loadModel('Users');
		if($this->request->is('post')) {

			$email=$this->request->data['email'];
			$to=$email;
			$useremail=$this->Users->find('all')->where(['Users.email' =>$email])->first(); 
			if(count($useremail)==0){
				$this->Flash->error(__('Invalid email or password, try again'));
				return $this->redirect(['controller' => 'users','action'=>'forgotpassword']);
			}else{
				$userid=$useremail['id'];
				$name=$useremail['name'];
				$site_url=SITE_URL;
				$fkey=rand(1,10000);
				
				$Pack = $this->Users->get($userid);
				$Pack->fkey = $fkey;
				$this->Users->save($Pack);
				$mid=base64_encode(base64_encode($userid.'/'.$fkey));
				$url=SITE_URL."users/forgetcpass/".$mid;
				$this->loadmodel('Templates');
				$profile = $this->Templates->find('all')->where(['Templates.id' =>FORGOTPASSWORD])->first();
				$subject = $profile['subject'];
				
				$from= $profile['from'];
				$fromname = $profile['fromname'];
				$to  = $email;
				$formats=$profile['description'];
				$site_url=SITE_URL;

				$message1 = str_replace(array('{Name}','{site_url}','{url}'), array($name,$site_url,$url), $formats);
				$message = stripslashes($message1);
				$message='
				<!DOCTYPE HTML>
				<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<title>Mail</title>
				</head>
				<body style="padding:0px; margin:0px;font-family:Arial,Helvetica,sans-serif; font-size:13px;">
					'.$message1.'
				</body>
				</html>
				';	
				//die;
				$headers = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    //$headers .= 'To: <'.$to.'>' . "\r\n";
				$headers .= 'From: '.$fromname.' <'.$from.'>' . "\r\n";
				$emailcheck= mail($to, $subject, $message, $headers);
				$this->Flash->success(__('Forgot passowrd link sent to Your Email'));
                                return $this->redirect(['controller' => 'Users','action'=>'forgotcpassword']);
			}
			
		}}
		
		
		public function forgetcpass($mid='',$userid='')
		{
	// pr($userid); 
			if(!empty($userid))
			{
				$id=$userid;   
			}
			else
			{    
				$mix=explode("/",base64_decode(base64_decode($mid)));
				$id=$mix[0];
				$fkey=$mix[1];
				$user=$this->Users->find('all')->select(['id', 'fkey'])->where(['Users.id' =>$id])->first(); 
				$usrfkey=$user['fkey'];
				if($usrfkey==$fkey)
				{
					$ftyp=1;
				}
				else
				{
					$ftyp=0;
				}
				$this->set('ftyp',$ftyp);
				$this->set('usrid',$id);
			}     
			if($this->request->is('post')) {
	    //	pr($this->request->data); die;
				$confirmpass=$this->request->data['password'];
				$Pack = $this->Users->get($id);
				$updpass = $this->_setPassword($this->request->data['password']);
				$Pack->password = $updpass;
				$Pack->confirm_pass = $confirmpass;
				if ($this->Users->save($Pack)) {
					$Pack = $this->Users->get($id);
					$Pack->fkey =0;
					$this->Users->save($Pack);
					$useremail= $this->Users->find('all')->where(['Users.id' =>$id])->first(); 
		//pr($useremail); die;

$this->Auth->setUser($useremail);
		//$this->User->find('first',array('fields'=>array('username','name','sname'),'recursive'=>-1,'conditions'=>array('User.id'=>$id)));
					$email=$useremail['email'];
					$name=$useremail['name'];
					$password=$this->request->data['password'];
					$this->loadmodel('Templates');
					$profile = $this->Templates->find('all')->where(['Templates.id' =>FORGOTPASSWORDCHANGED])->first();
					$subject = $profile['subject'];
					$from= $profile['from'];
					$fromname = $profile['fromname'];
					$to  = $email;
					$formats=$profile['description'];
					$site_url=SITE_URL;
					$message1 = str_replace(array('{Name}','{Email}','{Password}','{site_url}'), array($name,$email,$password,$site_url), $formats);
					$message = stripslashes($message1);
					$message='
					<!DOCTYPE HTML>
					<html>
					<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
						<title>Mail</title>
					</head>
					<body style="padding:0px; margin:0px;font-family:Arial,Helvetica,sans-serif; font-size:13px;">
						'.$message1.'
					</body>
					</html>
					';	
					$headers = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		//$headers .= 'To: <'.$to.'>' . "\r\n";
					$headers .= 'From: '.$fromname.' <'.$from.'>' . "\r\n";
					$emailcheck= mail($to, $subject, $message, $headers);
				if($useremail['role_id']==2){
				$this->Flash->success(__('Password Changed Successfully'));
					return $this->redirect(['controller' => 'homes','action'=>'myevent']);
				}else{
					$this->Flash->success(__('Password Changed Successfully'));
					return $this->redirect(['controller' => 'tickets','action'=>'myticket']);
				}
				}
			}
		}
		
public function updateprofile($id=null){
	// $user_id=$id;
	//$this->request->data['id']= $user_id;
	$id=$this->request->session()->read('Auth.User.id');
	$this->loadModel('Users');
	$user = $this->Users->get($id);
	$this->set('user', $user);
	//pr($user);die;
	if ($this->request->is(['post', 'put'])) {
	//pr($this->request->data);die;
	
	if($this->request->data['password']){
	
	$this->request->data['confirm_pass'] =$this->request->data['confirm_pass'];
	$this->request->data['password'] = (new DefaultPasswordHasher)->hash($this->request->data['confirm_pass']); 
	$user = $this->Users->patchEntity($user, $this->request->data);
	//pr($this->request->data);die;
	if ($user=$this->Users->save($user)) {
$this->Auth->logout();
	$this->Flash->success(__('Your password has been changed successfully'));
	return $this->redirect(['controller' => 'logins','action'=>'signup']);
	//$this->autoRender = false;  
	}
	
}else{
	$profiledata['mobile'] = $this->request->data['mobile'];
$profiledata['name']= $this->request->data['name'];
	$user = $this->Users->patchEntity($user, $profiledata);
	//pr($this->request->data);die;
	if ($user=$this->Users->save($user)) {
	$this->Flash->success(__('Your profile details has been sucessfully updated.'));
	return $this->redirect(['action' => '/'.'updateprofile']);
	//$this->autoRender = false;  
	}
	
	
}
	
	}

	}


	}
