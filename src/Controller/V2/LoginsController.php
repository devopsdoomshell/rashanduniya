<?php

namespace App\Controller\V2;

use App\Controller\V2\AppController;
use Cake\Event\Event;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

class LoginsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['userLogin', 'userOtpVerify', 'uploadToken', 'resendOtp', 'notificationList']);

        $this->loadModel('Users');
        $this->loadModel('Carts');
        $this->loadModel('Vendors');
        $this->loadModel('UserAddresses');
        $this->loadModel('Notifications');
    }
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->getEventManager()->off($this->Csrf);

    }
    public function file_get_contents_curl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    public function userLogin()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['mobile'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $userExist = $this->Users->find()->where(['mobile' => $this->request->data['mobile']])->first();
            // $otp = rand(1001, 9999);
            $otp = 1234;
            if ($userExist) {
                if ($userExist->status == 'N') {
                    $response['success'] = false;
                    $response['message'] = "Account Suspended, please contact Support team";
                    $this->response->type('application/json');
                    $this->response->body(json_encode($response));
                    return $this->response;
                }
                $userExist->otp = $otp;
                if ($this->Users->save($userExist)) {
                    // $mesg = "Your One Time Password for Ebatua is " . $otp;
                    // $new = $this->file_get_contents_curl('http://alerts.prioritysms.com/api/web2sms.php?workingkey=A2960bddf6f159a76d113973b6831bf79&to=' . trim($this->request->data['mobile']) . '&sender=DAACIN&message=' . urlencode($mesg));
                    $response['success'] = true;
                    $response['message'] = "Please verify OTP sent to your registered number";
                    $this->response->type('application/json');
                    $this->response->body(json_encode($response));
                    return $this->response;
                }
            } else {
                $newUser = $this->Users->newEntity();
                $newUser->referral_code = $this->random_strings(10);
                $newUser->mobile = $this->request->data['mobile'];
                $newUser->role_id = 4;
                $newUser->otp = $otp;
                if ($user = $this->Users->save($newUser)) {
                    // $mesg = "Your One Time Password for Ebatua is " . $otp;
                    // $new = $this->file_get_contents_curl('http://alerts.prioritysms.com/api/web2sms.php?workingkey=A2960bddf6f159a76d113973b6831bf79&to=' . trim($this->request->data['mobile']) . '&sender=DAACIN&message=' . urlencode($mesg));
                    $response['success'] = true;
                    $response['message'] = "Please verify OTP sent to your registered number";
                    $this->response->type('application/json');
                    $this->response->body(json_encode($response));
                    return $this->response;
                }

            }
            $response['success'] = false;
            $response['message'] = "Please Try after sometime";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = 0;
            $response['message'] = "Invalid Data Type";
            echo json_encode($response);
            return;
        }
    }
    public function userOtpVerify()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            if (empty($this->request->data['otp']) || empty($this->request->data['mobile']) || empty($this->request->data['deviceId'])) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $verifiedUser = $this->Users->find('all')->where(['mobile' => $this->request->data['mobile'], 'otp' => $this->request->data['otp']])->first();
            if (empty($verifiedUser)) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid OTP";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->loadModel('Tokens');
            $deviceExist = $this->Tokens->find('all')->where(['id' => $this->request->data['deviceId']])->first();
            if (empty($deviceExist)) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid Device Id";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response = array();
            $response['success'] = true;
            if ($verifiedUser['role_id'] == 2) {
                // $vendorId = $this->Users->find()->contain(['vendor'])->where(['Users.id' => $verifiedUser->id])->first()->vendor->id;
                $response['userInfo']['userId'] = $verifiedUser->id;
                $response['userInfo']['roleType'] = 'Vendor';
                $response['userInfo']['cartItemCount'] = $this->Carts->find()->select(['count' => 'sum(quantity)'])->where(['user_id' => $verifiedUser->id])->first()->count;
                $response['userInfo']['cartItemCount'] = $response['userInfo']['cartItemCount'] == null ? 0 : $response['userInfo']['cartItemCount'];
            }
            if ($verifiedUser['role_id'] == 4) {
                $response['userInfo']['userId'] = $verifiedUser->id;
                $response['userInfo']['roleType'] = 'Customer';
                $response['userInfo']['cartItemCount'] = $this->Carts->find()->select(['count' => 'sum(quantity)'])->where(['user_id' => $verifiedUser->id])->first()->count;
                $response['userInfo']['cartItemCount'] = $response['userInfo']['cartItemCount'] == null ? 0 : $response['userInfo']['cartItemCount'];
            }
            if ($verifiedUser['role_id'] == 3) {
                $response['userInfo']['userId'] = $verifiedUser->id;
                $response['userInfo']['roleType'] = 'DeliveryBoy';
                $response['userInfo']['cartItemCount'] = $this->Carts->find()->select(['count' => 'sum(quantity)'])->where(['user_id' => $verifiedUser->id])->first()->count;
                $response['userInfo']['cartItemCount'] = $response['userInfo']['cartItemCount'] == null ? 0 : $response['userInfo']['cartItemCount'];
            }
            $response['userInfo']['deviceId'] = $this->request->data['deviceId'];
            $response['userInfo']['authToken'] = JWT::encode([
                'sub' => $verifiedUser['id'],
                'exp' => time() + 2592000,
            ],
                Security::salt());
            $verifiedUser['token_id'] = $this->request->data['deviceId'];
            $this->Users->save($verifiedUser);
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        } else {
            $response = array();
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function resendOtp()
    {
        $this->autoRender = false;
        $this->loadModel('Users');
        if ($this->request->is('post')) {
            if (empty($this->request->data['mobile'])) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                echo json_encode($response);
                return;
            }
            $user = $this->Users->find()->where(['mobile' => $this->request->data['mobile']])->first();
            if (empty($user)) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid Number";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            // $mesg = "Your One Time Password for Ezypayroll is " . $user->otp;
            // $new = $this->file_get_contents_curl('http://alerts.prioritysms.com/api/web2sms.php?workingkey=A2960bddf6f159a76d113973b6831bf79&to=' . trim($user['mobile']) . '&sender=DAACIN&message=' . urlencode($mesg));
            $response = array();
            $response['success'] = true;
            $response['message'] = "OTP resent successfully";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        } else {
            $response = array();
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function uploadToken()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            if (empty($this->request->data['deviceId']) || !isset($this->request->data['token']) || empty($this->request->data['vendorId'])) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                echo json_encode($response);
                return;
            }
            extract($this->request->data);
            $this->loadmodel('Tokens');
            $deviceExist = $this->Tokens->find()->where(['device_id' => $deviceId])->first();
            if ($deviceExist) {
                $deviceExist['token'] = $token;
                $this->Tokens->save($deviceExist);
                $body['success'] = true;
                $body['userInfo']['deviceId'] = $deviceExist['id'];
                $cartCount = $this->Carts->find()->select(['count' => 'sum(quantity)'])->where(['token_id' => $deviceExist['id'], 'vendor_id' => $vendorId])->first()->count;
                $body['userInfo']['cartItemCount'] = $cartCount == null ? 0 : (INT) $cartCount;
            } else {
                $newDevice = $this->Tokens->newEntity();
                $newDevice['device_id'] = $deviceId;
                $newDevice['token'] = $token;
                $device = $this->Tokens->save($newDevice);
                $body['success'] = true;
                $body['userInfo']['deviceId'] = $device['id'];
                $body['userInfo']['cartItemCount'] = 0;
            }
        } else {
            $body['success'] = false;
            $body['message'] = "Invalid method";
        }

        $this->response->type('application/json');
        $this->response->body(json_encode($body));
        return $this->response;
    }
    public function random_strings($length_of_string)
    {

        // String of all alphanumeric character
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

        // Shufle the $str_result and returns substring
        // of specified length
        return substr(str_shuffle($str_result),
            0, $length_of_string);

    }
    public function resetNotificationCount()
    {
        $this->autoRender = false;
        $user = $this->Auth->user();
        $this->loadmodel('Users');
        $user_id = $user['id'];
        $checkusername = $this->Users->find('all')->where(['Users.id' => $user_id])->count();

        if ($checkusername == 1) {
            $user_reg = $this->Users->get($user_id);
            $this->request->data['notification_counter'] = '0';
            $user_reg_add = $this->Users->patchEntity($user_reg, $this->request->data());
            $results = $this->Users->save($user_reg_add);
            $body['success'] = true;
            $body['message'] = "Notification count reseted successfully";
        } else {
            $body['success'] = false;
            $body['message'] = "This user doesn't exists";
        }

        $this->response->type('application/json');
        $this->response->body(json_encode($body));
        return $this->response;
    }
    public function getNotificationCount()
    {
        $this->autoRender = false;
        $response["success"] = true;
        $response["notificationCount"] = 0;
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
        $user = $this->Auth->user();
        $user_id = $user['id'];
        $this->loadmodel('Users');
        $result = $this->Users->find('all')->where(['Users.id' => $userid])->first();
        //pr($result); die;
        if (!empty($result)) {
            $response["success"] = true;
            $response["notificationCount"] = (int) $result['notification_counter'];
        } else {
            $response["success"] = false;
            $response["message"] = "This user doesn't exists";
        }

        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
    public function notificationList()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            if (empty($this->request->data['deviceId'])) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                echo json_encode($response);
                return;
            }
            $this->loadModel('Notifications');
            $order = $this->Notifications->find('all')->where(['user_id' => $this->request->data['userId'], 'status' => 'Y'])->order(['created' => 'DESC'])->toarray();
            if ($order) {
                foreach ($order as $view) {
                    $carddata['title'] = $view['title'];
                    $carddata['message'] = $view['message'];
                    $carddata['date'] = date('d-M-Y', strtotime($view['created']));

                    $cardlist[] = $carddata;
                }
                $body['success'] = true;
                $body['output'] = $cardlist;
            } else {
                $body['success'] = false;
                $body['message'] = "No Notification";
            }
        } else {
            $body['success'] = false;
            $body['message'] = "Invalid method";
        }
        echo json_encode($body);
        return;
    }

    public function offers()
    {
        $response = array();
        $this->autoRender = false;
        $user = $this->Auth->user();
        $userId = $user['id'];

        $userAddresses = $this->UserAddresses->find()->where(['user_id' => $userId])->toarray();
        if (empty($userAddresses)) {
            $response['success'] = false;
            $response['message'] = "No Offers avaialable";
            echo json_encode($response);
            return;
        }
        $vendorId = [];
        $vendors = $this->Vendors->find()->contain(['VendorAreas'])->where(['Vendors.status' => 1])->toarray();
        foreach ($userAddresses as $userAddress) {
            foreach ($vendors as $vendor) {
                if (empty($vendor['vendor_areas'])) {
                    continue;
                }
                $serviceArea = [];
                foreach ($vendor['vendor_areas'] as $vendorArea) {
                    $data = [];
                    $data['lat'] = $vendorArea['latitude'];
                    $data['lng'] = $vendorArea['longitude'];
                    $serviceArea[] = $data;
                }
                $exist = \GeometryLibrary\PolyUtil::containsLocation(
                    ['lat' => $userAddress['latitude'], 'lng' => $userAddress['longitude']], $serviceArea);
                if ($exist == true) {
                    if (!in_array($vendor['id'], $vendorId)) {
                        $vendorId[] = $vendor['id'];
                    }
                    break;
                }
            }
        }
        if (empty($vendorId)) {
            $response['success'] = false;
            $response['message'] = "No Offers avaialable";
            echo json_encode($response);
            return;
        }
        $offers = $this->Notifications->find()->where(['type' => 'offers', 'vendor_id IN' => $vendorId])->toarray();
        if (empty($offers)) {
            $response['success'] = false;
            $response['message'] = "No Offers avaialable";
            echo json_encode($response);
            return;
        }
        $response['success'] = true;
        foreach ($offers as $offer) {
            $data = [];
            $data['image'] = null;
            $data['title'] = $offer['title'];
            $data['message'] = $offer['message'];
            $data['date'] = date('d M Y', strtotime($offer['date']));
            $data['location'] = 'jaipur';
            $response['offers'][] = $data;
        }
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;

    }
}