<?php

namespace App\Controller\V2;

use App\Controller\AppController;
use Cake\Event\Event;

class CategoriesController extends AppController
{
    public function beforeFilter(Event $event)
    {
        $this->loadModel('Users');
        parent::beforeFilter($event);
        $this->Auth->allow(['categories']);
    }
    

}