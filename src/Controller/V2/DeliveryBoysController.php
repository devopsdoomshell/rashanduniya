<?php
namespace App\Controller\V2;

use App\Controller\V2\AppController;
use Cake\Event\Event;

class DeliveryBoysController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->getEventManager()->off($this->Csrf);
        $this->loadModel('Users');
        $this->loadModel('Categories');
        $this->loadModel('Vendors');
        $this->loadModel('VendorAreas');
        $this->loadModel('Products');
        $this->loadModel('VendorCategories');
        $this->loadModel('Attributes');
        $this->loadModel('Carts');
        $this->loadModel('UserAddresses');
        $this->loadModel('ProductsAttributes');
        $this->loadModel('Payments');
        $this->loadModel('Wallets');
        $this->loadModel('Slots');
        $this->loadModel('Orders');
        $this->loadModel('OrderDetails');
        $this->loadModel('DeliveryBoys');
        $this->loadModel('Users');
        $this->loadModel('Vendorpayments');
        $this->loadModel('Coupons');
    }
    public function pendingOrders()
    {
        $this->autoRender = false;
        $response = array();
        $user = $this->Auth->user();
        $this->request->data['deliveryBoyId'] = $user['id'];

        $deliveryBoyId = $this->request->data['deliveryBoyId'];
        $deliveryBoy = $this->DeliveryBoys->find()->where(['user_id' => $deliveryBoyId, 'status' => 1])->order(['id' => 'DESC'])->first();
        if (empty($deliveryBoy)) {
            $response['success'] = false;
            $response['message'] = "Invalid Delivery Boy";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
        $vendorId = $deliveryBoy['vendor_id'];
        $pendingOrders = $this->Orders->find()->where(['Orders.delivery_boy_id' => $deliveryBoy['id'], 'AND' => [['Orders.order_status <>' => 'delivered'], ['Orders.order_status <>' => 'cancelled']]])->order(['to_deliver_date' => 'ASC', 'slot_id' => 'ASC'])->toarray();
        if (empty($pendingOrders)) {
            $response['success'] = false;
            $response['message'] = "No Pending Orders";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
        $slots = $this->Slots->find('list', ['keyField' => 'id', 'valueField' => 'slot_name'])->toarray();
        $vendor = $this->Vendors->find()->where(['id' => $vendorId])->first();
        $response['success'] = true;
        $response['orderDetails'] = [];
        foreach ($pendingOrders as $pendingOrder) {
            $data = [];
            $slotData = [];
            $userAddress = $this->UserAddresses->find()->where(['id' => $pendingOrder['user_address_id']])->first();
            $data['orderId'] = $pendingOrder['id'];
            $data['name'] = $userAddress['name'];
            $data['deliveryAddress'] = $userAddress['flat_no'] . ',' . $userAddress['address_1'];
            $data['latitude'] = $userAddress['latitude'];
            $data['longitude'] = $userAddress['longitude'];
            $slotData['title'] = $slots[$pendingOrder['slot_id']];
            $data['placedDate'] = date('D d M Y,H.i A', strtotime($pendingOrder['created']));
            $data['deliveryDate'] = date('D, d M Y', strtotime($pendingOrder['to_deliver_date'])) . ' ' . $slotData['title'];
            $data['amount'] = $pendingOrder['total_amount'] - $pendingOrder['delivery_charge'];
            $data['orderStatus'] = $pendingOrder['order_status'];
            $data['deliveryCharge'] = $pendingOrder['delivery_charge'] == 0 ? 'Free' : $pendingOrder['delivery_charge'];
            $data['finalAmount'] = $pendingOrder['total_amount'];
            $data['paymentType'] = $pendingOrder['transaction_mode'];
            $data['image'] = "http://dc84viddw6ggp.cloudfront.net/products/" . $this->OrderDetails->find()->contain(['Orders', 'Products'])->where(['Orders.id' => $pendingOrder['id']])->first()->product->image;
            $data['assignedDetails'] = null;
            if (!empty($pendingOrder['delivery_boy_id'])) {
                $deliveryBoys = $this->DeliveryBoys->find('list', ['keyField' => 'id', 'valueField' => 'name_mobile'])->where(['vendor_id' => $vendorId])->toarray();
                $data['assignedDetails']['deliveryBoy'] = $deliveryBoys[$pendingOrder['delivery_boy_id']];
                $data['assignedDetails']['assignedDate'] = date('d M Y', strtotime($pendingOrder['assign_date']));
                $data['assignedDetails']['assignedSlot'] = $slots[$pendingOrder['assign_slot_id']];
            }
            $response['orderDetails'][] = $data;
        }
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
    public function completedOrders()
    {
        $this->autoRender = false;
        $response = array();

        $user = $this->Auth->user();
        $this->request->data['deliveryBoyId'] = $user['id'];

        $deliveryBoyId = $this->request->data['deliveryBoyId'];
        $deliveryBoy = $this->DeliveryBoys->find()->where(['user_id' => $deliveryBoyId, 'status' => 1])->first();
        if (empty($deliveryBoy)) {
            $response['success'] = false;
            $response['message'] = "Invalid Delivery Boy";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
        $vendorId = $this->request->data['vendorId'];
        $pendingOrders = $this->Orders->find()->where(['Orders.delivery_boy_id' => $deliveryBoy['id'], 'Orders.order_status' => 'delivered'])->order(['to_deliver_date' => 'ASC', 'slot_id' => 'ASC'])->toarray();
        if (empty($pendingOrders)) {
            $response['success'] = false;
            $response['message'] = "No Pending Orders";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
        $slots = $this->Slots->find('list', ['keyField' => 'id', 'valueField' => 'slot_name'])->toarray();
        $vendor = $this->Vendors->find()->where(['id' => $vendorId])->first();
        $response['success'] = true;
        $response['pendingOrders'] = [];
        foreach ($pendingOrders as $pendingOrder) {
            $data = [];
            $slotData = [];
            $data['orderId'] = $pendingOrder['id'];
            $data['vendorName'] = $vendor->name;
            $slotData['title'] = $slots[$pendingOrder['slot_id']];
            $data['placedDate'] = date('D d M Y,H.i A', strtotime($pendingOrder['created']));
            $data['deliveryDate'] = date('D, d M Y', strtotime($pendingOrder['to_deliver_date'])) . ' ' . $slotData['title'];
            $data['amount'] = $pendingOrder['total_amount'] - $pendingOrder['delivery_charge'];
            $data['orderStatus'] = $pendingOrder['order_status'];
            $data['deliveryCharge'] = $pendingOrder['delivery_charge'] == 0 ? 'Free' : $pendingOrder['delivery_charge'];
            $data['finalAmount'] = $pendingOrder['total_amount'];
            $data['paymentType'] = $pendingOrder['transaction_mode'];
            $data['image'] = "http://dc84viddw6ggp.cloudfront.net/products/" . $this->OrderDetails->find()->contain(['Orders', 'Products'])->where(['Orders.id' => $pendingOrder['id']])->first()->product->image;
            $data['assignedDetails'] = null;
            if (!empty($pendingOrder['delivery_boy_id'])) {
                $deliveryBoys = $this->DeliveryBoys->find('list', ['keyField' => 'id', 'valueField' => 'name_mobile'])->where(['vendor_id' => $vendorId])->toarray();
                $data['assignedDetails']['deliveryBoy'] = $deliveryBoys[$pendingOrder['delivery_boy_id']];
                $data['assignedDetails']['assignedDate'] = date('d M Y', strtotime($pendingOrder['assign_date']));
                $data['assignedDetails']['assignedSlot'] = $slots[$pendingOrder['assign_slot_id']];
            }
            $response['completedOrders'][] = $data;
        }
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }

    public function deliveryConfirmation()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['mobile']) || empty($this->request->data['deliveredToName']) || empty($this->request->data['orderId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }

            $user = $this->Auth->user();
            $this->request->data['deliveryBoyId'] = $user['id'];

            extract($this->request->data);
            $deliveryBoy = $this->DeliveryBoys->find()->where(['user_id' => $deliveryBoyId, 'status' => 1])->order(['id' => 'DESC'])->first();
            if (empty($deliveryBoy)) {
                $response['success'] = false;
                $response['message'] = "Invalid Delivery Boy";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $order = $this->Orders->find()->where(['id' => $orderId, 'delivery_boy_id' => $deliveryBoy['id']])->first();
            if (empty($order)) {
                $response['success'] = false;
                $response['message'] = "Invalid delivery Boy or Order Id";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $order['delivered_to_name'] = $deliveredToName;
            $order['delivered_to_mobile'] = $mobile;
            if (!empty($comment)) {
                $order['delivery_comment'] = $comment;
            }
            $order['delivery_date'] = date('Y-m-d H:i:s');
            $order['order_status'] = 'delivered';
            if ($this->Orders->save($order)) {
                if ($order->cashback != 0 && $order->coupon_id != "") {
                    $coupon = $this->Coupons->find()->where(['id' => $order->coupon_id])->first();
                    $wallet = $this->Wallets->newEntity();
                    $wallet['order_id'] = $order->id;
                    $wallet['user_id'] = $order['userId'];
                    $wallet['amount'] = $order->cashback;
                    $wallet['is_expiry'] = 1;
                    $wallet['coupon_id'] = $order->coupon_id;
                    $wallet['amount_type'] = 'promotional';
                    $wallet['expiry_date'] = $coupon->cashback_expiry_date;
                    $wallet['max_redeem_type'] = $coupon->max_redeem_type;
                    $wallet['max_redeem_rate'] = $coupon->max_redeem_rate;
                    $this->Wallets->save($wallet);
                }
                $response['success'] = true;
                $response['message'] = "Order Delivered Successfully";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            } else {
                $response['success'] = false;
                $response['message'] = "Error While delivering Order";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function callToCustomer()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['orderId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $user = $this->Auth->user();
            $this->request->data['deliveryBoyId'] = $user['id'];

            extract($this->request->data);
            $deliveryBoy = $this->DeliveryBoys->find()->where(['user_id' => $deliveryBoyId])->first();
            if (empty($deliveryBoy)) {
                $response['success'] = false;
                $response['message'] = "Invalid Delivery Boy";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $order = $this->Orders->find()->where(['id' => $orderId, 'delivery_boy_id' => $deliveryBoy['id']])->first();
            if (empty($order)) {
                $response['success'] = false;
                $response['message'] = "Invalid Order";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $user = $this->Users->find()->where(['id' => $order['user_id']])->first();
            $url = 'http://ivr.virtuo.in/c2ctrishlastro.php?user=c2cTrishlastro&pwd=c2cTrishlastro2020&agent=' . $deliveryBoy['mobile'] . '&customer=' . $user['mobile'] . '&uid=' . $order['id'];
            $result = file_get_contents($url);
            echo json_encode($result);die;
            $response['success'] = true;
            $response['message'] = "Request for call successfull";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
}