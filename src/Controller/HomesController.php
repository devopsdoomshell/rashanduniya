<?php

namespace App\Controller;

use App\Controller\AppController;
use PHPMailer\PHPMailer\PHPMailer;

include ROOT . DS . "vendor" . DS . "PHPMailer/" . DS . "PHPMailerAutoload.php";

class HomesController extends AppController
{

    public function beforeFilter()
    {
        $this->Auth->allow(['refundpolicy', 'returnpolicy', 'index', 'faq', 'privacy', 'terms', 'contactus', 'aboutus', 'subscribtion', 'consume', 'distribute', 'produce', 'cart', 'checkout', 'myOrder', 'offerZone', 'orderDetail', 'productDetail', 'profile', 'referFriend', 'wallet']);
    }

    public function index()
    {
        $this->viewBuilder()->layout('home');

    }

    public function cart()
    {
        $this->viewBuilder()->layout('home');

    }

    public function checkout()
    {
        $this->viewBuilder()->layout('home');

    }
    public function orderDetail()
    {
        $this->viewBuilder()->layout('home');

    }
    public function productDetail()
    {
        $this->viewBuilder()->layout('home');

    }
    public function profile()
    {
        $this->viewBuilder()->layout('home');

    }
    public function referFriend()
    {
        $this->viewBuilder()->layout('home');

    }
    public function wallet()
    {
        $this->viewBuilder()->layout('home');

    }
    public function faq()
    {

    }

    public function privacy()
    {

    }

    public function terms()
    {

    }

    public function returnpolicy()
    {

    }
    public function refundpolicy()
    {

    }

    public function contactus()
    {

        if ($this->request->data) {

            $imagefilename = $this->request->data['file']['name'];
            $file_type = $this->request->data['file']['type'];
            $tmp_name = $this->request->data['file']['tmp_name'];
            $size = $this->request->data['file']['size'];
            $ext = end(explode('.', $imagefilename));
            $name = md5(time($imagefilename));
            $imagename = $name . '.' . $ext;

            if (move_uploaded_file($tmp_name, "documents/" . $imagename)) {
                //    pr($imagename);
            }

            $names = $this->request->data['name'];
            $email = $this->request->data['email'];
            $phn = $this->request->data['phn'];
            $openings = $this->request->data['openings'];
            $Description = $this->request->data['description'];
            $siteurl = SITE_URL;
            $mail = new \PHPMailer;
            //From email address and name
            $mail->From = "info@veggiegreenhouse.com";
            $mail->FromName = $names;
            //$mail->addCC("lokesh@doomshell.com");
            $mail->addAddress("info@veggiegreenhouse.com", "Me");
            $mail->addAttachment("documents/" . $imagename); //Filename is optional
            $mail->isHTML(true);
            $mail->Subject = "Contact us";
            $mail->Body = '<!DOCTYPE HTML>
					<html>
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<title>Mail</title>
					</head>
					<div style="width:600px; margin: auto; font-family:Arial, Helvetica, sans-serif; font-size:13px;  border:4px solid #8BC34A;">
						<div style="background-color:#ebf7de;     margin-top: -30px;">
						<a href="' . $siteurl . '" target="_blank"><img src="' . $siteurl . 'frontfile/images/logo (1).png" alt="logo"style="width:30%;"></a>

						</div>

					<div style="padding:10px; text-align:left; margin-top: -100px;">

					<h3><span style="color:#1c2630; font-size:16px; ">Dear</span>&nbsp;' . ucfirst($names) . ',</h3>

					<body style="padding:0px; margin:0px;font-family:Arial,Helvetica,sans-serif; font-size:13px;">
					<div style=" text-align:left; font-size:15px;">You have successfully Signed Up ,Please login with below credential details

					<br><br><b>user email address:&nbsp;</b>' . $email . '
					<br><b>Phone number:&nbsp;</b>' . $phn . '
					<br><b>Current Openings:&nbsp;</b>' . $openings . '
					<br><b>Descripton:&nbsp;</b>' . $Description . '


					<br>
					<br>
					Thanks & Regards, <br> <br>
					<span style="color:#1c2630; font-size:16px; ">Veggiegreen house</span>
					</div>
						</div>
							</div>
								</body>
									</html>';
            // pr($mail); die;
            //$mail->AltBody = "This is the plain text version of the email content";
            if (!$mail->send()) {
                //echo "Mailer Error: " . $mail->ErrorInfo;
                $this->Flash->error(__('Send mail failed'));
                return $this->redirect(['action' => 'contactus']);
            } else {
                $this->Flash->success(__('Send mail successfully'));
            }

            return $this->redirect(['action' => 'contactus']);
        }

    }

    public function myOrder()
    {
        $this->viewBuilder()->layout('home');

    }
    public function offerZone()
    {
        $this->viewBuilder()->layout('home');

    }
    public function aboutus()
    {

    }

    public function subscribtion()
    {
        $number = $this->request->data['number'];

        if ($number) {

            $msg = "Veggiegreenhouse App URL is http://bit.ly/2S3EMnX";
            //$result=$this->file_get_contents_curl('http://alerts.prioritysms.com/api/web2sms.php?workingkey=A2ee06f402bf4946d1c0960a74795d3ba&sender=VEGGIE&to='.$number.'&sender=VEGGIE&message='.urlencode($mesg));

            $this->sendmsg($number, $msg);

            return $this->redirect(['action' => 'index']);
        } else {
            return $this->redirect(['action' => 'index']);
        }

    }

// Message Sent Function
    public function file_get_contents_curl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function consume()
    {

    }
    public function distribute()
    {

    }
    public function produce()
    {

    }

}