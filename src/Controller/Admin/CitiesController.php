<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;

class CitiesController extends AppController
{

    public function beforeFilter(Event $event)
    {

    }

    public function index($id = null)
    {
        $this->viewBuilder()->layout('admin');
        $cities = $this->Cities->find('all')->toarray();
        $this->set('cities', $cities);
        if ($id) {
            $city = $this->Cities->find()->where(['id' => $id])->first();
            $this->set(compact('city'));
        }
    }

    public function add()
    {
        $this->viewBuilder()->layout('admin');
        $cities = $this->Cities->newEntity();
        if ($this->request->is(['post', 'put'])) {
            $cities['name'] = $this->request->data['name'];
            if ($this->Cities->save($cities)) {
                $this->Flash->success(__('City has been saved successfully'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Please try after some time'));
                return $this->redirect(['action' => 'index']);
            }
        }

    }

    public function edit($id)
    {
        if ($this->request->is(['post', 'put'])) {
            $city = $this->Cities->find()->where(['id' => $id])->first();
            $city['name'] = $this->request->data['name'];
            if ($this->Cities->save($city)) {
                $this->Flash->success(__('City has been updated Successfully.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Please try after some time'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function delete($id = null)
    {
        $city = $this->Cities->find()->where(['id' => $id])->first();
        if ($this->Cities->delete($city)) {
            $this->Flash->success(__('City has been updated Successfully.'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('Please try after some time'));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function status($id, $status)
    {
        if (isset($id) && !empty($id)) {
            $city = $this->Cities->find()->where(['id' => $id])->first();
            $city->status = $status;
            if ($this->Cities->save($city)) {
                $this->Flash->success(__('City status has been updated.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Please try after some time'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }
    public function isAuthorized($user)
    {
        if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 101)) {
            return true;
        }

        return false;
    }

}
