<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

include '../vendor/PHPExcel/Classes/PHPExcel.php';
include '../vendor/PHPExcel/Classes/PHPExcel/IOFactory.php';

class ProductController extends AppController
{

    public function beforeFilter(Event $event)
    {
        $this->loadModel('Products');
    }

    public function index()
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Products');
        $this->loadModel('Productprice');
        $this->loadModel('VendorCategories');
        $this->loadModel('Categories');
        $this->loadModel('Brands');
        $this->loadModel('Attributes');
        $this->loadModel('Vendors');
        $user = $this->Auth->user();
        $brands = $this->Brands->find('list')->toarray();
        $categories = $this->Categories->find('list')->toarray();
        $data = $this->request->query;
        if (!empty($data['product_name'])) {
            $cond['Products.productnameen LIKE'] = '%' . $data['product_name'] . '%';
        }
        if (!empty($data['brand_id'])) {
            $cond['Products.brand_id'] = $data['brand_id'];
        }
        if ($user['role_id'] == '1' && !empty($data['product_type'])) {
            if ($data['product_type'] == 'vendors_product') {
                $cond['Products.vendor_id !='] = 0;
            } else if ($data['product_type'] == 'admin_product') {
                $cond['Products.vendor_id'] = 0;
            } else {
                $cond['Products.vendor_id'] = $data['product_type'];
            }
        }
        if ($user['role_id'] == '1') {
            $categoriesSearch = $this->Categories->parentCategories->find('list')->toarray();
        } else {
            $vendorId = $this->vendorId();
            $vendorCategories = $this->VendorCategories->find('list', ['keyField' => 'id', 'valueField' => 'category_id'])->where(['vendor_id' => $vendorId])->toarray();
            $categoriesSearch = $this->Categories->parentCategories->find('list')->where(['id IN' => $vendorCategories])->toarray();
        }
        $attributes = $this->Attributes->find('list')->toarray();
        $vendors = $this->Vendors->find('list')->toarray();
        $tmppricecount = count($producttmpprice);
        $this->set('tmpprice', $tmppricecount);

        if ($user['role_id'] == '1') {
            if ($data['category_id']) {
                $cond['Products.category_id'] = $data['category_id'];
            }
            $product = $this->Products->find('all')->contain(['Attributes'])->order(['Products.productnameen' => 'ASC'])->where($cond);
            // $product = $this->Products->find('all')->contain(['Attributes' => ['ParentAttributes'], 'Categories' => ['ParentCategories']])->order(['Products.id' => 'DESC']);
        } else {
            $vendorId = $this->vendorId();
            $product = $this->Products->find('all')->contain(['Attributes' => function ($q) use ($vendorId) {
                return $q->where(['OR' => [['ProductsAttributes.vendor_id' => '0'], ['ProductsAttributes.vendor_id' => $vendorId]]]);
            }, 'ParentCategory' => function ($q) use ($vendorCategories, $data) {
                if (!empty($data['category_id'])) {
                    return $q->where(['ParentCategory.id IN' => $data['category_id'], 'ParentCategory.status' => 'Y']);
                } else {
                    return $q->where(['ParentCategory.id IN' => $vendorCategories, 'ParentCategory.status' => 'Y']);
                }
            }])->where($cond)->order(['Products.productnameen' => 'ASC']);
        }

        $this->set('product', $this->paginate($product)->toarray());
        $this->set(compact('user', 'vendorId', 'brands', 'categories', 'attributes', 'vendors', 'categoriesSearch'));
    }

    public function add()
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Products');
        $this->loadModel('Attributes');
        $this->loadModel('Categories');
        $this->loadModel('Brands');
        $attributes = $this->Attributes->find('list', ['keyField' => 'id', 'valueField' => 'name'])->where(['parent_id IS' => null])->toarray();
        $categories = $this->Categories->find('list', ['keyField' => 'id', 'valueField' => 'name'])->where(['parent_id IS' => null])->toarray();
        $brands = $this->Brands->find('list', ['keyField' => 'id', 'valueField' => 'name'])->where(['parent_id IS' => null])->toarray();
        $this->set(compact('attributes', 'categories', 'brands'));
        if ($this->request->is(['post'])) {
            $this->request->data['productnameen'] = ucfirst($this->request->data['productnameen']);
            $user = $this->Auth->user();
            if ($user['role_id'] == 2) {
                $vendorId = $this->vendorId();
            }
            $this->request->data['vendor_id'] = $user['role_id'] == 1 ? 0 : $vendorId;
            $prod = $this->Products->newEntity($this->request->data);
            if ($productsave = $this->Products->save($prod)) {
                $this->loadModel('ProductsAttributes');
                foreach ($this->request->data['attribute'] as $attribute) {
                    $attr = "";
                    $attr = $this->ProductsAttributes->newEntity();
                    $attr['product_id'] = $productsave['id'];
                    $attr['attribute_id'] = $attribute['attribute_id'];
                    $attr['attribute_type'] = $attribute['attribute_type'];
                    $attr['is_stock'] = $attribute['is_stock'];
                    $attr['price'] = $attribute['price'];
                    $attr['discount_price'] = $attribute['discount_price'];
                    $attr['sort'] = $attribute['sort'];
                    $attr['vendor_id'] = $user['role_id'] == 1 ? 0 : $vendorId;
                    $this->ProductsAttributes->save($attr);
                }

                $this->Flash->success(__('Product  has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Product not has been saved'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function edit($id)
    {
        $this->loadModel('Products');
        $product = $this->Products->get($id);
        $this->set('product', $product);
        $this->viewBuilder()->layout('admin');
        if ($this->request->is(['post', 'put'])) {
            // pr($this->request->data); die;
            $imagefilename = $this->request->data['image']['name'];
            $item = $this->request->data['image']['tmp_name'];
            $ext = end(explode('.', $imagefilename));
            $name = md5(time($filename));
            $imagename = $name . '.' . $ext;
            if (!empty($imagefilename)) {

                $this->request->data['image'] = $imagename;
                if (move_uploaded_file($item, "images/" . $imagename)) {
                    $this->request->data['image'] = $imagename;
                }
            } else {
                $this->request->data['image'] = $product->image;
            }
            $productsave = $this->Products->patchEntity($product, $this->request->data);
            if ($productedit = $this->Products->save($productsave)) {
                $this->Flash->success(__('' . ucwords($productedit['productnameen']) . ' has been updated Successfully.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function delete($id = null)
    {
        $this->loadModel('Products');
        $this->loadModel('ProductsAttributes');
        $prod_data = $this->Products->get($id);
        if ($prod_data) {
            $this->Products->delete($prod_data);
            $this->ProductsAttributes->deleteAll(['product_id' => $id]);
            $this->Flash->success(__('' . ucwords($prod_data['productnameen']) . ' has been deleted successfully.'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('' . ucwords($prod_data['productnameen']) . ' has not been deleted because this event have entry in some manager'));
            return $this->redirect(['action' => 'index']);

        }
    }


    public function status()
    {

        $this->loadModel('Products');
        $id = $this->request->data['check_id'];
        $status = $this->request->data['check_status'];
        if ($status == 'Y' && isset($id) && !empty($id)) {

            $product = $this->Products->get($id);
            $product->status = $status;
            if ($updatest = $this->Products->save($product)) {
                $updatestatus = 1;
                echo json_encode($updatestatus);
                die;
            }
        } else {
            $product = $this->Products->get($id);
            $product->status = $status;
            if ($updatest = $this->Products->save($product)) {
                $updatestatus = 2;
                echo json_encode($updatestatus);
                die;
            }
        }
    }

    public function search()
    {

        $this->loadModel('Products');
        $this->loadModel('Productprice');
        $this->loadModel('VendorCategories');
        $this->loadModel('Categories');
        $this->loadModel('Brands');
        $this->loadModel('Attributes');
        $this->loadModel('Vendors');
        $brands = $this->Brands->find('list')->toarray();
        $categories = $this->Categories->find('list')->toarray();
        $attributes = $this->Attributes->find('list')->toarray();
        $vendors = $this->Vendors->find('list')->toarray();
        $user = $this->Auth->user();
        $data = $this->request->query;
        $cond = [];
        if (!empty($data['product_name'])) {
            $cond['Products.productnameen LIKE'] = '%' . $data['product_name'] . '%';
        }
        if (!empty($data['brand_id'])) {
            $cond['Products.brand_id'] = $data['brand_id'];
        }
        if ($user['role_id'] == '1' && !empty($data['product_type'])) {
            if ($data['product_type'] == 'vendors_product') {
                $cond['Products.vendor_id !='] = 0;
            } else if ($data['product_type'] == 'admin_product') {
                $cond['Products.vendor_id'] = 0;
            } else {
                $cond['Products.vendor_id'] = $data['product_type'];
            }

        }
        if ($user['role_id'] == '1') {
            if ($data['category_id']) {
                $cond['Products.category_id'] = $data['category_id'];
            }
            $product = $this->Products->find('all')->contain(['Attributes'])->order(['Products.productnameen' => 'ASC'])->where($cond);
            // $product = $this->Products->find('all')->contain(['Attributes' => ['ParentAttributes'], 'Categories' => ['ParentCategories']])->order(['Products.id' => 'DESC']);
        } else {
            $vendorId = $this->vendorId();
            $vendorCategories = $this->VendorCategories->find('list', ['keyField' => 'id', 'valueField' => 'category_id'])->where(['vendor_id' => $vendorId])->toarray();
            $product = $this->Products->find('all')->contain(['Attributes' => function ($q) use ($vendorId) {
                return $q->where(['OR' => [['ProductsAttributes.vendor_id' => '0'], ['ProductsAttributes.vendor_id' => $vendorId]]]);
            }, 'ParentCategory' => function ($q) use ($vendorCategories, $data) {
                if (!empty($data['category_id'])) {
                    return $q->where(['ParentCategory.id IN' => $data['category_id'], 'ParentCategory.status' => 'Y']);
                } else {
                    return $q->where(['ParentCategory.id IN' => $vendorCategories, 'ParentCategory.status' => 'Y']);
                }
            }])->where($cond)->order(['Products.productnameen' => 'ASC']);
        }
        // $this->set('product', $product);
        $this->set('product', $this->paginate($product)->toarray());
        $this->set(compact('user', 'vendorId', 'brands', 'categories', 'attributes', 'vendors'));

    }

    public function quickedit($ids = null)
    {
        $this->loadModel('Products');
        $this->loadModel('Attributes');
        $this->loadModel('Users');
        $this->loadModel('Pricerequest');
        $this->loadModel('Productprice');
        $this->loadModel('ProductsAttributes');
        $user = $this->Auth->user();
        $pro = explode(",", $ids);
        $attributes = $this->Attributes->find('list')->toarray();
        $this->set(compact('attributes', 'user'));

        if ($user['role_id'] == 1) {
            $products = $this->Products->find('all')->contain(['Attributes' => function ($q) {
                return $q->where(['ProductsAttributes.vendor_id' => 0]);
            }])->where(['Products.id IN' => $pro, 'Products.status' => 'Y'])->order(['Products.ptype' => 'ASC', 'Products.sortnumber' => 'ASC'])->toarray();
        } else {
            $vendorId = $this->vendorId();
            $products = $this->Products->find('all')->contain(['Attributes'])->where(['Products.id IN' => $pro, 'Products.status' => 'Y'])->order(['Products.ptype' => 'ASC', 'Products.sortnumber' => 'ASC'])->toarray();
            $this->set(compact('vendorId'));
        }

        $this->set(compact('products'));
        $adminnum = $this->Users->find('all')->select(['mobile'])->where(['Users.role_id' => 1])->first();
        $datetme = date('Y-m-d H:i');
        $prodrq = $this->Pricerequest->newEntity();

        if ($this->request->is(['post', 'put'])) {
            $datas = $this->request->data['data'];
            if ($user['role_id'] == 1) {
                foreach ($datas as $key => $value) {
                    $prodAttribute = $this->ProductsAttributes->find()->where(['id' => $key])->first();
                    $prodAttribute->price = $value['price'];
                    $prodAttribute->discount_price = $value['discount_price'];
                    $prodAttribute->is_stock = $value['is_stock'];
                    $this->ProductsAttributes->save($prodAttribute);
                }
            } else if ($user['role_id'] == 2) {
                foreach ($datas as $key => $value) {
                    if ($value['vendor_id'] == 0) {
                        $prodAttribute = $this->ProductsAttributes->find()->where(['id' => $key])->first();
                        if ($prodAttribute['price'] != $value['price'] || $prodAttribute['discount_price'] != $value['discount_price'] || $prodAttribute['is_stock'] != $value['is_stock']) {
                            $newProdAttribute = $this->ProductsAttributes->newEntity();
                            $newProdAttribute['product_id'] = $prodAttribute['product_id'];
                            $newProdAttribute['vendor_id'] = $vendorId;
                            $newProdAttribute['attribute_id'] = $prodAttribute['attribute_id'];
                            $newProdAttribute['attribute_type'] = $prodAttribute['attribute_type'];
                            $newProdAttribute['price'] = $value['price'];
                            $newProdAttribute['discount_price'] = $value['discount_price'];
                            $newProdAttribute['is_stock'] = $value['is_stock'];
                            $this->ProductsAttributes->save($newProdAttribute);
                        }
                    } else {
                        $prodAttribute = $this->ProductsAttributes->find()->where(['id' => $key])->first();
                        $prodAttribute->price = $value['price'];
                        $prodAttribute->discount_price = $value['discount_price'];
                        $prodAttribute->is_stock = $value['is_stock'];
                        $this->ProductsAttributes->save($prodAttribute);
                    }
                }
            }

            $this->Flash->success(__('Products price has been updated'));
            return $this->redirect(['action' => 'index']);

        }

    }
    public function vendor_attributes($id)
    {
        $this->loadModel('Attributes');
        $this->loadModel('Vendors');
        $this->loadModel('ProductsAttributes');
        $attributes = $this->Attributes->find('list')->toarray();
        $vendors = $this->Vendors->find('list')->toarray();
        $vendorAttributes = $this->ProductsAttributes->find()->contain(['Products'])->where(['product_id' => $id, 'ProductsAttributes.vendor_id !=' => 0])->toarray();
        $this->set(compact('attributes', 'vendorAttributes', 'vendors'));
    }

    public function downloadexcl()
    {
        $this->loadModel('Attributes');
        $this->loadModel('ProductsAttributes');
        $this->loadModel('Categories');
        $this->loadModel('Vendors');
        $this->loadModel('Brands');
        $user = $this->Auth->user();
        $parentAttributes = $this->Attributes->ParentAttributes->find('all')->toarray();
        $attributes = [];
        foreach ($parentAttributes as $parentAttribute) {
            $tmp = $this->Attributes->find()->where(['parent_id' => $parentAttribute['id']])->toarray();
            $tmp2 = array_map(function ($value) use ($parentAttribute) {
                return $value['name'] . ' ' . $parentAttribute['name'];
            }, $tmp);
            $attributes = array_merge($attributes, $tmp2);
        }
        $childCategories = $this->Categories->ChildCategories->find('list')->toarray();
        $parentCategories = $this->Categories->ParentCategories->find('list')->toarray();
        $vendors = $this->Vendors->find('list')->toarray();
        $brands = $this->Brands->find('list')->toarray();
        $categories = $this->Categories->find('list')->toarray();
        $user = $this->Auth->user();
        if ($user['role_id'] == '1') {
            $products = $this->ProductsAttributes->find('all')->contain(['Attributes' => ['ParentAttributes'], 'Products'])->order(['Products.productnameen' => 'ASC'])->toarray();
        } else if ($user['role_id'] == '2') {
            $vendorId = $this->vendorId();
            $products = $this->ProductsAttributes->find('all')->contain(['Attributes' => ['ParentAttributes'], 'Products'])->where(['OR' => [['ProductsAttributes.vendor_id' => 0], ['ProductsAttributes.vendor_id' => $vendorId]], 'OR' => [['Products.vendor_id' => 0], ['Products.vendor_id' => $vendorId]]])->order(['Products.productnameen' => 'ASC'])->toarray();
        }
        $this->set(compact('categories', 'vendors', 'attributes', 'parentCategories', 'childCategories', 'brands', 'user', 'vendorId'));
        // $product = $this->Products->find('all')->contain(['Attributes' => ['ParentAttributes'], 'Categories' => ['ParentCategories']])->order(['Products.productnameen' => 'DESC'])->toarray();
        $this->set('products', $products);
        if ($user['role_id'] == 2) {
            $this->render('vendor_downloadexcl');
        }
    }

    public function vendorDownloadexcl()
    {
        $this->loadModel('Attributes');
        $this->loadModel('ProductsAttributes');
        $this->loadModel('Categories');
        $this->loadModel('Vendors');
        $this->loadModel('Brands');
        $user = $this->Auth->user();
        $parentAttributes = $this->Attributes->ParentAttributes->find('all')->toarray();
        $attributes = [];
        foreach ($parentAttributes as $parentAttribute) {
            $tmp = $this->Attributes->find()->where(['parent_id' => $parentAttribute['id']])->toarray();
            $tmp2 = array_map(function ($value) use ($parentAttribute) {
                return $value['name'] . ' ' . $parentAttribute['name'];
            }, $tmp);
            $attributes = array_merge($attributes, $tmp2);
        }
        $childCategories = $this->Categories->ChildCategories->find('list')->toarray();
        $parentCategories = $this->Categories->ParentCategories->find('list')->toarray();
        $vendors = $this->Vendors->find('list')->toarray();
        $brands = $this->Brands->find('list')->toarray();
        $categories = $this->Categories->find('list')->toarray();
        $vendorId = $this->vendorId();
        $products = $this->ProductsAttributes->find('all')->contain(['Attributes' => ['ParentAttributes'], 'Products'])->where(['OR' => [['ProductsAttributes.vendor_id' => 0], ['ProductsAttributes.vendor_id' => $vendorId]], 'OR' => [['Products.vendor_id' => 0], ['Products.vendor_id' => $vendorId]]])->order(['Products.productnameen' => 'ASC'])->toarray();
        $this->set(compact('categories', 'vendors', 'attributes', 'parentCategories', 'childCategories', 'brands', 'user', 'vendorId'));
        // $product = $this->Products->find('all')->contain(['Attributes' => ['ParentAttributes'], 'Categories' => ['ParentCategories']])->order(['Products.productnameen' => 'DESC'])->toarray();
        $this->set('products', $products);
    }
    public function excel_search()
    {
        $this->loadModel('Attributes');
        $this->loadModel('ProductsAttributes');
        $this->loadModel('Categories');
        $this->loadModel('Vendors');
        $this->loadModel('Brands');
        $user = $this->Auth->user();
        $parentAttributes = $this->Attributes->ParentAttributes->find('all')->toarray();
        $attributes = [];
        foreach ($parentAttributes as $parentAttribute) {
            $tmp = $this->Attributes->find()->where(['parent_id' => $parentAttribute['id']])->toarray();
            $tmp2 = array_map(function ($value) use ($parentAttribute) {
                return $value['name'] . ' ' . $parentAttribute['name'];
            }, $tmp);
            $attributes = array_merge($attributes, $tmp2);
        }
        $childCategories = $this->Categories->ChildCategories->find('list')->toarray();
        $parentCategories = $this->Categories->ParentCategories->find('list')->toarray();
        $vendors = $this->Vendors->find('list')->toarray();
        $brands = $this->Brands->find('list')->toarray();
        $categories = $this->Categories->find('list')->toarray();
        $user = $this->Auth->user();
        $data = $this->request->data;
        $cond = [];
        if (!empty($data['product_name'])) {
            $cond['Products.productnameen LIKE'] = '%' . $data['product_name'] . '%';
        }
        if (!empty($data['brand_id'])) {
            $cond['Products.brand_id'] = $data['brand_id'];
        }
        if ($user['role_id'] == '1' && !empty($data['product_type'])) {
            if ($data['product_type'] == 'vendors_product') {
                $cond['Products.vendor_id !='] = 0;
            } else {
                $cond['Products.vendor_id'] = $data['product_type'];
            }

        }
        if (!empty($data['category_id'])) {
            $cond['Products.category_id'] = $data['category_id'];
        }
        if ($user['role_id'] == '1') {
            $products = $this->ProductsAttributes->find('all')->contain(['Attributes' => ['ParentAttributes'], 'Products'])->where($cond)->order(['Products.productnameen' => 'ASC'])->toarray();
        }
        $this->set(compact('categories', 'vendors', 'attributes', 'parentCategories', 'childCategories', 'brands', 'user', 'vendorId'));
        // $product = $this->Products->find('all')->contain(['Attributes' => ['ParentAttributes'], 'Categories' => ['ParentCategories']])->order(['Products.productnameen' => 'DESC'])->toarray();
        $this->set('products', $products);
        $this->render('downloadexcl');
    }
    public function import()
    {
        $this->viewBuilder()->layout('admin');
        $user = $this->Auth->user();
        $this->set(compact('user'));
    }

    public function get_excel_data($inputfilename)
    {

        try {
            $objPHPExcel = \PHPExcel_IOFactory::load($inputfilename);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputfilename, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestDataRow();
        $highestColumn = $sheet->getHighestDataColumn();
        $c = 0;

        $firstrow = 1;
        $firstsop = $sheet->rangeToArray('A' . $firstrow . ':' . $highestColumn . $firstrow, null, true, false);

        for ($row = 2; $row <= $highestRow; $row++) {
            $exceldata = array();
            $filesop = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, false);
            if (empty($filesop[0][0]) && empty($filesop[0][1])) {
                return $csv_data;
            }
            $exceldata['id'] = $filesop[0][0];
            $exceldata['productnameen'] = $filesop[0][1];
            $exceldata['productnamehi'] = $filesop[0][2];
            $exceldata['category'] = $filesop[0][3];
            $exceldata['subcategory'] = $filesop[0][4];
            $exceldata['attribute'] = $filesop[0][5];
            $exceldata['vendor'] = $filesop[0][6];
            $exceldata['image'] = $filesop[0][7];
            $exceldata['brand'] = $filesop[0][8];
            $exceldata['description'] = $filesop[0][9];
            $exceldata['price'] = $filesop[0][10];
            $exceldata['discount_price'] = $filesop[0][11];
            $exceldata['is_stock'] = $filesop[0][12];
            $exceldata['sort'] = $filesop[0][13];
            $exceldata['is_top_saving'] = $filesop[0][14];
            // if (in_array('', array_map('trim', $val))) {
            //     //  pr($val); die;
            //     $ret = "null";
            //     return $ret;
            // }

            $csv_data[] = $exceldata;

        }
        return $csv_data;
    }
    public function add_excel()
    {

        if ($this->request->is(['post'])) {
            $this->loadModel('Categories');
            $this->loadModel('Product');
            $this->loadModel('Brands');
            $this->loadModel('Attributes');
            $this->loadModel('ProductsAttributes');
            $this->loadModel('VendorCategories');
            $vendorId = $this->vendorId();
            try {
                $role_id = $this->Auth->user('role_id');
                $user = $this->Auth->user();
                if ($this->request->data['product_excel']['tmp_name']) {

                    $empexcel = $this->request->data['product_excel'];
                    $excel_array = $this->get_excel_data($empexcel['tmp_name']);
                    if ($excel_array == "null") {

                        $this->Flash->error(__('Please Fill Mandatory Fields'));
                        $this->set('error', $error);
                        return $this->redirect(['action' => 'addcsv']);
                    }
                    if (!empty($excel_array['message'])) {

                        $this->Flash->error(__($excel_array['message']));
                        $this->set('error', $error);
                        return $this->redirect(['action' => 'addcsv']);
                    }
                    $categories = $this->Categories->find('list')->toarray();
                    $brands = $this->Brands->find('list')->toarray();
                    $attributes = $this->Attributes->find('list')->toarray();
                    foreach ($excel_array as $refer_data) {
                        $newProduct = [];
                        $attribute_type = "";
                        $attribute_id = "";
                        $prodAttributes = "";
                        $brand_id = array_search($refer_data['brand'], $brands);
                        if (!empty($refer_data['id'])) {
                            $prodExists = $this->Products->find()->where(['id' => $refer_data['id']])->first();
                        } else {
                            $prodExists = $this->Products->find()->where(['productnameen' => $refer_data['productnameen'], 'brand_id' => $brand_id])->first();
                        }
                        if (empty($prodExists)) {
                            $newProduct = $this->Products->newEntity();
                            $newProduct['productnameen'] = $refer_data['productnameen'];
                            $newProduct['productnamehi'] = $refer_data['productnamehi'];
                            $newProduct['image'] = $refer_data['image'];
                            $newProduct['description'] = $refer_data['description'];
                            $newProduct['is_top_saving'] = $refer_data['is_top_saving'];
                            $newProduct['category_id'] = array_search($refer_data['category'], $categories);
                            $newProduct['subcategory_id'] = array_search($refer_data['subcategory'], $categories);
                            $newProduct['brand_id'] = array_search($refer_data['brand'], $brands);
                            $newProduct['vendor_id'] = $role_id == 1 ? 0 : $vendorId;
                            if ($prodExists = $this->Products->save($newProduct)) {
                                $product_id = $newProduct->id;
                                $prodAttributes = explode(' ', $refer_data['attribute']);
                                $attribute_type = array_search($prodAttributes[1], $attributes);
                                $attribute_id = $this->Attributes->find()->where(['name' => $prodAttributes[0], 'parent_id' => $attribute_type])->first()->id;
                                $attributeData = $this->Attributes->find()->where(['id' => $attribute_id])->first();
                                $attributeData->_joinData = new Entity(['vendor_id' => $newProduct['vendor_id'], 'attribute_type' => $attribute_type, 'is_stock' => $refer_data['is_stock'], 'price' => $refer_data['price'], 'sort' => $refer_data['sort']], ['markNew' => true]);
                                $newProdAttribute = $this->Products->Attributes->link($prodExists, [$attributeData]);
                            } else {
                                $this->Flash->error(__('Error While uploading Product ', $refer_data['productnameen']));
                                return $this->redirect(['action' => 'import']);
                            }
                        } else {
                            if ($role_id == 1) {
                                $prodExists->productnameen = $refer_data['productnameen'];
                                $prodExists->productnamehi = $refer_data['productnamehi'];
                                $prodExists->image = $refer_data['image'];
                                $prodExists->description = $refer_data['description'];
                                $prodExists->is_top_saving = $refer_data['is_top_saving'];
                                $prodExists->vendor_id = 0;
                                $prodExists->category_id = array_search($refer_data['category'], $categories);
                                $prodExists->subcategory_id = array_search($refer_data['subcategory'], $categories);
                                $prodExists->brand_id = array_search($refer_data['brand'], $brands);
                                $this->Products->save($prodExists);
                                $prodAttributes = explode(' ', $refer_data['attribute']);
                                $attribute_type = array_search($prodAttributes[1], $attributes);
                                $attribute_id = $this->Attributes->find()->where(['name' => $prodAttributes[0], 'parent_id' => $attribute_type])->first()->id;
                                $prodAttrExists = $this->ProductsAttributes->find()->where(['product_id' => $prodExists['id'], 'vendor_id' => 0, 'attribute_id' => $attribute_id, 'attribute_type' => $attribute_type])->first();
                                if (empty($prodAttrExists)) {
                                    $attributeData = $this->Attributes->find()->where(['id' => $attribute_id])->first();
                                    $attributeData->_joinData = new Entity(['vendor_id' => 0, 'attribute_type' => $attribute_type, 'is_stock' => $refer_data['is_stock'], 'price' => $refer_data['price'], 'discount_price' => $refer_data['discount_price'], 'sort' => $refer_data['sort']], ['markNew' => true]);
                                    $newProdAttribute = $this->Products->Attributes->link($prodExists, [$attributeData]);
                                } else {
                                    $prodAttrExists->is_stock = $refer_data['is_stock'];
                                    $prodAttrExists->price = $refer_data['price'];
                                    $prodAttrExists->discount_price = $refer_data['discount_price'];
                                    $prodAttrExists->sort = $refer_data['sort'];
                                    $this->ProductsAttributes->save($prodAttrExists);
                                }
                            } else {
                                $category_id = array_search($refer_data['category'], $categories);
                                $catExist = $this->VendorCategories->exists(['category_id' => $category_id, 'vendor_id' => $vendorId, 'status' => 1]);
                                if ($catExist) {
                                    $prodAttributes = explode(' ', $refer_data['attribute']);
                                    $attribute_type = array_search($prodAttributes[1], $attributes);
                                    $attribute_id = array_search($prodAttributes[0], $attributes);
                                    $prodAttrExists = $this->ProductsAttributes->find()->where(['product_id' => $prodExists['id'], 'vendor_id' => $vendorId, 'attribute_id' => $attribute_id, 'attribute_type' => $attribute_type])->first();
                                    if (empty($prodAttrExists)) {
                                        $vendorProdAttrExists = $this->ProductsAttributes->find()->where(['product_id' => $prodExists['id'], 'vendor_id' => 0, 'attribute_id' => $attribute_id, 'attribute_type' => $attribute_type, 'price' => $refer_data['price'], 'discount_price' => $refer_data['discount_price'], 'is_stock' => $refer_data['is_stock']])->first();
                                        if (empty($vendorProdAttrExists)) {
                                            $attributeData = $this->Attributes->find()->where(['id' => $attribute_id])->first();
                                            $attributeData->_joinData = new Entity(['vendor_id' => $vendorId, 'attribute_type' => $attribute_type, 'is_stock' => $refer_data['is_stock'], 'price' => $refer_data['price'], 'discount_price' => $refer_data['discount_price'], ['markNew' => true], 'sort' => $refer_data['sort']], ['markNew' => true]);
                                            $newProdAttribute = $this->Products->Attributes->link($prodExists, [$attributeData]);
                                        }

                                    } else {
                                        $prodAttrExists->is_stock = $refer_data['is_stock'];
                                        $prodAttrExists->price = $refer_data['price'];
                                        $prodAttrExists->discount_price = $refer_data['discount_price'];
                                        $prodAttrExists->sort = $refer_data['sort'];
                                        $this->ProductsAttributes->save($prodAttrExists);
                                    }
                                }
                            }
                            $product_id = $prodExists->id;
                        }
                        // $prodExists->pro

                    }

                    $this->Flash->success(__('Product has been saved.'));
                    return $this->redirect(['action' => 'index']);
                }
                die;
                $this->Flash->error(__('Employeesalary  has not been saved.'));
                return $this->redirect(['action' => 'index']);

            } catch (\PDOException $e) {
                pr($e);die;
                $this->Flash->error(__('Product updation failed' . $error));
                $this->set('error', $error);
                return $this->redirect(['action' => 'import']);
            }

        }

    }

    public function get_vendor_excel_data($inputfilename)
    {

        try {
            $objPHPExcel = \PHPExcel_IOFactory::load($inputfilename);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputfilename, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestDataRow();
        $highestColumn = $sheet->getHighestDataColumn();
        $c = 0;

        $firstrow = 1;
        $firstsop = $sheet->rangeToArray('A' . $firstrow . ':' . $highestColumn . $firstrow, null, true, false);

        for ($row = 2; $row <= $highestRow; $row++) {
            $exceldata = array();
            $filesop = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, false);
            if (empty($filesop[0][0]) && empty($filesop[0][1])) {
                return $csv_data;
            }
            $exceldata['id'] = $filesop[0][0];
            $exceldata['productnameen'] = $filesop[0][1];
            $exceldata['productnamehi'] = $filesop[0][2];
            $exceldata['attribute'] = $filesop[0][3];
            $exceldata['price'] = $filesop[0][5];
            $exceldata['discount_price'] = $filesop[0][6];
            $exceldata['is_stock'] = $filesop[0][7];
            $exceldata['sort'] = $filesop[0][8];
            $exceldata['is_top_saving'] = $filesop[0][9];
            // if (in_array('', array_map('trim', $val))) {
            //     //  pr($val); die;
            //     $ret = "null";
            //     return $ret;
            // }

            $csv_data[] = $exceldata;

        }
        return $csv_data;
    }
    public function add_vendor_excel()
    {
        if ($this->request->is(['post'])) {
            $this->loadModel('Categories');
            $this->loadModel('Product');
            $this->loadModel('Brands');
            $this->loadModel('Attributes');
            $this->loadModel('ProductsAttributes');
            $this->loadModel('VendorCategories');
            $this->loadModel('ProductTopSavings');
            $this->loadModel('Vendors');
            $vendorId = $this->vendorId();
            try {
                $role_id = $this->Auth->user('role_id');
                $user = $this->Auth->user();
                if ($this->request->data['product_excel']['tmp_name']) {

                    $empexcel = $this->request->data['product_excel'];
                    $excel_array = $this->get_vendor_excel_data($empexcel['tmp_name']);
                    if ($excel_array == "null") {

                        $this->Flash->error(__('Please Fill Mandatory Fields'));
                        $this->set('error', $error);
                        return $this->redirect(['action' => 'addcsv']);
                    }
                    if (!empty($excel_array['message'])) {

                        $this->Flash->error(__($excel_array['message']));
                        $this->set('error', $error);
                        return $this->redirect(['action' => 'addcsv']);
                    }
                    $categories = $this->Categories->find('list')->toarray();
                    $brands = $this->Brands->find('list')->toarray();
                    $attributes = $this->Attributes->find('list')->toarray();
                    $this->ProductTopSavings->deleteAll(['vendor_id' => $vendorId]);
                    $topSaving = [];
                    foreach ($excel_array as $refer_data) {
                        $newProduct = [];
                        $attribute_type = "";
                        $attribute_id = "";
                        $prodAttributes = "";
                        $brand_id = array_search($refer_data['brand'], $brands);
                        $prodExists = $this->Products->find()->where(['id' => $refer_data['id']])->first();
                        if (empty($prodExists)) {
                            continue;
                        }
                            $prodAttributes = explode(' ', $refer_data['attribute']);
                            $attribute_type = array_search($prodAttributes[1], $attributes);
                            $attribute_id = array_search($prodAttributes[0], $attributes);
                            $prodAttrExists = $this->ProductsAttributes->find()->where(['product_id' => $prodExists['id'], 'vendor_id' => $vendorId, 'attribute_id' => $attribute_id, 'attribute_type' => $attribute_type])->first();
                            if (empty($prodAttrExists)) {
                                $vendorProdAttrExists = $this->ProductsAttributes->find()->where(['product_id' => $prodExists['id'], 'vendor_id' => 0, 'attribute_id' => $attribute_id, 'attribute_type' => $attribute_type, 'price' => $refer_data['price'], 'discount_price' => $refer_data['discount_price'], 'is_stock' => $refer_data['is_stock']])->first();
                                if (empty($vendorProdAttrExists)) {
                                    $attributeData = $this->Attributes->find()->where(['id' => $attribute_id])->first();
                                    $attributeData->_joinData = new Entity(['vendor_id' => $vendorId, 'attribute_type' => $attribute_type, 'is_stock' => $refer_data['is_stock'], 'price' => $refer_data['price'], 'discount_price' => $refer_data['discount_price'], ['markNew' => true], 'sort' => $refer_data['sort']], ['markNew' => true]);
                                    $newProdAttribute = $this->Products->Attributes->link($prodExists, [$attributeData]);
                                }
                            } else {
                                $prodAttrExists->is_stock = $refer_data['is_stock'];
                                $prodAttrExists->price = $refer_data['price'];
                                $prodAttrExists->discount_price = $refer_data['discount_price'];
                                $prodAttrExists->sort = $refer_data['sort'];
                                $this->ProductsAttributes->save($prodAttrExists);
                            }
                        $product_id = $prodExists->id;
                        if ($refer_data['is_top_saving'] == 'Y') {
                            if(!in_array($product_id,$new)){
                            $new[]=$product_id;
                            $saving=$this->ProductTopSavings->newEntity();
                            $saving['product_id']=$product_id;
                            $saving['vendor_id']=$vendorId;
                            $this->ProductTopSavings->save($saving);
                            }
                        }

                    }
                    $this->Flash->success(__('Product has been saved.'));
                    return $this->redirect(['action' => 'index']);
                }
                die;
                $this->Flash->error(__('Employeesalary  has not been saved.'));
                return $this->redirect(['action' => 'index']);

            } catch (\PDOException $e) {
                pr($e);die;
                $this->Flash->error(__('Product updation failed' . $error));
                $this->set('error', $error);
                return $this->redirect(['action' => 'import']);
            }

        }

    }

    public function isAuthorized($user)
    {
        if (in_array($this->request->params['action'], ['edit', 'delete'])) {
            if ($user['role_id'] == 1) {
                return true;
            }
            $productId = (int) $this->request->params['pass']['0'];
            $vendorId = $this->vendorId();
            if ($this->Products->isOwnedBy($productId, $vendorId)) {
                return true;
            }
        }
        if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 101 || $user['role_id'] == 2)) {
            return true;
        }

        return false;
    }

}
