<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Routing\Router;

class AttributesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['']);
    }

// View brand
    public function index()
    {
        $this->viewBuilder()->layout('admin');
        $mainattributes = $this->Attributes->find()->contain(['ParentAttributes'])->order(['Attributes.parent_id' => 'ASC']);
        $this->set(compact('mainattributes'));
        $this->set('_serialize', ['mainattributes']);

    }

    public function category($id)
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Attributes');
        $mainattributes = $this->Attributes->find('all')->where(['parent_id' => $id])->order(['Attributes.id' => 'desc']);

        $this->set('mainattributes', $this->paginate($mainattributes)->toarray());
    }

// Add Brand
    public function add()
    {
        $this->viewBuilder()->layout('admin');
        $attribute = $this->Attributes->newEntity();
        if ($this->request->is('post')) {
            $attribute = $this->Attributes->patchEntity($attribute, $this->request->getData());
            if ($this->Attributes->save($attribute)) {
                $this->Flash->success(__('The attribute has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The attribute could not be saved. Please, try again.'));
        }
        $parentAttributes = $this->Attributes->ParentAttributes->find('list', ['limit' => 200]);
        $this->set(compact('attribute', 'parentAttributes'));

    }

    public function edit($id = null)
    {
        $this->viewBuilder()->layout('admin');
        $attribute = $this->Attributes->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $attribute = $this->Attributes->patchEntity($attribute, $this->request->getData());
            if ($this->Attributes->save($attribute)) {
                $this->Flash->success(__('The attribute has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The attribute could not be saved. Please, try again.'));
        }
        $parentAttributes = $this->Attributes->ParentAttributes->find('list')->where(['id NOT IN' => $id])->order(['created' => 'desc'])->toarray();

        $this->set(compact('attribute', 'parentAttributes'));
    }

// Change brand status
    public function status($id, $status)
    {

        $this->loadModel('Attributes');
        if (isset($id) && !empty($id)) {

            $product = $this->Attributes->get($id);
            $product->status = $status;
            if ($this->Attributes->save($product)) {
                $this->Flash->success(__('Attribute status has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

// Change brand status
    public function categorystatus($id, $status)
    {

        $this->loadModel('Attributes');
        if (isset($id) && !empty($id)) {

            $product = $this->Attributes->get($id);
            $product->status = $status;
            if ($this->Attributes->save($product)) {
                $this->Flash->success(__('Attribute category status has been updated.'));
                $this->redirect(Router::url($this->referer(), true));
            }
        }
    }

// delete brand
    public function delete($id = null)
    {
        $this->loadModel('Attributes');
        $prod_data = $this->Attributes->get($id);
        if ($prod_data) {
            $this->Attributes->delete($prod_data);
            $this->Flash->success(__('Attribute has been deleted successfully.'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('Attribute has not been deleted'));
            return $this->redirect(['action' => 'index']);

        }

    }

// delete brand
    public function categorydelete($id = null)
    {
        $this->loadModel('Attributes');
        $prod_data = $this->Attributes->get($id);
        if ($prod_data) {
            $this->Attributes->delete($prod_data);
            $this->Flash->success(__('Attribute category has been deleted successfully.'));
            $this->redirect(Router::url($this->referer(), true));
        } else {
            $this->Flash->error(__('Attribute category has not been deleted'));
            $this->redirect(Router::url($this->referer(), true));
        }

    }

    public function categoryedit($id)
    {
        $redirecturl = $this->referer();
        $session = $this->request->session();
        if ($this->request->session()->read('redirectattributecategory') == '') {
            $session->write('redirectattributecategory', $redirecturl);
        }
        //pr($_SESSION);
        $this->loadModel('Attributes');
        $this->viewBuilder()->layout('admin');
        //add select type
        $mainattributes = $this->Attributes->find('list')->where(['parent_id' => '0'])->order(['Attributes.id' => 'asc'])->toarray();
        $this->set('names', $mainattributes);

        $edit_entity = $this->Attributes->get($id);
        $this->set('edit_entity', $edit_entity);
        if ($this->request->is(['post', 'put'])) {

            if (empty($this->request->data['parent_id'])) {
                $this->request->data['parent_id'] = 0;
            } else {
                $this->request->data['parent_id'] = $this->request->data['parent_id'];
            }
            $edit_patch = $this->Attributes->patchEntity($edit_entity, $this->request->data);
            if ($edit_save = $this->Attributes->save($edit_patch)) {

                $this->Flash->success(__('Attributes name  has been updated.'));
                $this->redirect(Router::url($this->request->session()->read('redirectattributecategory'), true));
                $session->delete('redirectattributecategory');
            } else {

                $this->Flash->success(__('Attributes name  not has been update'));
                $this->redirect(Router::url($this->request->session()->read('redirectattributecategory'), true));
                $session->delete('redirectattributecategory');
            }

        }
    }
    public function search_attribute()
    {
        $this->autoRender = false;
        $id = $this->request->data['id'];
        $attributes = $this->Attributes->find('list', ['keyField' => 'id', 'valueField' => 'name'])->where(['parent_id' => $id])->toarray();
        echo json_encode($attributes);
        return;
    }
    public function isAuthorized($user)
    {
        if (in_array($this->request->params['action'], ['search_attribute'])) {
            if (isset($user['role_id']) && $user['role_id'] == 2) {
                return true;
            }
        }
        if (isset($user['role_id']) && ($user['role_id'] == 1)) {
            return true;
        }

        return false;
    }

}
