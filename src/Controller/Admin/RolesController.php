<?php

namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Network\Email\Email;


class RolesController extends AppController
{


	public function beforeFilter(Event $event)
	{    
		
	}


	public function index(){ 
		//pr(date('Y-m-d')); die;
		$role_id=$this->request->session()->read('Auth.User.role_id');
		if ($role_id==1) {
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Users');
		$roles = $this->Users->find('all')->where(['role_id NOT IN'=>[0,1]]);	
	//pr($wallets); die;
	//$this->set('product',$product);
		$this->set('wallets', $this->paginate($roles)->toarray());
	}else{
			$this->Flash->error(__('You are not authorized to access that location.'));
			return $this->redirect(['controller'=>'dashboard' ,'action' => 'index']);
		}
	}
	

	public function add()
	{
		$role_id=$this->request->session()->read('Auth.User.role_id');
		if ($role_id==1) {
		
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Users');
		$rolesadd = $this->Users->newEntity();
		if ($this->request->is(['post', 'put'])) {
  		//pr($this->request->data); die;
			
			$this->request->data['confirm_pass']=$this->request->data['pass'];
			$this->request->data['otp']='0';
			$this->request->data['remarkresponse']='null';
			$this->request->data['created']=date('Y-m-d');
			$this->request->data['password'] = (new DefaultPasswordHasher)->hash($this->request->data['pass']);        
			$user_reg_add = $this->Users->patchEntity($rolesadd, $this->request->data());
			$results=$this->Users->save($user_reg_add);
			if ($results){
					$this->Flash->success(__('Roles has been saved.'));
					return $this->redirect(['action' => 'index']);	
				}
			}
		}else{
			return $this->redirect(['action' => 'index']);
		}
			
	}

	public function edit($id)
	{
		$this->loadModel('Users');
		$walletadd = $this->Users->get($id);
		$this->set('walletadd', $walletadd);
		$this->viewBuilder()->layout('admin');	
		if ($this->request->is(['post', 'put'])) {

			$var = $this->request->data['expirydate'];
			$this->request->data['expirydate']=date("Y-m-d", strtotime($var) );     
			$walletsave = $this->Wallet->patchEntity($walletadd, $this->request->data);
			if ($this->Users->save($walletsave)) {
				$this->Flash->success(__('Wallet money has been updated Successfully.'));
				return $this->redirect(['action' => 'index']);	
			}
		}
	}

	
	public function delete($id=null)
	{
		$role_id=$this->request->session()->read('Auth.User.role_id');
		if ($role_id==1) {
		$this->loadModel('Users');
		$roleuser = $this->Users->get($id);
		if($roleuser){
			$this->Users->delete($roleuser);
			$this->Flash->success(__('User Role has been deleted successfully.'));
			return $this->redirect(['action' => 'index']);
		}else{
			$this->Flash->error(__('User role not deleted'));
			return $this->redirect(['action' => 'index']);

		}
		}else{
			return $this->redirect(['action' => 'index']);
		}
	}

	public function status($id,$status){
		$role_id=$this->request->session()->read('Auth.User.role_id');
		if ($role_id==1) {
		$this->loadModel('Users');
		if(isset($id) && !empty($id)){

			$roleuser = $this->Users->get($id);
			$roleuser->status = $status;
			if ($this->Users->save($roleuser)) {
				$this->Flash->success(__('Roles status has been updated.'));
				return $this->redirect(['action' => 'index']);  
			}
		}
		}else{
			return $this->redirect(['action' => 'index']);
		}
	}

}
