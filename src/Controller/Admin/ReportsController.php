<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

include '../vendor/PHPExcel/Classes/PHPExcel.php';
include '../vendor/PHPExcel/Classes/PHPExcel/IOFactory.php';

class ReportsController extends AppController
{

    public function vendorpayment()
    {
        $this->loadModel('Vendorpayments');
        $payment = $this->Vendorpayments->newEntity();
        $this->request->data['vendor_id'] = $this->request->data['vendor_rec'];
        $this->request->data['amount'] = $this->request->data['amount'];
        $this->request->data['descri'] = $this->request->data['descri'];
        $this->request->data['transactionnumber'] = uniqid();
        $cartsave = $this->Vendorpayments->patchEntity($payment, $this->request->data);
        $savedata = $this->Vendorpayments->save($cartsave);
        if ($savedata) {
            echo '0';die;
        } else {
            echo '1';die;
        }
    }

//Vendor detail record
    public function vendordetail()
    {
        $this->loadModel('Users');
        $this->loadModel('Vendorpayments');
        $this->loadmodel('Vendors');
        $this->loadmodel('Orders');

        $id = $this->request->data['vendor_id'];
        $vendor = $this->Vendors->find('all')->where(['id' => $id])->first();
        $onlinePayment = $this->Orders->find()->select(['sum' => 'SUM(transaction_amount)'])->where(['vendor_id' => $id, 'transaction_mode' => 'online', 'order_status' => 'delivered'])->first()->sum;
        $totalCommission = $this->Orders->find()->select(['sum' => 'SUM(commission)'])->where(['vendor_id' => $id, 'order_status' => 'delivered'])->first()->sum;
        $totalPayment = $this->Vendorpayments->find()->select(['sum' => 'SUM(amount)'])->where(['vendor_id' => $id])->first()->sum;
        $final_balance = $onlinePayment - round($totalCommission, 2) - $totalPayment;
        $data['vendorname'] = $vendor['name'];
        $data['vendormobile'] = $vendor['mobile_1'];
        $data['vendorbalance'] = $final_balance;
        echo json_encode($data);die;
    }

//Payment history search
    public function paymenthistorysearch()
    {
        $this->loadModel('Vendorpayments');
        $query = $this->request->data();
        $cond = [];
        $vendor_id = trim($this->request->data['vendor_id']);
        $from_date = trim($this->request->data['from_date']);

        if (!empty($vendor_id)) {
            $cond['Vendorpayments.vendor_id'] = $vendor_id;
        }

        if (!empty($from_date)) {
            $dates = explode('-', $from_date);
            $from = date('Y-m-d', strtotime(str_replace('/', '-', $dates[0])));
            $to = date('Y-m-d', strtotime(str_replace('/', '-', $dates[1])));
        }

        if (!empty($from_date)) {
            $cond['DATE(Vendorpayments.created) >='] = $from;
            $cond['DATE(Vendorpayments.created) <='] = $to;
        }
        $vendor_payments = $this->Vendorpayments->find('all')->where([$cond])->contain(['Vendors'])->order(['Vendorpayments.id' => 'desc'])->toarray();
        $this->set('vendor_payments', $vendor_payments);

    }

//Payment history
    public function paymenthistory()
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Vendors');
        $this->loadModel('Vendorpayments');
        $vendor = $this->Vendors->find('list')->order(['id' => 'desc'])->toarray();
        $vendor_payments = $this->Vendorpayments->find('all')->contain(['Vendors'])->order(['Vendorpayments.id' => 'desc'])->toarray();
        $this->set('vendor_payments', $vendor_payments);
        $this->set('vendor', $vendor);
    }

// Vendor list
    public function vendorledger()
    {
        $user = $this->Auth->user();
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Vendors');
        $vendor = $this->Vendors->find('list')->where(['status' => 1])->order(['name' => 'desc'])->toarray();
        $this->set('vendor', $vendor);
        $this->set('user', $user);
    }

// Ledger search
    public function ledgersearch()
    {
        $this->loadModel('Orders');
        $this->loadModel('Vendorpayments');
        $response = array();
        extract($this->request->data);
        $cond = [];
        $payment = [];
        $user = $this->Auth->user();
        if ($user['role_id'] == 1) {
            $vendorId = trim($this->request->data['vendor_id']);
        } else {
            $vendorId = $this->vendorId();
        }
        $openingBalance = 0;

        $cond['Orders.vendor_id'] = $vendorId;
        $payment['Vendorpayments.vendor_id'] = $vendorId;
        if (empty($from_date)) {
            $fromDate = date('Y-m-d', strtotime('-30 days'));
            $toDate = date('Y-m-d');
        } else {
            $date = explode('-', $from_date);
            $fromDate = str_replace('/', '-', $date[0]);
            $toDate = str_replace('/', '-', $date[1]);
            $fromDate = date('Y-m-d', strtotime($fromDate));
            $toDate = date('Y-m-d', strtotime($toDate));
        }
        $conncc = TableRegistry::get('Orders');
        $query = $conncc->find();
        $suborder_total = $query->select(['Sum' => $query->func()->sum('Orders.total_amount')])->where(['Orders.vendor_id' => $vendorId, 'Orders.order_status' => 'delivered', 'transaction_mode' => 'online', 'Orders.created <' => $fromDate])->first();
        $connccvendor = TableRegistry::get('Vendorpayments');
        $queryvendor = $connccvendor->find();
        $vendorpay_total = $queryvendor->select(['Sum' => $queryvendor->func()->sum('Vendorpayments.amount')])->where(['Vendorpayments.vendor_id' => $vendorId, 'Vendorpayments.created <' => $fromDate])->first();
        $vendorCommission = $this->Orders->find()->select(['Sum' => 'sum(Orders.commission)'])->where(['Orders.vendor_id' => $vendorId, 'Orders.order_status' => 'delivered', 'Orders.created <' => $fromDate])->first();
        $openingBalance = $suborder_total['Sum'] - $vendorpay_total['Sum'] - $vendorCommission['Sum'];
        $cond['DATE(Orders.created) >='] = $fromDate;
        $cond['DATE(Orders.created) <='] = $toDate;
        $payment['DATE(Vendorpayments.created) >='] = $fromDate;
        $payment['DATE(Vendorpayments.created) <='] = $toDate;
        $cond['Orders.order_status'] = 'delivered';
        $balance_total = 0;
        $totalOrder = 0;
        $totalPayment = 0;
        $vendor_order = $this->Orders->find('all')->contain(['Vendors'])->where([$cond])->toarray();
        foreach ($vendor_order as $ordervalue) {
            $ledger_vendor = [];
            $ledger_vendor['order_id'] = $ordervalue['id'];
            if ($ordervalue['transaction_mode'] != 'online') {
                $ledger_vendor['Descripation'] = "Sales to goods(COD)";
                $ledger_vendor['total'] = $ordervalue['total_amount'];
                $ledger_vendor['comission'] = $ordervalue['commission'];
                $ledger_vendor['credit'] = $ordervalue['total_amount'];
                $ledger_vendor['debit'] = 0;
                $ledger_vendor['balance'] = -($ordervalue['commission']);
                $totalPayment += $ordervalue['total_amount'];
            } else {
                $ledger_vendor['Descripation'] = "Sales to goods(Online Payment)";
                $ledger_vendor['total'] = $ordervalue['total_amount'];
                $ledger_vendor['comission'] = $ordervalue['commission'];
                $ledger_vendor['debit'] = $ordervalue['total_amount'];
                $ledger_vendor['credit'] = 0;
                $ledger_vendor['balance'] = ($ordervalue['total_amount'] - $ordervalue['commission']);
            }
            $totalOrder += $ordervalue['total_amount'];
            $ledger_vendor['created'] = $ordervalue['created'];
            $ledger_data[] = $ledger_vendor;
        }
        $vendor_payment = $this->Vendorpayments->find('all')->contain(['Vendors'])->where([$payment])->toarray();

        foreach ($vendor_payment as $paymentvalue) {
            $ledger_payment['order_id'] = $paymentvalue['transactionnumber'];
            $ledger_payment['Descripation'] = $paymentvalue['descri'] . "(ADMIN)";
            $ledger_payment['total'] = null;
            $ledger_payment['comission'] = null;
            $ledger_payment['debit'] = 0;
            $ledger_payment['credit'] = $paymentvalue['amount'];
            $ledger_payment['created'] = $paymentvalue['created'];
            $ledger_payment['balance'] = -$paymentvalue['amount'];
            $ledger_payment_data[] = $ledger_payment;
            $totalPayment += $paymentvalue['amount'];
        }
        if ($ledger_payment_data && $ledger_data) {
            $total_ledger = array_merge($ledger_data, $ledger_payment_data);
        } else if ($ledger_data) {
            $total_ledger = $ledger_data;
        } else if ($ledger_payment_data) {
            $total_ledger = $ledger_payment_data;
        }
        $totalBalance = $openingBalance;
        $response['success'] = true;
        $response['output']['openingBalance'] = $openingBalance;
        $response['output']['totalOrder'] = $totalOrder;
        $response['output']['totalComission'] = null;
        $response['output']['totalPayment'] = $totalPayment;
        $response['output']['totalBalance'] = null;
        if (empty($total_ledger)) {
            $response['output']['totalBalance'] = $openingBalance;
            $response['output']['transactions'] = null;
            $response['output']['message'] = "No Transactions Found";
            $this->set(compact('response'));
            return;
        }

        usort($total_ledger, function ($a, $b) {
            $t1 = $a['created'];
            $t2 = $b['created'];
            return strtotime($t1) - strtotime($t2);
        });
        $totalamount = 0;
        $totalcomission = 0;
        foreach ($total_ledger as $ledger) {
            $data = [];
            $data['order_id'] = $ledger['order_id'];
            $data['date'] = $ledger['created'];
            $data['description'] = $ledger['Descripation'];
            $data['orderTotal'] = $ledger['total'];
            $data['commission'] = $ledger['comission'];
            $data['credit'] = $ledger['credit'] == 0 ? '-' : $ledger['credit'];
            $data['debit'] = $ledger['debit'] == 0 ? '-' : $ledger['debit'];
            $totalBalance += $ledger['balance'];
            $data['balance'] = $totalBalance;
            $totalcomission += $ledger['comission'];
            $response['output']['transactions'][] = $data;
        }
        $response['output']['totalComission'] = round($totalcomission, 2);
        $response['output']['totalBalance'] = $totalBalance;
        $this->set(compact('response', 'openingBalance'));
        if (isset($type) && $type == 'excel') {
            $this->render('ledger_excel');
        }
    }

    public function isAuthorized($user)
    {

        if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 101 || $user['role_id'] == 2)) {
            return true;
        }

        return false;
    }

}
