<?php 
namespace App\Controller\Admin;
use App\Controller\AppController;

use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
class UsersController extends AppController
{
	
	public function initialize()
	{
		parent::initialize();
		$this->loadModel('Users');
		$this->Auth->allow(['_setPassword','signup','findUsername','login','logout','verify','getphonecode','forgotpassword','forgetCpass','sociallogin']);
	}
	public function index()
	{
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Users');
		$id=$this->request->session()->read('Auth.User.id');
		$user =$this->Users->get($this->Auth->user('id'));

		$useroldpass =$this->Users->find('all')->where(['Users.id' =>$id])->first(); 
		if ($this->request->is(['post', 'put'])) {

			try { 
				if($this->request->data['oldpassword']!=''){
					if ($this->request->data['oldpassword']==$useroldpass['confirm_pass']) 
					{
						if($this->request->data['spassword']!=''){

							if($this->request->data['cpasswords']==$this->request->data['spassword'])
							{
								$this->request->data['confirm_pass'] =$this->request->data['cpasswords'];
								$this->request->data['password'] = (new DefaultPasswordHasher)->hash($this->request->data['confirm_pass']);		
							}else
							{
								$this->Flash->error(__('Your new password and confirm password should be same.'));
								return $this->redirect(['controller'=>'users','action' => 'index/'.$this->Auth->user('id')]);	
							}	//change password
						}
					}
					else{
						$this->Flash->error(__('Your old password is wrong.'));
						return $this->redirect(['controller'=>'users','action' => 'index/'.$this->Auth->user('id')]);
					}
				}else{
					$this->request->data['confirm_pass'] =$useroldpass['confirm_pass'];
					$this->request->data['password'] = (new DefaultPasswordHasher)->hash($useroldpass['confirm_pass']);
				} 	

				if($id=='1') { 


			//pr($certificate); die;
					$oldimage=$useroldpass['signature_image'];	  
					if(!empty($this->request->data['signature_image']['name']))
					{
						$filename=$this->request->data['signature_image']['name'];
						$item=$this->request->data['signature_image']['tmp_name'];				
						$ext=  end(explode('.',$filename));
						$name = md5(time($filename)); 
						$imagename=$name.'.'.$ext;							
						if(move_uploaded_file($item,"images/".$imagename))
						{						
							$this->request->data['signature_image']=$imagename;
						}
					}
					else
					{					
						$this->request->data['signature_image']=$oldimage;
					}
				}
				$user = $this->Users->patchEntity($user, $this->request->data); 
		      // pr($user); die;
			  // edit site setting

				if ($this->Users->save($user)) {
					//	pr($this->Users->save($user)); die;
					if ($user['confirm_pass']=='' || $this->request->data['oldpassword']=='') {
						$this->Flash->success(__('Your site setting has been updated.'));
						return $this->redirect(['action' => 'index']);
					} else if($this->request->data['cpasswords']!='') {
						$this->Flash->success(__('Your password is successfully changed so login with your new password.'));
						return $this->redirect(['controller'=>'users','action' => 'login/'.$this->Auth->user('id')]);
					}else{
						$this->Flash->error(__('Your new password field is blank.'));
						return $this->redirect(['action' => 'index']);
					}


				}
			}
			catch (\PDOException $e) {

				$this->Flash->error(__('User Name Already Exits'));
				$this->set('error', $error);
				return $this->redirect(['action' => 'index']);
			}
		}
		$this->set('Users', $user);
	}


	public function login($value='')
	{
		$this->redirect(SITE_URL.'admin');
	}

}
