<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

class BrandController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['']);
    }

// View brand
    public function index()
    {
        $this->loadModel('Brands');
        $this->viewBuilder()->layout('admin');
        $brand = $this->Brands->find()->order(['created' => 'desc']);
        $this->set(compact('brand'));
        $this->set('_serialize', ['brand']);
    }

// Add Brand
    public function add()
    {
        $this->loadModel('Brands');
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Seo');
        $brand = $this->Brands->newEntity();
        if ($this->request->is('post')) {
            $brand = $this->Brands->patchEntity($brand, $this->request->getData());
            //pr($this->request->data); die;
            if ($brandsave = $this->Brands->save($brand)) {
                $brand_last_id = $brandsave['id'];
                $paththumb = 'brandimage/';
                $brand_image = $this->request->data['image'];
                $gallery_brand_image = $this->move_images($brand_image);
                $this->brand_images($gallery_brand_image, $brand_last_id, 'thumb', array(900, 900), $paththumb);
                $this->unlink_images($gallery_brand_image);

                $seo = $this->Seo->newEntity();
                $seo_data['name'] = $this->request->data['name'];
                $seo_data['location'] = SITE_URL . 'brand/' . $this->request->data['slug'];
                $seo_data['title'] = $this->request->data['seotitle'];
                $seo_data['keyword'] = $this->request->data['seokeyword'];
                $seo_data['descripation'] = $this->request->data['seodescripation'];
                $seo_data['type'] = 'brand';
                $seo_data['data_id'] = $brand_last_id;
                $seo = $this->Seo->patchEntity($seo, $seo_data);
                $this->Seo->save($seo);

                $this->Flash->success(__('The brand has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The brand could not be saved. Please, try again.'));
        }
        //$parentBrand = $this->Brands->ParentBrand->find('list', ['limit' => 200]);
        $this->set(compact('brand', 'parentBrand'));

    }

    public function edit($id = null)
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Seo');
        $brand = $this->Brands->get($id, [
            'contain' => [],
        ]);
        $seo = $this->Seo->find()->where(['Seo.data_id' => $id, 'Seo.type' => 'brand'])->first();
        if ($this->request->is(['patch', 'post', 'put'])) {

            if ($this->request->data['image']['name'] == '') {
                $this->request->data['image'] = $brand['image'];
            }

            $this->request->data['name'] = ucfirst($this->request->data['name']);
            $brand = $this->Brands->patchEntity($brand, $this->request->data);
            if ($this->Brands->save($brand)) {
                //pr($this->request->data); die;

                if ($seo) {
                    $seo_data = $this->Seo->get($seo['id']);
                    $seo_data->title = $this->request->data['seotitle'];
                    $seo_data->location = SITE_URL . 'brand/' . $this->request->data['slug'];
                    $seo_data->keyword = $this->request->data['seokeyword'];
                    $seo_data->descripation = $this->request->data['seodescripation'];
                    $this->Seo->save($seo_data);
                } else {
                    $seo = $this->Seo->newEntity();
                    $seo_data['name'] = $this->request->data['name'];
                    $seo_data['location'] = SITE_URL . 'brand/' . $this->request->data['slug'];
                    $seo_data['title'] = $this->request->data['seotitle'];
                    $seo_data['keyword'] = $this->request->data['seokeyword'];
                    $seo_data['descripation'] = $this->request->data['seodescripation'];
                    $seo_data['type'] = 'brand';
                    $seo_data['data_id'] = $id;
                    $seo = $this->Seo->patchEntity($seo, $seo_data);
                    $this->Seo->save($seo);
                }

                if ($this->request->data['image']['name']) {
                    $paththumb = 'brandimage/';
                    $brand_image = $this->request->data['image'];
                    $gallery_brand_image = $this->move_images($brand_image);
                    $this->brand_images($gallery_brand_image, $id, 'thumb', array(900, 900), $paththumb);
                    $this->unlink_images($gallery_brand_image);
                }
                $this->Flash->success(__('The brand has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The brand could not be saved. Please, try again.'));
        }
        //$parentBrand = $this->Brands->ParentBrand->find('list', ['limit' => 200]);
        $this->set(compact('brand', 'parentBrand', 'seo'));
    }

// Change brand status
    public function status($id, $status)
    {

        $this->loadModel('Brands');
        if (isset($id) && !empty($id)) {

            $product = $this->Brands->get($id);
            $product->status = $status;
            if ($this->Brands->save($product)) {
                $this->Flash->success(__('Brand status has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

// delete brand
    public function delete($id = null)
    {
        $this->loadModel('Brands');
        $prod_data = $this->Brands->get($id);
        if ($prod_data) {
            $this->Brands->delete($prod_data);
            $this->Flash->success(__('Brand has been deleted successfully.'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('Brand has not been deleted'));
            return $this->redirect(['action' => 'index']);

        }
    }

// check brand delete
    //     public function delete($id=null)
    //     {
    //         $this->loadModel('Brands');
    //         $this->loadModel('Service');

    //     $servicewasher=$this->Service->find('all')->where(['Service.city_id'=>$id])->first();
    //     if ($servicewasher) {
    //         $this->Flash->error(__('City not delete, because city exists in service'));
    //         return $this->redirect(['action' => 'index']);
    //     }else{
    //         $prod_data = $this->City->get($id);
    //         if($prod_data){
    //             $this->City->delete($prod_data);
    //             $this->Flash->success(__('City has been deleted successfully.'));
    //             return $this->redirect(['action' => 'index']);
    //         }else{
    //             $this->Flash->error(__('City has not been deleted'));
    //             return $this->redirect(['action' => 'index']);

    //         }
    //     }
    // }

}
