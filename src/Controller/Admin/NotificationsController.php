<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;

class NotificationsController extends AppController
{

    public function beforeFilter(Event $event)
    {

    }

    public function index()
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Notifications');
        $this->loadModel('Vendors');
        $vendors = $this->Vendors->find('list')->toarray();
        $this->set(compact('vendors'));

        $Notifications = $this->Notifications->find('all');
        $this->set('notifications', $this->paginate($Notifications)->toarray());
    }
    public function push_notification_android($device_id, $message)
    {

        //API URL of FCM
        $url = 'https://fcm.googleapis.com/fcm/send';

        $api_key = 'AAAAKZLje1I:APbGQDw8FD...TjmtuINVB-g';

        $fields = array(
            'registration_ids' => array(
                $device_id,
            ),
            'data' => array(
                "message" => $message,
            ),
        );

        //header includes Content type and api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . $api_key,
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === false) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    public function add()
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Tokens');
        $this->loadModel('Vendors');
        $vendors = $this->Vendors->find('list')->toarray();
        $this->set(compact('vendors'));
        if ($this->request->is(['post'])) {
            $this->loadModel('Notifications');
            $walletadd = $this->Notifications->newEntity();
            $imagefilename = $this->request->data['image']['name'];
            $imagefiletype = $this->request->data['image']['type'];
            $item = $this->request->data['image']['tmp_name'];
            $ext = end(explode('.', $imagefilename));
            if ($ext != "JPG" || $ext != "jpg" || $ext != "jpeg" || $ext != "JPEG" || $ext != "PNG" || $ext != "png") {
                $this->Flash->error(__('Only JPEG,JPG,PNG file allowed'));
                return $this->redirect(['action' => 'add']);
            }
            $name = md5(time($filename));
            $imagename = $name . '.' . $ext;
            $this->request->data['image'] = $imagename;
            if (move_uploaded_file($item, "images/" . $imagename)) {
                $this->request->data['image'] = $imagename;
            }

            $walletsave = $this->Notifications->patchEntity($walletadd, $this->request->data);
            if ($this->Notifications->save($walletsave)) {
                if ($this->request->data['type'] != "offers") {
                    $tokens = $this->Tokens->find('list', ['keyField' => 'Tokens.id', 'valueField' => 'Tokens.token'])->toarray();
                    $this->push_notification_android($tokens, $this->request->data['message']);
                }
                $this->Flash->success(__('Notification has been save.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Notification not save'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function edit($id = null)
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Slider');
        $slider = $this->Slider->get($id);
        $this->set(compact('question'));

        $oldimage = $slider['image'];
//pr($question); die;
        if ($this->request->is(['post', 'put'])) {

            if (!empty($this->request->data['image']['name'])) {
                $filename = $this->request->data['image']['name'];
                $item = $this->request->data['image']['tmp_name'];
                $ext = end(explode('.', $filename));
                $name = md5(time($filename));
                $imagename = $name . '.' . $ext;

                unlink('imagess/' . $oldimage);
                if (move_uploaded_file($item, "images/" . $imagename)) {
                    $this->request->data['image'] = $imagename;
                } else {
                    $this->request->data['image'] = "";
                }
            } else {
                $this->request->data['image'] = $oldimage;
            }
            $slid = $this->Slider->patchEntity($slider, $this->request->data);

            if ($this->Slider->save($slid)) {
                $this->Flash->success(__('Slider has been updated Successfully.'));
                return $this->redirect(['action' => 'index']);
            }

        }
        $this->set('slider', $slider);
    }

    public function delete($id = null)
    {
        $this->loadModel('Slider');
        $sliderdata = $this->Slider->get($id);
        if ($sliderdata) {
            $this->Slider->delete($sliderdata);
            $this->Flash->success(__('Slider has been delete successfully.'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('Wallet money not  deleted'));
            return $this->redirect(['action' => 'index']);

        }
    }

    public function status($id, $status)
    {

        $this->loadModel('Notifications');
        if (isset($id) && !empty($id)) {

            $Notifications = $this->Notifications->get($id);
            $Notifications->status = $status;
            if ($this->Notifications->save($Notifications)) {
                $this->Flash->success(__('Notification status has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

}