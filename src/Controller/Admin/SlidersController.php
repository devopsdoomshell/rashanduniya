<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;

class SlidersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        $this->loadModel('Sliders');
        $this->loadModel('Categories');
        $this->loadModel('Products');
    }

    public function index()
    {
        $this->viewBuilder()->layout('admin');
        $user = $this->Auth->user();
        if ($user['role_id'] == 1) {
            $this->loadModel('Vendors');
            $vendors = $this->Vendors->find('list')->toarray();
            $vendors[0] = 'Admin';
            $this->set(compact('vendors'));
            $sliders = $this->Sliders->find()->order(['id' => 'DESC'])->toarray();
        } else {
            $vendorId = $this->vendorId();
            $sliders = $this->Sliders->find()->where(['vendor_id' => $vendorId])->order(['id' => 'DESC'])->toarray();
        }
        $this->set(compact('sliders', 'user'));
    }

    public function add()
    {
        $this->viewBuilder()->layout('admin');
        $categories=$this->Categories->ChildCategories->find('list')->where(['status'=>'Y'])->toarray();
		$products=$this->Products->find('list')->where(['status'=>'Y'])->toarray();
		$this->set(compact('categories','products')); 
        if ($this->request->is('post')) {
            extract($this->request->data);
            $user = $this->Auth->user();
            $newSlider = $this->Sliders->newEntity();
            if ($user['role_id'] == 1) {
                $newSlider['vendor_id'] = 0;
            } else {
                $vendorId = $this->vendorId();
                $newSlider['vendor_id'] = $vendorId;
            }
            $newSlider['name'] = $name;
            $newSlider['type'] = $type;
            $newSlider['is_click'] = $is_click;
            if ($is_click == 1) {
                $newSlider['action'] = $action;
                if($action=='category'){
                $newSlider['category_id'] = $category_id;
                }else if($action=='product'){
                $newSlider['product_id'] = $product_id;
                }
            }
            $file = $this->request->getData()['image']['name'];
            if (!empty($file)) {
                $filename = $this->request->getData()['image']['name'];
                $item = $this->request->getData()['image']['tmp_name'];
                $ext = end(explode('.', $filename));
                $names = md5(time($filename));
                $imagename = $names . '.' . $ext;
                $filePath = 'images/sliders/' . $imagename;
                if (move_uploaded_file($item, $filePath)) {
                    $newSlider['image'] = $imagename;
                } else {
                    $this->Flash->error(__('Error While uploading Image'));
                    return $this->redirect(['action' => 'add']);
                }
            }
            $this->Sliders->save($newSlider);
            $this->Flash->success(__('Slider Added successfully'));
            return $this->redirect(['action' => 'index']);
        }
    }
    public function status()
    {
        extract($this->request->data);
        $slider = $this->Sliders->find()->where(['id' => $id])->first();
        if ($status == 'Y') {
            $slider['status'] = 'Y';
            $data = 1;
        } else {
            $slider['status'] = 'N';
            $data = 2;
        }
        $this->Sliders->save($slider);
        echo $data;die;
    }

    public function edit($id)
    {
        $this->viewBuilder()->layout('admin');
        $slider = $this->Sliders->find()->where(['id' => $id])->first();
        $this->set(compact('slider'));
        $categories=$this->Categories->ChildCategories->find('list')->where(['status'=>'Y'])->toarray();
		$products=$this->Products->find('list')->where(['status'=>'Y'])->toarray();
		$this->set(compact('categories','products')); 
        if ($this->request->is(['put','post'])) {
            extract($this->request->data);
            if ($user['role_id'] == 1) {
                $slider['vendor_id'] = 0;
            } else {
                $vendorId = $this->vendorId();
                $slider['vendor_id'] = $vendorId;
            }
            $slider['name'] = $name;
            $slider['type'] = $type;
            $slider['is_click'] = $is_click;
            if ($is_click == 1) {
                $slider['action'] = $action;
                if($action=='category'){
                $slider['category_id'] = $category_id;
                }else if($action=='product'){
                $slider['product_id'] = $product_id;
                }
            }
            $file = $this->request->getData()['image']['name'];
            if (!empty($file)) {
                $filename = $this->request->getData()['image']['name'];
                $item = $this->request->getData()['image']['tmp_name'];
                $ext = end(explode('.', $filename));
                $names = md5(time($filename));
                $imagename = $names . '.' . $ext;
                $filePath = 'images/sliders/' . $imagename;
                if (move_uploaded_file($item, $filePath)) {
                    $slider['image'] = $imagename;
                } else {
                    $this->Flash->error(__('Error While uploading Image'));
                    return $this->redirect(['action' => 'add']);
                }
            }
            $this->Sliders->save($slider);
            $this->Flash->success(__('Slider edited successfully'));
            return $this->redirect(['action' => 'index']);
        }
    }
}
