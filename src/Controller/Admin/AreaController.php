<?php

namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

class AreaController extends AppController
{


	public function beforeFilter(Event $event)
	{    
		

		$this->Auth->allow(['veigieupload']); 
	}
  // for view or index page

	public function index(){ 
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Area');
		$areas = $this->Area->find('all')->order(['Area.sortnumber' => 'ASC']);	
	//pr($areas); die;
	//$this->set('product',$product);
		$this->set('areas', $this->paginate($areas)->toarray());
	}
	

	public function add()
	{
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Area');
		$areas = $this->Area->newEntity();
		if ($this->request->is(['post'])) 
		{
			//pr($this->request->data); die;
			$this->request->data['area']=ucfirst($this->request->data['area']);
			$area = $this->Area->patchEntity($areas, $this->request->data);
			if ($areasave=$this->Area->save($area)) {
				$this->Flash->success(__('Area  has been saved.'));
				return $this->redirect(['action' => 'index']);
			}
			else 
			{
				$this->Flash->error(__('Area not has been saved'));
				return $this->redirect(['action' => 'index']);
			}
		}
	}

	public function edit($id)
	{
		$this->loadModel('Products');
		$product = $this->Area->get($id);
		$this->set('product', $product);
		$this->viewBuilder()->layout('admin');	
		if ($this->request->is(['post', 'put'])) {
			$imagefilename=$this->request->data['image']['name'];
			$item=$this->request->data['image']['tmp_name'];
			$ext=  end(explode('.',$imagefilename));
			$name = md5(time($filename)); 
			$imagename=$name.'.'.$ext;     
			if(!empty($imagefilename)){

				$this->request->data['image']=$imagename;
				if(move_uploaded_file($item,"images/".$imagename)){                       
					$this->request->data['image']=$imagename;
				}
			}
			else{
				$this->request->data['image']=$product->image;
			}            
			$productsave = $this->Products->patchEntity($product, $this->request->data);
			if ($productedit=$this->Products->save($productsave)) {
				$this->Flash->success(__(''.ucwords($productedit['productnameen']).' has been updated Successfully.'));
				return $this->redirect(['action' => 'index']);	
			}
		}
	}

	
	public function delete($id=null)
	{
		$this->loadModel('Area');
		$prod_data = $this->Area->get($id);
		if($prod_data){
			$this->Area->delete($prod_data);
			$this->Flash->success(__('Area has been deleted successfully.'));
			return $this->redirect(['action' => 'index']);
		}else{
			$this->Flash->error(__('Area has not been deleted'));
			return $this->redirect(['action' => 'index']);

		}
	}


	public function sortingcheck()
	{
		$this->loadModel('Area');
		$sorting = $this->Area->find('all')->where(['Area.sortnumber' =>$this->request->data['sortno']])->toarray();
		$area = $this->Area->find('all')->where(['Area.area' =>$this->request->data['areaValue']])->toarray();

		if($sorting || $area){
			echo 1; die;

		}else{
			echo 0; die;
		}


	}


	public function status($id,$status){

		$this->loadModel('Area');
		if(isset($id) && !empty($id)){

			$product = $this->Area->get($id);
			$product->status = $status;
			if ($this->Area->save($product)) {
				$this->Flash->success(__('Area status has been updated.'));
				return $this->redirect(['action' => 'index']);  
			}



		}
	}

	public function search(){

		$this->loadModel('Products'); 
        //$this->loadModel('Orders'); 
		$req_data = $this->request->data['radioValue'];
       // pr($req_data); die;
		$cond = [];
		$session = $this->request->session(); 
		$session->delete('cond');       

		if(!empty($expiryf))
		{
			$cond['Products.ptype =']=$req_data;
		}
             //$session->write('cond',$cond); 
		$session = $this->request->session();
		$session->write('cond', $cond); 
             //pr($cond); die;
		$veggsearch = $this->Products->find('all')->where([$cond,'ptype'=>$req_data])->order(['Products.ptype' => 'ASC','Products.sortnumber' => 'ASC'])->toarray();
           //pr($users_data); die;
		$this->set('veggsearch', $veggsearch);
	}


}
