<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;

class CustomersController extends AppController
{

    public function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }

    public function index()
    {
        $this->loadModel('Cities');
        $this->loadModel('Users');
        $this->viewBuilder()->layout('admin');
        $cities = $this->Cities->find('list')->where(['status' => 1])->toarray();
        $this->set(compact('cities'));
        $customers = $this->Users->find()->contain(['UserAddresses'])->matching('UserAddresses', function ($q) {
            return $q->where(['UserAddresses.is_default' => 1, 'UserAddresses.status' => 1]);
        })->order(['Users.id' => 'DESC']);
        $this->set('customers', $this->paginate($customers)->toarray());
    }

    public function add($id = null)
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Users');
        $this->loadModel('Cities');
        $this->loadModel('UserAddresses');
        $cities = $this->Cities->find('list')->where(['status' => 1])->toarray();
        $this->set(compact('cities'));
        if ($id) {
            $customer = $this->Users->find()->contain(['UserAddresses' => function ($q) {
                return $q->where(['UserAddresses.is_default' => 1, 'UserAddresses.status' => 1])->limit(1);
            }])->where(['Users.id' => $id])->first();
            $customer['mobile_1'] = $customer['mobile'];
            $customer['flat_no'] = $customer['user_addresses'][0]['flat_no'];
            $customer['building_name'] = $customer['user_addresses'][0]['building_name'];
            $customer['address_1'] = $customer['user_addresses'][0]['address_1'];
            $customer['address_2'] = $customer['user_addresses'][0]['address_2'];
            $customer['city_id'] = array_search($customer['user_addresses'][0]['city'], $cities);
            $this->set('customer', $customer);
        }
        if ($this->request->is(['post', 'put'])) {
            try {
                $user = $this->Users->find()->where(['mobile' => $this->request->data['mobile_1']])->first();
                if ($user) {
                    $this->Flash->error(__('Mobile Number already Exist'));
                    return $this->redirect(['action' => 'index']);
                }
                $user = $this->Users->newEntity();
                $user['name'] = $this->request->data['name'];
                $user['email'] = $this->request->data['email'];
                $user['password'] = $this->_setPassword($this->request->data['mobile_1']);
                $user['confirm_pass'] = $this->request->data['mobile_1'];
                $user['mobile'] = $this->request->data['mobile_1'];
                $user['status'] = 'Y';
                $user['role_id'] = '4';
                if ($user = $this->Users->save($user)) {
                    $newCustAddress = $this->CustomerAddresses->newEntity();
                    $newCustAddress->user_id = $user->id;
                    $newCustAddress->flat_no = $this->request->data['flat_no'];
                    $newCustAddress->building_name = $this->request->data['building_name'];
                    $newCustAddress->address_1 = $this->request->data['address_1'];
                    $newCustAddress->address_2 = $this->request->data['address_2'];
                    $newCustAddress->city = $cities[$this->request->data['city_id']];
                    if ($this->CustomerAddresses->save($newCustAddress)) {
                        $this->Flash->success(__('Customer Details has been saved.'));
                        return $this->redirect(['action' => 'index']);
                    }
                }
                $this->Flash->error(__('Error While saving Customer Details'));
                return $this->redirect(['action' => 'index']);
            } catch (\PDOException $e) {
                pr($e);die;
            }
        }
    }
    public function edit($id)
    {
        $this->loadModel('Cities');
        $this->loadModel('Users');
        $this->loadModel('UserAddresses');
        $cities = $this->Cities->find('list')->where(['status' => 1])->toarray();
        $this->viewBuilder()->layout('admin');
        if ($this->request->is(['post', 'put'])) {
            try {
                $this->request->data['mobile'] = $this->request->data['mobile_1'];
                $customer = $this->Users->find()->where(['id' => $id])->first();
                $patch = $this->Users->patchEntity($customer, $this->request->data);
                if ($customer = $this->Users->save($patch)) {
                    $newCustAddress = $this->UserAddresses->find()->where(['user_id' => $id, 'is_default' => 1, 'status' => 1])->first();
                    $newCustAddress->customer_id = $id;
                    $newCustAddress->flat_no = $this->request->data['flat_no'];
                    $newCustAddress->building_name = $this->request->data['building_name'];
                    $newCustAddress->address_1 = $this->request->data['address_1'];
                    $newCustAddress->address_2 = $this->request->data['address_2'];
                    $newCustAddress->city = $cities[$this->request->data['city_id']];
                    if ($this->UserAddresses->save($newCustAddress)) {
                        $this->Flash->success(__('Customer Details has been updated.'));
                        return $this->redirect(['action' => 'index']);
                    }
                }
                $this->Flash->error(__('Error While saving Customer Detail'));
                return $this->redirect(['action' => 'index']);
            } catch (\PDOException $e) {
                pr($e);die;
            }
        }
    }

    public function delete($id = null)
    {
        $this->loadModel('Users');
        $customer = $this->Customers->find()->where(['id' => $id])->first();
        $this->Users->delete($customer);
        $this->Flash->success(__('Customer has been deleted successfully'));
        return $this->redirect(['action' => 'index']);

    }

    public function status($id, $status)
    {
        $this->loadModel('Users');
        $this->loadModel('Customers');
        if (isset($id) && !empty($id)) {
            $customer = $this->Customers->find()->where(['id' => $id])->first();
            $customer->status = $status;
            if ($this->Customers->save($customer)) {
                $this->Flash->success(__('Customer status has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }
    public function search()
    {
        if ($this->request->is('post')) {
            extract($this->request->data);
            $cond = [];
            if (!empty($name)) {
                $cond['name'] = $name;
            }
            if (!empty($mobile)) {
                $cond['OR'] = ['mobile_1' => $mobile, 'mobile_2' => $mobile];
            }
            if (!empty($city)) {
                $cond['city'] = $city;
            }
            $customers = $this->Customers->find()->contain(['CustomerAddresses' => function ($q) {
                return $q->where(['CustomerAddresses.is_default' => 1, 'CustomerAddresses.status' => 1])->limit(1);
            }])->where($cond)->first();
            $this->set('customers', $this->paginate($customers)->toarray());
        }
    }
    public function isAuthorized($user)
    {
        if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 101)) {
            return true;
        }

        return false;
    }

}
