<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;

class CouponsController extends AppController
{

    public function beforeFilter(Event $event)
    {

    }
    // for view or index page

    public function index()
    {
        $this->viewBuilder()->layout('admin');
        $user = $this->Auth->user();
        if ($user['role_id'] == 2) {
            $vendorId = $this->vendorId();
            $coupancode = $this->Coupons->find('all')->contain(['Categories', 'Products'])->where(['Coupons.vendor_id' => $vendorId])->order(['valid_to' => 'DESC']);
        } else {
            $coupancode = $this->Coupons->find('all')->contain(['Categories', 'Products'])->order(['valid_to' => 'DESC']);

        }
        $this->set('coupancode', $this->paginate($coupancode)->toarray());
    }

    public function add()
    {
        $user = $this->Auth->user();
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Categories');
        $this->loadModel('Products');
        $categories = $this->Categories->ParentCategories->find('list')->where(['status', 1])->order(['name' => 'DESC'])->toarray();
        $products = $this->Products->find('list')->where(['status', 1])->order(['productnameen' => 'DESC'])->toarray();
        $applicableTo = ['all' => 'All', 'category' => 'Categories', 'product' => 'Product', 'delivery_charge' => 'Delivery Charges'];

        $this->set(compact('categories', 'products', 'applicableTo'));

        if ($this->request->is(['post'])) {
            try {
                extract($this->request->data);
                $data = $this->Coupons->newEntity();
                if ($user['role_id'] == 2) {
                    $data['vendor_id'] = $this->vendorId();
                } else {
                    $data['vendor_id'] = 0;
                }
                $data['code'] = $code;
                $data['applicable_to'] = $applicable_to;
                $data['applicable_type'] = $applicable_type;
                $data['discount_type'] = $discount_type;
                $data['minimum_order_value'] = $minimum_order_value;
                $data['maximum_discount'] = $maximum_discount;
                $data['discount_rate'] = $discount_rate;
                $data['valid_from'] = date('Y-m-d', strtotime($valid_from));
                $data['valid_to'] = date('Y-m-d', strtotime($valid_to));
                $data['max_redeem_type'] = $max_redeem_type;
                $data['max_redeem_rate'] = $max_redeem_rate;
                $data['description'] = $description;
                $data['cashback_expiry_date'] = !empty($cashback_expiry_date) ? date('Y-m-d H:i:s', strtotime($cashback_expiry_date)) : null;
                if ($coupon = $this->Coupons->save($data)) {
                    if ($data['applicable_to'] == 'category') {
                        $categories = $this->Categories->find()->where(['id IN' => $categories])->toarray();
                        $this->Coupons->Categories->link($coupon, $categories);
                    } else if ($data['applicable_to'] == 'product') {
                        $products = $this->Products->find()->where(['id IN' => $products])->toarray();
                        $this->Coupons->Products->link($coupon, $products);
                    }
                    $this->Flash->success(__('Coupan-code  has been saved.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    pr($data);die;
                    $this->Flash->error(__('Coupan-code not save'));
                    return $this->redirect(['action' => 'index']);
                }
            } catch (\PDOException $e) {
                pr($e);die;
            }
        }
    }

    public function edit($id)
    {
        $user = $this->Auth->user();
        $this->loadModel('Categories');
        $this->loadModel('Products');  
        $editCoupon = $this->Coupons->find()->where(['id' => $id])->first();
        $applicableTo = ['all' => 'All', 'category' => 'Categories', 'product' => 'Product', 'delivery_charge' => 'Delivery Charges'];
        $categories = $this->Categories->ParentCategories->find('list')->where(['status', 1])->order(['name' => 'DESC'])->toarray();
        $products = $this->Products->find('list')->where(['status', 1])->order(['productnameen' => 'DESC'])->toarray();
        $selctedcategories = $this->Categories->find('list', ['keyField' => 'id', 'valueField' => 'id'])->matching('Coupons', function ($q) use ($editCoupon) {
            return $q->where(['Coupons.id' => $editCoupon['id']]);
        })->where(['Categories.status' => 'Y'])->toarray();
        $selctedproducts = $this->Products->find('list', ['keyField' => 'id', 'valueField' => 'id'])->matching('Coupons', function ($q) use ($editCoupon) {
            return $q->where(['Coupons.id' => $editCoupon['id']]);
        })->where(['Products.status' => 'Y'])->toarray();
        $this->set(compact('categories', 'products', 'applicableTo', 'selctedcategories', 'selctedproducts', 'id', 'editCoupon'));
        $this->viewBuilder()->layout('admin');
        if ($this->request->is(['post', 'put'])) {
            try {
                extract($this->request->data);
                $data = $this->Coupons->find()->where(['id' => $id])->first();
                if ($user['role_id'] == 2) {
                    $data['vendor_id'] = $this->vendorId();
                } else {
                    $data['vendor_id'] = 0;
                }
                $data['code'] = $code;
                $data['applicable_to'] = $applicable_to;
                $data['applicable_type'] = $applicable_type;
                $data['discount_type'] = $discount_type;
                $data['minimum_order_value'] = $minimum_order_value;
                $data['maximum_discount'] = $maximum_discount;
                $data['discount_rate'] = $discount_rate;
                $data['valid_from'] = date('Y-m-d', strtotime($valid_from));
                $data['valid_to'] = date('Y-m-d', strtotime($valid_to));
                $data['max_redeem_type'] = $max_redeem_type;
                $data['max_redeem_rate'] = $max_redeem_rate;
                $data['description'] = $description;
                $data['cashback_expiry_date'] = !empty($cashback_expiry_date) ? date('Y-m-d H:i:s', strtotime($cashback_expiry_date)) : null;
                if ($coupon = $this->Coupons->save($data)) {
                    $allCategories = $this->Categories->find()->toarray();
                    $allProducts = $this->Products->find()->toarray();
                    $this->Coupons->Categories->unlink($coupon, $allCategories);
                    $this->Coupons->Products->unlink($coupon, $allProducts);
                    if ($data['applicable_to'] == 'category') {
                        $categories = $this->Categories->find()->where(['id IN' => $categories])->toarray();
                        $this->Coupons->Categories->link($coupon, $categories);
                    } else if ($data['applicable_to'] == 'product') {
                        $products = $this->Products->find()->where(['id IN' => $products])->toarray();
                        $this->Coupons->Products->link($coupon, $products);
                    }
                    $this->Flash->success(__('Coupan-code  has been updated.'));
                    return $this->redirect(['action' => 'index']);
                } else {
                    pr($data);die;
                    $this->Flash->error(__('Coupan-code not save'));
                    return $this->redirect(['action' => 'index']);
                }
            } catch (\PDOException $e) {
                pr($e);die;
            }
        }
    }

    public function delete($id = null)
    {
        $this->loadModel('Coupancode');
        $prod_data = $this->Coupancode->get($id);
        if ($prod_data) {
            $this->Coupancode->delete($prod_data);
            $this->Flash->success(__('Coupancode has been deleted successfully.'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('Coupancode has not been deleted'));
            return $this->redirect(['action' => 'index']);

        }
    }

    public function status($id, $status)
    {

        if (isset($id) && !empty($id)) {

            $product = $this->Coupons->get($id);
            $product->status = $status;
            if ($this->Coupons->save($product)) {
                $this->Flash->success(__('Coupon status has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function search()
    {

        //$this->loadModel('Orders');
        $req_data = $this->request->data;
        $copancodes = $req_data['coupan_code'];
        $cond = [];
        $session = $this->request->session();
        $session->delete('cond');

        if (!empty($copancodes)) {
            $cond['Coupons.code LIKE'] = "%" . $copancodes . "%";
        }
        //$session->write('cond',$cond);
        $session = $this->request->session();
        $session->write('cond', $cond);
        $coupancode = $this->Coupons->find('all')->contain(['Categories', 'Products'])->where($cond)->toarray();
        //pr($coupancode); die;
        $this->set('coupancode', $coupancode);
    }

    public function isAuthorized($user)
    {
        if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 2)) {
            return true;
        }
        if (in_array($this->request->params['action'], ['edit', 'delete'])) {
            if ($user['role_id'] == 1) {
                return true;
            }
            $couponId = (int) $this->request->params['pass']['0'];
            $vendorId = $this->vendorId();
            if ($this->Coupons->isOwnedBy($couponId, $vendorId)) {
                return true;
            }
        }

        return false;
    }

}
