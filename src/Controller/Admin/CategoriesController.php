<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

class CategoriesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }
    public function index()
    {
        $this->viewBuilder()->layout('admin');

        $categories = $this->Categories->find()->order(['lft' => 'ASC']);
        $this->set('categories', $this->paginate($categories, ['limit' => '50'])->toarray());

        // $this->set('_serialize', ['categories']);
    }
// Add categories tree menu.
    public function add()
    {
        $this->loadModel('Seo');
        $this->viewBuilder()->layout('admin');
        $category = $this->Categories->newEntity();
        if ($this->request->is('post')) {
            //pr($this->request->data); die;
            $category = $this->Categories->patchEntity($category, $this->request->getData());
            if ($categoriessave = $this->Categories->save($category)) {
                $categories_last_id = $categoriessave['id'];
                // category image
                if ($this->request->data['cat_image']) {
                    $paththumb = 'categoryimage/thumb/';
                    $pathsmall = 'categoryimage/small/';
                    $category_image = $this->request->data['cat_image'];
                    $gallery_category_image = $this->move_images($category_image);
                    $this->category_images('cat_image', $gallery_category_image, $categories_last_id, 'thumb', array(900, 900), $paththumb);
                    $this->category_images('cat_image', $gallery_category_image, $categories_last_id, 'small', array(300, 300), $pathsmall);
                    $this->unlink_images($gallery_category_image);
                }
                //category icon
                if ($this->request->data['cat_icon']) {
                    $paththumb = 'categoryicon/thumb/';
                    $pathsmall = 'categoryicon/small/';
                    $category_icon = $this->request->data['cat_icon'];
                    $gallery_category_icon = $this->move_images($category_icon);
                    $this->category_images('cat_icon', $gallery_category_icon, $categories_last_id, 'thumb', array(900, 900), $paththumb);
                    $this->category_images('cat_icon', $gallery_category_icon, $categories_last_id, 'small', array(300, 300), $pathsmall);
                    $this->unlink_images($gallery_category_icon);
                }
                if ($this->request->data['seotitle'] || $this->request->data['seokeyword'] || $this->request->data['seodescripation']) {
                    $seo = $this->Seo->newEntity();
                    $seo_data['name'] = $this->request->data['name'];
                    $seo_data['location'] = SITE_URL . 'categories/' . $this->request->data['slug'];
                    $seo_data['title'] = $this->request->data['seotitle'];
                    $seo_data['keyword'] = $this->request->data['seokeyword'];
                    $seo_data['descripation'] = $this->request->data['seodescripation'];
                    $seo_data['type'] = 'categories';
                    $seo_data['data_id'] = $categories_last_id;
                    $seo = $this->Seo->patchEntity($seo, $seo_data);
                    $this->Seo->save($seo);
                }
                $this->Flash->success(__('The category has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The category could not be saved. Please, try again.'));
        }
        $parentCategories = $this->Categories->ParentCategories->find('list', ['limit' => 200]);
        $this->set(compact('category', 'parentCategories'));
    }

// Edit categories tree menu.
    public function edit($id = null)
    {

        $this->viewBuilder()->layout('admin');
        $this->loadModel('Seo');
        $category = $this->Categories->get($id);
        $seo = $this->Seo->find()->where(['Seo.data_id' => $id, 'Seo.type' => 'categories'])->first();
        if ($this->request->is(['patch', 'post', 'put'])) {

            //pr($this->request->data); die;
            if ($this->request->data['cat_image']['name'] == '') {
                $this->request->data['cat_image'] = $category['cat_image'];
            }
            if ($this->request->data['cat_icon']['name'] == '') {
                $this->request->data['cat_icon'] = $category['cat_icon'];
            }

            $category = $this->Categories->patchEntity($category, $this->request->data());
            if ($this->Categories->save($category)) {
                if ($seo) {
                    $seo_data = $this->Seo->get($seo['id']);
                    $seo_data->title = $this->request->data['seotitle'];
                    $seo_data->location = SITE_URL . 'categories/' . $this->request->data['slug'];
                    $seo_data->keyword = $this->request->data['seokeyword'];
                    $seo_data->descripation = $this->request->data['seodescripation'];
                    $this->Seo->save($seo_data);
                } else {
                    $seo = $this->Seo->newEntity();
                    $seo_data['name'] = $this->request->data['name'];
                    $seo_data['location'] = SITE_URL . 'categories/' . $this->request->data['slug'];
                    $seo_data['title'] = $this->request->data['seotitle'];
                    $seo_data['keyword'] = $this->request->data['seokeyword'];
                    $seo_data['descripation'] = $this->request->data['seodescripation'];
                    $seo_data['type'] = 'categories';
                    $seo_data['data_id'] = $id;
                    $seo = $this->Seo->patchEntity($seo, $seo_data);
                    $this->Seo->save($seo);
                }
                if ($this->request->data['cat_image']['name']) {
                    $paththumb = 'categoryimage/thumb/';
                    $pathsmall = 'categoryimage/small/';
                    $category_image = $this->request->data['cat_image'];
                    $gallery_category_image = $this->move_images($category_image);
                    $this->category_images('cat_image', $gallery_category_image, $id, 'thumb', array(900, 900), $paththumb);
                    $this->category_images('cat_image', $gallery_category_image, $id, 'small', array(300, 300), $pathsmall);
                    $this->unlink_images($gallery_category_image);
                }

                //category icon
                if ($this->request->data['cat_icon']['name']) {
                    $paththumb = 'categoryicon/thumb/';
                    $pathsmall = 'categoryicon/small/';
                    $category_icon = $this->request->data['cat_icon'];
                    $gallery_category_icon = $this->move_images($category_icon);
                    $this->category_images('cat_icon', $gallery_category_icon, $id, 'thumb', array(900, 900), $paththumb);
                    $this->category_images('cat_icon', $gallery_category_icon, $id, 'small', array(300, 300), $pathsmall);
                    $this->unlink_images($gallery_category_icon);
                }
                $this->Flash->success(__('The category has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The category could not be saved. Please, try again.'));
        }
        $parentCategories = $this->Categories->ParentCategories->find('list')->where(['id NOT IN' => $id])->order(['created' => 'desc'])->toarray();
        //  $parentCategories = $this->Categories->ParentCategories->find('list',[where] ['limit' => 200]);
        $this->set(compact('category', 'parentCategories', 'seo'));
    }

// Delete Category
    public function delete($id = null)
    {
        //$this->request->allowMethod(['post', 'delete']);
        $this->loadModel('Products');
        $product_category = $this->Products->find('all')->where(['Products.category_id' => $id])->order(['Products.created' => 'desc'])->toarray();
        //pr($product_category);
        $products_sub_category = $this->Products->find('all')->where(['Products.subcategory_id' => $id])->order(['Products.created' => 'desc'])->toarray();
        //echo $id; die;
        if ($product_category || $products_sub_category) {
            $this->Flash->error(__('Category could not be deleted. you have used category in product.'));
            return $this->redirect(['action' => 'index']);
        }

        $category = $this->Categories->get($id);
        if ($this->Categories->delete($category)) {
            $this->Flash->success(__('The category has been deleted.'));
        } else {
            $this->Flash->error(__('The category could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

// Change brand status
    public function status($id, $status)
    {

        $this->loadModel('Categories');
        if (isset($id) && !empty($id)) {

            $category = $this->Categories->get($id);
            $category->status = $status;
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('Category status has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function search()
    {
        $this->loadModel('Categories');

        $query = $this->request->getQueryParams();
        //pr($query); die;
        $category_name = trim($query['categorydata']);
        $cond = [];
        // $session = $this->request->session();
        // $session->delete('cond');       $this->request->getQueryParams['']
        if (!empty($category_name)) {
            $cond['Categories.name LIKE'] = '%' . $category_name . '%';
        }
        //$session = $this->request->session();
        //$this->request->session()->write('cond',$cond);
        if ($cond) {
            $categories = $this->Categories->find()->order(['lft' => 'ASC'])->where([$cond]);
            //$this->set(compact('categories'));
            //$this->set('_serialize', ['categories']);
            $this->set('categories', $this->paginate($categories, ['limit' => 50])->toarray());
        }

    }
    public function search_category()
    {
        $this->autoRender = false;
        $id = $this->request->data['id'];
        $Categories = $this->Categories->find('list', ['keyField' => 'id', 'valueField' => 'name'])->where(['parent_id' => $id])->toarray();
        echo json_encode($Categories);
        return;
    }
    public function isAuthorized($user)
    {
        // Admin can access every action
        if (in_array($this->request->params['action'], ['search_category'])) {
            if ($user['role_id'] == 2) {
                return true;
            }
        }
        if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 101)) {
            return true;
        }

        // Default deny
        return false;
    }

}
