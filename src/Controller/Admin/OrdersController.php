<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

class OrdersController extends AppController
{

    public function index()
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Vendors');
        $user = $this->Auth->user();
        $cond = [];
        $vendors = $this->Vendors->find('list')->order(['name' => 'ASC'])->toarray();
        extract($this->request->query);
        $orderSearch = [];
        if ($user['role_id'] == 1 && !empty($vendorId)) {
            $cond['Orders.vendor_id'] = $vendorId;
            $orderSearch['vendorId'] = $vendorId;
        } else if ($user['role_id'] == 2) {
            $vendorId = $this->vendorId();
            $cond['Orders.vendor_id'] = $vendorId;
        }
        if (!empty($date_from)) {
            $date_from = date('Y-m-d', strtotime($date_from));
            $cond['DATE(Orders.created) >='] = $date_from;
            $orderSearch['date_from'] = $date_from;
        }
        if (!empty($date_to)) {
            $date_to = date('Y-m-d', strtotime($date_to));
            $cond['DATE(Orders.created) <='] = $date_to;
            $orderSearch['date_to'] = $date_to;
        }
        if (!empty($mobile)) {
            $cond['Users.mobile'] = $mobile;
            $orderSearch['mobile'] = $mobile;
        }
        if (!empty($orderStatus)) {
            $cond['Orders.order_status'] = $orderStatus;
            $orderSearch['order_status'] = $orderStatus;
        }
        $orders = $this->Orders->find()->contain(['Users', 'UserAddresses', 'Vendors'])->where($cond)->order(['Orders.id' => 'DESC']);
        $this->set('orders', $this->paginate($orders, ['limit' => 50])->toarray());
        $this->set(compact('user', 'vendors', 'orderSearch'));

    }

    public function orderdetail($id)
    {
        $order = $this->Orders->find()->contain(['OrderDetails' => ['Products'], 'UserAddresses'])->where(['Orders.id' => $id])->first();
        $this->set(compact('order'));
    }
    public function isAuthorized($user)
    {

        if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 101 || $user['role_id'] == 2)) {
            return true;
        }

        return false;
    }

}
