<?php

namespace App\Controller\Admin;
use App\Controller\AppController;
use Cake\Core\Configure; 
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

class SliderController extends AppController
{


	public function beforeFilter(Event $event)
	{    
		
	}


	public function index(){ 
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Slider');
		$this->loadModel('Categories');
		$this->loadModel('Products'); 
		$slider = $this->Slider->find('all');	
		$this->set('slider', $this->paginate($slider)->toarray());
	}
	

	public function add()
	{
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Slider');
		$walletadd = $this->Slider->newEntity();
		if ($this->request->is(['post'])) 
		{
			//pr($this->request->data); die;
			$imagefilename=$this->request->data['image']['name'];
			$imagefiletype=$this->request->data['image']['type'];
			$item=$this->request->data['image']['tmp_name'];
			$ext=  end(explode('.',$imagefilename));
			$name = md5(time($filename)); 
			$imagename=$name.'.'.$ext; 
			$this->request->data['image']=$imagename;
			if(move_uploaded_file($item,"images/".$imagename)){                       
				$this->request->data['image']=$imagename;
			}

			$walletsave = $this->Slider->patchEntity($walletadd, $this->request->data);
			if ($this->Slider->save($walletsave)) {
				$this->Flash->success(__('Slider has been save.'));
				return $this->redirect(['action' => 'index']);
			}
			else 
			{
				$this->Flash->error(__('Slider not save'));
				return $this->redirect(['action' => 'index']);
			}
		}
	}


	public function edit($id=null){ 
		$this->viewBuilder()->layout('admin');
		$this->loadModel('Slider');
		$slider=$this->Slider->get($id);
		$this->set(compact('question'));

		$oldimage=$slider['image'];
//pr($question); die;
		if ($this->request->is(['post', 'put'])) {

			
			if(!empty($this->request->data['image']['name']))
			{
				$filename=$this->request->data['image']['name'];
				$item=$this->request->data['image']['tmp_name'];
				$ext=  end(explode('.',$filename));
				$name = md5(time($filename)); 
				$imagename=$name.'.'.$ext;	

				unlink('imagess/'.$oldimage);
				if(move_uploaded_file($item,"images/".$imagename))
				{						
					$this->request->data['image']=$imagename;
				}
				else
				{
					$this->request->data['image']="";
				}	
			}
			else
			{					
				$this->request->data['image']=$oldimage;
			}
			$slid = $this->Slider->patchEntity($slider, $this->request->data);

			if ($this->Slider->save($slid)) {
				$this->Flash->success(__('Slider has been updated Successfully.'));	
				return $this->redirect(['action' => 'index']);
			}
		
		}
		$this->set('slider', $slider);
	}

	
	public function delete($id=null)
	{
		$this->loadModel('Slider');
		$sliderdata = $this->Slider->get($id);
		if($sliderdata){
			$this->Slider->delete($sliderdata);
			$this->Flash->success(__('Slider has been delete successfully.'));
			return $this->redirect(['action' => 'index']);
		}else{
			$this->Flash->error(__('Wallet money not  deleted'));
			return $this->redirect(['action' => 'index']);

		}
	}

	public function status($id,$status){

		$this->loadModel('Slider');
		if(isset($id) && !empty($id)){

			$slider = $this->Slider->get($id);
			$slider->status = $status;
			if ($this->Slider->save($slider)) {
				$this->Flash->success(__('Slider status has been updated.'));
				return $this->redirect(['action' => 'index']);  
			}
		}
	}


}
