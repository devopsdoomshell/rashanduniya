<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

class DashboardController extends AppController
{
    //$this->loadcomponent('Session');
    public function initialize()
    {
        //load all models
        parent::initialize();
    }

    public function yearrevenue()
    {
        //$this->viewBuilder()->layout('admin');
        $this->loadModel('Orders');
        $selectedyear = $this->request->data['selectedyear'];

        $m = date('Y-m-d');

        $nmonth = date("m", strtotime($m));
        $year = date("Y", strtotime($m));
        $day = date("d", strtotime($m));
        $query = $this->Orders->find();
        $orders = $query->select(['resum' => $query->func()->sum('Orders.totalamount')])->where(['Orders.order_status' => 'DL', 'YEAR(Orders.created)' => $selectedyear])->first();

        $yearrev = $orders['resum'];
        echo json_encode($yearrev);

        die;

    }

    public function monthrevenue()
    {
        //$this->viewBuilder()->layout('admin');
        $this->loadModel('Orders');
        $selectedyear = $this->request->data['selectedyear'];
        $selectedmonth = $this->request->data['selectedmonth'];

        $m = date('Y-m-d');

        $nmonth = date("m", strtotime($m));
        $year = date("Y", strtotime($m));
        $day = date("d", strtotime($m));
        $query = $this->Orders->find();
        $orders = $query->select(['resum' => $query->func()->sum('Orders.totalamount')])->where(['Orders.order_status' => 'DL', 'YEAR(Orders.created)' => $selectedyear, 'Month(Orders.created)' => $selectedmonth])->first();

        $yearrev = $orders['resum'];
        echo json_encode($yearrev);

        die;

    }

    public function dayrevenue()
    {
        //$this->viewBuilder()->layout('admin');
        $this->loadModel('Orders');
        $selectedyear = $this->request->data['selectedyear'];
        $selectedmonth = $this->request->data['selectedmonth'];
        $selecteday = $this->request->data['selecteday'];

        $m = date('Y-m-d');

        $nmonth = date("m", strtotime($m));
        $year = date("Y", strtotime($m));
        $day = date("d", strtotime($m));
        $query = $this->Orders->find();
        $orders = $query->select(['resum' => $query->func()->sum('Orders.totalamount')])->where(['Orders.order_status' => 'DL', 'YEAR(Orders.created)' => $selectedyear, 'Month(Orders.created)' => $selectedmonth, 'Day(Orders.created)' => $selecteday])->first();

        $yearrev = $orders['resum'];
        echo json_encode($yearrev);

        die;

    }
    public function index()
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Users');
        $this->loadModel('Products');
        $this->loadModel('Orders');
        $this->loadModel('OrderDetails');
        $this->loadModel('Vendors');
        $id = $this->request->session()->read('Auth.User.id');
//Revenue record...
        $m = date('Y-m-d');

        $nmonth = date("m", strtotime($m));
        $year = date("Y", strtotime($m));
        $day = date("d", strtotime($m));

        $years = $this->Orders->find('all')->where(['Orders.order_status' => 'delivered'])->group(['YEAR(Orders.created)'])->toarray();
        $this->set('years', $years);
        $totalearing = $this->Orders->find('all')->where(['Orders.order_status' => 'delivered'])->order(['Orders.id' => 'DESC'])->toarray();

        //'Month(Event.startdate)'=>$nmonth,'YEAR(Event.startdate)'=>$year

        $yearlyearning = $this->Orders->find('all')->where(['Orders.order_status' => 'delivered', 'YEAR(Orders.created)' => $year])->order(['Orders.id' => 'DESC'])->toarray();
        //pr($yearlyearning);

        $monthlyearning = $this->Orders->find('all')->where(['Orders.order_status' => 'delivered', 'YEAR(Orders.created)' => $year, 'Month(Orders.created)' => $nmonth])->order(['Orders.id' => 'DESC'])->toarray();

        $dailyearning = $this->Orders->find('all')->where(['Orders.order_status' => 'delivered', 'YEAR(Orders.created)' => $year, 'Month(Orders.created)' => $nmonth, 'Day(Orders.created)' => $day])->order(['Orders.id' => 'DESC'])->toarray();
        //pr($monthlyearning);

//Revenue record...

        $retailercount = $this->Users->find('all')->where(['Users.id !=' => $id, 'Users.status' => 'Y'])->count();
        $productcount = $this->Products->find('all')->where(['Products.status' => 'Y'])->count();
        $orderscount = $this->Orders->find('all')->count();

        $userlatest = $this->Orders->find('all')->contain(['Users', 'Vendors'])->order(['Orders.id' => 'DESC'])->limit(5);

        $retailerlatest = $this->Vendors->find('all')->where(['Vendors.status' => 1])->order(['Vendors.id' => 'desc'])->limit(5);

        $latestproduct = $this->Products->find('all')->contain(['Categories','Vendors'])->order(['Products.id' => 'DESC'])->limit(5);

        $this->set(compact('retailercount', 'productcount', 'orderscount', 'userlatest', 'retailerlatest', 'latestproduct', 'totalearing', 'yearlyearning', 'monthlyearning', 'dailyearning'));
    }
    public function isAuthorized($user)
    {
        if ($this->request->param('action') === 'index' && (($user['role_id'] == 1 || $user['role_id'] == 101))) {
            return true;
        }
        return false;
        return parent::isAuthorized($user);
    }

}
