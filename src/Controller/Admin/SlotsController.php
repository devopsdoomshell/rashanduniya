<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

class SlotsController extends AppController
{

    public function index()
    {
        $this->viewBuilder()->layout('admin');
        $vendorId = $this->vendorId();
        $slots = $this->Slots->find()->where(['vendor_id' => $vendorId])->toarray();
        $slotTimings = [
            '07.00 AM - 10.00 AM',
            '10.00 AM - 01.00 PM',
            '01.00 PM - 04.00 PM',
            '04.00 PM - 07.00 PM',
        ];
        $weekdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
        foreach ($weekdays as $weekday) {
            $data = [];
            $vendorSlots = $this->Slots->find()->where(['vendor_id' => $vendorId, 'weekdays' => $weekday, 'status' => 'Y'])->toarray();
            foreach ($vendorSlots as $vendorSlot) {
                $slotData = date('h.i A', strtotime($vendorSlot['mintime'])) . ' - ' . date('h.i A', strtotime($vendorSlot['maxtime']));
                $data[] = $slotData;
            }
            $slotsInfo[$weekday] = empty($vendorSlots) ? null : $data;
        }
        $this->set(compact('slotTimings', 'slots', 'slotsInfo'));
    }

    public function add()
    {
        $slots = $this->request->data['slot'];
        $vendorId = $this->vendorId();
        $this->Slots->updateSlots($vendorId);
            foreach ($slots as $weekday => $slot) {
                if (!empty($slot)) {
                    foreach ($slot as $slotTime) {
                        $time = explode('-', $slotTime);
                        $data = $this->Slots->newEntity();
                        $data['vendor_id'] = $vendorId;
                        $data['weekdays'] = $weekday;
                        $data['mintime'] = date('H:i:s', strtotime($time[0]));
                        $data['maxtime'] = date('H:i:s', strtotime($time[1]));
                        $this->Slots->save($data);
                    }
                }
            }
            $this->Flash->success(__('Slots has been updated.'));
            return $this->redirect(['action' => 'index']);
        
        $this->Flash->error(__('Error while updating Slots'));
        return $this->redirect(['action' => 'index']);
    }

    public function isAuthorized($user)
    {
        if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 2)) {
            return true;
        }
        return false;
    }

}
