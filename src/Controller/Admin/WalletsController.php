<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;

include '../vendor/PHPExcel/Classes/PHPExcel.php';
include '../vendor/PHPExcel/Classes/PHPExcel/IOFactory.php';

class WalletsController extends AppController
{

    public function beforeFilter(Event $event)
    {

    }

    public function index()
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Wallet');
        //$this->loadModel('Users');
        $wallets = $this->Wallets->find('all')->order(['id' => DESC]);
        $this->set('wallets', $this->paginate($wallets)->toarray());
    }

    public function search()
    {
        extract($this->request->data);
        $today = date('Y-m-d');
        $total = "";
        $cond = [];
        if (!empty($customer_name)) {
            $cond['Users.name LIKE'] = '%' . $customer_name . '%';
        }
        if (!empty($customer_mobile)) {
            $cond['Users.mobile'] = $customer_mobile;
        }
        if (!empty($from_date)) {
            $dates = explode('-', $from_date);
            $from = date('Y-m-d', strtotime(str_replace('/', '-', $dates[0])));
            $to = date('Y-m-d', strtotime(str_replace('/', '-', $dates[1])));
        }
        if (empty($type)) {
            if (!empty($from_date)) {
                $cond['DATE(Wallets.created) >='] = $from;
                $cond['DATE(Wallets.created) <='] = $to;
            }
            $wallets = $this->Wallets->find()->contain(['Users'])->where($cond)->order(['Wallets.created' => 'DESC']);
        } else if ($type == 'promotional') {
            if (!empty($cashback_type)) {
                if ($cashback_type == 'expired') {
                    $cond['DATE(CashBacks.expiry_date) < '] = $today;
                } else {
                    $cond['DATE(CashBacks.expiry_date) >= '] = $today;
                }
            }
            if (!empty($from_date)) {
                $cond['DATE(CashBacks.created) >='] = $from;
                $cond['DATE(CashBacks.created) <='] = $to;
            }
            $wallets = $this->Wallets->CashBacks->find()->contain(['Users'])->where($cond)->order(['CashBacks.created' => 'DESC']);
            $walletsList = $this->Wallets->CashBacks->find('list', ['keyField' => 'id', 'valueField' => 'id'])->contain(['Users'])->where($cond)->order(['CashBacks.created' => 'DESC']);
            (INT) $cashbackUsed = $this->Wallets->find()->select(['sum' => 'SUM(amount)'])->contain(['Users'])->where(['wallet_id IN' => $walletsList])->first()->sum;
            (INT) $cashbackGiven = $this->Wallets->CashBacks->find()->select(['sum' => 'SUM(amount)'])->contain(['Users'])->where($cond)->order(['CashBacks.created' => 'DESC'])->first()->sum;
            $total = "<b>Total Cashback:</b>" . (INT) $cashbackGiven . " <b>Total Cashback Used:</b>" . (INT) $cashbackUsed;
        } else if ($type == 'cashback_used') {
            if (!empty($from_date)) {
                $cond['DATE(WalletExpenses.created) >='] = $from;
                $cond['DATE(WalletExpenses.created) <='] = $to;
            }
            $cond['wallet_id !='] = 0;
            $wallets = $this->Wallets->WalletExpenses->find()->contain(['Users'])->where($cond)->order(['WalletExpenses.created' => 'DESC']);
            (INT) $cashbackUsed = $this->Wallets->WalletExpenses->find()->select(['sum' => 'SUM(WalletExpenses.amount)'])->contain(['Users'])->where($cond)->first()->sum;
            $total = "<b>Total Cashback Used:</b>" . (INT) $cashbackUsed;
        } else if ($type == 'income') {
            if (!empty($from_date)) {
                $cond['DATE(WalletIncomes.created) >='] = $from;
                $cond['DATE(WalletIncomes.created) <='] = $to;
            }
            $cond['wallet_id'] = 0;
            $wallets = $this->Wallets->WalletIncomes->find()->contain(['Users'])->where($cond)->order(['WalletIncomes.created' => 'DESC']);
            (INT) $walletIncome = $this->Wallets->WalletIncomes->find()->select(['sum' => 'SUM(WalletIncomes.amount)'])->contain(['Users'])->where($cond)->first()->sum;
            $total = "<b>Wallet Amount:</b>" . (INT) $walletIncome;
        } else if ($type == 'expense') {
            if (!empty($from_date)) {
                $cond['DATE(WalletExpenses.created) >='] = $from;
                $cond['DATE(WalletExpenses.created) <='] = $to;
            }
            $cond['wallet_id'] = 0;
            $wallets = $this->Wallets->WalletExpenses->find()->contain(['Users'])->where($cond)->order(['WalletExpenses.created' => 'DESC']);
            (INT) $walletUsed = $this->Wallets->WalletExpenses->find()->select(['sum' => 'SUM(WalletExpenses.amount)'])->contain(['Users'])->where($cond)->first()->sum;
            $total = "<b>Wallet Amount Used:</b>" . (INT) $walletUsed;
        }
        $this->set('total', $total);
        $this->set('wallets', $this->paginate($wallets)->toarray());
    }

    /*public function downloadexcl(){
    $this->loadModel('Wallet');

    }*/

    public function downloadexcl()
    {
        $this->loadModel('Wallet');

        $output = "";
        $output .= 'Promo Name,';
        $output .= 'Retailer Mobile Number,';
        $output .= 'Wallet Money,';
        $output .= 'Effective Date (dd-mm-yyyy),';
        $output .= 'Expiry Date (dd-mm-yyyy),';

        $output .= "\n";

        $filename = "walletexcel.csv";
        header('Content-type: application/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        echo $output;
        die;
        $this->redirect($this->referer());
    }

    public function uploadexcl()
    {

        $this->loadModel('Wallet');
        $this->loadModel('Users');
        $datetme = date('Y-m-d h:i');
        $adminid = $this->Auth->user('id');
        if ($this->request->is(['post', 'put'])) {
            //pr($this->request->data); die;
            $csv = $this->request->data['uploadexcl'];
            $csv_array = $this->get_csv_data($csv['tmp_name']);
            //pr($csv_array); die;
            for ($i = 0; $i < count($csv_array); $i++) {

                $retlrname = $this->Users->find('all')->where(['mobile' => $csv_array[$i]['mnumber']])->first();
                //pr($retlrname); die;
                $walletadd = $this->Wallet->newEntity();

                $this->request->data['adminuser_id'] = $adminid;
                $this->request->data['retailername'] = $retlrname['name'];
                $this->request->data['boothno'] = $retlrname['boothno'];
                $this->request->data['user_id'] = $retlrname['id'];

                $this->request->data['name'] = $csv_array[$i]['name'];
                $this->request->data['walletamount'] = $csv_array[$i]['walletamount'];

                $this->request->data['expirydate'] = date("Y-m-d", strtotime($csv_array[$i]['expirydate']));
                $this->request->data['effectivedate'] = date("Y-m-d", strtotime($csv_array[$i]['effectivedate']));

                //pr($this->request->data); die;
                $walletsave = $this->Wallet->patchEntity($walletadd, $this->request->data);
                //pr($walletsave);
                $this->Wallet->save($walletsave);

            }
            $this->Flash->success(__('Products has been updated Successfully.'));
            return $this->redirect(['action' => 'index']);
        }

    }

    public function add()
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Wallet');
        $this->loadModel('Users');
        $adminid = $this->Auth->user('id');
        //pr($adminid); die;
        $walletadd = $this->Wallet->newEntity();
        if ($this->request->is(['post'])) {
            //pr($this->request->data); die;
            if ($this->request->data['expirydate'] == '') {
                $this->request->data['expirydate'] = date('Y-m-d');
            } else {
                $var = $this->request->data['expirydate'];
                $this->request->data['expirydate'] = date("Y-m-d", strtotime($var));

            }
            $varr = $this->request->data['effectivedate'];
            $this->request->data['effectivedate'] = date("Y-m-d", strtotime($varr));
            $this->request->data['adminuser_id'] = $adminid;

            $walletsave = $this->Wallet->patchEntity($walletadd, $this->request->data);

            if ($this->Wallet->save($walletsave)) {
                $this->Flash->success(__('Wallet money has been transferd .'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Coupan-code not save'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function edit($id)
    {
        $this->loadModel('Wallet');
        $walletadd = $this->Wallet->get($id);
        $this->set('walletadd', $walletadd);
        $this->viewBuilder()->layout('admin');
        if ($this->request->is(['post', 'put'])) {
            $var = $this->request->data['expirydate'];
            $this->request->data['expirydate'] = date("Y-m-d", strtotime($var));
            $walletsave = $this->Wallet->patchEntity($walletadd, $this->request->data);
            if ($this->Wallet->save($walletsave)) {
                $this->Flash->success(__('Wallet money has been updated Successfully.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function delete($id = null)
    {
        $this->loadModel('Wallet');
        $walletdata = $this->Wallet->get($id);

        $selectedTime = date('Y-m-d');
        $cutime = strtotime($selectedTime);

        $wallettime = date('Y-m-d', strtotime($walletdata['created']));
        $walletdate = strtotime($wallettime);

        $walletused = $walletdata['usedamount'];

        if ($cutime == $walletdate) {

            if ($walletused == '0') {

                $this->Wallet->delete($walletdata);
                $this->Flash->success(__('Wallet money has been deleted successfully.'));
                return $this->redirect(['action' => 'index']);

            } else {
                $this->Flash->error(__('Wallet cannot be delete, because wallet was used.'));
                return $this->redirect(['action' => 'index']);
            }

        } else {
            $this->Flash->error(__('Wallet does not deletable'));
            return $this->redirect(['action' => 'index']);

        }
    }

    public function status($id, $status)
    {

        $this->loadModel('Wallet');
        if (isset($id) && !empty($id)) {

            $product = $this->Wallet->get($id);
            $product->status = $status;
            if ($this->Wallet->save($product)) {
                $this->Flash->success(__('Wallet status has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function searchbooth($retlname = null)
    {

        $this->loadModel('Users');
        $this->loadModel('Wallet');
        //pr($this->request->data); die;
        $number = $this->request->data['relnum'];
        $retlrname = $this->Users->find('all')->select(['id', 'name', 'boothno'])->where(['mobile' => $number])->first();
        //pr($retlrname); die;
        $relirs = array();
        $relirs['boothno'] = $retlrname['boothno'];
        $relirs['names'] = $retlrname['name'];
        $relirs['userid'] = $retlrname['id'];

        echo json_encode($relirs);

        die;
    }

    // CSV Data
    public function get_csv_data($inputfilename)
    {
        //pr($inputfilename); die;
        if ($_POST) {
            //$inputfilename = $_FILES['file']['tmp_name'];
            try {
                $objPHPExcel = \PHPExcel_IOFactory::load($inputfilename);
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputfilename, PATHINFO_BASENAME) . '": ' . $e->getMessage());
            }
            $sheet = $objPHPExcel->getSheet(0);
            //pr($sheet); die;
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            $c = 0;

            $firstrow = 1;
            $firstsop = $sheet->rangeToArray('A' . $firstrow . ':' . $highestColumn . $firstrow, null, true, false);
            for ($row = 2; $row <= $highestRow; $row++) {
                $filesop = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, false);

                $colB = $objPHPExcel->getActiveSheet()->getCell('A' . $row)->getValue();

                if ($colB == null || $colB == '') {
                    $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $objPHPExcel->getActiveSheet()->getCell('A' . ($row - 1))->getValue());

                }
                //pr($filesop);
                $exceldata['name'] = $filesop[0][0];
                $exceldata['mnumber'] = $filesop[0][1];
                $exceldata['walletamount'] = $filesop[0][2];
                $exceldata['effectivedate'] = $filesop[0][3];
                $exceldata['expirydate'] = $filesop[0][4];
                $csv_data[] = $exceldata;
            }
            //die;
            // $this->Flash->success(__('Refer has been added successfully'));

            return $csv_data;
        } else {
        }
    }

}
