<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;

class VendorsController extends AppController
{
    public function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }

    public function beforeFilter(Event $event)
    {

    }

    public function index()
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Categories');
        $this->loadModel('Cities');
        $cities = $this->Cities->find('list', ['keyField' => 'id', 'valueField' => 'name'])->order(['name' => 'ASC'])->toarray();
        $categories = $this->Categories->ParentCategories->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toarray();
        $vendors = $this->Vendors->find('all')->order(['id' => 'DESC']);
        $this->set('vendors', $this->paginate($vendors)->toarray());
        $this->set(compact('categories', 'cities'));
    }

    public function add($id = null)
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Users');
        $this->loadModel('Categories');
        $this->loadModel('VendorCategories');
        $this->loadModel('Cities');
        $cities = $this->Cities->find('list', ['keyField' => 'id', 'valueField' => 'name'])->order(['name' => 'ASC'])->toarray();
        $categories = $this->Categories->ParentCategories->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toarray();
        $this->set(compact('categories', 'cities'));
        if ($id) {
            $vendor = $this->Vendors->find('all')->where(['id' => $id])->first();
            $categoriesSelected = $this->VendorCategories->find('list', ['keyField' => 'category_id', 'valueField' => 'category_id'])->where(['vendor_id' => $vendor['id']])->toarray();
            $this->set(compact('vendor', 'categoriesSelected'));
        }
        if ($this->request->is(['post', 'put'])) {
            $mailExist = $this->Users->exists(['email' => $this->request->data['email']]);
            if ($mailExist) {
                $this->Flash->error(__('Email already Exists'));
                return $this->redirect(['action' => 'add']);
            }
            if ($this->request->data['banner_images']['name']) {
                $category_image = $this->request->data['banner_images'];
                $filename = $category_image['name'];
                $ext = end(explode('.', $filename));
                $name = md5(time($filename));
                $rnd = mt_rand();
                $imagename = trim($name . $rnd . $i . '.' . $ext, " ");
                if (move_uploaded_file($category_image['tmp_name'], "vendors/" . $imagename)) {
                    $this->request->data['banner_images'] = $imagename;
                } else {
                    $this->request->data['banner_images'] = "";
                }
            }
            $this->request->data['status'] = 0;
            $VendorCategories = $this->Categories->find()->where(['id IN' => $this->request->data['categories']])->first();
            try {
                $user = $this->Users->newEntity();
                $user['name'] = $this->request->data['name'];
                $user['email'] = $this->request->data['email'];
                $user['password'] = $this->_setPassword($this->request->data['mobile_1']);
                $user['confirm_pass'] = $this->request->data['mobile_1'];
                $user['mobile'] = $this->request->data['mobile_1'];
                $user['status'] = 'N';
                $user['role_id'] = '2';
                $user = $this->Users->save($user);
                $this->request->data['user_id'] = $user->id;
                $newVendor = $this->Vendors->newEntity($this->request->data);
                $vendor = $this->Vendors->save($newVendor);
                foreach ($this->request->data['categories'] as $category) {
                    $newVendorCategory = $this->VendorCategories->newEntity();
                    $newVendorCategory->vendor_id = $vendor->id;
                    $newVendorCategory->category_id = $category;
                    $this->VendorCategories->save($newVendorCategory);
                }

            } catch (\PDOException $e) {
                pr($e);die;
            }
            $this->Flash->success(__('Vendor has been saved successfully'));
            return $this->redirect(['action' => 'index']);
        }

    }

    public function edit($id)
    {
        $this->loadModel('Categories');
        $this->loadModel('VendorCategories');
        if ($this->request->is(['post', 'put'])) {
            $vendor = $this->Vendors->find('all')->where(['id' => $id])->first();
            if ($this->request->data['banner_images']['name']) {
                $category_image = $this->request->data['banner_images'];
                $filename = $category_image['name'];
                $ext = end(explode('.', $filename));
                $name = md5(time($filename));
                $rnd = mt_rand();
                $imagename = trim($name . $rnd . $i . '.' . $ext, " ");
                if (move_uploaded_file($category_image['tmp_name'], "vendors/" . $imagename)) {
                    $this->request->data['banner_images'] = $imagename;
                    if (!empty($vendor->banner_images)) {
                        unlink("vendors/" . $vendor->banner_images);
                    }
                } else {
                    $this->request->data['banner_images'] = "";
                }
            }
            $this->request->data['status'] = 0;
            $VendorCategories = $this->Categories->find()->where(['id IN' => $this->request->data['categories']])->first();
            try {
                $newVendor = $this->Vendors->patchEntity($vendor, $this->request->data);
                $vendor = $this->Vendors->save($newVendor);
                if (!empty($this->request->data['categories'])) {
                    $this->VendorCategories->deleteAll(['vendor_id' => $id]);
                    foreach ($this->request->data['categories'] as $category) {
                        $newVendorCategory = $this->VendorCategories->newEntity();
                        $newVendorCategory->vendor_id = $vendor->id;
                        $newVendorCategory->category_id = $category;
                        $this->VendorCategories->save($newVendorCategory);
                    }
                }

            } catch (\PDOException $e) {
                pr($e);die;
            }
            $this->Flash->success(__('Vendor has been saved successfully'));
            return $this->redirect(['action' => 'index']);
        }
    }

    public function delete($id = null)
    {
        $this->loadModel('Users');
        $vendor = $this->Vendors->find()->where(['id' => $id])->first();
        $this->Users->delete($vendor);
        $this->Flash->success(__('vendor has been deleted successfully'));
        return $this->redirect(['action' => 'index']);

    }

    public function status($id, $status)
    {
        $this->loadModel('Users');
        $this->loadModel('VendorAreas');
        if (isset($id) && !empty($id)) {
            $vendorArea = $this->VendorAreas->find()->where(['vendor_id' => $id])->toarray();
            if (empty($vendorArea) && $status == 1) {
                $this->Flash->error(__('Please Assign area to vendor before activating'));
                return $this->redirect(['action' => 'index']);
            }
            $vendor = $this->Vendors->find()->where(['id' => $id])->first();
            $vendor->status = $status;
            if ($this->Vendors->save($vendor)) {
                $this->Flash->success(__('Vendor status has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }
    public function detail($id)
    {
        $this->viewBuilder()->layout('admin');
        $vendor = $this->Vendors->find()->contain(['VendorCategories' => ['Categories']])->where(['id' => $id])->first();
        $this->set(compact('vendor'));
    }
    public function search()
    {
        if ($this->request->is('post')) {
            extract($this->request->data);
            $cond = [];
            if (!empty($name)) {
                $cond['name'] = $name;
            }
            if (!empty($mobile)) {
                $cond['OR'] = ['mobile_1' => $mobile, 'mobile_2' => $mobile];
            }
            if (!empty($city)) {
                $cond['city'] = $city;
            }
            $vendors = $this->Vendors->find()->where($cond);
            $this->set('vendors', $this->paginate($vendors)->toarray());
        }
    }
    public function area($id = null)
    {
        $this->viewBuilder()->layout('admin');
        $this->set(compact('id'));
    }
    public function areaSelected($id)
    {
        $this->loadModel('VendorAreas');
        $services_area = $this->VendorAreas->find('all')->where(['vendor_id' => $id])->order(['VendorAreas.id' => 'ASC'])->toarray();
        $data = [];
        foreach ($services_area as $value) {
            $servicearea_detail['lat'] = $value['latitude'];
            $servicearea_detail['lng'] = $value['longitude'];
            $data[] = $servicearea_detail;
        }
        header('Content-Type: application/json');
        echo json_encode($data);
        exit();

    }
    public function areaAdd()
    {
        $this->loadModel('VendorAreas');
        if ($this->request->is(['post', 'put'])) {
            $this->VendorAreas->deleteAll(['vendor_id' => $this->request->data['id']]);
            $explode_data = explode("+", $this->request->data['latlong']);
            array_pop($explode_data);
            foreach ($explode_data as $value) {
                $user_reg = $this->VendorAreas->newEntity();
                $exp_value = explode(",", $value);
                $servicearea_data['vendor_id'] = $this->request->data['id'];
                $servicearea_data['latitude'] = $exp_value[0];
                $servicearea_data['longitude'] = $exp_value[1];
                $servicearea_add = $this->VendorAreas->patchEntity($user_reg, $servicearea_data);
                $results = $this->VendorAreas->save($servicearea_add);

            }

        }

        die;
    }

    public function getDistance()
    {
        $min_lat = $this->Servicearea->find()->select(['minlatitude' => 'MIN(Servicearea.latitude)'])->first();

        $max_lat = $this->Servicearea->find()->select(['maxlatitude' => 'MAX(Servicearea.latitude)'])->first();

        $min_long = $this->Servicearea->find()->select(['minlongitude' => 'MIN(Servicearea.longitude)'])->first();

        $max_long = $this->Servicearea->find()->select(['maxlongitude' => 'MAX(Servicearea.longitude)'])->first();

        $latitude1 = $min_lat['minlatitude'];
        echo $latitude1 . "<br>";
        $longitude1 = str_replace("-", "", $min_long['minlongitude']);

        echo $longitude1 . "<br>";
        $latitude2 = $max_lat['maxlatitude'];
        echo $latitude2 . "<br>";
        $longitude2 = str_replace("-", "", $max_long['maxlongitude']);

        echo $longitude2 . "<br>";
        $earth_radius = 6371;
        $user_latitude = '14.105444';
        $user_longitude = '87.204837';
        if ($latitude1 <= $user_latitude && $latitude2 >= $user_latitude) {
            $resultlat = 1;
        } else {
            $resultlat = 0;
        }
        echo $resultlat;
        if ($longitude1 <= $user_longitude && $longitude2 >= $user_longitude) {
            $resultlong = 1;
        } else {
            $resultlong = 0;
        }
        echo $resultlong;
        if ($resultlat == $resultlong) {
            echo '0';die;
        } else {
            echo '1';die;
        }

        $dLat = deg2rad($latitude2 - $latitude1);
        $dLon = deg2rad($longitude2 - $longitude1);

        $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon / 2) * sin($dLon / 2);
        $c = 2 * asin(sqrt($a));
        $distance = $earth_radius * $c;
        echo $distance;

    }

    public function remove()
    {
        $this->loadModel('VendorAreas');
        if ($this->request->is(['post', 'put'])) {
            $this->VendorAreas->deleteAll(['vendor_id' => $this->request->data['id']]);
        }

    }
    public function getDistancesecond()
    {
        $services_area_boundfirst = $this->Servicearea->find('all')->order(['Servicearea.id' => 'ASC'])->first();
        $latitude1 = '14.098228';
        $longitude1 = '-87.216343';
        $latitude2 = '14.298914';
        $longitude2 = '-87.379783';
        $earth_radius = 6371;

        $dLat = deg2rad($latitude2 - $latitude1);
        $dLon = deg2rad($longitude2 - $longitude1);

        $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon / 2) * sin($dLon / 2);
        $c = 2 * asin(sqrt($a));
        $distance = $earth_radius * $c;
        echo $distance;
        if ($distance <= 3.3843332410262) {
            echo "Within 100 kilometer radius";die;
        } else {
            echo "Outside 100 kilometer radius";die;
        }
    }
    public function isAuthorized($user)
    {
        if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 101)) {
            return true;
        }

        return false;
    }

}
