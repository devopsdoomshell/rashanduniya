<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

class DeliveryBoysController extends AppController
{
    public function index()
    {
        $this->viewBuilder()->layout('admin');
        $vendorId = $this->vendorId();
        $deliveryBoys = $this->DeliveryBoys->find()->where(['vendor_id' => $vendorId])->toarray();
        $this->set(compact('deliveryBoys'));
    }

    public function add($id = null)
    {
        $this->viewBuilder()->layout('admin');
        $vendorId = $this->vendorId();
        if ($id) {
            $deliveryBoys = $this->DeliveryBoys->find()->where(['vendor_id' => $vendorId, 'id' => $id])->first();
            $this->set(compact('deliveryBoys'));
        }
        if ($this->request->is('post', 'put')) {
            extract($this->request->data);
            $mobileExist = $this->DeliveryBoys->exists(['mobile' => $mobile, 'vendor_id' => $vendorId]);
            if ($mobileExist) {
                $this->Flash->error(__('Mobile Number already Exist'));
                return $this->redirect(['action' => 'index']);
            }
            $newUser=$this->Users->newEntity();
            $newUser['name']=$name;
            $newUser['mobile']=$mobile;
            $newUser['status']="Y";
            $newUser['registration_status']="Y";
            $newUser['role_id']="3";
            $newUser=$this->Users->save($newUser);
            $new = $this->DeliveryBoys->newEntity();
            if (!empty($image['name'])) {
                $filename = $image['name'];
                $ext = end(explode('.', $filename));
                $img_name = md5(time($filename));
                $rnd = mt_rand();
                $imagename = trim($img_name . $rnd . $i . '.' . $ext, " ");
                if (move_uploaded_file($image['tmp_name'], "delivery_boys/" . $imagename)) {
                    $new['image'] = $imagename;
                } else {
                    $this->Flash->error(__('Error while uploading image'));
                    return $this->redirect(['action' => 'index']);
                }
            }

            $new['user_id'] = $newUser->id;
            $new['vendor_id'] = $vendorId;
            $new['name'] = $name;
            $new['mobile'] = $mobile;
            $new['id_card_type'] = $id_card_type;
            $new['id_card_number'] = $id_card_number;
            if ($this->DeliveryBoys->save($new)) {
                $this->Flash->success(__('Delivery Boy has been saved successfully'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Error while adding DeliveryBoy'));
            return $this->redirect(['action' => 'index']);

        }
    }
    public function edit($id)
    {
        $this->autoRender = false;
        $vendorId = $this->vendorId();
        $deliveryBoys = $this->DeliveryBoys->find()->where(['vendor_id' => $vendorId, 'id' => $id])->first();
        if (!empty($deliveryBoys)) {
            extract($this->request->data);
            $deliveryBoys->name = $name;
            $deliveryBoys->mobile = $mobile;
            $deliveryBoys->id_card_type = $id_card_type;
            if (!empty($image['name'])) {
                $filename = $image['name'];
                $ext = end(explode('.', $filename));
                $img_name = md5(time($filename));
                $rnd = mt_rand();
                $imagename = trim($img_name . $rnd . $i . '.' . $ext, " ");
                if (move_uploaded_file($image['tmp_name'], "delivery_boys/" . $imagename)) {
                    unlink("delivery_boys/" . $deliveryBoys->image);
                    $deliveryBoys->image = $imagename;
                } else {
                    $this->Flash->error(__('Error while uploading image'));
                    return $this->redirect(['action' => 'index']);
                }
            }
            if ($this->DeliveryBoys->save($deliveryBoys)) {
                $this->Flash->success(__('Delivery Boy has been updated successfully'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Error while updating DeliveryBoy'));
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error(__('Error while updating DeliveryBoy'));
            return $this->redirect(['action' => 'index']);
        }
    }
    public function status($id, $status)
    {
        if (isset($id) && !empty($id)) {
            $customer = $this->DeliveryBoys->find()->where(['id' => $id])->first();
            $customer->status = $status;
            if ($this->DeliveryBoys->save($customer)) {
                $this->Flash->success(__('DeliveryBoys status has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }

    public function isAuthorized($user)
    {
        if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 2)) {
            return true;
        }
        return false;
    }
}
