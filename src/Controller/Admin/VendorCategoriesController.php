<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;

class VendorCategoriesController extends AppController
{

    public function beforeFilter(Event $event)
    {

    }

    public function index()
    {
        $user = $this->Auth->user();
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Categories');
        $categories = $this->Categories->ChildCategories->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toarray();
        if ($user->role_id == 3) {
            $vendorCategories = $this->VendorCategories->find()->contain(['Categories'])->where(['vendor_id' => $user->vendor_id])->order(['Categories.name' => 'DESC'])->toarray();
        } else {
            $vendorCategories = $this->VendorCategories->find()->contain(['Categories'])->order(['Categories.name' => 'DESC'])->toarray();
        }
        $this->set(compact('vendorCategories', 'categories'));
    }
    public function add_discount()
    {
        if ($this->request->is('post')) {
            extract($this->request->data);
            $vendor = $this->VendorCategories->find()->where(['id' => $id])->first();
            $vendor->discount_type = $discount_type;
            $vendor->discount = $discount_rate;
            $vendor->discount_from = date('Y-m-d', strtotime($discount_from));
            $vendor->discount_to = date('Y-m-d', strtotime($discount_to));
            if ($this->VendorCategories->save($vendor)) {
                $this->Flash->success(__('Vendor Category has been updated successfully'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('Vendor Category has been updated failed'));
                return $this->redirect(['action' => 'index']);
            }

        }
    }
    public function status($id, $status)
    {
        $this->loadModel('Users');
        $this->loadModel('VendorAreas');
        if (isset($id) && !empty($id)) {
            $vendor = $this->VendorCategories->find()->where(['id' => $id])->first();
            $vendor->status = $status;
            if ($this->VendorCategories->save($vendor)) {
                $this->Flash->success(__('Vendor Category status has been updated.'));
                return $this->redirect(['action' => 'index']);
            }
        }
    }
    public function search()
    {
        if ($this->request->is('post')) {
            extract($this->request->data);
            $vendors = $this->VendorCategories->find()->contain(['Categories'])->where(['category_id' => $category_id]);
            $this->set('vendors', $this->paginate($vendors)->toarray());
        }
    }

    public function isAuthorized($user)
    {
        // echo $this->request->params('pass.0'); die;
        if (isset($user['role_id']) && ($user['role_id'] == 1 || $user['role_id'] == 101 || $user['role_id'] == 3)) {
            return true;
        }

        return false;
    }

}
