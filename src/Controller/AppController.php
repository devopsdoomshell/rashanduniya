<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Routing\Router;

include ROOT . DS . "vendor" . DS . "watimage/" . DS . "Watimage.php";

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    public $paginate = ['limit' => 70];
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Csrf');
        $this->set('BASE_URL', Configure::read('BASE_URL'));
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email', 'password' => 'password'],
                ],
            ],
            'loginAction' => '/',

        ]);

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see http://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */

    public function beforeFilter(Event $event)
    {

        $this->Auth->allow(['sendmsg']);
    }

    public function beforeRender(Event $event)
    {

        if (isset($this->request->params['prefix']) && $this->request->params['prefix'] == "admin") {

        }

        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }

    }

    public function vendorId()
    {
        $user = $this->Auth->user();
        if ($user['role_id'] == 2) {
            $this->loadModel('Users');
            $vendorId = $this->Users->find()->contain(['vendor'])->where(['Users.id' => $user['id']])->first()->vendor->id;
            return $vendorId;
        }
    }

    public function accessdenied()
    {
        $this->redirect(Router::url($this->referer(), true));
    }

    public function boothFilter($id = null)
    {

        $this->loadModel('Users');

        $stsearch = $this->request->data['fetch'];
        $i = $this->request->data['i'];
        $usest = array('Users.boothno LIKE' => $stsearch . '%');
        $searchst = $this->Users->find('all', array('conditions' => $usest));
//pr($searchst); die;
        foreach ($searchst as $value) {
            echo '<li onclick="cllbcktest(' . "'" . $value['boothno'] . "'" . ',' . "'" . $value['id'] . "'" . ',' . "'" . $i . "'" . ')"><a href="#">' . $value['boothno'] . '</a></li>';
        }

        die;
    }

    public function retailFilter($id = null)
    {

        $this->loadModel('Users');

        $stsearch = $this->request->data['fetch'];
        $i = $this->request->data['i'];
        $usest = array('Users.name LIKE' => $stsearch . '%');
        $searchst = $this->Users->find('all', array('conditions' => $usest));
//pr($searchst); die;
        foreach ($searchst as $value) {
            echo '<li onclick="cllbckretail(' . "'" . $value['name'] . "'" . ',' . "'" . $value['id'] . "'" . ',' . "'" . $i . "'" . ')"><a href="#">' . $value['name'] . '</a></li>';
        }

        die;
    }

    public function retailmobileFilter($id = null)
    {

        $this->loadModel('Users');

        $stsearch = $this->request->data['fetch'];
        $i = $this->request->data['i'];
        $usest = array('Users.mobile LIKE' => $stsearch . '%');
        $searchst = $this->Users->find('all', array('conditions' => $usest));
//pr($searchst); die;
        foreach ($searchst as $value) {
            echo '<li onclick="cllbckretailnum(' . "'" . $value['mobile'] . "'" . ',' . "'" . $value['id'] . "'" . ',' . "'" . $i . "'" . ')"><a href="#">' . $value['mobile'] . '</a></li>';
        }

        die;
    }

    public function sendmsg($mobile, $msg)
    {
        $curl = curl_init();
        $mobil = "91" . $mobile;
        $msgs = $msg;
        $url = 'http://107.20.199.106/sms/1/text/query?username=veggie&password=Veggie302019&to=' . $mobil . '&text=' . urlencode($msgs);
//pr($url); die;
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return $err;
    }
    public function isAuthorized($user)
    {
        // Admin can access every action
        if (isset($user['role_id']) && ($user['role_id'] === 1 || $user['role_id'] === 101)) {
            return true;
        }
        if (isset($user['role_id']) && ($user['role_id'] == 2)) {
            if ($this->request->params['controller'] == 'Product') {
                return true;
            }
            if ($this->request->params['controller'] == 'Slots') {
                return true;
            }
        }

        // Default deny
        return false;
    }
    public function move_images($k = '')
    {
        // simgle image condition
        if (count($k['name']) == 1) {
            $filename = $k['name'];
            $ext = end(explode('.', $filename));
            $name = md5(time($filename));
            $rnd = mt_rand();
            $imagename = trim($name . $rnd . $i . '.' . $ext, " ");
            if (move_uploaded_file($k['tmp_name'], "trash_image/" . $imagename)) {
                $kk[] = $imagename;
            }
        } else {
            foreach ($k as $item) {
                $filename = $item['name'];
                $ext = end(explode('.', $filename));
                $name = md5(time($filename));
                $rnd = mt_rand();
                $imagename = trim($name . $rnd . $i . '.' . $ext, " ");
                if (move_uploaded_file($item['tmp_name'], "trash_image/" . $imagename)) {
                    $kk[] = $imagename;
                }
                $i++;
            }
        }
        return $kk;
    }

// product resize image
    public function upload_images($k = '', $product_last_id = '', $type = '', $sizeArray = '', $path)
    {
        //echo $product_last_id; die;
        $i = 1;foreach ($k as $item) {
            $wm = new \Watimage();
            $wm->setImage("trash_image/" . $item);
            $wm->setQuality(80);
            $wm->resize(array('type' => 'resize', 'size' => $sizeArray));
            if (!$wm->generate($path . $item)) {
                // handle errors...
                // print_r($wm->errors);
            }
            $this->loadModel('Productimages');
            $product_images = $this->Productimages->newEntity();
            $product_data['product_id'] = $product_last_id;
            $product_data['name'] = $item;
            $product_data['image_type'] = $type;
            $product_patch = $this->Productimages->patchEntity($product_images, $product_data);
            $this->Productimages->save($product_patch);
            $i++;
        }
    }

// Brand resize image
    public function brand_images($k = '', $product_last_id = '', $type = '', $sizeArray = '', $path)
    {
        $i = 1;foreach ($k as $item) {
            $wm = new \Watimage();
            $wm->setImage("trash_image/" . $item);
            $wm->setQuality(80);
            $wm->resize(array('type' => 'resize', 'size' => $sizeArray));
            if (!$wm->generate($path . $item)) {
                // handle errors...
                // print_r($wm->errors);
            }
            $this->loadModel('Brands');
            $brand = $this->Brands->get($product_last_id);
            $brand->image = $item;
            $this->Brands->save($brand);
            $i++;
        }
    }

// category resize  image
    public function category_images($fieldname, $k = '', $product_last_id = '', $type = '', $sizeArray = '', $path)
    {

        if ($fieldname == 'cat_image') {
            $i = 1;foreach ($k as $item) {
                $wm = new \Watimage();
                $wm->setImage("trash_image/" . $item);
                $wm->setQuality(80);
                $wm->resize(array('type' => 'resize', 'size' => $sizeArray));
                if (!$wm->generate($path . $item)) {
                    // handle errors...
                    print_r($wm->errors);die;
                }
                $this->loadModel('Categories');
                $category = $this->Categories->get($product_last_id);
                $category->cat_image = $item;
                $this->Categories->save($category);
                $i++;
            }
        }

        if ($fieldname == 'cat_icon') {
            $i = 1;foreach ($k as $item) {
                $wm = new \Watimage();
                $wm->setImage("trash_image/" . $item);
                $wm->setQuality(80);
                $wm->resize(array('type' => 'resize', 'size' => $sizeArray));
                if (!$wm->generate($path . $item)) {
                    // handle errors...
                    print_r($wm->errors);
                }

                $this->loadModel('Categories');
                $category = $this->Categories->get($product_last_id);
                $category->cat_icon = $item;
                $this->Categories->save($category);
                $i++;
            }
        }
    }

// unlink images
    public function unlink_images($k = '')
    {
        foreach ($k as $item) {
            unlink("trash_image/" . $item);
        }
    }

}
