<?php
namespace App\Controller\App;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class VendorsController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->getEventManager()->off($this->Csrf);
        $this->Auth->allow(['pendingOrders', 'getVendorId', 'viewSlots', 'addSlots', 'viewPendingOrderDetails', 'addDeliveryBoy', 'viewDeliveryBoys', 'assignList', 'viewSlotsByDate', 'assignOrder', 'updateDeliveryBoyStatus', 'paymentHistory', 'vendorLedger', 'completedOrders']);
        $this->loadModel('Users');
        $this->loadModel('Categories');
        $this->loadModel('Vendors');
        $this->loadModel('VendorAreas');
        $this->loadModel('Products');
        $this->loadModel('VendorCategories');
        $this->loadModel('Attributes');
        $this->loadModel('Carts');
        $this->loadModel('UserAddresses');
        $this->loadModel('ProductsAttributes');
        $this->loadModel('Payments');
        $this->loadModel('Wallets');
        $this->loadModel('Slots');
        $this->loadModel('Orders');
        $this->loadModel('OrderDetails');
        $this->loadModel('DeliveryBoys');
        $this->loadModel('Users');
        $this->loadModel('Vendorpayments');
    }
    public function getVendorId($userId)
    {
        return $this->Vendors->find()->where(['user_id' => $userId])->order(['id' => 'DESC'])->first()->id;
    }
    public function pendingOrders()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $vendorId = $this->getVendorId($this->request->data['vendorId']);
            $pendingOrders = $this->Orders->find()->where(['Orders.vendor_id' => $vendorId, 'AND' => [['Orders.order_status <>' => 'delivered'], ['Orders.order_status <>' => 'cancelled']],'Orders.payment_status'=>'approved'])->order(['to_deliver_date' => 'ASC', 'slot_id' => 'ASC'])->toarray();
            if (empty($pendingOrders)) {
                $response['success'] = false;
                $response['message'] = "No Pending Orders";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $slots = $this->Slots->find('list', ['keyField' => 'id', 'valueField' => 'slot_name'])->toarray();
            $vendor = $this->Vendors->find()->where(['id' => $vendorId])->first();
            $response['success'] = true;
            $response['pendingOrders'] = [];
            foreach ($pendingOrders as $pendingOrder) {
                $data = [];
                $slotData = [];
                $orderProduct = $this->OrderDetails->find()->contain(['Products'])->where(['order_id' => $pendingOrder['id']])->first();
                $data['orderId'] = $pendingOrder['id'];
                $data['vendorName'] = $vendor->name;
                $slotData['title'] = $slots[$pendingOrder['slot_id']];
                $data['placedDate'] = date('D d M Y,H.i A', strtotime($pendingOrder['created']));
                $data['deliveryDate'] = date('D, d M Y', strtotime($pendingOrder['to_deliver_date'])) . ' ' . $slotData['title'];
                $data['amount'] = $pendingOrder['total_amount'] - $pendingOrder['delivery_charge'];
                $data['orderStatus'] = $pendingOrder['order_status'];
                $data['deliveryCharge'] = $pendingOrder['delivery_charge'] == 0 ? 'Free' : $pendingOrder['delivery_charge'];
                $data['finalAmount'] = $pendingOrder['total_amount'];
                $data['paymentType'] = $pendingOrder['transaction_mode'];
                $data['image'] = !empty($orderProduct['product']['image']) ? 'http://dc84viddw6ggp.cloudfront.net/products/' . $orderProduct['product']['image'] : PRODUCT_DEFAULT_IMAGE;
                $data['assignedDetails'] = null;
                if (!empty($pendingOrder['delivery_boy_id'])) {
                    $deliveryBoys = $this->DeliveryBoys->find('list', ['keyField' => 'id', 'valueField' => 'name_mobile'])->where(['vendor_id' => $vendorId])->toarray();
                    $data['assignedDetails']['deliveryBoy'] = $deliveryBoys[$pendingOrder['delivery_boy_id']];
                    $data['assignedDetails']['assignedDate'] = date('d M Y', strtotime($pendingOrder['assign_date']));
                    $data['assignedDetails']['assignedSlot'] = $slots[$pendingOrder['assign_slot_id']];
                }
                $response['pendingOrders'][] = $data;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function viewPendingOrderDetails()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['orderId']) || empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->request->data['vendorId'] = $this->getVendorId($this->request->data['vendorId']);
            $order = $this->Orders->find()->contain(['OrderDetails'])->where(['id' => $this->request->data['orderId'], 'vendor_id' => $this->request->data['vendorId']])->first();
            if (!empty($order)) {
                $this->loadModel('PaymentTypes');
                $this->loadModel('Brands');
                $brands = $this->Brands->find('list')->toarray();
                $attributes = $this->Attributes->find('list')->toarray();
                $vendor = $this->Vendors->find()->contain(['Cities'])->where(['Vendors.id' => $order['vendor_id']])->first();
                $response['success'] = true;
                $response['vendorName'] = $vendor->name . ' - ' . $vendor['city']['name'];
                $response['placedDate'] = date('D d M Y,H:i A', strtotime($order['created']));
                if ($order['order_status'] == "delivered") {
                    $response['deliveryDate'] = date('D, d M Y H:i A', strtotime($order['delivery_date']));
                } else {
                    $response['deliveryDate'] = date('D, d M Y', strtotime($order['to_deliver_date'])) . ' ' . $slotresponse['title'];
                }
                $response['orderStatus'] = $order['order_status'];
                $response['MRP'] = $order['sub_total'];
                $response['productsDiscount'] = $order['product_discount'];
                $response['couponDiscount'] = $order['coupon_discount'];
                $response['deliveryCharge'] = $order['delivery_charge'] == 0 ? 'Free' : $order['delivery_charge'];
                $response['finalAmount'] = $order['total_amount'];
                $response['itemCount'] = null;
                $response['paymentType'] = $this->PaymentTypes->find()->where(['id' => $order['payment_type_id']])->first()->name;
                $address = $this->UserAddresses->find()->where(['UserAddresses.id' => $order['user_address_id']])->first();
                $response['deliveryAddress']['addressNickName'] = $address['address_nick_name'];
                $response['deliveryAddress']['name'] = $address['name'];
                $response['deliveryAddress']['address'] = $address['flat_no'] . ',' . $address['address_1'] . ',' . $address['city'];
                $response['orderDetails'] = null;
                $qty = 0;

                foreach ($order['order_details'] as $orderDetail) {
                    $data = [];
                    $product = $this->Products->find()->where(['id' => $orderDetail['product_id']])->first();
                    $qty += $orderDetail['quantity'];                    $data['productName'] = $product['productnameen'];
                    $data['productBrand'] = $brands[$product['brand_id']];
                    $data['productImage'] =  !empty( $product['image']) ? 'http://dc84viddw6ggp.cloudfront.net/products/' .$product['image'] : PRODUCT_DEFAULT_IMAGE;
                    $data['productQuantity'] = $orderDetail['quantity'];
                    $data['size'] = $attributes[$orderDetail['attribute_id']] . ' ' . $attributes[$orderDetail['attribute_type']];
                    $data['actualPrice'] = $orderDetail['actual_price'];
                    $data['discountPrice'] = ($orderDetail['actual_price'] - $orderDetail['price']) != 0 ? ($orderDetail['actual_price'] - $orderDetail['price']) : null;
                    $data['discountPercent'] = ($orderDetail['actual_price'] - $orderDetail['price']) == 0 ? null :
                    floor(($orderDetail['actual_price'] - $orderDetail['price']) * 100 / $orderDetail['actual_price']);
                    $response['orderDetails'][] = $data;
                }
                $response['itemCount'] = $qty;
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            } else {
                $response['success'] = false;
                $response['message'] = "Invalid Data Type";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function viewSlots()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->request->data['vendorId'] = $this->getVendorId($this->request->data['vendorId']);
            $vendorExist = $this->Vendors->exists(['id' => $this->request->data['vendorId']]);
            if (empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid VendorId";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = true;
            $response['slotTimings'] = [
                '07.00 AM - 10.00 AM',
                '10.00 AM - 01.00 PM',
                '01.00 PM - 04.00 PM',
                '04.00 PM - 07.00 PM',
            ];
            $weekdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
            $response['slotsInfo'] = null;
            foreach ($weekdays as $weekday) {
                $data = [];
                $vendorSlots = $this->Slots->find()->where(['vendor_id' => $this->request->data['vendorId'], 'weekdays' => $weekday, 'status' => 'Y'])->toarray();
                foreach ($vendorSlots as $vendorSlot) {
                    $slotData = date('h.i A', strtotime($vendorSlot['mintime'])) . ' - ' . date('h.i A', strtotime($vendorSlot['maxtime']));
                    $data[] = $slotData;
                }
                $response['slotsInfo'][$weekday] = empty($vendorSlots) ? null : $data;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function addSlots()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['vendorId']) || empty($this->request->data['slots'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->request->data['vendorId'] = $this->getVendorId($this->request->data['vendorId']);
            extract($this->request->data);
            $vendorExist = $this->Vendors->exists(['id' => $vendorId]);
            if (empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid VendorId";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->Slots->updateSlots($vendorId);
            $slots = json_decode($slots);
            foreach ($slots as $weekday => $slot) {
                if (!empty($slot)) {
                    foreach ($slot as $slotTime) {
                        $time = explode('-', $slotTime);
                        $data = $this->Slots->newEntity();
                        $data['vendor_id'] = $vendorId;
                        $data['weekdays'] = $weekday;
                        $data['mintime'] = date('H:i:s', strtotime($time[0]));
                        $data['maxtime'] = date('H:i:s', strtotime($time[1]));
                        $this->Slots->save($data);
                    }
                }
            }
            $response['success'] = true;
            $response['message'] = "Slot saved successfully";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function addDeliveryBoy()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['vendorId']) || empty($this->request->data['name']) || empty($this->request->data['mobile'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->request->data['vendorId'] = $this->getVendorId($this->request->data['vendorId']);
            extract($this->request->data);
            $vendorExist = $this->Vendors->exists(['id' => $vendorId]);
            if (empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid VendorId";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $newUser = $this->Users->find()->where(['mobile' => $mobile])->order(['id' => 'DESC'])->first();
            if (empty($newUser)) {
                $newUser = $this->Users->newEntity();
            }
            $newUser['mobile'] = $mobile;
            $newUser['role_id'] = 3;
            $user = $this->Users->save($newUser);
            $newDeliveryBoy = $this->DeliveryBoys->newEntity();
            $newDeliveryBoy['user_id'] = $user->id;
            $newDeliveryBoy['vendor_id'] = $vendorId;
            $newDeliveryBoy['name'] = $name;
            $newDeliveryBoy['mobile'] = $mobile;
            if ($this->DeliveryBoys->save($newDeliveryBoy)) {
                $response['success'] = true;
                $response['message'] = "DeliveryBoy Added Successfully";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = false;
            $response['message'] = "Error while adding";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function viewDeliveryBoys()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->request->data['vendorId'] = $this->getVendorId($this->request->data['vendorId']);
            extract($this->request->data);
            $vendorExist = $this->Vendors->exists(['id' => $vendorId]);
            if (!$vendorExist) {
                $response['success'] = false;
                $response['message'] = "Invalid VendorId";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $deliveryboys = $this->DeliveryBoys->find()->where(['vendor_id' => $vendorId])->order(['name' => 'ASC'])->toarray();
            if (empty($deliveryboys)) {
                $response['success'] = false;
                $response['message'] = "No DeliveryBoys To display";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = true;
            $response['deliveryBoysInfo'] = null;
            foreach ($deliveryboys as $deliveryboy) {
                $data = [];
                $data['id'] = $deliveryboy['id'];
                $data['name'] = $deliveryboy['name'];
                $data['mobile'] = $deliveryboy['mobile'];
                $data['status'] = $deliveryboy['status'] == 1 ? 'Active' : 'Inactive';
                $response['deliveryBoysInfo'][] = $data;

            }

            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function assignList()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['vendorId']) || empty($this->request->data['orderId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->request->data['vendorId'] = $this->getVendorId($this->request->data['vendorId']);
            extract($this->request->data);
            $vendorExist = $this->Vendors->exists(['id' => $vendorId]);
            if (empty($vendorExist)) {
                $response['success'] = false;
                $response['message'] = "Invalid VendorId";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $order = $this->Orders->find()->where(['id' => $orderId])->first();
            if (empty($order)) {
                $response['success'] = false;
                $response['message'] = "Invalid OrderId";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $slot = $this->Slots->find()->where(['id' => $order['slot_id']])->first();
            $response['success'] = true;
            $response['orderSelectedSlot'] = date('h A', strtotime($slot['mintime'])) . '-' . date('h A', strtotime($slot['maxtime']));
            $response['orderSelectedDeliveryDate'] = date('Y-m-d', strtotime($order['to_deliver_date']));
            $response['orderScheduleDates'] = [date('Y-m-d', strtotime($order['to_deliver_date'])), date('Y-m-d', strtotime($order['to_deliver_date'] . ' +1 days')), date('Y-m-d', strtotime($order['to_deliver_date'] . ' +2 days')), date('Y-m-d', strtotime($order['to_deliver_date'] . ' +3 days'))];
            $deliveryboys = $this->DeliveryBoys->find()->where(['vendor_id' => $vendorId, 'status' => 1])->order(['name' => 'ASC'])->toarray();
            if (empty($deliveryboys)) {
                $response['deliveryBoysList'] = null;
            }
            foreach ($deliveryboys as $deliveryboy) {
                $data = [];
                $data['id'] = $deliveryboy['id'];
                $data['name'] = $deliveryboy['name'] . '-' . $deliveryboy['mobile'];
                $response['deliveryBoysList'][] = $data;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }

    }
    public function viewSlotsByDate()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['date'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $day = date('D', strtotime($this->request->data['date']));
            $slots = $this->Slots->find()->where(['weekdays' => $day, 'status' => 'Y'])->toarray();
            if (empty($slots)) {
                $response['success'] = false;
                $response['slotList'] = null;
                $response['message'] = "No Slots Available";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = true;
            $response['slotList'] = null;
            foreach ($slots as $slot) {
                $slotData = [];
                $slotData['id'] = $slot['id'];
                $slotData['title'] = date('h A', strtotime($slot['mintime'])) . '-' . date('h A', strtotime($slot['maxtime']));
                $response['slotList'][] = $slotData;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function assignOrder()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['slotId']) || empty($this->request->data['date']) || empty($this->request->data['deliveryBoyId']) || empty($this->request->data['orderId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->request->data['vendorId'] = $this->getVendorId($this->request->data['vendorId']);
            extract($this->request->data);
            $order = $this->Orders->find()->where(['id' => $orderId])->first();
            if (empty($order)) {
                $response['success'] = false;
                $response['message'] = "Invalid OrderId";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $order['order_status'] = 'packed';
            $order['assign_date'] = date('y-m-d', strtotime($date));
            $order['delivery_boy_id'] = $deliveryBoyId;
            $order['assign_slot_id'] = $slotId;
            if ($this->Orders->save($order)) {
                $response['success'] = true;
                $response['message'] = "Delivery Boy assigned successfully";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function updateDeliveryBoyStatus()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['deliveryBoyId']) || empty($this->request->data['vendorId']) || empty($this->request->data['status']) || ($this->request->data['status'] != 'Active' && $this->request->data['status'] != 'Inactive')) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->request->data['vendorId'] = $this->getVendorId($this->request->data['vendorId']);
            extract($this->request->data);
            $deliveryBoy = $this->DeliveryBoys->find()->where(['id' => $deliveryBoyId, 'vendor_id' => $vendorId])->first();
            if (empty($deliveryBoy)) {
                $response['success'] = false;
                $response['message'] = "Invalid Id";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $deliveryBoy['status'] = $status == 'Active' ? 1 : 0;
            if ($this->DeliveryBoys->save($deliveryBoy)) {
                $response['success'] = true;
                $response['message'] = "Delivery Boy status updated successfully";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = false;
            $response['message'] = "Error while updating Status";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function paymentHistory()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->request->data['vendorId'] = $this->getVendorId($this->request->data['vendorId']);
            extract($this->request->data);
            $vendor = $this->Vendors->find('all')->where(['id' => $vendorId])->order(['id' => 'desc'])->first();
            if (empty($vendor)) {
                $response['success'] = false;
                $response['message'] = "Invalid Vendor ID";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response = [];
            $response['success'] = true;
            $conncc = TableRegistry::get('Orders');
            $query = $conncc->find();
            $suborder_total = $query->select(['Sum' => $query->func()->sum('Orders.total_amount')])->where(['Orders.vendor_id' => $vendorId, 'Orders.order_status' => 'delivered', 'transaction_mode' => 'online'])->first();
            $connccvendor = TableRegistry::get('Vendorpayments');
            $queryvendor = $connccvendor->find();
            $vendorpay_total = $queryvendor->select(['Sum' => $queryvendor->func()->sum('Vendorpayments.amount')])->where(['Vendorpayments.vendor_id' => $vendorId])->first();
            $vendorCommission = $this->Orders->find()->select(['Sum' => 'sum(Orders.commission)'])->where(['vendor_id' => $vendorId, 'Orders.order_status' => 'delivered'])->first();
            $final_balance = $suborder_total['Sum'] - $vendorpay_total['Sum'] - $vendorCommission['Sum'];
            $response['output']['outstanding'] = $final_balance;
            $cond['Vendorpayments.vendor_id'] = $vendorId;
            if (!empty($fromDate) && !empty($toDate)) {
                $cond['DATE(Vendorpayments.created) >='] = $fromDate;
                $cond['DATE(Vendorpayments.created) <='] = $toDate;
            } else {
                $cond['DATE(Vendorpayments.created) >='] = date('Y-m-d', strtotime('-30 days'));
                $cond['DATE(Vendorpayments.created) <='] = date('Y-m-d');
            }

            $vendor_payments = $this->Vendorpayments->find('all')->where([$cond])->contain(['Vendors'])->order(['Vendorpayments.id' => 'desc'])->toarray();
            if (empty($vendor_payments)) {
                $response['output']['vendorPayments'] = null;
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }

            foreach ($vendor_payments as $vendor_payment) {
                $data = [];
                $data['date'] = date('d-M-Y', strtotime($vendor_payment['created']));
                $data['amount'] = $vendor_payment['amount'];
                $data['description'] = $vendor_payment['descri'];
                $response['output']['vendorPayments'][] = $data;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function vendorLedger()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->request->data['vendorId'] = $this->getVendorId($this->request->data['vendorId']);
            extract($this->request->data);
            $cond = [];
            $payment = [];
            $vendorId = trim($this->request->data['vendorId']);
            $openingBalance = 0;

            $cond['Orders.vendor_id'] = $vendorId;
            $payment['Vendorpayments.vendor_id'] = $vendorId;
            if (empty($fromDate) && empty($toDate)) {
                $fromDate = date('Y-m-d', strtotime('-30 days'));
                $toDate = date('Y-m-d');
            } else {
                $fromDate = date('Y-m-d', strtotime($fromDate));
                $toDate = date('Y-m-d', strtotime($toDate));
            }
            $conncc = TableRegistry::get('Orders');
            $query = $conncc->find();
            $suborder_total = $query->select(['Sum' => $query->func()->sum('Orders.total_amount')])->where(['Orders.vendor_id' => $vendorId, 'Orders.order_status' => 'delivered', 'transaction_mode' => 'online', 'Orders.created <' => $fromDate])->first();
            $connccvendor = TableRegistry::get('Vendorpayments');
            $queryvendor = $connccvendor->find();
            $vendorpay_total = $queryvendor->select(['Sum' => $queryvendor->func()->sum('Vendorpayments.amount')])->where(['Vendorpayments.vendor_id' => $vendorId, 'Vendorpayments.created <' => $fromDate])->first();
            $vendorCommission = $this->Orders->find()->select(['Sum' => 'sum(Orders.commission)'])->where(['Orders.vendor_id' => $vendorId, 'Orders.order_status' => 'delivered', 'Orders.created <' => $fromDate])->first();
            $openingBalance = $suborder_total['Sum'] - $vendorpay_total['Sum'] - $vendorCommission['Sum'];
            $cond['DATE(Orders.created) >='] = $fromDate;
            $cond['DATE(Orders.created) <='] = $toDate;
            $payment['DATE(Vendorpayments.created) >='] = $fromDate;
            $payment['DATE(Vendorpayments.created) <='] = $toDate;
            $cond['Orders.order_status'] = 'delivered';
            $balance_total = 0;
            $totalOrder = 0;
            $totalPayment = 0;
            $vendor_order = $this->Orders->find('all')->contain(['Vendors'])->where([$cond])->toarray();
            foreach ($vendor_order as $ordervalue) {
                $ledger_vendor = [];
                $ledger_vendor['order_id'] = $ordervalue['order_id'];
                if ($ordervalue['transaction_mode'] != 'online') {
                    $ledger_vendor['Descripation'] = "Sales to goods(COD)";
                    $ledger_vendor['total'] = $ordervalue['total_amount'];
                    $ledger_vendor['comission'] = $ordervalue['commission'];
                    $ledger_vendor['credit'] = $ordervalue['total_amount'];
                    $ledger_vendor['debit'] = 0;
                    $ledger_vendor['balance'] = -($ordervalue['commission']);
                    $totalPayment += $ordervalue['total_amount'];
                } else {
                    $ledger_vendor['Descripation'] = "Sales to goods(Online Payment)";
                    $ledger_vendor['total'] = $ordervalue['total_amount'];
                    $ledger_vendor['comission'] = $ordervalue['commission'];
                    $ledger_vendor['debit'] = $ordervalue['total_amount'];
                    $ledger_vendor['credit'] = 0;
                    $ledger_vendor['balance'] = number_format(($ordervalue['total_amount'] - $ordervalue['commission']),2);
                }
                $totalOrder += $ordervalue['total_amount'];
                $ledger_vendor['created'] = date('d M Y', strtotime($ordervalue['created']));
                $ledger_data[] = $ledger_vendor;
            }
            $vendor_payment = $this->Vendorpayments->find('all')->contain(['Vendors'])->where([$payment])->toarray();

            foreach ($vendor_payment as $paymentvalue) {
                $ledger_payment['order_id'] = $paymentvalue['transactionnumber'];
                $ledger_payment['Descripation'] = $paymentvalue['descri'] . "(ADMIN)";
                $ledger_payment['total'] = null;
                $ledger_payment['comission'] = null;
                $ledger_payment['debit'] = 0;
                $ledger_payment['credit'] = $paymentvalue['amount'];
                $ledger_payment['created'] = date('d M Y', strtotime($paymentvalue['created']));
                $ledger_payment['balance'] = -$paymentvalue['amount'];
                $ledger_payment_data[] = $ledger_payment;
                $totalPayment += $paymentvalue['amount'];
            }
            if ($ledger_payment_data && $ledger_data) {
                $total_ledger = array_merge($ledger_data, $ledger_payment_data);
            } else if ($ledger_data) {
                $total_ledger = $ledger_data;
            } else if ($ledger_payment_data) {
                $total_ledger = $ledger_payment_data;
            }
            $totalBalance = $openingBalance;
            $response['success'] = true;
            $response['output']['openingBalance'] = number_format($openingBalance,2);
            $response['output']['totalOrder'] = $totalOrder;
            $response['output']['totalComission'] = null;
            $response['output']['totalPayment'] = $totalPayment;
            $response['output']['totalBalance'] = null;
            if (empty($total_ledger)) {
                $response['output']['totalBalance'] = $openingBalance;
                $response['output']['transactions'] = null;
                $response['output']['message'] = "No Transactions Found";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            function cmp($a, $b)
            {
                $t1 = $a['created'];
                $t2 = $b['created'];
                return strtotime($t1) - strtotime($t2);
            }
            usort($total_ledger, "cmp");
            $totalamount = 0;
            $totalcomission = 0;
            foreach ($total_ledger as $ledger) {
                $data = [];
                $data['date'] = $ledger['created'];
                $data['description'] = $ledger['Descripation'];
                $data['orderTotal'] = $ledger['total'];
                $data['commission'] = $ledger['comission'];
                $data['credit'] = $ledger['credit'] == 0 ? '-' : $ledger['credit'];
                $data['debit'] = $ledger['debit'] == 0 ? '-' : $ledger['debit'];
                $totalBalance += $ledger['balance'];
                $data['balance'] = number_format($totalBalance,2);
                $totalcomission += $ledger['comission'];
                $response['output']['transactions'][] = $data;
            }
            $response['output']['totalComission'] = $totalcomission;
            $response['output']['totalBalance'] = number_format($totalBalance,2);
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function completedOrders()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->request->data['vendorId'] = $this->getVendorId($this->request->data['vendorId']);
            $vendorId = $this->request->data['vendorId'];
            $pendingOrders = $this->Orders->find()->where(['Orders.vendor_id' => $vendorId, 'Orders.order_status' => 'delivered'])->order(['delivery_date' => 'DESC'])->toarray();
            if (empty($pendingOrders)) {
                $response['success'] = false;
                $response['message'] = "No Completed Orders";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $deliveryBoys = $this->DeliveryBoys->find('list', ['keyField' => 'id', 'valueField' => 'name_mobile'])->where(['vendor_id' => $vendorId])->toarray();
            $slots = $this->Slots->find('list')->toarray();
            $vendor = $this->Vendors->find()->where(['id' => $vendorId])->first();
            $response['success'] = true;
            $response['completedOrders'] = [];
            foreach ($pendingOrders as $pendingOrder) {
                $data = [];
                $slotData = [];
                $orderProduct = $this->OrderDetails->find()->contain(['Products'])->where(['order_id' => $pendingOrder['id']])->first();
                $slots = $this->Slots->find('list', ['keyField' => 'id', 'valueField' => 'slot_name'])->toarray();
                $data['orderId'] = $pendingOrder['id'];
                $data['vendorName'] = $vendor->name;
                $slotData['title'] = $slots[$pendingOrder['slot_id']];
                $data['placedDate'] = date('D d M Y,H.i A', strtotime($pendingOrder['created']));
                $data['deliveryDate'] = date('D, d M Y', strtotime($pendingOrder['to_deliver_date'])) . ' ' . $slotData['title'];
                $data['amount'] = $pendingOrder['total_amount'] - $pendingOrder['delivery_charge'];
                $data['orderStatus'] = $pendingOrder['order_status'];
                $data['deliveryCharge'] = $pendingOrder['delivery_charge'] == 0 ? 'Free' : $pendingOrder['delivery_charge'];
                $data['finalAmount'] = $pendingOrder['total_amount'];
                $data['image'] = !empty($orderProduct['product']['image']) ? 'http://dc84viddw6ggp.cloudfront.net/products/' . $orderProduct['product']['image'] : PRODUCT_DEFAULT_IMAGE;
                $data['deliveryBoy'] = $deliveryBoys[$pendingOrder['delivery_boy_id']];
                $data['deliveryDetails'] = null;
                if (!empty($pendingOrder['delivery_boy_id'])) {
                    $deliveryBoys = $this->DeliveryBoys->find('list', ['keyField' => 'id', 'valueField' => 'name_mobile'])->where(['vendor_id' => $vendorId])->toarray();
                    $data['deliveryDetails']['deliveryBoy'] = $deliveryBoys[$pendingOrder['delivery_boy_id']];
                    $data['deliveryDetails']['deliveryDate'] = date('d M Y H:i:s', strtotime($pendingOrder['delivery_date']));
                    $data['deliveryDetails']['deliveredTo'] = $pendingOrder['delivered_to_mobile'];
                    $data['deliveryDetails']['deliveredName'] = $pendingOrder['delivered_to_name'];
                    $data['deliveryDetails']['delivertComment'] = empty($pendingOrder['delivery_comment']) ? null : $pendingOrder['delivery_comment'];
                }
                $response['completedOrders'][] = $data;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
}