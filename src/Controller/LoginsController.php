<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;
use Cake\Network\Email\Email;

class LoginsController extends AppController
{
    public function beforeFilter(Event $event)
    {
        //pr($this->request->params);
        $this->loadModel('Users');

        parent::beforeFilter($event);
        $this->loadComponent('Cookie');
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['login', 'logout', 'forgot', 'signup', 'checkemail', 'frontlogin', 'forgotpassword', 'forgetcpass']);
    }

/*    public function frontlogin(){
$this->autoRender=false;

if ($this->request->is('post'))
{
$this->request->data['email']=$this->request->data['email'];
$user = $this->Auth->identify();
if ($user)
{
$this->Auth->setUser($user);
$uty=$this->request->session()->read('Auth.User.role_id');
if($uty=='2'){
return $this->redirect(['controller' => 'homes','action'=>'myevent']);
}
if($uty=='3'){
return $this->redirect(['controller' => 'tickets','action'=>'myticket']);
}
}
else
{
$this->Flash->error(__( ($multiplelang[19]['title']) ? $multiplelang[19]['title'] : "Invalid email or password, try again"));
return $this->redirect(['action'=>'signup']);
}

}
}
 */

    public function frontlogout()
    {
        $this->Auth->logout();
        return $this->redirect(['controller' => 'logins', 'action' => 'signup']);
    }

    public function login()
    {
        $this->viewBuilder()->layout('admin/login');
        if ($this->request->is('post')) {
            //($this->request->data);
            $user = $this->Auth->identify();

            if ($user['role_id'] == 1 || $user['role_id'] == 101) {
                if ($user) {
                    $this->Auth->setUser($user);
                    return $this->redirect(['controller' => 'admin/dashboard', 'action' => 'index']);
                    return $this->redirect($this->Auth->redirectUrl());
                }
            } elseif ($user['role_id'] == 2) {
                if ($user) {
                    $this->Auth->setUser($user);
                    return $this->redirect(['controller' => 'admin/product', 'action' => 'index']);
                    return $this->redirect($this->Auth->redirectUrl());
                }
            } elseif ($user['role_id'] == 3) {
                if ($user) {
                    $this->Auth->setUser($user);
                    return $this->redirect(['controller' => 'admin/stocks', 'action' => 'index']);
                    return $this->redirect($this->Auth->redirectUrl());
                }
            } elseif ($user['role_id'] == 4) {
                if ($user) {
                    $this->Auth->setUser($user);
                    return $this->redirect(['controller' => 'admin/orders', 'action' => 'index']);
                    return $this->redirect($this->Auth->redirectUrl());
                }
            }

            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }

    public function logout()
    {
        $this->Auth->logout();
        return $this->redirect(['controller' => 'logins', 'action' => 'login']);
    }

/*    public function signup(){
$this->loadModel('Users');
$internal = $this->Users->newEntity();
if ($this->request->is(['post', 'put'])) {
//pr($this->request->data);die;
$roleid=$this->request->data['role_id'];
//    $this->request->data['confirm_pass'] =$this->request->data['confirmpassword'];
$this->request->data['password'] = (new DefaultPasswordHasher)->hash($this->request->data['confirmpassword']);
$organiser = $this->Users->patchEntity($internal, $this->request->data);
$res=$this->Users->save($organiser);
if ($res) {
/*sending email start */
    /*$this->loadmodel('Templates');
    $profile = $this->Templates->find('all')->where(['Templates.id' =>ORGANISER])->first();

    $subject = $profile['subject'];
    $from= $profile['from'];
    $fromname = $profile['fromname'];
    $name = $res['name'];
    $email = $res['email'];
    $password = $this->request->data['confirmpassword'];
    $to  = $email;
    $formats=$profile['description'];
    $site_url=SITE_URL;
    $message1 = str_replace(array('{Name}','{Email}','{Password}','{site_url}','{Useractivation}'), array($name,$email,$password,$site_url), $formats);
    $message = stripslashes($message1);
    $message='
    <!DOCTYPE HTML>
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Mail</title>
    </head>
    <body style="padding:0px; margin:0px;font-family:Arial,Helvetica,sans-serif; font-size:13px;">
    '.$message1.'
    </body>
    </html>
    ';    //die;
    //    echo $message; die;
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    //$headers .= 'To: <'.$to.'>' . "\r\n";
    $headers .= 'From: '.$fromname.' <'.$from.'>' . "\r\n";
    $emailcheck= mail($to, $subject, $message, $headers);
    /*   sending email end */

    /*    $profess = $this->Users->find('all')->where(['id' =>$res['id']])->first();
    $this->Auth->setUser($profess);

    return $this->redirect(['controller' => 'homes','action'=>'myevent']);

    }
    }else{
    }

    } */

    public function checkemail()
    {
        $this->autoRender = false;
        $this->loadModel('Users');
        $name = $this->request->data['name'];
        $email = $this->request->data['email'];
        $phone = $this->request->data['mobile'];

        //pr($check_count); die;
        if (!empty($email)) {
            $check_count = $this->Users->find('all')->where(['Users.email LIKE ' => $email])->count();
            echo $check_count;die;
        }
        if (!empty($phone)) {
            $check_count = $this->Users->find('all')->where(['Users.mobile LIKE ' => $phone])->count();
            echo $check_count;die;
        }

    }

    public function checkemobile()
    {
        $this->autoRender = false;
        $this->loadModel('Users');
        $name = $this->request->data['name'];
        $phone = $this->request->data['mobile'];

        //pr($check_count); die;
        if (!empty($phone)) {
            $check_count = $this->Users->find('all')->where(['Users.mobile LIKE ' => $phone])->count();
            echo $check_count;die;
        }

    }

    public function forgotpassword()
    {
        $this->viewBuilder()->layout('admin');
        $this->loadModel('Users');
        if ($this->request->is('post')) {
//pr($this->request->data); die;
            $username = $this->request->data['username'];

            $useremail = $this->Users->find('all')->where(['Users.email' => $username])->first();
            $email = $useremail->email;
            //echo $email; die;
            $to = $email;
            if (count($useremail) == 0) {
                $this->Flash->error(__('Invalid email or password, try again'));
                return $this->redirect(['controller' => 'logins', 'action' => 'forgotpassword']);
            } else {
                $userid = $useremail['id'];
                $name = $useremail['name'];
                $site_url = SITE_URL;
                $fkey = rand(1, 10000);
                $Pack = $this->Users->get($userid);
                $Pack->fkey = $fkey;
                $this->Users->save($Pack);
                $mid = base64_encode(base64_encode($userid . '/' . $fkey));
                $url = SITE_URL . "logins/forgetcpass/" . $mid;

                $this->loadmodel('Templates');
                $profile = $this->Templates->find('all')->where(['Templates.id' => FORGOTPASSWORD])->first();
                $subject = $profile['subject'];

                $from = $profile['from'];
                $fromname = $profile['fromname'];
                $to = $email;
                $formats = $profile['description'];
                $siteurl = SITE_URL;
                $site_url = SITE_URL;
                $message1 = str_replace(array('{Name}', '{site_url}', '{url}'), array($name, $site_url, $url), $formats);
                $message = stripslashes($message1);
                $message = '
			<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<title>Mail</title>
			</head>
			<body style="padding:0px; margin:0px;font-family:Arial,Helvetica,sans-serif; font-size:13px;">
			' . $message1 . '
			</body>
			</html>
			';

                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                //$headers .= 'To: <'.$to.'>' . "\r\n";
                $headers .= 'From: ' . $fromname . ' <' . $from . '>' . "\r\n";

                $emailcheck = mail($to, $subject, $message, $headers);

                $this->Flash->success(__('Forgot passowrd link sent to Your Email'));
            }

        }
    }

    public function forgetcpass($mid = '', $userid = '')
    {
        $this->viewBuilder()->layout('admin');
        //pr($userid); die;
        if (!empty($userid)) {
            $id = $userid;
        } else {
            $mix = explode("/", base64_decode(base64_decode($mid)));
            //    pr($mix); die;
            $id = $mix[0];
            $fkey = $mix[1];
            $user = $this->Users->find('all')->select(['id', 'fkey'])->where(['Users.id' => $id])->first();
            $usrfkey = $user['fkey'];
            if ($usrfkey == $fkey) {
                $ftyp = 1;
            } else {
                $ftyp = 0;
            }
            $this->set('mid', $mid);
            $this->set('ftyp', $ftyp);
            $this->set('usrid', $id);
        }
        if ($this->request->is('post')) {

            if ($this->request->data['password'] == $this->request->data['cpassword']) {

                $Pack = $this->Users->get($id);

                $updpass = (new DefaultPasswordHasher)->hash($this->request->data['password']);

                $Pack->password = $updpass;
                $Pack->cpassword = $this->request->data['password'];
                $Pack->fkey = 0;
                if ($this->Users->save($Pack)) {

                    $useremail = $this->Users->find('all')->where(['Users.id' => $id])->first();

                    $email = $useremail['email'];
                    $name = $useremail['name'];
                    $password = $this->request->data['password'];
                    $this->loadmodel('Templates');
                    $profile = $this->Templates->find('all')->where(['Templates.id' => FORGOTPASSWORDCHANGED])->first();
                    $subject = $profile['subject'];
                    $from = $profile['from'];
                    $fromname = $profile['fromname'];
                    $to = $email;
                    $formats = $profile['description'];
                    $site_url = SITE_URL;
                    $siteurl = SITE_URL;
                    $message1 = str_replace(array('{Name}', '{Email}', '{Password}', '{site_url}'), array($name, $email, $password, $site_url), $formats);
                    $message = stripslashes($message1);
                    $message = '
				<!DOCTYPE HTML>
				<html>
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<title>Mail</title>
				</head>
				<body style="padding:0px; margin:0px;font-family:Arial,Helvetica,sans-serif; font-size:13px;">
				' . $message1 . '
				</body>
				</html>
				';

                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    //$headers .= 'To: <'.$to.'>' . "\r\n";
                    $headers .= 'From: ' . $fromname . ' <' . $from . '>' . "\r\n";
                    $emailcheck = mail($to, $subject, $message, $headers);
                    $this->Flash->success(__('Reset Password Sucessfully !!!!'));
                    $this->redirect(array('controller' => 'logins', 'action' => 'login'));

                }
            } else {
                $this->Flash->error(__('Your new password and confirm password does not match, try again.'));

                return $this->redirect(['action' => 'forgetcpass/']);
            }
        }
    }

}
