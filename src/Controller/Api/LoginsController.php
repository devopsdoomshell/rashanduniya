<?php

namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;

class LoginsController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->getEventManager()->off($this->Csrf);
        $this->loadModel('Users');
        $this->loadModel('Carts');
        $this->Auth->allow(['userLogin', 'userOtpVerify', 'uploadToken', 'resendOtp', 'resetNotificationCount', 'getNotificationCount', 'notificationList']);
    }
    public function file_get_contents_curl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    public function userLogin()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['mobile'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $userExist = $this->Users->find()->where(['mobile' => $this->request->data['mobile']])->first();
            // $otp = rand(1001, 9999);
            $otp = 1234;
            if ($userExist) {
                if ($userExist->status == 'N') {
                    $response['success'] = false;
                    $response['message'] = "Account Suspended, please contact Support team";
                    $this->response->type('application/json');
                    $this->response->body(json_encode($response));
                    return $this->response;
                }
                $userExist->otp = $otp;
                if ($this->Users->save($userExist)) {
                    // $mesg = "Your One Time Password for Ebatua is " . $otp;
                    // $new = $this->file_get_contents_curl('http://alerts.prioritysms.com/api/web2sms.php?workingkey=A2960bddf6f159a76d113973b6831bf79&to=' . trim($this->request->data['mobile']) . '&sender=DAACIN&message=' . urlencode($mesg));
                    $response['success'] = true;
                    $response['message'] = "Please verify OTP sent to your registered number";
                    $this->response->type('application/json');
                    $this->response->body(json_encode($response));
                    return $this->response;
                }
            } else {
                $newUser = $this->Users->newEntity();
                $newUser->referral_code = $this->random_strings(10);
                $newUser->mobile = $this->request->data['mobile'];
                $newUser->role_id = 4;
                $newUser->otp = $otp;
                if ($user = $this->Users->save($newUser)) {
                    // $mesg = "Your One Time Password for Ebatua is " . $otp;
                    // $new = $this->file_get_contents_curl('http://alerts.prioritysms.com/api/web2sms.php?workingkey=A2960bddf6f159a76d113973b6831bf79&to=' . trim($this->request->data['mobile']) . '&sender=DAACIN&message=' . urlencode($mesg));
                    $response['success'] = true;
                    $response['message'] = "Please verify OTP sent to your registered number";
                    $this->response->type('application/json');
                    $this->response->body(json_encode($response));
                    return $this->response;
                }

            }
            $response['success'] = false;
            $response['message'] = "Please Try after sometime";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = 0;
            $response['message'] = "Invalid Data Type";
            echo json_encode($response);
            return;
        }
    }
    public function userOtpVerify()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            if (empty($this->request->data['otp']) || empty($this->request->data['mobile'])) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $verifiedUser = $this->Users->find('all')->where(['mobile' => $this->request->data['mobile'], 'otp' => $this->request->data['otp']])->first();
            if (empty($verifiedUser)) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid OTP";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response = array();
            $response['success'] = true;
            if ($verifiedUser['role_id'] == 2) {
                $vendorId = $this->Users->find()->contain(['vendor'])->where(['Users.id' => $verifiedUser->id])->first()->vendor->id;
                $response['userInfo']['vendorId'] = $vendorId;
                $response['userInfo']['roleType'] = 'Vendor';
            }
            if ($verifiedUser['role_id'] == 4) {
                $response['userInfo']['userId'] = $verifiedUser->id;
                $response['userInfo']['roleType'] = 'Customer';
                $response['userInfo']['cartItemCount'] = $this->Carts->find()->select(['count' => 'sum(quantity)'])->where(['user_id' => $verifiedUser->id])->first()->count;
                $response['userInfo']['cartItemCount'] = $response['userInfo']['cartItemCount'] == null ? 0 : $response['userInfo']['cartItemCount'];
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        } else {
            $response = array();
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function resendOtp()
    {
        $this->autoRender = false;
        $this->loadModel('Users');
        if ($this->request->is('post')) {
            if (empty($this->request->data['mobile'])) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                echo json_encode($response);
                return;
            }
            $user = $this->Users->find()->where(['mobile' => $this->request->data['mobile']])->first();
            if (empty($user)) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid Number";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            // $mesg = "Your One Time Password for Ezypayroll is " . $user->otp;
            // $new = $this->file_get_contents_curl('http://alerts.prioritysms.com/api/web2sms.php?workingkey=A2960bddf6f159a76d113973b6831bf79&to=' . trim($user['mobile']) . '&sender=DAACIN&message=' . urlencode($mesg));
            $response = array();
            $response['success'] = true;
            $response['message'] = "OTP resent successfully";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        } else {
            $response = array();
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function uploadToken()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            if (empty($this->request->data['userId']) && empty($this->request->data['token'])) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                echo json_encode($response);
                return;
            }
            $this->loadmodel('Users');
            $userid = $this->request->data["userId"];
            $checkusername = $this->Users->find('all')->where(['Users.id' => $userid])->count();
            if ($checkusername > 0) {
                $user_reg = $this->Users->get($userid);
                $user_reg['app_token'] = $this->request->data['token'];
                $results = $this->Users->save($user_reg);
                $body['success'] = true;
                $body['message'] = "Token Generated Successfully";
            } else {
                $body['success'] = false;
                $body['message'] = "This user doesn't exists";
            }
        } else {
            $body['success'] = false;
            $body['message'] = "Invalid method";
        }

        $this->response->type('application/json');
        $this->response->body(json_encode($body));
        return $this->response;
    }
    public function random_strings($length_of_string)
    {

        // String of all alphanumeric character
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

        // Shufle the $str_result and returns substring
        // of specified length
        return substr(str_shuffle($str_result),
            0, $length_of_string);

    }
    public function resetNotificationCount()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            if (empty($this->request->data['userId'])) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->loadmodel('Users');
            $user_id = $this->request->data['userId'];
            $checkusername = $this->Users->find('all')->where(['Users.id' => $user_id])->count();

            if ($checkusername == 1) {
                $user_reg = $this->Users->get($user_id);
                $this->request->data['notification_counter'] = '0';
                $user_reg_add = $this->Users->patchEntity($user_reg, $this->request->data());
                $results = $this->Users->save($user_reg_add);
                $body['success'] = true;
                $body['message'] = "Notification count reseted successfully";
            } else {
                $body['success'] = false;
                $body['message'] = "This user doesn't exists";
            }
        } else {
            $body['success'] = false;
            $body['message'] = "Invalid method";
        }
        $this->response->type('application/json');
        $this->response->body(json_encode($body));
        return $this->response;
    }
    public function getNotificationCount()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            if (empty($this->request->data['userId'])) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->loadmodel('Users');

            $userid = $_POST["userId"];

            $result = $this->Users->find('all')->where(['Users.id' => $userid])->first();
            //pr($result); die;
            if (!empty($result)) {
                $response["success"] = true;
                $response["notificationCount"] = (int) $result['notification_counter'];
            } else {
                $response["success"] = false;
                $response["message"] = "This user doesn't exists";
            }
        } else {
            $response["success"] = false;
            $response["message"] = "Ivalid Method";
        }
        $this->response->type('application/json');
        $this->response->body(json_encode($response));
        return $this->response;
    }
    public function notificationList()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            if (empty($this->request->data['userId'])) {
                $response = array();
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                echo json_encode($response);
                return;
            }
            $this->loadModel('Notifications');
            $order = $this->Notifications->find('all')->where(['user_id' => $this->request->data['userId'], 'status' => 'Y'])->order(['created' => 'DESC'])->toarray();
            if ($order) {
                foreach ($order as $view) {
                    $carddata['title'] = $view['title'];
                    $carddata['message'] = $view['message'];
                    $carddata['date'] = date('d-M-Y', strtotime($view['created']));

                    $cardlist[] = $carddata;
                }
                $body['success'] = true;
                $body['output'] = $cardlist;
            } else {
                $body['success'] = false;
                $body['message'] = "No Notification";
            }
        } else {
            $body['success'] = false;
            $body['message'] = "Invalid method";
        }
        echo json_encode($body);
        return;
    }
}
