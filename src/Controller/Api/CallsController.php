<?php

namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Client;

class CallsController extends AppController
{
    public function beforeFilter(Event $event)
    {
        $this->loadModel('CustomerCalls');
        parent::beforeFilter($event);
        $this->Auth->allow(['success', 'failure']);
    }

    public function success()
    {
        $this->autoRender = false;
        extract($this->request->query);
        $explode = explode('-', $uniqueid);
        if ($explode[0] == 'A') {
			//pr($this->request->query); die;
            $data = [];
            $data['userId'] = $explode[1];
            $data['callDuration'] = $caller_duration;
            $http = new Client();
            $result = $http->post('https://new.astrotrishla.com/api/profile/callcompletion', $data);
           // pr($result->body); die;
        } else if ($explode[0] == 'W') {
            $data = [];  
            $data['expertId'] = $explode[1];
            $data['userId'] = $explode[2];
            $data['callDuration'] = $caller_duration;
            $http = new Client();
            $result = $http->post('https://thewisewords.in/api/Mobile/callCompletion', $data);
        }
        $newCall = $this->CustomerCalls->newEntity();
        $newCall['order_id'] = $uniqueid;

        $newCall['deliver_boy_mobile'] = $Agent;
        $newCall['customer_mobile'] = $custnumber;
        $newCall['call_date'] = $calldate;  
        $newCall['recording'] = $recording;
        $newCall['call_duration'] = $caller_duration;
        $newCall['agent_duration'] = $agent_duration;
        $newCall['status'] = 'success';
        if ($this->CustomerCalls->save($newCall)) {
            $response['success'] = true;
            $response['message'] = "record saved successfully";
            echo json_encode($response);
        } else {
            $response['success'] = false;
            $response['message'] = "Error while saving record";
            echo json_encode($response);
        }
    }

    public function failure()
    {
        $this->autoRender = false;
        extract($this->request->query);
        $newCall = $this->CustomerCalls->newEntity();
        $newCall['order_id'] = $uniqueid;
        $newCall['deliver_boy_mobile'] = $agent;
        $newCall['customer_mobile'] = $custnumber;
        $newCall['message'] = $calldate;
        $newCall['status'] = 'fail';
        if ($this->CustomerCalls->save($newCall)) {
            $response['success'] = true;
            $response['message'] = "record saved successfully";
            echo json_encode($response);
        } else {
            $response['success'] = false;
            $response['message'] = "Error while saving record";
            echo json_encode($response);
        }
    }

}
