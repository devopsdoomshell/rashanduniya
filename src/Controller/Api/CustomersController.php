<?php

namespace App\Controller\Api;

use App\Controller\AppController;
use Cake\Event\Event;
use Razorpay\Api\Api;

// include "../vendor/razorpay/vendor/autoload.php";

class CustomersController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->getEventManager()->off($this->Csrf);
        $this->Auth->allow(['getVendorId', 'getDistance', 'categories', 'categoryProducts', 'addToCart', 'cartDetails', 'cartItems', 'myAddress', 'addAddress', 'deleteAddress', 'addWalletAmount', 'myWallet', 'productDetail', 'checkout', 'paymentOptions', 'placeOrder', 'viewOrders', 'viewOrderDetails', 'couponList', 'couponAmount', 'applyCoupon', 'onlinePaymentVerification', 'walletOptionsList', 'viewWalletTransaction', 'home', 'searchProduct', 'searchHistory', 'referralCode', 'cancelOrder']);
        $this->loadModel('Users');
        $this->loadModel('Categories');
        $this->loadModel('Vendors');
        $this->loadModel('VendorAreas');
        $this->loadModel('Products');
        $this->loadModel('VendorCategories');
        $this->loadModel('Attributes');
        $this->loadModel('Carts');
        $this->loadModel('UserAddresses');
        $this->loadModel('ProductsAttributes');
        $this->loadModel('Payments');
        $this->loadModel('Wallets');
        $this->loadModel('Slots');
        $this->loadModel('Orders');
        $this->loadModel('Coupons');
        $this->loadModel('Payments');
        $this->loadModel('OrderDetails');
        $this->loadModel('Brands');
        $this->loadModel('Sliders');

    }

    public function getDistance($user_lat, $user_long)
    {

        $latitude1 = $min_lat['minlatitude'];
        //echo $latitude1;
        $latitude2 = $max_lat['maxlatitude'];

        $longitude1 = str_replace("-", "", $min_long['minlongitude']);
        $longitude2 = str_replace("-", "", $max_long['maxlongitude']);
        $earth_radius = 6371;

        $user_latitude = $user_lat;
        $user_longitude = str_replace("-", "", $user_long);
        if ($latitude1 <= $user_latitude && $latitude2 >= $user_latitude) {
            $resultlat = 1;
        } else {
            $resultlat = 0;
        }
        // echo $resultlat;
        if ($longitude1 <= $user_longitude && $longitude2 >= $user_longitude) {
            $resultlong = 1;
        } else {
            $resultlong = 0;
        }
        if ($resultlat == '1' && $resultlong == '1') {
            return 0;
        } else {
            return 1;
        }

    }
    public function getVendorId(Type $var = null)
    {
        $this->autoRender = false;
        //to be removed
        $body = [];
        $body['success'] = true;
        $body['vendorId'] = 2;
        $this->response->type('application/json');
        $this->response->body(json_encode($body));
        return $this->response;
        //to be removed
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['lat']) || empty($this->request->data['long'])) {
                // $response['success'] = false;
                // $response['message'] = "Invalid Parameters";
                // $this->response->type('application/json');
                // $this->response->body(json_encode($response));
                // return $this->response;
                $body['success'] = true;
            $body['vendorId'] = 2;
            $this->response->type('application/json');
            $this->response->body(json_encode($body));
            return $this->response;
            }
            require '../vendor/geometry-library/PolyUtil.php';
            $vendorId = "";
            $vendors = $this->Vendors->find()->contain(['VendorAreas'])->where(['Vendors.status' => 1])->toarray();
            foreach ($vendors as $vendor) {
                if (empty($vendor['vendor_areas'])) {
                    continue;
                }
                $serviceArea = [];
                foreach ($vendor['vendor_areas'] as $vendorArea) {
                    $data = [];
                    $data['lat'] = $vendorArea['latitude'];
                    $data['lng'] = $vendorArea['longitude'];
                    $serviceArea[] = $data;
                }
                $response = \GeometryLibrary\PolyUtil::containsLocation(
                    ['lat' => $this->request->data['lat'], 'lng' => $this->request->data['long']], $serviceArea);
                if ($response == true) {
                    $vendorId = $vendor['id'];
                    break;
                }
            }
            if (empty($vendorId)) {
                $response['success'] = false;
                $response['message'] = "No service at your location";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $body = [];
            $body['success'] = true;
            $body['vendorId'] = $vendorId;
            $this->response->type('application/json');
            $this->response->body(json_encode($body));
            return $this->response;

        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function addressVerify($vendorId, $addressId)
    {
        return true;

        require '../vendor/geometry-library/PolyUtil.php';
        $vendor = $this->Vendors->find()->contain(['VendorAreas'])->where(['id' => $vendorId, 'Vendors.status' => 1])->first();
        $address = $this->UserAddresses->find()->where(['id' => $addressId])->first();

        if (empty($vendor) || empty($address)) {
            return false;
        }
        $serviceArea = [];
        foreach ($vendor['vendor_areas'] as $vendorArea) {
            $data = [];
            $data['lat'] = $vendorArea['latitude'];
            $data['lng'] = $vendorArea['longitude'];
            $serviceArea[] = $data;
        }

        $response = \GeometryLibrary\PolyUtil::containsLocation(
            ['lat' => $address['latitude'], 'lng' => $address['longitude']], $serviceArea);
        if ($response == true) {
            return true;
        }
        return false;
    }
    public function categories(Type $var = null)
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $vendorCategories = $this->VendorCategories->find('list', ['keyField' => 'id', 'valueField' => 'category_id'])->where(['vendor_id' => $this->request->data['vendorId']])->toarray();
            if (empty($vendorCategories)) {
                $response['success'] = false;
                $response['message'] = "No Categories to show";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $categories = $this->Categories->ParentCategories->find()->contain(['ChildCategories' => function ($q) {
                return $q->where(['ChildCategories.status' => 'Y']);
            }])->where(['ParentCategories.status' => 'Y', 'ParentCategories.id IN' => $vendorCategories])->toarray();
            $response = [];
            if (empty($categories)) {
                $response['success'] = false;
                $response['message'] = "No Categories to show";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = true;
            foreach ($categories as $category) {
                $data = [];
                $data['title'] = $category['name'];
                $data['description'] = (!empty($category['description'])) ? $category['description'] : null;
                $data['categoryIcon'] = SITE_URL . 'categoryicon/small/' . $category['cat_icon'];
                $data['discount'] = (!empty($category['discount'])) ? true : false;
                $data['discountRate'] = (!empty($category['discount']) && $category['discount'] != 0) ? $category['discount'] : null;
                $data['subCategories'] = null;
                foreach ($category['child_categories'] as $subCategory) {
                    $sub = [];
                    $sub['subCategoryId'] = $subCategory['id'];
                    $sub['title'] = $subCategory['name'];
                    $sub['subTitle'] = "Up To 50% OFF";
                    $sub['subCategoryIcon'] = SITE_URL . 'categoryicon/small/' . $subCategory['cat_icon'];
                    $data['subCategories'][] = $sub;
                }
                $response['categories'][] = $data;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function categoryProducts()
    {
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['vendorId']) || empty($this->request->data['subCategoryId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $vendorId = $this->request->data['vendorId'];
            $subCategoryId = $this->request->data['subCategoryId'];
            $attributes = $this->Attributes->ParentAttributes->find('list')->toarray();
            $products = $this->Products->find('all')->contain(['Attributes' => function ($q) use ($vendorId) {
                return $q->where(['OR' => [['ProductsAttributes.vendor_id' => '0'], ['ProductsAttributes.vendor_id' => $vendorId]], 'is_stock' => 'Y'])->order(['ProductsAttributes.sort' => 'DESC']);
            }])->where(['OR' => [['Products.vendor_id' => 0], ['Products.vendor_id' => $vendorId]], 'Products.status' => 'Y', 'Products.subcategory_id' => $subCategoryId])->order(['Products.productnameen' => 'ASC'])->toarray();
            if (empty($products)) {
                $response['success'] = false;
                $response['message'] = "No Products Available";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = true;
            foreach ($products as $product) {
                if (empty($product['attributes'])) {
                    continue;
                }
                $data = [];
                $data['productId'] = $product['id'];
                $data['productName'] = $product['productnameen'];
                foreach ($product['attributes'] as $attribute) {
                    if ($attribute['_joinData']['vendor_id'] == 0) {
                        $vendorAttrExist = $this->ProductsAttributes->find()->where(['vendor_id' => $this->request->data['vendorId'], 'attribute_type' => $attribute['parent_id'], 'attribute_id' => $attribute['id'], 'product_id' => $product['id']])->first();
                        if ($vendorAttrExist) {
                            continue;
                        }
                    }
                    $productData = [];
                    $productData['size'] = $attribute['name'] . ' ' . $attributes[$attribute['parent_id']];
                    $productData['sizeId'] = $attribute['id'];
                    $productData['price']['actualPrice'] = $attribute['_joinData']['price'];
                    $productData['price']['discountPrice'] = !empty($attribute['_joinData']['discount_price']) || $attribute['_joinData']['discount_price'] != 0 ? $attribute['_joinData']['discount_price'] : null;
                    $productData['price']['discountPercent'] = empty($productData['price']['discountPrice']) || $productData['price']['discountPrice'] == 0 ? null :
                    floor(($productData['price']['actualPrice'] - $productData['price']['discountPrice']) * 100 / $productData['price']['actualPrice']);
                    $data['productSizes'][] = $productData;
                }
                $data['image'] = !empty($product['image']) ? 'http://dc84viddw6ggp.cloudfront.net/products/' . $product['image'] : PRODUCT_DEFAULT_IMAGE;
                $response['productInfo'][] = $data;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function addToCart()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId']) || empty($this->request->data['productId']) || empty($this->request->data['quantity']) || empty($this->request->data['sizeId']) || empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            extract($this->request->data);
            $prodExist = $this->Products->exists(['id' => $productId]);
            $userExist = $this->Users->exists(['id' => $userId]);
            $sizeExist = $this->Attributes->find()->contain(['ParentAttributes'])->where(['Attributes.id' => $sizeId])->first();
            $sizeTypeExist = $this->Attributes->exists(['id' => $sizeType]);
            $vendorExist = $this->Vendors->exists(['id' => $vendorId]);
            if ($prodExist && $userExist && $sizeExist && $vendorExist) {
                $cartItem = $this->Carts->find()->where(['user_id' => $userId, 'vendor_id' => $vendorId, 'product_id' => $productId, 'attribute_id' => $sizeId])->first();
                if (!empty($cartItem)) {
                    $cartItem->quantity += $quantity;
                    if ($cartItem->quantity == 0) {
                        $this->Carts->delete($cartItem);
                        $response['success'] = true;
                        $response['message'] = "Cart Updated successfully";
                        $response['cartCount'] = $this->Carts->find()->select(['count' => 'sum(quantity)'])->where(['user_id' => $userId, 'vendor_id' => $vendorId])->first()->count;
                        $this->response->type('application/json');
                        $this->response->body(json_encode($response));
                        return $this->response;
                    } else {
                        if ($this->Carts->save($cartItem)) {
                            $response['success'] = true;
                            $response['message'] = "Cart Updated successfully";
                            $response['cartCount'] = $this->Carts->find()->select(['count' => 'sum(quantity)'])->where(['user_id' => $userId, 'vendor_id' => $vendorId])->first()->count;
                            $this->response->type('application/json');
                            $this->response->body(json_encode($response));
                            return $this->response;
                        }
                    }
                    $response['success'] = false;
                    $response['message'] = "Error while updating";
                    $this->response->type('application/json');
                    $this->response->body(json_encode($response));
                    return $this->response;
                } else {
                    $newCartItem = $this->Carts->newEntity();
                    $newCartItem['product_id'] = $productId;
                    $newCartItem['user_id'] = $userId;
                    $newCartItem['attribute_id'] = $sizeId;
                    $newCartItem['attribute_type'] = $sizeExist['parent_attribute']['id'];
                    $newCartItem['vendor_id'] = $vendorId;
                    $newCartItem['quantity'] = $quantity;
                    if ($this->Carts->save($newCartItem)) {
                        $response['success'] = true;
                        $response['message'] = "Product added to cart successfully";
                        $response['cartCount'] = $this->Carts->find()->select(['count' => 'sum(quantity)'])->where(['user_id' => $userId, 'vendor_id' => $vendorId])->first()->count;
                        $this->response->type('application/json');
                        $this->response->body(json_encode($response));
                        return $this->response;
                    } else {
                        $response['success'] = false;
                        $response['message'] = "Error while adding to cart";
                        $this->response->type('application/json');
                        $this->response->body(json_encode($response));
                        return $this->response;
                    }
                }
            } else {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }

    }
    public function cartDetails()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId']) || empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $vendorId = $this->request->data['vendorId'];
            $attributes = $this->Attributes->ParentAttributes->find('list')->toarray();
            $cartItems = $this->Carts->find()->contain(['Products', 'ChildAttributes'])->where(['user_id' => $this->request->data['userId']])->toarray();
            $response['success'] = true;
            $response['mrp'] = 0;
            $response['discount'] = 0;
            $response['deliveryCharge'] = 'Free';
            $response['subTotal'] = 0;
            $response['productsInfo'] = null;
            foreach ($cartItems as $cartItem) {
                $data = [];
                $data['productId'] = $cartItem['product_id'];
                $data['productName'] = $cartItem['product']['productnameen'];
                $data['productQuantity'] = $cartItem['quantity'];
                $data['size'] = $cartItem['child_attribute']['name'] . ' ' . $attributes[$cartItem['child_attribute']['parent_id']];
                $data['sizeId'] = $cartItem['attribute_id'];
                $data['image'] = !empty($cartItem['product']['image']) ? 'http://dc84viddw6ggp.cloudfront.net/products/' . $cartItem['product']['image'] : PRODUCT_DEFAULT_IMAGE;
                $productPrice = $this->ProductsAttributes->find()->where(['product_id' => $cartItem['product_id'], 'attribute_id' => $cartItem['attribute_id'], 'vendor_id' => $cartItem['vendor_id']])->first();
                if (!empty($productPrice)) {
                    $data['price']['actualPrice'] = $productPrice['price'];
                    $data['price']['discountPrice'] = (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? 0 : $productPrice['discount_price']);
                    $data['price']['discountPercent'] = empty($data['price']['discountPrice']) || $data['price']['discountPrice'] == 0 ? null :
                    floor(($data['price']['actualPrice'] - $data['price']['discountPrice']) * 100 / $data['price']['actualPrice']);
                    $response['mrp'] += ($productPrice['price'] * $cartItem['quantity']);
                    $response['discount'] += (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? 0 : (($productPrice['price'] - $productPrice['discount_price']) * $cartItem['quantity']));
                } else {
                    $productPrice = $this->ProductsAttributes->find()->where(['product_id' => $cartItem['product_id'], 'attribute_id' => $cartItem['attribute_id'], 'vendor_id' => 0])->first();
                    $data['price']['actualPrice'] = $productPrice['price'];
                    $data['price']['discountPrice'] = (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? 0 : $productPrice['discount_price']);
                    $data['price']['discountPercent'] = empty($data['price']['discountPrice']) || $data['price']['discountPrice'] == 0 ? null :
                    floor(($data['price']['actualPrice'] - $data['price']['discountPrice']) * 100 / $data['price']['actualPrice']);
                    $response['mrp'] += ($productPrice['price'] * $cartItem['quantity']);
                    $response['discount'] += (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? 0 : (($productPrice['price'] - $productPrice['discount_price']) * $cartItem['quantity']));
                }
                $response['productsInfo'][] = $data;
            }
            $response['subTotal'] = $response['mrp'] - $response['discount'];
            if (empty($cartItems)) {
                $response = [];
                $response['success'] = false;
                $response['message'] = "No items in your cart";
            }
            $products = $this->Products->find('all')->contain(['Attributes' => function ($q) use ($vendorId) {
                return $q->where(['OR' => [['ProductsAttributes.vendor_id' => '0'], ['ProductsAttributes.vendor_id' => $vendorId]], 'is_stock' => 'Y'])->order(['ProductsAttributes.sort' => 'DESC']);
            }])->matching('ProductTopSavings', function ($q) use ($vendorId) {
                return $q->where(['ProductTopSavings.vendor_id' => $vendorId]);
            })->where(['OR' => [['Products.vendor_id' => 0], ['Products.vendor_id' => $vendorId]], 'Products.status' => 'Y'])->order(['Products.productnameen' => 'ASC'])->toarray();
            if (empty($products)) {
                $response['topSavingProducts'] = null;
            }
            foreach ($products as $product) {
                if (empty($product['attributes'])) {
                    continue;
                }
                if ($product['attributes'][0]['_joinData']['vendor_id'] == 0) {
                    $vendorAttrExist = $this->ProductsAttributes->find()->where(['vendor_id' => $vendorId, 'attribute_type' => $product['attributes'][0]['parent_id'], 'attribute_id' => $product['attributes'][0]['id'], 'product_id' => $product['id']])->first();
                    if ($vendorAttrExist) {
                        $product['attributes'][0]['_joinData']['price'] = $vendorAttrExist['price'];
                        $product['attributes'][0]['_joinData']['discount_price'] = $vendorAttrExist['discount_price'];
                    }
                }
                $data = [];
                $data['productId'] = $product['id'];
                $data['productName'] = $product['productnameen'];
                $data['size'] = $product['attributes'][0]['name'] . ' ' . $attributes[$product['attributes'][0]['parent_id']];
                $data['sizeId'] = $product['attributes'][0]['id'];
                $data['sizeType'] = $product['attributes'][0]['parent_id'];
                $data['price']['actualPrice'] = $product['attributes'][0]['_joinData']['price'];
                $data['price']['discountPrice'] = !empty($product['attributes'][0]['_joinData']['discount_price']) || $product['attributes'][0]['_joinData']['discount_price'] != 0 ? $product['attributes'][0]['_joinData']['discount_price'] : null;
                $data['price']['discountPercent'] = empty($data['price']['discountPrice']) || $data['price']['discountPrice'] == 0 ? null :
                floor(($data['price']['actualPrice'] - $data['price']['discountPrice']) * 100 / $data['price']['actualPrice']);
                $data['image'] = !empty($product['image']) ? 'http://dc84viddw6ggp.cloudfront.net/products/' . $product['image'] : PRODUCT_DEFAULT_IMAGE;
                $response['topSavingProducts'][] = $data;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function myAddress()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $addresses = $this->UserAddresses->find()->where(['user_id' => $this->request->data['userId'], 'status' => 1])->toarray();
            if (empty($addresses)) {
                $response['success'] = false;
                $response['message'] = "No Address Found";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = true;
            $response['addresses'] = null;
            foreach ($addresses as $address) {
                $data = [];
                $data['addressId'] = $address['id'];
                $data['location']['lat'] = $address['latitude'];
                $data['location']['long'] = $address['longitude'];
                $data['nickName'] = $address['address_nick_name'];
                $data['name'] = $address['name'];
                $data['address'] = $address['flat_no'] . $address['address_1'];
                $data['isDefaultAddress'] = $address['is_default'] == 1 ? true : false;
                $response['addresses'][] = $data;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function addAddress()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId']) || empty($this->request->data['lat']) || empty($this->request->data['long']) || empty($this->request->data['flatNo']) || empty($this->request->data['address']) || empty($this->request->data['name']) || empty($this->request->data['nickName']) || empty($this->request->data['isDefaultAddress'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $newAddress = $this->UserAddresses->newEntity();
            extract($this->request->data);
            $newAddress['user_id'] = $userId;
            $newAddress['latitude'] = $lat;
            $newAddress['longitude'] = $long;
            $newAddress['name'] = $name;
            $newAddress['address_nick_name'] = $nickName;
            $newAddress['flat_no'] = $flatNo;
            $newAddress['address_1'] = $address;
            $newAddress['is_default'] = $isDefaultAddress == true ? 1 : 0;
            $userAddressExist = $this->UserAddresses->exists(['user_id' => $userId]);
            if (!$userAddressExist) {
                $newAddress['is_default'] = 1;
            }
            if ($isDefaultAddress == true) {
                $this->UserAddresses->updateDefault($userId);
            }
            if ($this->UserAddresses->save($newAddress)) {
                $response['success'] = true;
                $response['message'] = "Address saved successfully";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = false;
            $response['message'] = "Error while saving address";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function deleteAddress()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId']) || empty($this->request->data['addressId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $address = $this->UserAddresses->find()->where(['user_id' => $this->request->data['userId'], 'id' => $this->request->data['addressId']])->first();
            if (empty($address)) {
                $response['success'] = false;
                $response['message'] = "Invalid Address Id";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            if ($address->is_default == 1) {
                $response['success'] = false;
                $response['message'] = "Cannot delete default address";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $address->status = 0;
            if ($this->UserAddresses->save($address)) {
                $response['success'] = true;
                $response['message'] = "Address Deleted successfully";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = false;
            $response['message'] = "Error while saving address";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function addWalletAmount()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId']) || empty($this->request->data['amount']) || empty($this->request->data['transactionId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            // $newPayment = $this->Payments->newEntity();
            // $newPayment['transaction_id'] = $this->request->data['transactionId'];
            // $newPayment['user_id'] = $this->request->data['userId'];
            // $newPayment['amount'] = $this->request->data['amount'];
            // $newPayment['status'] = "approved";
            // $newPayment['payment_type_id'] = 1;
            // if ($payment = $this->Payments->save($newPayment)) {
            $newWallet = $this->Wallets->newEntity();
            $newWallet['transaction_id'] = $this->request->data['transactionId'];
            $newWallet['user_id'] = $this->request->data['userId'];
            $newWallet['amount'] = $this->request->data['amount'];
            $newWallet['amount_type'] = 'payment';
            if ($this->Wallets->save($newWallet)) {
                $response['success'] = true;
                $response['message'] = "Amount added successfully to Wallet";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = false;
            $response['message'] = "Error while saving to wallet";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function myWallet()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $today = date('Y-m-d');
            $walletAmount = $this->Wallets->find()->select(['sum' => 'SUM(amount)'])->where(['OR' => [['is_expiry' => 0], ['is_expiry' => 1, 'DATE(expiry_date) >=' => $today]]])->first();
            // $paymentSpent = $this->Payments->find()->contain(['PaymentTypes'])->select(['sum' => 'SUM(PaymentTypes.amount)'])->where(['Payments.user_id', 'PaymentTypes.name' => 'wallet'])->first();
            // pr($paymentSpent);die;
            $response['success'] = true;
            $response['walletAmount'] = $walletAmount->sum;
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function productDetail()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['vendorId']) || empty($this->request->data['productId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $vendorId = $this->request->data['vendorId'];
            $product = $this->Products->find()->contain(['Attributes'])->where(['Products.id' => $this->request->data['productId']])->first();
            if (empty($product)) {
                $response['success'] = false;
                $response['message'] = "Invalid Product Id";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $attributes = $this->Attributes->find('list')->toarray();
            $response['success'] = true;
            $response['productId'] = $product['id'];
            $response['productName'] = $product['productnameen'];
            $response['productSizes'] = null;
            $response['image'] = !empty($product['image']) ? 'http://dc84viddw6ggp.cloudfront.net/products/' . $product['image'] : PRODUCT_DEFAULT_IMAGE;
            foreach ($product['attributes'] as $attribute) {
                if ($attribute['_joinData']['vendor_id'] == 0) {
                    $vendorAttrExist = $this->ProductsAttributes->find()->where(['vendor_id' => $this->request->data['vendorId'], 'attribute_type' => $attribute['parent_id'], 'attribute_id' => $attribute['id'], 'product_id' => $product['id']])->first();
                    if ($vendorAttrExist) {
                        continue;
                    }
                }
                $data = [];
                $data['size'] = $attribute['name'] . ' ' . $attributes[$attribute['parent_id']];
                $data['sizeId'] = $attribute['id'];
                $data['price']['actualPrice'] = $attribute['_joinData']['price'];
                $data['price']['discountPrice'] = !empty($attribute['_joinData']['discount_price']) || $attribute['_joinData']['discount_price'] != 0 ? $attribute['_joinData']['discount_price'] : null;
                $data['price']['discountPercent'] = empty($data['price']['discountPrice']) || $data['price']['discountPrice'] == 0 ? null :
                floor(($data['price']['actualPrice'] - $data['price']['discountPrice']) * 100 / $data['price']['actualPrice']);
                $response['productSizes'][] = $data;
            }
            $response['description'] = $product['description'];
            $products = $this->Products->find('all')->contain(['Attributes' => function ($q) use ($vendorId) {
                return $q->where(['OR' => [['ProductsAttributes.vendor_id' => '0'], ['ProductsAttributes.vendor_id' => $vendorId]], 'is_stock' => 'Y'])->order(['ProductsAttributes.sort' => 'DESC']);
            }])->matching('ProductTopSavings', function ($q) use ($vendorId) {
                return $q->where(['ProductTopSavings.vendor_id' => $vendorId]);
            })->where(['OR' => [['Products.vendor_id' => 0], ['Products.vendor_id' => $vendorId]], 'Products.status' => 'Y'])->order(['Products.productnameen' => 'ASC'])->toarray();
            if (empty($products)) {
                $response['topSavingProducts'] = null;
            }
            foreach ($products as $product) {
                if (empty($product['attributes'])) {
                    continue;
                }
                if ($product['attributes'][0]['_joinData']['vendor_id'] == 0) {
                    $vendorAttrExist = $this->ProductsAttributes->find()->where(['vendor_id' => $vendorId, 'attribute_type' => $product['attributes'][0]['parent_id'], 'attribute_id' => $product['attributes'][0]['id'], 'product_id' => $product['id']])->first();
                    if ($vendorAttrExist) {
                        $product['attributes'][0]['_joinData']['price'] = $vendorAttrExist['price'];
                        $product['attributes'][0]['_joinData']['discount_price'] = $vendorAttrExist['discount_price'];
                    }
                }
                $data = [];
                $data['productId'] = $product['id'];
                $data['productName'] = $product['productnameen'];
                $data['size'] = $product['attributes'][0]['name'] . ' ' . $attributes[$product['attributes'][0]['parent_id']];
                $data['sizeId'] = $product['attributes'][0]['id'];
                $data['sizeType'] = $product['attributes'][0]['parent_id'];
                $data['price']['actualPrice'] = $product['attributes'][0]['_joinData']['price'];
                $data['price']['discountPrice'] = !empty($product['attributes'][0]['_joinData']['discount_price']) || $product['attributes'][0]['_joinData']['discount_price'] != 0 ? $product['attributes'][0]['_joinData']['discount_price'] : null;
                $data['price']['discountPercent'] = empty($data['price']['discountPrice']) || $data['price']['discountPrice'] == 0 ? null :
                floor(($data['price']['actualPrice'] - $data['price']['discountPrice']) * 100 / $data['price']['actualPrice']);
                $data['image'] = !empty($product['image']) ? 'http://dc84viddw6ggp.cloudfront.net/products/' . $product['image'] : PRODUCT_DEFAULT_IMAGE;
                $response['topSavingProducts'][] = $data;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function checkout()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId']) || empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $address = $this->UserAddresses->find()->where(['user_id' => $this->request->data['userId']])->toarray();
            if (empty($address)) {
                $response['success'] = false;
                $response['message'] = "Please add Address before procedding";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = true;
            $response['addressInfo'] = null;
            if (!empty($this->request->data['addressId'])) {
                $address = $this->UserAddresses->find()->where(['id' => $this->request->data['addressId']])->first();
            } else {
                $address = $this->UserAddresses->find()->where(['user_id' => $this->request->data['userId'], 'is_default' => 1])->first();
            }
            $response['addressInfo']['id'] = $address['id'];
            $response['addressInfo']['nickName'] = $address['address_nick_name'];
            $response['addressInfo']['name'] = $address['name'];
            $response['addressInfo']['flatNo'] = $address['flat_no'];
            $response['addressInfo']['addressLine'] = $address['address_1'];
            $response['slotsInfo'] = null;

            $today = date('Y-m-d');
            $toDate = date('Y-m-d', strtotime('+5 days'));
            while (strtotime($today) <= strtotime($toDate)) {
                $data = [];
                $day = date('D', strtotime($today));
                if (strtotime($today) == strtotime(date('Y-m-d'))) {
                    $slots = $this->Slots->find()->where(['status' => 'Y', 'mintime >' => date('H:i:s'), 'vendor_id' => $this->request->data['vendorId'], 'weekdays' => $day])->toarray();
                } else {
                    $slots = $this->Slots->find()->where(['status' => 'Y', 'vendor_id' => $this->request->data['vendorId'], 'weekdays' => $day])->toarray();
                }
                $data['date'] = $today;
                if (strtotime($today) == strtotime(date('Y-m-d'))) {
                    $data['alias'] = 'Today';
                } else if (strtotime($today) == strtotime(date('Y-m-d') . '+1 days')) {
                    $data['alias'] = 'Tomorrow';
                } else {
                    $data['alias'] = date('d M', strtotime($today));
                }
                $data['day'] = date('D', strtotime($today));
                $data['slots'] = null;
                if (empty($slots)) {
                    $today = date('Y-m-d', strtotime($today . '+1 days'));
                    continue;
                }
                foreach ($slots as $slot) {
                    $slotData = [];
                    $slotData['id'] = $slot['id'];
                    $slotData['title'] = date('h A', strtotime($slot['mintime'])) . '-' . date('h A', strtotime($slot['maxtime']));
                    $data['slots'][] = $slotData;
                }
                $response['slotsInfo'][] = $data;
                $today = date('Y-m-d', strtotime($today . '+1 days'));
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function paymentOptions()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            extract($this->request->data);
            $vendorId = $this->request->data['vendorId'];
            $attributes = $this->Attributes->ParentAttributes->find('list')->toarray();
            $cartItems = $this->Carts->find()->contain(['Products', 'ChildAttributes'])->where(['user_id' => $this->request->data['userId']])->toarray();
            if (empty($cartItems)) {
                $response['success'] = false;
                $response['message'] = "No items in your cart";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $total = 0;
            $totalDiscount = 0;
            foreach ($cartItems as $cartItem) {
                $data = [];
                $productPrice = $this->ProductsAttributes->find()->where(['product_id' => $cartItem['product_id'], 'attribute_id' => $cartItem['attribute_id'], 'vendor_id' => $cartItem['vendor_id']])->first();

                if (!empty($productPrice)) {
                    $price = (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? $productPrice['price'] : $productPrice['discount_price']) * $cartItem['quantity'];
                    $total += $price;
                    $discount = (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? 0 : (($productPrice['price'] - $productPrice['discount_price']) * $cartItem['quantity']));
                    $totalDiscount += $discount;
                } else {
                    $productPrice = $this->ProductsAttributes->find()->where(['product_id' => $cartItem['product_id'], 'attribute_id' => $cartItem['attribute_id'], 'vendor_id' => 0])->first();
                    $price = (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? $productPrice['price'] : $productPrice['discount_price']) * $cartItem['quantity'];
                    $total += $price;
                    $discount = (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? 0 : (($productPrice['price'] - $productPrice['discount_price']) * $cartItem['quantity']));
                    $totalDiscount += $discount;
                }
            }
            $response['totalPrice'] = $total;
            $response['totaldiscount'] = $totalDiscount;
            $response['amountPayable'] = $total - $totalDiscount;
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function placeOrder()
    {

        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId']) || empty($this->request->data['addressId']) || empty($this->request->data['deliveryDate']) || empty($this->request->data['slotId']) || empty($this->request->data['vendorId']) || empty($this->request->data['paymentMode'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            extract($this->request->data);

            $addressVerify = $this->addressVerify($vendorId, $addressId);
            if ($addressVerify == false) {
                $response['success'] = false;
                $response['message'] = "service not available to selected address";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }

            $vendor = $this->Vendors->find()->where(['id' => $vendorId])->first();
            $attributes = $this->Attributes->ParentAttributes->find('list')->toarray();
            $cartItems = $this->Carts->find()->contain(['Products', 'ChildAttributes'])->where(['user_id' => $this->request->data['userId']])->toarray();
            if (empty($cartItems)) {
                $response['success'] = false;
                $response['message'] = "No items in your cart";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $couponDiscount = 0;
            $deliveryDiscount = 0;
            $cashback = 0;
            if (!empty($this->request->data['couponCode'])) {
                $coupon = $this->Coupons->find()->where(['vendor_id' => $vendorId, 'code' => $couponCode])->order(['id' => 'DESC'])->first();
                if (empty($coupon)) {
                    $response['success'] = false;
                    $response['message'] = "Invalid Coupon";
                    $this->response->type('application/json');
                    $this->response->body(json_encode($response));
                    return $this->response;
                }
                $data = $this->couponAmount($userId, $coupon, $vendorId);
                if ($data['applicable'] == true) {
                    $couponDiscount = (!empty($data['couponDiscount'])) ? $data['couponDiscount'] : $couponDiscount;
                    $deliveryDiscount = (!empty($data['deliveryDiscount'])) ? $data['deliveryDiscount'] : $deliveryDiscount;
                    $cashback = (!empty($data['cashback'])) ? $data['cashback'] : $cashback;
                }
            }
            $newOrder = $this->Orders->newEntity();
            $newOrder['vendor_id'] = $vendorId;
            $newOrder['user_id'] = $userId;
            $newOrder['user_address_id'] = $addressId;
            $newOrder['to_deliver_date'] = date('Y-m-d', strtotime($deliveryDate));
            $newOrder['slot_id'] = $slotId;
            if (!empty($couponId)) {
                $newOrder['coupon_id'] = $couponId;
            }
            $total = 0;
            $totalDiscount = 0;
            $orderDetails = [];
            foreach ($cartItems as $cartItem) {
                $data = [];
                $productPrice = $this->ProductsAttributes->find()->where(['product_id' => $cartItem['product_id'], 'attribute_id' => $cartItem['attribute_id'], 'vendor_id' => $cartItem['vendor_id']])->first();

                if (!empty($productPrice)) {
                    $price = (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? $productPrice['price'] : $productPrice['discount_price']) * $cartItem['quantity'];
                    $total += ($productPrice['price'] * $cartItem['quantity']);
                    $discount = (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? 0 : (($productPrice['price'] - $productPrice['discount_price']) * $cartItem['quantity']));
                    $totalDiscount += $discount;
                } else {
                    $productPrice = $this->ProductsAttributes->find()->where(['product_id' => $cartItem['product_id'], 'attribute_id' => $cartItem['attribute_id'], 'vendor_id' => 0])->first();
                    $price = (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? $productPrice['price'] : $productPrice['discount_price']) * $cartItem['quantity'];
                    $total += ($productPrice['price'] * $cartItem['quantity']);
                    $discount = (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? 0 : (($productPrice['price'] - $productPrice['discount_price']) * $cartItem['quantity']));
                    $totalDiscount += $discount;
                }
                $newOrderDetail = $this->OrderDetails->newEntity();
                $newOrderDetail['product_id'] = $cartItem['product_id'];
                $newOrderDetail['attribute_id'] = $cartItem['attribute_id'];
                $newOrderDetail['attribute_type'] = $this->Attributes->find()->where(['id' => $cartItem['attribute_id']])->first()->parent_id;
                $newOrderDetail['actual_price'] = $productPrice['price'] * $cartItem['quantity'];
                $newOrderDetail['price'] = $price;
                $newOrderDetail['quantity'] = $cartItem['quantity'];
                $orderDetails[] = $newOrderDetail;
            }
            $newOrder['sub_total'] = $total;
            $newOrder['coupon_discount'] = $couponDiscount;
            $newOrder['product_discount'] = $totalDiscount;
            $newOrder['delivery_charge'] = 0 - $deliveryDiscount;
            $newOrder['total_amount'] = $newOrder['sub_total'] - $newOrder['coupon_discount'] - $newOrder['product_discount'] - $newOrder['delivery_charge'];
            $newOrder['commission'] = ($newOrder['total_amount'] * $vendor['commision']) / 100;
            $newOrder['cashback'] = $cashback;

            $walletBalance = 0;
            if (isset($walletId) && $walletId !== "") {
                if ($walletId == 0) {
                    $greenCash = $this->Wallets->WalletIncomes->find()->select(['sum' => 'SUM(amount)'])->where(['user_id' => $userId, 'status' => 1])->first()->sum;
                    $greenCashUsed = $this->Wallets->WalletExpenses->find()->select(['sum' => 'SUM(amount)'])->where(['user_id' => $userId, 'status' => 1, 'wallet_id' => 0])->first()->sum;
                    $walletBalance = (INT) $greenCash - (INT) $greenCashUsed;
                } else {
                    $today = date('Y-m-d');
                    $type = $this->Wallets->find()->where(['id' => $walletId])->first();
                    if ($type->amount_type == 'referral') {
                        $referralReward = $this->Wallets->find()->where(['user_id' => $userId, 'status' => 1, 'id' => $walletId])->first();
                        $referralRewardUsed = $this->Wallets->WalletExpenses->find()->select(['sum' => 'SUM(amount)'])->where(['user_id' => $userId, 'status' => 1, 'wallet_id' => $referralReward['id']])->first()->sum;
                        $walletBalance = (INT) $referralReward['amount'] - (INT) $cashbackUsed;
                    } else {
                        $cashback = $this->Wallets->CashBacks->find()->where(['user_id' => $userId, 'DATE(expiry_date) >=' => $today, 'status' => 1, 'id' => $walletId])->first();
                        $cashbackUsed = $this->Wallets->WalletExpenses->find()->select(['sum' => 'SUM(amount)'])->where(['user_id' => $userId, 'status' => 1, 'wallet_id' => $cashback['id']])->first()->sum;
                        $walletBalance = (INT) $cashback['amount'] - (INT) $cashbackUsed;
                        if ($walletBalance > 0) {
                            if ($cashback['max_redeem_type'] == 'percentage') {
                                $max = ($newOrder['total_amount'] * $cashback['max_redeem_rate']) / 100;
                                if ($walletBalance > $max) {
                                    $walletBalance = $max;
                                }
                            } else if ($walletBalance > $cashback['max_redeem_rate']) {
                                $walletBalance = $cashback['max_redeem_rate'];
                            }
                        }
                    }
                }
                if ($walletBalance > 0) {
                    if ($walletBalance >= $newOrder['total_amount']) {
                        $walletBalance = $newOrder['total_amount'];
                    }
                    $newOrder['wallet'] = 1;
                    $newOrder['wallet_id'] = $walletId;
                    $newOrder['wallet_amount'] = $walletBalance;
                } else {
                    $walletBalance = 0;
                }

            }
            if ($paymentMode == "COD") {
                $newOrder['payment_status'] = 'approved';
                $newOrder['transaction_mode'] = 'cod';
            } else if ($paymentMode == "ONLINE") {
                $newOrder['payment_status'] = 'pending';
                $newOrder['transaction_mode'] = 'online';
                $newOrder['transaction_mode'] = 'online';
            }

            if ($newOrder['total_amount'] == $walletBalance) {
                $newWallet = $this->Wallets->newEntity();
                $newWallet['wallet_id'] = $walletId;
                $newWallet['amount'] = $walletBalance;
                $newWallet['amount_type'] = 'expense';
                $newWallet['user_id'] = $userId;
                $wallet = $this->Wallets->save($newWallet);
                $newOrder['payment_status'] = 'approved';
                $newOrder['transaction_mode'] = 'wallet';
            }
            if (($paymentMode == "ONLINE" || $paymentMode == "COD") && $this->request->data['remainingPayableAmount'] != ($newOrder['total_amount'] - $walletBalance)) {
                $response['success'] = false;
                $response['message'] = "Amount Mismatch";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            if ($order = $this->Orders->save($newOrder)) {
                if ($this->Orders->OrderDetails->link($order, $orderDetails)) {
                    if ($paymentMode != "ONLINE") {
                        $this->Carts->deleteAll(['user_id' => $userId]);
                    }
                    if ($newOrder['total_amount'] == $walletBalance) {
                        $wallet['order_id'] = $order->id;
                        $wallet = $this->Wallets->save($wallet);
                        $response['success'] = true;
                        $response['placed'] = true;
                        if ($paymentMode != "ONLINE") {
                            $response['cartCount'] = 0;
                        }
                        $response['paymentMode'] = 'WALLET/COUPON';
                        // $response['orderId'] = $order->id;
                        $response['message'] = "Order Placed Successfully";
                        $this->response->type('application/json');
                        $this->response->body(json_encode($response));
                        return $this->response;
                    }
                    $response['success'] = true;
                    if ($paymentMode == "ONLINE") {
                        $api = new Api(RAZOR_API_KEY, RAZOR_API_SECRET);
                        $razorPayorder = $api->order->create(array(
                            'receipt' => $order->id,
                            'amount' => ($order->total_amount - $walletBalance) * 100,
                            'payment_capture' => 1,
                            'currency' => 'INR',
                        )
                        );
                        $order->razorpay_order_id = $razorPayorder->id;
                        $this->Orders->save($order);
                        $response['placed'] = false;
                        $response['orderId'] = $order->id;
                        $response['paymentMode'] = "ONLINE";
                        $response['onlinePaymentInfo']['onlineOrderId'] = $razorPayorder->id;
                        $response['onlinePaymentInfo']['onlineKeyId'] = RAZOR_API_KEY;
                        $response['onlinePaymentInfo']['currency'] = "INR";
                        $response['onlinePaymentInfo']['merchantName'] = "Rashan Duniya";
                        $response['onlinePaymentInfo']['description'] = "Your Grocery Store";
                        $response['onlinePaymentInfo']['merchantName'] = "Rashan Duniya";
                        $response['onlinePaymentInfo']['merchantLogo'] = "https://rashanduniya.com/img/logo_new.png";

                    } else if ($paymentMode == "COD") {
                        $response['placed'] = true;
                        $response['paymentMode'] = "COD";
                        $response['cartCount'] = 0;
                        $response['message'] = "Order Saved Successfully";
                    }
                    // $response['balancePayment'] = $order->total_amount - $walletBalance;
                    $this->response->type('application/json');
                    $this->response->body(json_encode($response));
                    return $this->response;
                }
            }
            $response['success'] = false;
            $response['message'] = "Error while saving order";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }
    public function viewOrders()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $userId = $this->request->data['userId'];
            $pendingOrders = $this->Orders->find()->where(['Orders.user_id' => $userId, 'Orders.payment_status' => 'approved'])->order(['id' => 'DESC'])->toarray();
            if (empty($pendingOrders)) {
                $response['success'] = false;
                $response['message'] = "No Pending Orders";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $slots = $this->Slots->find('list')->toarray();
            $response['success'] = true;
            $response['ordersInfo'] = [];
            foreach ($pendingOrders as $pendingOrder) {
                $data = [];
                $slotData = [];
                $vendor = $this->Vendors->find()->contain(['Cities'])->where(['Vendors.id' => $pendingOrder['vendor_id']])->first();
                $slot = $this->Slots->find()->where(['status' => 'Y', 'id' => $pendingOrder['slot_id']])->first();
                $orderProduct = $this->OrderDetails->find()->contain(['Products'])->where(['order_id' => $pendingOrder['id']])->first();
                $data['orderId'] = $pendingOrder['id'];
                $data['vendorName'] = $vendor->name . ' - ' . $vendor['city']['name'];
                $slotData['title'] = date('h A', strtotime($slot['mintime'])) . '-' . date('h A', strtotime($slot['maxtime']));
                $data['placedDate'] = date('D d M Y,H.i A', strtotime($pendingOrder['created']));
                if ($pendingOrder['order_status'] == "delivered") {
                    $data['deliveryDate'] = date('D, d M Y H:i A', strtotime($pendingOrder['delivery_date']));
                } else {
                    $data['deliveryDate'] = date('D, d M Y', strtotime($pendingOrder['to_deliver_date'])) . ' ' . $slotData['title'];
                }
                $data['amount'] = $pendingOrder['total_amount'] - $pendingOrder['delivery_charge'];
                $data['orderStatus'] = $pendingOrder['order_status'];
                $data['deliveryCharge'] = $pendingOrder['delivery_charge'] == 0 ? 'Free' : $pendingOrder['delivery_charge'];
                $data['finalAmount'] = $pendingOrder['total_amount'];
                $data['image'] = !empty($orderProduct['product']['image']) ? 'http://dc84viddw6ggp.cloudfront.net/products/' . $orderProduct['product']['image'] : PRODUCT_DEFAULT_IMAGE;
                $response['ordersInfo'][] = $data;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function viewOrderDetails()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['orderId']) || empty($this->request->data['userId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $order = $this->Orders->find()->contain(['OrderDetails'])->where(['id' => $this->request->data['orderId'], 'user_id' => $this->request->data['userId']])->first();
            if (!empty($order)) {
                $this->loadModel('PaymentTypes');
                $attributes = $this->Attributes->find('list')->toarray();
                $vendor = $this->Vendors->find()->contain(['Cities'])->where(['Vendors.id' => $order['vendor_id']])->first();
                $response['success'] = true;
                $response['vendorName'] = $vendor->name . ' - ' . $vendor['city']['name'];
                $response['placedDate'] = date('D d M Y,H:i A', strtotime($order['created']));
                if ($order['order_status'] == "delivered") {
                    $response['deliveryDate'] = date('D, d M Y H:i A', strtotime($order['delivery_date']));
                } else {
                    $response['deliveryDate'] = date('D, d M Y', strtotime($order['to_deliver_date'])) . ' ' . $slotresponse['title'];
                }
                $response['orderStatus'] = $order['order_status'];
                $response['MRP'] = $order['sub_total'];
                $response['productsDiscount'] = $order['product_discount'];
                $response['couponDiscount'] = $order['coupon_discount'];
                $response['deliveryCharge'] = $order['delivery_charge'] == 0 ? 'Free' : $order['delivery_charge'];
                $response['finalAmount'] = $order['total_amount'];
                $response['itemCount'] = null;
                $response['paymentType'] = $this->PaymentTypes->find()->where(['id' => $order['payment_type_id']])->first()->name;
                $address = $this->UserAddresses->find()->where(['user_id' => $this->request->data['userId'], 'UserAddresses.id' => $order['user_address_id']])->first();
                $response['deliveryAddress']['addressNickName'] = $address['address_nick_name'];
                $response['deliveryAddress']['name'] = $address['name'];
                $response['deliveryAddress']['address'] = $address['flat_no'] . ',' . $address['address_1'] . ',' . $address['city'];
                $response['orderDetails'] = null;
                $qty = 0;
                foreach ($order['order_details'] as $orderDetail) {
                    $data = [];
                    $product = $this->Products->find()->where(['id' => $orderDetail['product_id']])->first();
                    $qty += $orderDetail['quantity'];
                    $data['productId'] = $product['id'];
                    $data['productName'] = $product['productnameen'];
                    $data['productImage'] = !empty($product['image']) ? 'http://dc84viddw6ggp.cloudfront.net/products/' . $product['image'] : PRODUCT_DEFAULT_IMAGE;
                    $data['productQuantity'] = $orderDetail['quantity'];
                    $data['size'] = $attributes[$orderDetail['attribute_id']] . ' ' . $attributes[$orderDetail['attribute_type']];
                    $data['actualPrice'] = $orderDetail['actual_price'];
                    $data['discountPrice'] = ($orderDetail['actual_price'] - $orderDetail['price']) != 0 ? ($orderDetail['price']) : null;
                    $data['discountPercent'] = ($orderDetail['actual_price'] - $orderDetail['price']) == 0 ? null :
                    floor(($orderDetail['actual_price'] - $orderDetail['price']) * 100 / $orderDetail['actual_price']);
                    $response['orderDetails'][] = $data;
                }
                $response['itemCount'] = $qty;
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            } else {
                $response['success'] = false;
                $response['message'] = "Invalid Data Type";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function couponList()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $vendorId = $this->request->data['vendorId'];
            $today = date('Y-m-d H:i:s');
            $coupons = $this->Coupons->find()->where(['vendor_id' => $vendorId, 'valid_from <=' => $today, 'valid_to >=' => $today])->toarray();
            if (empty($coupons)) {
                $response['success'] = false;
                $response['message'] = "No Coupons Available";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = true;
            $response['couponsInfo'] = null;
            foreach ($coupons as $coupon) {
                $data = [];
                $data['id'] = $coupon['id'];
                $data['code'] = $coupon['code'];
                $data['description'] = $coupon['description'];
                $response['couponsInfo'][] = $data;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function applyCoupon()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['vendorId']) || empty($this->request->data['userId']) || empty($this->request->data['couponCode'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            extract($this->request->data);
            $coupon = $this->Coupons->find()->where(['vendor_id' => $vendorId, 'code' => $couponCode])->order(['id' => 'DESC'])->first();
            if (empty($coupon)) {
                $response['success'] = false;
                $response['message'] = "Invalid Coupon";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $data = $this->couponAmount($userId, $coupon, $vendorId);
            if ($data['applicable'] == false) {
                $response['success'] = false;
                $response['message'] = $data['message'];
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = true;
            $response['couponDiscount'] = $data['couponDiscount'];
            $response['cashback'] = $data['cashback'];
            $response['deliveryDiscount'] = $data['deliveryDiscount'];
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function couponAmount($userId, $coupon, $vendorId)
    {
        if ($coupon['applicable_to'] == 'category') {
            $categories = $this->Categories->find('list', ['keyField' => 'id', 'valueField' => 'id'])->matching('Coupons', function ($q) use ($coupon) {
                return $q->where(['Coupons.id' => $coupon['id']]);
            })->where(['Categories.status' => 'Y'])->toarray();
            $products = $this->Products->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['category_id IN' => $categories])->where(['Products.status' => 'Y'])->toarray();
        }
        if ($coupon['applicable_to'] == 'product') {
            $products = $this->Products->find('list', ['keyField' => 'id', 'valueField' => 'id'])->matching('Coupons', function ($q) use ($coupon) {
                return $q->where(['Coupons.id' => $coupon['id']]);
            })->where(['Products.status' => 'Y'])->toarray();
        }
        if ($coupon['applicable_to'] == 'all') {
            $products = $this->Products->find('list', ['keyField' => 'id', 'valueField' => 'id'])->where(['Products.status' => 'Y'])->toarray();
        }
        $cartItems = $this->Carts->find()->contain(['Products', 'ChildAttributes'])->where(['user_id' => $userId, 'product_id IN' => $products, 'Carts.vendor_id' => $vendorId])->toarray();
        if (empty($cartItems)) {
            $data['applicable'] = false;
            $data['message'] = "Coupon not applicable";
            return $data;
        }
        $total = 0;
        $totalDiscount = 0;
        $data = [];
        $data['applicable'] = false;
        $data['couponDiscount'] = null;
        $data['cashback'] = null;
        $data['deliveryDiscount'] = null;
        foreach ($cartItems as $cartItem) {
            $data = [];
            $productPrice = $this->ProductsAttributes->find()->where(['product_id' => $cartItem['product_id'], 'attribute_id' => $cartItem['attribute_id'], 'vendor_id' => $cartItem['vendor_id']])->first();

            if (!empty($productPrice)) {
                $price = (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? $productPrice['price'] : $productPrice['discount_price']) * $cartItem['quantity'];
                $total += $price;

            } else {
                $productPrice = $this->ProductsAttributes->find()->where(['product_id' => $cartItem['product_id'], 'attribute_id' => $cartItem['attribute_id'], 'vendor_id' => 0])->first();
                $price = (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? $productPrice['price'] : $productPrice['discount_price']) * $cartItem['quantity'];
                $total += $price;
            }
        }
        if (($coupon['applicable_to'] == 'delivery_charge')) {
            $data['applicable'] = true;
            $data['deliveryDiscount'] = 0;
        } else {
            if ($coupon['applicable_type'] == 'amount') {
                if ($total >= $coupon['minimum_order_value']) {
                    $data['applicable'] = true;
                    if ($coupon['discount_type'] == 'cashback') {
                        $data['couponDiscount'] = null;
                        $data['cashback'] = $coupon['discount_rate'];
                    } else {
                        $data['couponDiscount'] = $coupon['discount_rate'];
                        $data['cashback'] = null;
                    }
                } else {
                    $data['applicable'] = false;
                    $data['message'] = "Coupon is applicable for minimum amount of Rs." . $coupon['minimum_order_value'] . " of selected products";
                }
            } else {
                if ($total >= $coupon['minimum_order_value']) {
                    $data['applicable'] = true;
                    $discount = ($total * $coupon['discount_rate']) / 100;
                    $discount = ($discount > $coupon['maximum_discount']) ? $coupon['maximum_discount'] : $discount;
                    if ($coupon['discount_type'] == 'cashback') {
                        $data['couponDiscount'] = null;
                        $data['cashback'] = $discount;
                    } else {
                        $data['couponDiscount'] = $discount;
                        $data['cashback'] = null;
                    }
                } else {
                    $data['applicable'] = false;
                    $data['message'] = "Coupon is applicable for minimum purchase of Rs." . $coupon['minimum_order_value'] . " of selected products";
                }
            }
        }
        return $data;
    }
    public function onlinePaymentVerification()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['orderId']) || empty($this->request->data['onlineOrderId']) || empty($this->request->data['userId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            if (empty($this->request->data['onlinePaymentId']) || empty($this->request->data['onlineSignature'])) {
                $response['success'] = false;
                $response['message'] = "Payment Failed";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            extract($this->request->data);
            $api = new Api(RAZOR_API_KEY, RAZOR_API_SECRET);
            $attributes = array(
                'razorpay_order_id' => $onlineOrderId,
                'razorpay_payment_id' => $onlinePaymentId,
                'razorpay_signature' => $onlineSignature,
            );
            try {
                $paymentStatus = $api->utility->verifyPaymentSignature($attributes);
            } catch (SignatureVerificationError $e) {
                $success = false;
                $response['message'] = "Invalid Signatures";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $razorOrder = $api->order->fetch($onlineOrderId);
            if (!empty($this->request->data['onlinePaymentId']) && $razorOrder->status == 'paid') {
                $order = $this->Orders->find()->where(['id' => $this->request->data['orderId']])->first();
                if ($order->wallet == 1) {
                    try {
                        $wallets = $this->Wallets->newEntity();
                        $wallets['order_id'] = $order->id;
                        $wallets['amount_type'] = 'expense';
                        $wallets['wallet_id'] = $order->wallet_id;
                        $wallets['amount'] = $order->wallet_amount;
                        $wallets['user_id'] = $this->request->data['userId'];
                        $newWallets = $this->Wallets->save($wallets);
                    } catch (\PDOException $e) {
                        pr($e);die;
                    }
                    if (!$newWallets) {
                        $response['success'] = false;
                        $response['message'] = 'Error while saving your order';
                        $this->response->type('application/json');
                        $this->response->body(json_encode($response));
                        return $this->response;
                    }
                }
                if ($order->cashback != 0 && $order->coupon_id != "") {
                    $coupon = $this->Coupons->find()->where(['id' => $order->coupon_id])->first();
                    $wallet = $this->Wallets->newEntity();
                    $wallet['order_id'] = $order->id;
                    $wallet['user_id'] = $this->request->data['userId'];
                    $wallet['amount'] = $order->cashback;
                    $wallet['is_expiry'] = 1;
                    $wallet['coupon_id'] = $order->coupon_id;
                    $wallet['amount_type'] = 'promotional';
                    $wallet['expiry_date'] = $coupon->cashback_expiry_date;
                    $wallet['max_redeem_type'] = $coupon->max_redeem_type;
                    $wallet['max_redeem_rate'] = $coupon->max_redeem_rate;
                    $this->Wallets->save($wallet);
                }
                $order->transaction_id = $onlinePaymentId;
                $order->razorpay_payment_id = $onlinePaymentId;
                $order->razorpay_signature = $onlineSignature;
                $order->transaction_amount = ($razorOrder->amount_paid) / 100;
                $order->payment_status = "approved";
                $this->Orders->save($order);
                $this->Carts->deleteAll(['user_id' => $userId]);
                $response['success'] = true;
                $response['cartCount'] = 0;
                $response['message'] = 'Order Saved Successfully';
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;

            }
            $order->transaction_id = $payment->id;
            $order->payment_status = "rejected";
            if ($this->Orders->save($order)) {
                $response['success'] = true;
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = false;
            $response['message'] = "Please try after sometime";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function walletOptionsList()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId']) || empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $userId = $this->request->data['userId'];
            $vendorId = $this->request->data['vendorId'];
            $cartItems = $this->Carts->find()->contain(['Products', 'ChildAttributes'])->where(['user_id' => $userId, 'Carts.vendor_id' => $vendorId])->toarray();
            $total = 0;
            foreach ($cartItems as $cartItem) {
                $productPrice = $this->ProductsAttributes->find()->where(['product_id' => $cartItem['product_id'], 'attribute_id' => $cartItem['attribute_id'], 'vendor_id' => $cartItem['vendor_id']])->first();

                if (!empty($productPrice)) {
                    $price = (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? $productPrice['price'] : $productPrice['discount_price']) * $cartItem['quantity'];
                    $total += $price;

                } else {
                    $productPrice = $this->ProductsAttributes->find()->where(['product_id' => $cartItem['product_id'], 'attribute_id' => $cartItem['attribute_id'], 'vendor_id' => 0])->first();
                    $price = (empty($productPrice['discount_price']) || $productPrice['discount_price'] == 0 ? $productPrice['price'] : $productPrice['discount_price']) * $cartItem['quantity'];
                    $total += $price;
                }
            }
            $response['success'] = true;
            $today = date('Y-m-d');
            $payments = $this->Wallets->WalletIncomes->find()->select(['sum' => 'SUM(amount)'])->where(['user_id' => $userId, 'status' => 1])->first()->sum;
            $paymentUsed = $this->Wallets->WalletExpenses->find()->select(['sum' => 'SUM(amount)'])->where(['user_id' => $userId, 'status' => 1, 'wallet_id' => 0])->first()->sum;
            if ((INT) $payments - (INT) $paymentUsed > 0) {
                $response['walletInfo']['greenCash']['id'] = 0;
                $response['walletInfo']['greenCash']['balance'] = (INT) $payments - (INT) $paymentUsed;
            } else {
                $response['walletInfo']['greenCash'] = null;
            }

            $referralRewards = $this->Wallets->find()->where(['user_id' => $userId, 'amount_type' => 'referral', 'status' => 1])->toarray();
            $cashbackDet = null;
            foreach ($referralRewards as $referralReward) {
                $data = [];
                $balance = "";
                $referralRewardUsed = $this->Wallets->WalletExpenses->find()->select(['sum' => 'SUM(amount)'])->where(['user_id' => $userId, 'status' => 1, 'wallet_id' => $referralReward['id']])->first()->sum;
                $balance = (INT) $referralReward['amount'] - (INT) $referralRewardUsed;

                if ($balance <= 0) {
                    continue;
                }
                $data['id'] = $referralReward['id'];
                $data['name'] = "Referral Balance";
                $data['balance'] = $balance;
                $data['usable'] = $balance;
                $data['expiryDate'] = null;

                $cashbackDet[] = $data;
            }

            $cashbacks = $this->Wallets->CashBacks->find()->where(['user_id' => $userId, 'DATE(expiry_date) >=' => $today, 'status' => 1])->toarray();
            foreach ($cashbacks as $cashback) {
                $data = [];
                $balance = "";
                $cashbackUsed = $this->Wallets->WalletExpenses->find()->select(['sum' => 'SUM(amount)'])->where(['user_id' => $userId, 'status' => 1, 'wallet_id' => $cashback['id']])->first()->sum;
                $balance = (INT) $cashback['amount'] - (INT) $cashbackUsed;

                if ($balance <= 0) {
                    continue;
                }
                $data['id'] = $cashback['id'];
                $data['name'] = "Coupon Balance";
                $data['expiryDate'] = date('d M Y', strtotime($cashback['expiry_date']));
                $data['balance'] = $balance;
                if ($cashback['max_redeem_type'] == 'percentage') {
                    $data['usable'] = ($total * $cashback['max_redeem_rate']) / 100;
                } else {
                    $data['usable'] = $balance;
                }
                if ($data['usable'] > $balance) {
                    $data['usable'] = $balance;
                }
                $cashbackDet[] = $data;
            }
            $response['walletInfo']['cashbacks'] = $cashbackDet;
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function viewWalletTransaction()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $user = $this->Users->find()->where(['id' => $this->request->data['userId']])->first();
            if (empty($user)) {
                $response['success'] = false;
                $response['message'] = "Invalid User Id";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = true;
            $today = date('Y-m-d');
            $payments = $this->Wallets->WalletIncomes->find()->select(['sum' => 'SUM(amount)'])->where(['user_id' => $this->request->data['userId'], 'status' => 1])->first()->sum;
            $paymentUsed = $this->Wallets->WalletExpenses->find()->select(['sum' => 'SUM(amount)'])->where(['user_id' => $this->request->data['userId'], 'status' => 1, 'wallet_id' => 0])->first()->sum;
            $response['walletInfo']['balance'] = (INT) $payments - (INT) $paymentUsed;
            $wallets = $this->Wallets->find()->where(['user_id' => $this->request->data['userId']])->order(['id' => 'DESC'])->toarray();
            if (empty($wallets)) {
                $response['walletInfo']['walletTransaction'] = null;
                $response['message'] = "No Transaction Found";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            foreach ($wallets as $wallet) {
                $data = [];
                $data['title'] = null;
                $data['type'] = null;
                $data['orderId'] = null;
                $data['description'] = null;
                $data['date'] = date('d M Y, h:i A', strtotime($wallet['created']));
                if ($wallet['amount_type'] == 'payment') {
                    $data['type'] = 'income';
                    $data['title'] = 'Payment';
                } else if ($wallet['amount_type'] == 'expense' && $wallet['wallet_id'] == 0) {
                    $data['title'] = 'Purchase';
                    $data['type'] = 'expense';
                    $data['orderId'] = $wallet['order_id'];
                } else if ($wallet['amount_type'] == 'expense' && $wallet['wallet_id'] != 0) {
                    $data['title'] = 'Purchase from Cashback';
                    $data['type'] = 'expense';
                    $data['orderId'] = $wallet['order_id'];
                } else if ($wallet['amount_type'] == 'promotional') {
                    $data['title'] = 'Cashback';
                    $data['type'] = 'income';
                    $data['expiryDate'] = date('d M Y', strtotime($wallet['expiry_date']));
                    $data['orderId'] = $wallet['order_id'];
                } else if ($wallet['amount_type'] == 'admin') {
                    $data['type'] = 'income';
                    $data['title'] = 'Payment';
                    $data['description'] = $wallet['description'];
                } else if ($wallet['amount_type'] == 'referral') {
                    $data['type'] = 'income';
                    $data['title'] = 'Referral Reward';
                    $data['description'] = $wallet['description'];
                }
                $data['amount'] = $wallet['amount'];
                $response['walletInfo']['walletTransaction'][] = $data;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function home()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId']) || empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $vendorId = $this->request->data['vendorId'];
            $user = $this->Users->find()->where(['id' => $this->request->data['userId']])->first();
            if (empty($user)) {
                $response['success'] = false;
                $response['message'] = "Invalid User Id";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = true;
            $response['output']['orderAcceptance']['arrangementOrder'] = 2;
            $response['output']['orderAcceptance']['status'] = true;
            $response['output']['slider'] = null;
            $response['output']['shopByCategory'] = null;
            $response['output']['topSavingProducts'] = null;
            $response['output']['banner'] = null;
            $sliders = $this->Sliders->find()->where(['OR' => [['vendor_id' => 0], ['vendor_id' => $this->request->data['vendorId']]], 'type' => 'home slider', 'status' => 'Y'])->toarray();
            if (!empty($sliders)) {
                $response['output']['slider']['arrangementOrder'] = 1;
                foreach ($sliders as $slider) {
                    $sliderData = [];
                    $sliderData['images'] = SITE_URL . 'images/sliders/' . $slider['image'];
                    if($slider['is_click'] == 1){
                    if($slider['action']=='category'){
                        $sliderData['url']='category';
                        $sliderData['detailId']=$slider['category_id'];
                    }else if($slider['action']=='product'){
                    $sliderData['url']='product';
                    $sliderData['detailId']=$slider['product_id'];
                    }else{
                        $sliderData['url']=null;
                        $sliderData['detailId']=null; 
                    }
                }else{
                    $sliderData['url']=null;
                    $sliderData['detailId']=null;
                }
                    $response['output']['slider']['images'][] = $sliderData;
                }
            }
            $vendorCategories = $this->VendorCategories->find('list', ['keyField' => 'id', 'valueField' => 'category_id'])->where(['vendor_id' => $this->request->data['vendorId']])->toarray();
            if (!empty($vendorCategories)) {
                $categories = $this->Categories->ParentCategories->find()->contain(['ChildCategories' => function ($q) {
                    return $q->where(['ChildCategories.status' => 'Y']);
                }])->where(['ParentCategories.status' => 'Y', 'ParentCategories.id IN' => $vendorCategories])->toarray();
                if (!empty($categories)) {
                    $response['output']['shopByCategory']['arrangementOrder'] = 3;
                    foreach ($categories as $category) {
                        $data = [];
                        $data['id'] = $category['id'];
                        $data['title'] = $category['name'];
                        $data['backgroundColor'] = '#ffc4f3';
                        $data['categoryIcon'] = SITE_URL . 'categoryicon/small/' . $category['cat_icon'];
                        $data['discount'] = (!empty($category['discount'])) ? true : false;
                        $data['discountRate'] = (!empty($category['discount']) && $category['discount'] != 0) ? $category['discount'] : null;
                        $response['output']['shopByCategory']['categories'][] = $data;
                    }
                }
            }
            $products = $this->Products->find('all')->contain(['Attributes' => function ($q) use ($vendorId) {
                return $q->where(['OR' => [['ProductsAttributes.vendor_id' => '0'], ['ProductsAttributes.vendor_id' => $vendorId]], 'is_stock' => 'Y'])->order(['ProductsAttributes.sort' => 'DESC']);
            }])->matching('ProductTopSavings', function ($q) use ($vendorId) {
                return $q->where(['ProductTopSavings.vendor_id' => $vendorId]);
            })->where(['OR' => [['Products.vendor_id' => 0], ['Products.vendor_id' => $vendorId]], 'Products.status' => 'Y'])->order(['Products.productnameen' => 'ASC'])->toarray();
            if (!empty($products)) {
                $response['output']['topSavingProducts']['arrangementOrder'] = 4;
                $attributes = $this->Attributes->ParentAttributes->find('list')->toarray();
                foreach ($products as $product) {
                    if (empty($product['attributes'])) {
                        continue;
                    }
                    if ($product['attributes'][0]['_joinData']['vendor_id'] == 0) {
                        $vendorAttrExist = $this->ProductsAttributes->find()->where(['vendor_id' => $vendorId, 'attribute_type' => $product['attributes'][0]['parent_id'], 'attribute_id' => $product['attributes'][0]['id'], 'product_id' => $product['id']])->first();
                        if ($vendorAttrExist) {
                            $product['attributes'][0]['_joinData']['price'] = $vendorAttrExist['price'];
                            $product['attributes'][0]['_joinData']['discount_price'] = $vendorAttrExist['discount_price'];
                        }
                    }
                    $data = [];
                    $data['productId'] = $product['id'];
                    $data['productName'] = $product['productnameen'];
                    $data['size'] = $product['attributes'][0]['name'] . ' ' . $attributes[$product['attributes'][0]['parent_id']];
                    $data['sizeId'] = $product['attributes'][0]['id'];
                    $data['sizeType'] = $product['attributes'][0]['parent_id'];
                    $data['price']['actualPrice'] = $product['attributes'][0]['_joinData']['price'];
                    $data['price']['discountPrice'] = !empty($product['attributes'][0]['_joinData']['discount_price']) || $product['attributes'][0]['_joinData']['discount_price'] != 0 ? $product['attributes'][0]['_joinData']['discount_price'] : null;
                    $data['price']['discountPercent'] = empty($data['price']['discountPrice']) || $data['price']['discountPrice'] == 0 ? null :
                    floor(($data['price']['actualPrice'] - $data['price']['discountPrice']) * 100 / $data['price']['actualPrice']);
                    $data['image'] = !empty($product['image']) ? 'http://dc84viddw6ggp.cloudfront.net/products/' . $product['image'] : PRODUCT_DEFAULT_IMAGE;
                    $response['output']['topSavingProducts']['products'][] = $data;
                }
            }
            $banners = $this->Sliders->find()->where(['OR' => [['vendor_id' => 0], ['vendor_id' => $this->request->data['vendorId']]], 'type' => 'home images', 'status' => 'Y'])->toarray();
            if (!empty($banners)) {
                $response['output']['banner']['arrangementOrder'] = 5;
                foreach ($banners as $banner) {
                    $bannerData = [];
                    $bannerData['image'] = SITE_URL . 'images/sliders/' . $banner['image'];
                    if($banner['is_click'] == 1){
                        if($banner['action']=='category'){
                            $bannerData['url']='category';
                            $bannerData['detailId']=$banner['category_id'];
                        }else if($banner['action']=='product'){
                        $bannerData['url']='product';
                        $bannerData['detailId']=$banner['product_id'];
                        }else{
                            $bannerData['url']=null;
                            $bannerData['detailId']=null; 
                        }
                    }else{
                        $bannerData['url']=null;
                        $bannerData['detailId']=null;
                    }
                }
                $response['output']['banner']['banners'][] = $bannerData;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function searchProduct()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId']) || empty($this->request->data['keyword']) || empty($this->request->data['vendorId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            extract($this->request->data);
            $user = $this->Users->find()->where(['id' => $userId])->first();
            if (empty($user)) {
                $response['success'] = false;
                $response['message'] = "Invalid User Id";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $attributes = $this->Attributes->ParentAttributes->find('list')->toarray();
            $products = $this->Products->find('all')->contain(['Attributes' => function ($q) use ($vendorId) {
                return $q->where(['OR' => [['ProductsAttributes.vendor_id' => '0'], ['ProductsAttributes.vendor_id' => $vendorId]], 'is_stock' => 'Y'])->order(['ProductsAttributes.sort' => 'DESC']);
            }])->where(['OR' => [['Products.vendor_id' => 0], ['Products.vendor_id' => $vendorId]], 'Products.status' => 'Y', 'Products.productnameen LIKE' => '%' . $keyword . '%'])->order(['Products.productnameen' => 'ASC'])->toarray();
            if (empty($products)) {
                $response['success'] = false;
                $response['message'] = "No Products Available";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->loadModel('UserSearches');
            $newSearch = $this->UserSearches->newEntity();
            $newSearch['user_id'] = $userId;
            $newSearch['keyword'] = $keyword;
            $this->UserSearches->save($newSearch);
            $response['success'] = true;
            foreach ($products as $product) {
                if (empty($product['attributes'])) {
                    continue;
                }
                $data = [];
                $data['productId'] = $product['id'];
                $data['productName'] = $product['productnameen'];
                foreach ($product['attributes'] as $attribute) {
                    if ($attribute['_joinData']['vendor_id'] == 0) {
                        $vendorAttrExist = $this->ProductsAttributes->find()->where(['vendor_id' => $this->request->data['vendorId'], 'attribute_type' => $attribute['parent_id'], 'attribute_id' => $attribute['id'], 'product_id' => $product['id']])->first();
                        if ($vendorAttrExist) {
                            continue;
                        }
                    }
                    $productData = [];
                    $productData['size'] = $attribute['name'] . ' ' . $attributes[$attribute['parent_id']];
                    $productData['sizeId'] = $attribute['id'];
                    $productData['price']['actualPrice'] = $attribute['_joinData']['price'];
                    $productData['price']['discountPrice'] = !empty($attribute['_joinData']['discount_price']) || $attribute['_joinData']['discount_price'] != 0 ? $attribute['_joinData']['discount_price'] : null;
                    $productData['price']['discountPercent'] = empty($productData['price']['discountPrice']) || $productData['price']['discountPrice'] == 0 ? null :
                    floor(($productData['price']['actualPrice'] - $productData['price']['discountPrice']) * 100 / $productData['price']['actualPrice']);
                    $data['productSizes'][] = $productData;
                }
                $data['image'] = !empty($product['image']) ? 'http://dc84viddw6ggp.cloudfront.net/products/' . $product['image'] : PRODUCT_DEFAULT_IMAGE;
                $response['productInfo'][] = $data;
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function searchHistory()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            extract($this->request->data);
            $user = $this->Users->find()->where(['id' => $userId])->first();
            if (empty($user)) {
                $response['success'] = false;
                $response['message'] = "Invalid User Id";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $this->loadModel('UserSearches');
            $response['success'] = true;
            $response['searchHistory'] = null;
            $response['trendingSearch'] = null;
            $history = $this->UserSearches->find()->where(['user_id' => $userId])->order(['id' => 'DESC'])->limit(5)->toarray();
            if (!empty($history)) {
                foreach ($history as $search) {
                    $response['searchHistory'][] = $search['keyword'];
                }
            }
            $trends = $this->UserSearches->find()->where(['user_id <>' => $userId])->order(['id' => 'DESC'])->limit(5)->toarray();
            if (!empty($trends)) {
                foreach ($trends as $trend) {
                    $response['trendingSearch'][] = $trend['keyword'];
                }
            }
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function referralCode()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            extract($this->request->data);
            $user = $this->Users->find()->where(['id' => $userId])->first();
            if (empty($user)) {
                $response['success'] = false;
                $response['message'] = "Invalid User Id";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            $response['success'] = true;
            $response['output'] = [];
            $response['output']['referralCode'] = $user['referral_code'];
            $response['output']['referralAmount'] = $this->Users->find()->where(['role_id' => 1])->first()->referred_reward;
            $response['output']['shareInfo']['title'] = "Rashan Duniya";
            $response['output']['shareInfo']['message'] = "Hey, I buy groceries at the lowest prices from Rashan Duniya, now you can too! Use my referral code " . $user['referral_code'] . " while signing up";
            $response['output']['shareInfo']['appUrl'] = "https://play.google.com/store/apps/details?id=in.daaconline.doomshell&hl=en_IN";
            $response['output']['shareInfo']['image']['url'] = "https://rashanduniya.com/notifications/76be372924908a181df43fb433f03c9f143731960.jpg";
            $response['output']['shareInfo']['image']['extension'] = "jpeg";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;

        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

    public function cancelOrder()
    {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            $response = array();
            if (empty($this->request->data['userId']) || empty($this->request->data['orderId'])) {
                $response['success'] = false;
                $response['message'] = "Invalid Parameters";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
            extract($this->request->data);
            $order = $this->Orders->find()->where(['user_id' => $userId, 'id' => $orderId])->first();
            $order['order_status'] = 'cancelled';
            if ($this->Orders->save($order)) {
                if ($order['transaction_mode'] == 'online') {
                    $api = new Api(RAZOR_API_KEY, RAZOR_API_SECRET);
                    $razorpayRefund = $api->refund->create(array('payment_id' => $order['razorpay_payment_id']));
                    $refund = $this->Refunds->newEntity();
                    $refund['user_id'] = $order['user_id'];
                    $refund['order_id'] = $order['id'];
                    $refund['razorpay_refund_id'] = $razorpayRefund->id;
                    $refund['refund_amount'] = $razorpayRefund->amount;
                    $this->Refunds->save($refund);
                }
                if ($order['wallet'] == 1) {
                    if ($order['wallet_id'] == 0) {
                        $newWallet = $this->Wallets->newEntity();
                        $newWallet['transaction_id'] = 0;
                        $newWallet['wallet_id'] = 0;
                        $newWallet['order_id'] = $order['id'];
                        $newWallet['user_id'] = $order['user_id'];
                        $newWallet['is_expiry'] = 0;
                        $newWallet['amount_type'] = 'payment';
                        $newWallet['description'] = 'refund';
                        $newWallet['status'] = 1;
                        $this->Wallets->save($newWallet);
                    } else {
                        $newWallet = $this->Wallets->find()->where(['parent_id' => $order['wallet_id'], 'order_id' => $order['id']])->first();
                        $newWallet['status'] = 0;
                        $this->Wallets->save($newWallet);
                    }
                }   
                $response['success'] = true;
                $response['message'] = "Order Cancelled Successfully";
                $this->response->type('application/json');
                $this->response->body(json_encode($response));
                return $this->response;
            }
        } else {
            $response['success'] = false;
            $response['message'] = "Invalid Data Type";
            $this->response->type('application/json');
            $this->response->body(json_encode($response));
            return $this->response;
        }
    }

}
