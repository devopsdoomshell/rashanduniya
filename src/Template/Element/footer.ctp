<footer>
<div class="container-fluid">
<div>

<div>
<ul class="iconsfooter text-center s-icon">
<li><a target="_blank" href="https://twitter.com/HouseVeggie"><i class="fa fa-3x fa-twitter" aria-hidden="true"></i></a></li>
<li><a target="_blank" href="https://www.facebook.com/Veggie-Green-House-2309549549268176/?modal=admin_todo_tour
"><i class="fa fa-3x fa-facebook-official" aria-hidden="true"></i></a></li>
<li><a target="_blank" href="https://www.pinterest.co.uk/veggiegreenhouse/"><i class="fa fa-3x fa-pinterest" aria-hidden="true"></i></a></li>
<li><a target="_blank" href="https://www.instagram.com/veggiegreenhouse/"><i class="fa fa-3x fa-instagram" aria-hidden="true"></i></a></li>
</ul>

<nav>
<ul class="list-inline text-center">
<!--<li><a href="contact_us.html">Contact</a></li>-->
<li><a href="<?php echo SITE_URL ?>faq">FAQ</a></li>
<li><a href="<?php echo SITE_URL ?>privacy">Privacy Policy</a></li>
<li><a href="<?php echo SITE_URL ?>terms">Terms & Condition</a></li>    
<li><a href="<?php echo SITE_URL ?>homes/refundpolicy">Refund Policy</a></li> 
<li><a href="<?php echo SITE_URL ?>homes/returnpolicy">Return Policy</a></li> 
<!--<li><a href="<?php echo SITE_URL ?>contact_us">Contact-Us</a></li>  -->   
</ul>
</nav>
<p class="text-center"><a href="<?php echo SITE_URL ?>">&copy; Veggiegreenhouse. </a>All Copyrights Reserved</p>


</footer>

  
</body>
</html>
