        <!-- Left Panel -->

        <aside id="left-panel" class="left-panel">
        	<nav class="navbar navbar-expand-sm navbar-default">

        		<div class="navbar-header">
        			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
        				<i class="fa fa-bars"></i>
        			</button>

        			<a class="navbar-brand" style="padding: 10px;" href="<?php echo SITE_URL;?>" target="_blank"><img src="<?php  echo SITE_URL ?>img/logo_new.png"/></a>
        			<a class="navbar-brand hidden" href="./dashboard"><img style="width: 68px;
height: 30px;" src="<?php  echo SITE_URL ?>img/logo_new.png"/></a>
        		</div>

        		<div id="main-menu" class="main-menu collapse navbar-collapse">
        			<ul class="nav navbar-nav">
        				<li class="active">
        					<a href="<?php echo SITE_URL; ?>admin/dashboard" title="Dashboard"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
        				</li>

                        <li class="active">
                            <a href="<?php echo SITE_URL; ?>admin/customer" title="product"> <i class="menu-icon fa fa-user"></i>Retailer Manager </a>
                        </li>
                        
                        <li class="active">
                            <a href="<?php echo SITE_URL; ?>admin/product" title="product"> <i class="menu-icon fa fa-calendar-o"></i>Product Manager </a>
                        </li>
                        
        			
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->
