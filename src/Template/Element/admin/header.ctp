<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Rashanduniya.com</title>
  <meta name="description" content="Sufee Admin - HTML5 Admin Template">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel='shortcut icon' href='<?php echo SITE_URL ?>favicon/favicon.ico'/>
  <!-- <link rel="shortcut icon" href="favicon.ico">-->
<!--   <link href="<?php echo SITE_URL ?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all"> -->


  <link rel="stylesheet" href="<?php echo SITE_URL ?>css/admin/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo SITE_URL ?>css/admin/css/normalize.css">
  <link rel="stylesheet" href="<?php echo SITE_URL ?>css/admin/css/bootstrap.min.css">
  <link href="<?php echo SITE_URL; ?>css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">
   <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" type="text/css" rel="stylesheet"/>

  <link rel="stylesheet" href="<?php echo SITE_URL ?>css/admin/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo SITE_URL ?>css/admin/css/themify-icons.css">
  <link rel="stylesheet" href="<?php echo SITE_URL ?>css/admin/css/flag-icon.min.css">

  <link rel="stylesheet" href="<?php echo SITE_URL ?>css/admin/css/cs-skin-elastic.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
  <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
  <link href="<?php echo SITE_URL ?>datepicker/bootstrap-formhelpers.min.css" rel="stylesheet" type="text/css"/>
  <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />



  <link rel="stylesheet" href="<?php echo SITE_URL ?>css/admin/scss/style.css">


  <link href="<?php echo SITE_URL ?>css/admin/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

  <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" crossorigin="anonymous"></script> -->
    <script src="<?php echo SITE_URL ?>datepicker/jquery-3.2.1.min.js"></script>
    <!-- <script src="<?php echo SITE_URL ?>datepicker/bootstrap.min.js" ></script> -->

  <script src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<!--     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script> -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js" type="text/javascript"></script>
     <script rel="stylesheet" href="<?php echo SITE_URL ?>js/admin/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
  <script>
var csrfToken = <?=json_encode($this->request->getParam('_csrfToken'))?>;
</script>



  <style type="text/css">
    .dropdown-menu .nav-link{ text-align: left; }
    header .user-area .user-menu, header .dropdown-menu{-webkit-box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.38);
-moz-box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.38);
box-shadow: 0px 0px 9px 0px rgba(0,0,0,0.38);}
header .user-area{ margin-top: 30px }
    header a.nav-link img{ display: block;  text-align: center; margin: auto; height: 25px;}
    .nav-pills .nav-link.active, .nav-pills .show>.nav-link{ background-color: #b8d032 !important; }
    header a.nav-link{ text-align: center; }
  .imgg{display: inline-block;}
  .form-control {
    font-size: 13px !important;
    border-radius: 0px !important;
    padding: 8px 4px !important; }
    .navaline{
      padding-top: 2%;
    }
    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
      color: #fff;
      background-color: #0c801a;
    }

    body {
      font-size: 12px !important;
    }
    .page-header {
     font-size: 16px !important;
   }

.dropdown-menu{
  top:100% !important ;
  border: 0px solid rgba(0,0,0,.15) !important;
  }
.dropdown-menu .nav-link{
  font-size: 14px;
    padding: 8px;
  }


/*=======================================All Searching input designs=============================
 * ================================================================================================
 * ===================================================================================================*/

 #myUL, #testUL, #retail{
    position: relative !important;
    z-index: 999 !important;
  }
  #myUL ul, #testUL ul, #retail ul {
    list-style-type: none;
    overflow-y: scroll;
    margin: 0;
    padding: 0;
    max-height:150px !important;
    position: absolute;
    width: 100% !important;
    background-color: #eaeaea !important;
  }

  #myUL li, #testUL li, #retail li {
    font-size:13px !important;
    border-bottom: 0px solid #ccc !important;

  }

  #myUL li  li:last-child {
    border: none;
  }

  #testUL li  li:last-child {
    border: none;
  }

  #retail li  li:last-child {
    border: none;
  }

  #myUL li  a, #testUL li a, #retail li a {
    text-decoration: none;
    color: #000;
    display: block;
    padding:  8px 13px !important;
    width: 100% !important;

    -webkit-transition: font-size 0.3s ease, background-color 0.3s ease;
    -moz-transition: font-size 0.3s ease, background-color 0.3s ease;
    -o-transition: font-size 0.3s ease, background-color 0.3s ease;
    -ms-transition: font-size 0.3s ease, background-color 0.3s ease;
    transition: font-size 0.3s ease, background-color 0.3s ease;
  }

   #myUL li  a:hover, #testUL li a:hover, #retail li a:hover {background-color: #ddd;}
}
 </style>

  </head>
  <body class="open" onload="initialize()">
    <!-- Left Panel -->
    <?php $controll = $this->request->params['controller'];?>
    <?php // echo $this->element('admin/left'); ?>
        <?php $role_id = $this->request->session()->read('Auth.User.role_id');?>
    <div id="right-panel" class="right-panel">

      <!-- Header-->
      <?php if ($role_id == '1' || $role_id == 101) {?>
      <header id="header" class="header">

        <div class="header-menu">

          <div class="col-sm-11">
           <!-- <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>-->
           <div class="row">
            <div class="col-sm-2">
            <a href="<?php echo SITE_URL; ?>admin/dashboard"> <img class="" src="<?php echo SITE_URL ?>img/logo_new.png" style="width: 55%;" /> </a>
           </div>
           <div class="col-sm-10 menu">
            <ul class="nav nav-pills nav-pills-success navaline">

              <li class="menu-item">
              </li>
              <li class="nav-item dropdown">
                <a href="#"
                class="nav-link dropdown-toggle
                <?php if ($controll == "product" || $controll == "Categories" || $controll == "brand" || $controll == "vendors" || $controll == "Slot") {echo "active";}?>
                " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <!-- <i class="menu-icon fa fa-calendar-o"></i> --><img class="" src="<?php echo SITE_URL ?>img/admin_head11.png"/>Master Manager
              </a>
              <ul class="dropdown-menu" >
                <?php if ($role_id == 1) {?>
                <li>
                  <a href="<?php echo SITE_URL; ?>admin/brand" class="nav-link <?php
if ($this->request->params['controller'] == "Brand") {echo "active";}?>" title="Washer"> <i class="menu-icon fa fa-calendar-o"></i>Brand Manager</a>
                </li>
                <li>
                  <a href="<?php echo SITE_URL; ?>admin/Categories" class="nav-link <?php
if ($this->request->params['controller'] == "Categories") {echo "active";}?>" title="Washer"> <i class="menu-icon fa fa-calendar-o"></i>Category Manager</a>
                </li>
                <li>
                  <a href="<?php echo SITE_URL; ?>admin/attributes" class="nav-link <?php
if ($this->request->params['controller'] == "Categories") {echo "active";}?>" title="Washer"> <i class="menu-icon fa fa-calendar-o"></i>Attribute Manager</a>
                </li>
                <?php }?>

                </ul>
              </li>
                <li class="nav-item">
                <a href="<?php echo SITE_URL; ?>admin/product" class="nav-link <?php if ($this->request->params['controller'] == "Product") {echo "active";}?>" title="product"> <!-- <i class="menu-icon fa fa-calendar-o"></i> --> <img class="" src="<?php echo SITE_URL ?>img/admin_head8.png"/>Product</a>
              </li>
              <li>
                  <a href="<?php echo SITE_URL; ?>admin/vendors" class="nav-link <?php
if ($this->request->params['controller'] == "vendors") {echo "active";}?>" title="Vendor"> <!-- <i class="menu-icon fa fa-calendar-o"></i> --><img class="" src="<?php echo SITE_URL ?>img/admin_head9.png"/>Vendors</a>
                </li>
<li> <a href="<?php echo SITE_URL; ?>admin/Coupons" class="nav-link <?php

    if ($this->request->params['controller'] == "Coupons") {echo "active";}?>" title="Coupons"> <!-- <i class="menu-icon fa fa-calendar-o"></i> --><img class="" src="<?php echo SITE_URL ?>img/admin_head2.png"/>Coupons</a></li>
              <li>
                  <a href="<?php echo SITE_URL; ?>admin/customers" class="nav-link <?php
if ($this->request->params['controller'] == "customers") {echo "active";}?>" title="Customers"> <!-- <i class="menu-icon fa fa-calendar-o"></i> --><img class="" src="<?php echo SITE_URL ?>img/admin_head3.png"/>Customers</a>
                </li>
              <li>
                  <a href="<?php echo SITE_URL; ?>admin/Reports/paymenthistory" class="nav-link <?php
if ($this->request->params['controller'] == "Reports" && $this->request->params['action'] == "paymenthistory") {echo "active";}?>" title="Vendor Payments"> <!-- <i class="menu-icon fa fa-calendar-o"></i> --><img class="" src="<?php echo SITE_URL ?>img/admin_head3.png"/>Vendor Payments</a>
                </li>
              <li>
                  <a href="<?php echo SITE_URL; ?>admin/Reports/vendorledger" class="nav-link <?php
if ($this->request->params['controller'] == "Reports" && $this->request->params['action'] == "vendorledger") {echo "active";}?>" title="Vendor Ledger"> <!-- <i class="menu-icon fa fa-calendar-o"></i> --><img class="" src="<?php echo SITE_URL ?>img/admin_head3.png"/>Ledger</a>
                </li>
              <li>
                  <a href="<?php echo SITE_URL; ?>admin/Orders" class="nav-link <?php
if ($this->request->params['controller'] == "Orders") {echo "active";}?>" title="Orders"> <!-- <i class="menu-icon fa fa-calendar-o"></i> --><img class="" src="<?php echo SITE_URL ?>img/admin_head3.png"/>Orders</a>
                </li>

                <li><a href="<?php echo SITE_URL; ?>admin/wallets" class="nav-link <?php
if ($this->request->params['controller'] == "Wallets") {echo "active";}?>" title="Wallet"> <!-- <i class="menu-icon fa fa-calendar-o"></i> --><img class="" src="<?php echo SITE_URL ?>img/admin_head10.png"/>Wallet</a></li>

                <li><a href="<?php echo SITE_URL; ?>admin/Sliders" class="nav-link <?php
if ($this->request->params['controller'] == "Sliders") {echo "active";}?>" title="Sliders"> <!-- <i class="menu-icon fa fa-calendar-o"></i> --><img class="" src="<?php echo SITE_URL ?>img/admin_head10.png"/>Sliders</a></li>



            </ul>
          </div>
        </div>

      </div>

      <div class="col-sm-1">
        <div class="user-area dropdown float-right">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="user-avatar rounded-circle" src="<?php echo SITE_URL ?>img/admin.jpeg"/>
          </a>

          <div class="user-menu dropdown-menu">
            <a class="nav-link" href="<?php echo SITE_URL; ?>admin/users/index/1"><i class="fa fa -cog"></i>Settings</a>
            <a class="nav-link" href="<?php echo SITE_URL; ?>logins/logout/"><i class="fa fa-power -off"></i>Logout</a>
          </div>
        </div>

      </div>
    </div>

  </header><!-- /header -->
  <?php }?>



<!--Role id 2-->


 <?php if ($role_id == '2') {?>
      <header id="header" class="header">

        <div class="header-menu">

          <div class="col-sm-11">
           <!-- <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>-->
           <div class="row">
            <div class="col-sm-2">
            <a href="<?php echo SITE_URL; ?>admin/dashboard"> <img class="" src="<?php echo SITE_URL ?>img/logo_new.png"  style="width: 55%;"/> </a>
           </div>
           <div class="col-sm-10">
            <ul class="nav nav-pills nav-pills-success navaline">

              <li class="nav-item">
                <a href="<?php echo SITE_URL; ?>admin/product" class="nav-link <?php if ($this->request->params['controller'] == "Product") {echo "active";}?>" title="product"> <!-- <i class="menu-icon fa fa-calendar-o"></i> --> <img class="" src="<?php echo SITE_URL ?>img/admin_head9.png"/>Product Manager </a>
              </li>

              <li> <a href="<?php echo SITE_URL; ?>admin/Coupons" class="nav-link <?php

    if ($this->request->params['controller'] == "Coupons") {echo "active";}?>" title="Coupons"> <!-- <i class="menu-icon fa fa-calendar-o"></i> --><img class="" src="<?php echo SITE_URL ?>img/admin_head2.png"/>Coupon Manager</a></li>
              <li> <a href="<?php echo SITE_URL; ?>admin/Slots" class="nav-link <?php

    if ($this->request->params['controller'] == "Slots") {echo "active";}?>" title="Slots"> <!-- <i class="menu-icon fa fa-calendar-o"></i> --><img class="" src="<?php echo SITE_URL ?>img/admin_head13.png"/>Slots Manager</a></li>

              <li> <a href="<?php echo SITE_URL; ?>admin/DeliveryBoys" class="nav-link <?php

    if ($this->request->params['controller'] == "DeliveryBoys") {echo "active";}?>" title="DeliveryBoys"> <!-- <i class="menu-icon fa fa-calendar-o"> --><img class="" src="<?php echo SITE_URL ?>img/admin_head12.png"/></i>DeliveryBoys Manager</a></li>
     <li>
                  <a href="<?php echo SITE_URL; ?>admin/Reports/vendorledger" class="nav-link <?php
if ($this->request->params['controller'] == "Reports" && $this->request->params['action'] == "vendorledger") {echo "active";}?>" title="Vendor Ledger"> <!-- <i class="menu-icon fa fa-calendar-o"></i> --><img class="" src="<?php echo SITE_URL ?>img/admin_head3.png"/>Vendor Ledger</a>
                </li>

              <!-- <li class="nav-item dropdown">
                <a href="<?php echo SITE_URL; ?>admin/wallet"
                class="nav-link <?php if ($controll == "Coupancode" || $controll == "Area" || $controll == "Wallet") {echo "active";}?>
                " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-calendar-o"></i>Wallet</a>
              </li> -->

            </ul>
          </div>
        </div>

      </div>

      <div class="col-sm-1">
        <div class="user-area dropdown float-right">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="user-avatar rounded-circle" src="<?php echo SITE_URL ?>images/admin.jpg"/>
          </a>

          <div class="user-menu dropdown-menu">
            <a class="nav-link" href="<?php echo SITE_URL; ?>admin/users/index/1"><i class="fa fa -cog"></i>Settings</a>
            <a class="nav-link" href="<?php echo SITE_URL; ?>logins/logout/"><i class="fa fa-power -off"></i>Logout</a>
          </div>
        </div>

      </div>
    </div>

  </header><!-- /header -->
  <?php }?>




  <!--Role id 3-->


 <?php if ($role_id == '3') {?>
      <header id="header" class="header">

        <div class="header-menu">

          <div class="col-sm-11">
           <!-- <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>-->
           <div class="row">
            <div class="col-sm-2">
            <a href="<?php echo SITE_URL; ?>admin/dashboard"> <img class="" src="<?php echo SITE_URL ?>img/logo_new.png"  style="width: 55%;"/> </a>
           </div>
           <div class="col-sm-10">
            <ul class="nav nav-pills nav-pills-success navaline">
             <li class="nav-item">
                <a href="<?php echo SITE_URL; ?>admin/stocks" class="nav-link <?php if ($this->request->params['controller'] == "Stocks") {echo "active";}?>" title="Stock"> <i class="menu-icon fa fa-calendar-o"></i>Stock Manager </a>
              </li>

            </ul>
          </div>
        </div>

      </div>

      <div class="col-sm-1">
        <div class="user-area dropdown float-right">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="user-avatar rounded-circle" src="<?php echo SITE_URL ?>images/admin.jpg"/>
          </a>

          <div class="user-menu dropdown-menu">

            <a class="nav-link" href="<?php echo SITE_URL; ?>logins/logout/"><i class="fa fa-power -off"></i>Logout</a>
          </div>
        </div>

      </div>
    </div>

  </header><!-- /header -->
  <?php }?>

  <!--Role id 4-->


 <?php if ($role_id == '4') {?>
      <header id="header" class="header">

        <div class="header-menu">

          <div class="col-sm-11">
           <!-- <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>-->
           <div class="row">
            <div class="col-sm-2">
            <a href="<?php echo SITE_URL; ?>admin/dashboard"> <img class="" src="<?php echo SITE_URL ?>img/logo_new.png"  style="width: 55%;"/> </a>
           </div>
           <div class="col-sm-10">
            <ul class="nav nav-pills nav-pills-success navaline">


             <li class="nav-item">
                <a href="<?php echo SITE_URL; ?>admin/orders" class="nav-link <?php if ($this->request->params['controller'] == "Orders") {echo "active";}?>" title="Order"> <i class="menu-icon fa fa-calendar-o"></i>Order Manager </a>
              </li>

            </ul>
          </div>
        </div>

      </div>

      <div class="col-sm-1">
        <div class="user-area dropdown float-right">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="user-avatar rounded-circle" src="<?php echo SITE_URL ?>images/admin.jpg"/>
          </a>

          <div class="user-menu dropdown-menu">

            <a class="nav-link" href="<?php echo SITE_URL; ?>logins/logout/"><i class="fa fa-power -off"></i>Logout</a>
          </div>
        </div>

      </div>
    </div>

  </header><!-- /header -->
  <?php }?>

<!-- <script>
$(document).ready(function(){
  $("header .dropdown ").click(function(){
    $("header .dropdown-menu").slideToggle();
  });


    $("header .dropdown.open").removeClass('open');

  });

</script> -->