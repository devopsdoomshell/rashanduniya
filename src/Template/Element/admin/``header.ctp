<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Veggiegreenhouse</title>
  <meta name="description" content="Sufee Admin - HTML5 Admin Template">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" href="apple-icon.png">
  <!-- <link rel="shortcut icon" href="favicon.ico">-->
  <link href="<?php  echo SITE_URL ?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">


  <link rel="stylesheet" href="<?php  echo SITE_URL ?>css/admin/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php  echo SITE_URL ?>css/admin/css/normalize.css">
  <link rel="stylesheet" href="<?php  echo SITE_URL ?>css/admin/css/bootstrap.min.css">

  <link rel="stylesheet" href="<?php  echo SITE_URL ?>css/admin/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php  echo SITE_URL ?>css/admin/css/themify-icons.css">
  <link rel="stylesheet" href="<?php  echo SITE_URL ?>css/admin/css/flag-icon.min.css">

  <link rel="stylesheet" href="<?php  echo SITE_URL ?>css/admin/css/cs-skin-elastic.css">
  <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
  <link href="<?php  echo SITE_URL ?>datepicker/bootstrap-formhelpers.min.css" rel="stylesheet" type="text/css"/> 


  <link rel="stylesheet" href="<?php  echo SITE_URL ?>css/admin/scss/style.css">
  

  <link href="<?php  echo SITE_URL ?>css/admin/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">

  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

  <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" crossorigin="anonymous"></script>
  <link href="<?php echo SITE_URL; ?>css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">

  <script src="<?php echo SITE_URL ?>datepicker/jquery-3.2.1.min.js"></script>
  <script src="<?php echo SITE_URL ?>js/admin/main.js"></script>

  <script src="<?php echo SITE_URL ?>js/admin/vendor/jquery-2.1.4.min.js"></script>
  <script src="<?php echo SITE_URL ?>datepicker/bootstrap.min.js" ></script>


  <style type="text/css">
  .imgg{display: inline-block;}
  .form-control {
    font-size: 13px !important;
    border-radius: 0px !important;
    padding: 8px 4px !important; }
    .navaline{
      padding-top: 2%;
    }
    .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
      color: #fff;
      background-color: #0c801a;
    }

    body {
      font-size: 12px !important;
    }
    .page-header {
     font-size: 16px !important;
   }


 </style>

  </head>
  <body class="open">
    <!-- Left Panel -->
    <?php // echo $this->element('admin/left'); ?>

    <div id="right-panel" class="right-panel">

      <!-- Header-->
      <header id="header" class="header">

        <div class="header-menu">

          <div class="col-sm-11">
           <!-- <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>-->
           <div class="row">
            <div class="col-sm-2">
            <a href="<?php echo SITE_URL;?>admin/dashboard"> <img class="" src="<?php  echo SITE_URL ?>img/logo_new.png"/> </a>
           </div> 
           <div class="col-sm-10">
            <ul class="nav nav-pills nav-pills-success navaline">
              <li class="nav-item">
                <a href="<?php echo SITE_URL; ?>admin/dashboard" class="nav-link <?php if($this->request->params['controller']=="Dashboard"){ echo "active";} ?> " title="Dashboard"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo SITE_URL; ?>admin/product" class="nav-link <?php if($this->request->params['controller']=="Product"){ echo "active";} ?>" title="product"> <i class="menu-icon fa fa-calendar-o"></i>Product Manager </a>
              </li>
              <li class="nav-item">
               <a href="<?php echo SITE_URL; ?>admin/retailer" class="nav-link <?php if($this->request->params['controller']=="Retailer"){ echo "active";} ?>" title="Retailer"> <i class="menu-icon fa fa-user"></i>Retailer Manager </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo SITE_URL; ?>admin/orders" class="nav-link <?php if($this->request->params['controller']=="Orders"){ echo "active";} ?>" title="Order"> <i class="menu-icon fa fa-calendar-o"></i>Order Manager </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo SITE_URL; ?>admin/stocks" class="nav-link <?php if($this->request->params['controller']=="Stocks"){ echo "active";} ?>" title="Stock"> <i class="menu-icon fa fa-calendar-o"></i>Stock Manager </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo SITE_URL; ?>admin/reports" class="nav-link <?php if($this->request->params['controller']=="Reports"){ echo "active";} ?>" title="Report"> <i class="menu-icon fa fa-calendar-o"></i>Report Manager </a>
              </li>

              <li class="nav-item">
                <a href="<?php echo SITE_URL; ?>admin/selling" class="nav-link <?php if($this->request->params['controller']=="Selling"){ echo "active";} ?>" title="Report"> <i class="menu-icon fa fa-calendar-o"></i>Selling Capacity </a>
              </li>
            </ul>
          </div>  
        </div>

      </div>

      <div class="col-sm-1">
        <div class="user-area dropdown float-right">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="user-avatar rounded-circle" src="<?php  echo SITE_URL ?>images/admin.jpg"/>
          </a>

          <div class="user-menu dropdown-menu">
            <a class="nav-link" href="<?php echo SITE_URL; ?>admin/users/index/1"><i class="fa fa -cog"></i>Settings</a>
            <a class="nav-link" href="<?php echo SITE_URL; ?>logins/logout/"><i class="fa fa-power -off"></i>Logout</a>
          </div>
        </div>

      </div>
    </div>

  </header><!-- /header -->
  <!-- Header-->
