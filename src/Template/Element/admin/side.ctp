        <!-- Left Panel -->

        <aside id="left-panel" class="left-panel">
        	<nav class="navbar navbar-expand-sm navbar-default">

        		<div class="navbar-header">
        			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
        				<i class="fa fa-bars"></i>
        			</button>

        			<a  class="navbar-brand" style="padding: 10px;" href="<?php echo SITE_URL;?>" target="_blank"> <img class="" src="<?php  echo SITE_URL ?>backend/images/logo.png" width="45px"/></a>
        			<a class="navbar-brand hidden" href="./dashboard"><img class="" src="<?php  echo SITE_URL ?>backend/images/logo.png" width="45px"/></a>
        		</div>

        		<div id="main-menu" class="main-menu collapse navbar-collapse">
        			<ul class="nav navbar-nav">
        				<li class="active">
        					<a href="<?php echo SITE_URL; ?>admin/dashboard" title="Dashboard"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
        				</li>
						
						<li class="active">
        					<a href="<?php echo SITE_URL; ?>admin/categories" title="Categories"> <i class="menu-icon fa fa-list-alt"></i>Categories Manager </a>
        				</li>
        			
					<li class="active">
        					<a href="<?php echo SITE_URL; ?>admin/brand" title="Brand"> <i class="menu-icon fa fa-bold"></i>Brand Manager </a>
        				</li>
					<li class="active">
        					<a href="<?php echo SITE_URL; ?>admin/attributes" title="Attibutes"> <i class="menu-icon fa fa-amazon"></i>Attribute Manager </a>
        				</li>
						
					<li class="active">
        					<a href="<?php echo SITE_URL; ?>admin/products" title="Products"> <i class="menu-icon fa fa-product-hunt"></i>Products Manager </a>
        				</li>
        		
					<li class="active">
        					<a href="<?php echo SITE_URL; ?>admin/seo" title="Seo"> <i class="menu-icon fa fa-scribd"></i>Seo Manager </a>
        				</li>
                    <li class="active">
                    	<a href="<?php echo SITE_URL; ?>admin/static" title="Static"> <i class="menu-icon fa fa-stack-exchange" title="Static Manager"></i>Static Manager </a>
                    </li> 
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->
