<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rashan Duniya</title>
    <link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>css/all.min.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>css/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>css/owl.theme.default.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Oxygen:wght@300;400;700&display=swap" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>css/style.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>css/responsive.css" rel="stylesheet">
</head>

<body>
    <div class="pageWrapper">

        <!-- Address Modal Start -->
        <div class="modal fade pageModal" id="locationModalPicker" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <img src="<?php echo SITE_URL; ?>images/logo_black.png" class="img-fluid" />
                        <h3>Enter your PIN-Code for Delivery</h3>
                        <form>
                            <input type="text" placeholder="PIN-Code" value="" class="form-control" />
                            <button>Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Address Modal End -->
        <!-- product Quantity Modal start -->
        <div class="modal fade pageModal" id="quantityModalPicker" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <img src="<?php echo SITE_URL; ?>images/product_1.png" class="img-fluid" />
                        <h3>Fortune Rice Bran Health Oil</h3>
                        <ul class="popupList">
                            <li>
                                <h5>5 Ltr. Jar</h5>
                                <p><strong>₹700</strong> <del>₹800</del> <span>10% Off</span> </p>
                            </li>
                            <li><a href="<?php echo SITE_URL; ?>cart.html">Add to Cart</a></li>
                        </ul>
                        <ul class="popupList">
                            <li>
                                <h5>1 Ltr. Jar</h5>
                                <p><strong>₹120</strong> <del>₹140</del> <span>10% Off</span> </p>
                            </li>
                            <li><a href="<?php echo SITE_URL; ?>cart.html">Add to Cart</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- product Quantity Modal End -->

        <header id="header">
            <div class="d-flex align-items-center">
                <div class="logo">
                    <a href="<?php echo SITE_URL; ?>">
                        <img src="<?php echo SITE_URL; ?>images/logo_white.png" alt="" class="img-fluid" />
                    </a>
                </div>
                <div class="userLocation">
                    <a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#locationModalPicker">
                        <p><i class="fas fa-map-marker-alt"></i> Jaipur <i class="fas fa-sort-down"></i></p>
                        <p>A3 Mall Road Vidhyadhar Nagar Jaipur Raj.</p>
                    </a>
                </div>
                <div class="headerSearch flex-fill">
                    <form class="d-flex">
                        <input class="form-control me-2 text-dark" type="search" placeholder="Search"
                            aria-label="Search">
                        <button class="btn btn-outline-success" type="submit"><i class="fas fa-search"></i></button>
                    </form>
                </div>
                <div class="dropdown myAccount">
                    <a class=" dropdown-toggle" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
                        My Account
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <li><a class="dropdown-item" href="<?php echo SITE_URL; ?>Homes/my_order"><i
                                    class="fas fa-box-open"></i> My
                                Orders</a></li>
                        <!-- <li><a class="dropdown-item" href="<?php echo SITE_URL; ?>wallet.html"><i class="fas fa-wallet"></i> My Wallet</a></li> -->
                        <!-- <li><a class="dropdown-item" href="<?php echo SITE_URL; ?>profile.html"><i class="fas fa-user"></i> My Profile</a></li> -->
                        <li><a class="dropdown-item" href="<?php echo SITE_URL; ?>Homes/offerZone"><i
                                    class="fas fa-tag"></i> Offer Zone</a>
                        </li>
                        <li><a class="dropdown-item" href="<?php echo SITE_URL; ?>Homes/referFriend"><i
                                    class="fas fa-redo"></i> Refer
                                your Friends</a></li>
                        <li><a class="dropdown-item logout" href="javascript:void(0)"><i class="fas fa-power-off"></i>
                                Logout</a></li>
                    </ul>
                </div>
                <div class="headerCart">
                    <a href="<?php echo SITE_URL; ?>Homes/cart">
                        <i class="fas fa-shopping-cart"></i> My Cart <span>0</span>
                    </a>
                </div>
            </div>
        </header>