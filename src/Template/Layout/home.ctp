<?php
echo $this->element('header'); ?>
<?php echo $this->fetch('content'); ?>
</div>
<script src="<?php echo SITE_URL; ?>js/jquery.min.js"></script>
<script src="<?php echo SITE_URL; ?>js/bootstrap.bundle.min.js"></script>
<script src="<?php echo SITE_URL; ?>js/owl.carousel.min.js"></script>

<script>
$('.category-carousel').owlCarousel({
    loop: false,
    margin: 0,
    responsiveClass: true,
    dots: false,
    nav: true,
    responsive: {
        480: {
            items: 3,
        },
        800: {
            items: 5,
        },
        1200: {
            items: 7,
        },
        1600: {
            items: 7,
        }
    }
});
</script>

<script>
$('.banner-carousel').owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 5000,
    smartSpeed: 1000,
    margin: 0,
    responsiveClass: true,
    dots: false,
    nav: false,
    responsive: {
        0: {
            items: 1,
        }
    }
});
</script>

</body>

</html>