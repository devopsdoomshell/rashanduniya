<style>
.anchr{margin-top: 26px; margin-bottom: 0px;}
.anchrr{margin-top: 8px; margin-bottom: 0px;}
.badge{font-size: 11px !important;}
</style>
<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
		</div>
	</div>
	<div class="col-sm-8">
		<div class="page-header float-right">
			<div class="page-title">
				<ol class="breadcrumb text-right">
					<li><a href="<?php echo SITE_URL; ?>admin/dashboard ">Dashboard</a></li>
					<li><a href="<?php echo ADMIN_URL; ?>vendors/index ">Vendor Category Manager</a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<?php echo $this->Flash->render(); ?>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Vendor Category Manager</strong>
					</div>


<div class="row" style="padding: 10px;">
    <script>
    $(document).ready(function () {
        $("#category_search").bind("submit", function (event)
        {
            $.ajax({
                async:true,
                data:$("#category_search").serialize(),
                dataType: "html",
                type:"POST",
                url:"<?php echo ADMIN_URL; ?>VendorCategories/search",
                success:function (data) {
                //alert(data);
                $("#search_data").html(data); },
            });
            return false;
        });

    });
    //]]>
    $(document).on('click', '.pagination a', function() {
var target = $(this).attr('href');
if(!target)
return false;
$.get(target, function(data) {
$('#search_data').html( data );
}, 'html');
return false;
});
    </script>


    	<?php echo $this->Form->create('Mysubscription', array('type' => 'POST', 'inputDefaults' => array('div' => false, 'label' => false), 'id' => 'category_search', 'class' => 'form-horizontal', 'method' => 'get', 'style' => 'width:100%')); ?>
            <div class="col-sm-3">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Category Name</label>
            		<?php echo $this->Form->input('category_id', array('class' => 'longinput form-control input-medium secrh-loc', 'options' => $categories, 'empty' => 'Select Category', 'type' => 'select', 'label' => false, 'required')); ?>
            		<div id="myUL"><ul></ul></div>
            	</div>
            </div>
  <div class="col-sm-1" style="padding-top: 32px;">
            			<button type="submit" class="btn btn-success" id="Mysubscriptionevent">Search</button>
            	</div>
            	<?php echo $this->Form->end(); ?>
</div>
					<div id="search_data" class="card-body">
						<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
								<th class="align-top">S.No.</th>
								<th scope="col">Category Name</th>
								<th scope="col">Discount</th>
								<th scope="col">Discount Type</th>
								<th scope="col">Discount From Date</th>
								<th scope="col">Discount To Date</th>
								<th scope="col" class="actions">Action</th>
								</tr>
							</thead>
							<tbody >
								<?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Category']['page'] - 1) * $this->request->params['paging']['Category']['perPage'];
if (isset($vendorCategories) && !empty($vendorCategories)) {
    foreach ($vendorCategories as $vendor) {$i++;
        ?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><a href="<?php echo ADMIN_URL; ?>vendors/detail/<?php echo $vendor['id']; ?>"> <?php echo $vendor['category']['name']; ?></a></td>
											<td><?php echo !empty($vendor->discount) ? $vendor->discount : '--'; ?></td>
											<td><?php echo !empty($vendor->discount_type) ? $vendor->discount_type : '--'; ?></td>
											<td><?php echo !empty($vendor->discount_from) ? date('d-m-Y', strtotime($vendor->discount_from)) : '--'; ?></td>
											<td><?php echo !empty($vendor->discount_to) ? date('d-m-Y', strtotime($vendor->discount_to)) : '--'; ?></td>
											<td>
												<?php if ($vendor['status'] == 1) {?>
													<?=$this->Html->link('', ['action' => 'status', $vendor->id, '0'], ['class' => 'fa fa-toggle-on', 'style' => 'color:green;font-size:20px;margin-right:5px'])?>
												<?php } else {?>
													<?=$this->Html->link('', ['action' => 'status', $vendor->id, '1'], ['class' => 'fa fa-toggle-off', 'style' => 'color:red;font-size:20px;margin-right:5px'])?>
												<?php }?>
												<a href="javascript:void(0)" data-toggle="modal" class="fa fa-pencil cat-edit" data-value="<?php echo $vendor['id']; ?>" style ='font-size:20px;margin-right:5px' data-name="<?php echo $vendor['category']['name']; ?>"></a>
											</td>
										</tr>
									<?php }} else {?>
										<tr><td colspan="10" style="text-align:center;">No Data Available</td></tr>
									<?php }?>
								</tbody>
							</table>
							<?php echo $this->element('admin/pagination'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="cat-dis">
	<div class="modal-dialog" style="">
		<div class="modal-content">
			<div class="modal-header">
  <h6 class="modal-title">Category Discount</h6>
  <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
			<div class="modal-body">
			<form action="<?php echo ADMIN_URL; ?>VendorCategories/add_discount" method="POST">
			<input type="hidden" name="id" id="vencat-id" value="">
			<div style="width:80%;margin:auto">
			<div class="row form-group">
			<h5 class="cat-name"></h5>
			</div>
			<div class="row form-group">
			<label>Discount Type:</label>
			<select name="discount_type" id="" class="form-control">
			<option value="">Select Discount Type</option>
			<option value="percent">Percentage</option>
			<option value="flat">Flat</option>
			</select>
			</div>
			<div class="row form-group">
			<label>Discount Rate:</label>
			<input type="text" name="discount_rate" id="" placeholder="Discount Rate" class="form-control">
			</div>
			<div class="row form-group">
			<label>Discount From Date:</label>
			<input type="text" name="discount_from" placeholder="From Date" id="date-from"  class="form-control">
			</div>
			<div class="row form-group">
			<label>Discount To Date:</label>
			<input type="text" name="discount_to" id="date-to" placeholder="To Date" class="form-control">
			</div>
			<div class="row form-group">
			<input type="submit" value="Submit">
			</div>
			</div>
			</div>
		</div>
	</div>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
$(document).ready(function(){
	$(document).on('click','.cat-edit',function(){
		let id=$(this).data('value');
		let name=$(this).data('name');
		$('.cat-name').html(name);
		$('#vencat-id').val(id);
		$('#cat-dis').modal('show');
	})
})
</script>












