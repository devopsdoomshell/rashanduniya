<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
								<th class="align-top">S.No.</th>
								<th scope="col">Category Name</th>
								<th scope="col">Discount</th>
								<th scope="col">Discount Type</th>
								<th scope="col">Discount From Date</th>
								<th scope="col">Discount To Date</th>
								<th scope="col" class="actions">Action</th>
								</tr>
							</thead>
							<tbody >
								<?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Category']['page'] - 1) * $this->request->params['paging']['Category']['perPage'];
if (isset($vendors) && !empty($vendors)) {
    foreach ($vendors as $vendor) {$i++;
        ?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><a href="<?php echo ADMIN_URL; ?>vendors/detail/<?php echo $vendor['id']; ?>"> <?php echo $vendor['category']['name']; ?></a></td>
											<td><?php echo !empty($vendor->discount) ? $vendor->discount : '--'; ?></td>
											<td><?php echo !empty($vendor->discount_type) ? $vendor->discount_type : '--'; ?></td>
											<td><?php echo !empty($vendor->discount_from) ? date('d-m-Y', strtotime($vendor->discount_from)) : '--'; ?></td>
											<td><?php echo !empty($vendor->discount_to) ? date('d-m-Y', strtotime($vendor->discount_to)) : '--'; ?></td>
											<td>
												<?php if ($vendor['status'] == 1) {?>
													<?=$this->Html->link('', ['action' => 'status', $vendor->id, '0'], ['class' => 'fa fa-toggle-on', 'style' => 'color:green;font-size:20px;margin-right:5px'])?>
												<?php } else {?>
													<?=$this->Html->link('', ['action' => 'status', $vendor->id, '1'], ['class' => 'fa fa-toggle-off', 'style' => 'color:red;font-size:20px;margin-right:5px'])?>
												<?php }?>
												<a href="javascript:void(0)" data-toggle="modal" class="fa fa-pencil cat-edit" data-value="<?php echo $vendor['id']; ?>" style ='font-size:20px;margin-right:5px' data-name="<?php echo $vendor['category']['name']; ?>"></a>
											</td>
										</tr>
									<?php }} else {?>
										<tr><td colspan="10" style="text-align:center;">No Data Available</td></tr>
									<?php }?>
								</tbody>
							</table>
							<?php echo $this->element('admin/pagination'); ?>