<style>
.anchr{margin-top: 26px; margin-bottom: 0px;}
.anchrr{margin-top: 8px; margin-bottom: 0px;}
.badge{font-size: 11px !important;}
</style>
<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
		</div>
	</div>
	<div class="col-sm-8">
		<div class="page-header float-right">
			<div class="page-title">
				<ol class="breadcrumb text-right">
					<li><a href="<?php echo SITE_URL; ?>admin/dashboard ">Dashboard</a></li>
					<li><a href="<?php echo ADMIN_URL; ?>vendors/index ">City Manager</a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<?php echo $this->Flash->render(); ?>
			<div class="col-md-12">
			<div class="card">
			<div class="card-header">
						<strong class="card-title">Add City</strong>
			</div>
			<div class="card-body">
			<div class="row">
			<?php if (isset($city)) {
    echo $this->Form->create($city, ['url' => ['action' => 'edit', $city['id']], 'style' => 'width:100%']);
} else {
    echo $this->Form->create('', ['url' => ['action' => 'add'], 'style' => 'width:100%']);
}?>
			<div class="col-sm-3">
			<?php echo $this->Form->input('name', ['type' => 'text', 'class' => 'form-control']); ?>
			</div>
			<div class="col-sm-3">
			<?php
if (isset($city)) {
    echo $this->Form->input('Update', ['type' => 'submit', 'class' => 'btn btn-primary']);
} else {
    echo $this->Form->input('Add', ['type' => 'submit', 'class' => 'btn btn-primary']);
}?>
			</div>
			</div>
			</div>
			</div>
				<div class="card">
					<div class="card-header">
						<strong class="card-title">City Manager</strong>
					</div>
					<div id="search_data" class="card-body">
						<?php //echo $this->Paginator->limitControl([10 => 10, 15 => 15,20=>20,25=>25,30=>30]);?>
						<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
								<th class="align-top">S.No.</th>
								<th scope="col">Name</th>
								<th scope="col">Action</th>
								</tr>
							</thead>
							<tbody >
								<?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Category']['page'] - 1) * $this->request->params['paging']['Category']['perPage'];
if (isset($cities) && !empty($cities)) {
    foreach ($cities as $city) {$i++;
        ?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo $city->name; ?></td>
											<td>
												<?php if ($city['status'] == 1) {?>
													<?=$this->Html->link('', ['action' => 'status', $city->id, '0'], ['class' => 'fa fa-toggle-on', 'style' => 'color:green;font-size:20px;margin-right:5px'])?>
												<?php } else {?>
													<?=$this->Html->link('', ['action' => 'status', $city->id, '1'], ['class' => 'fa fa-toggle-off', 'style' => 'color:red;font-size:20px;margin-right:5px'])?>
												<?php }?>
												<?php echo $this->Html->link(__(''), ['action' => 'index', $city->id], array('class' => 'fa fa-pencil', 'title' => 'Edit', 'style' => 'font-size:20px;margin-right:5px')) ?>
												<?php
echo $this->Html->link('', [
            'action' => 'delete',
            $city->id,
        ], ['class' => 'fa fa-trash', 'style' => 'font-size:19px; color:#FF0000;margin-right:5px', "onClick" => "javascript: return confirm('Are you sure do you want to delete this City')"]); ?>

												<?php ?>
											</td>
										</tr>
									<?php }} else {?>
										<tr><td colspan="10" style="text-align:center;">No Data Available</td></tr>
									<?php }?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>











