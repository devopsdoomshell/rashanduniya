
<div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li><a href="<?php echo ADMIN_URL; ?>dashboard ">Dashboard</a></li>
          <li><a href="<?php echo ADMIN_URL; ?>slider/index ">Slider Manager</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>

  <?php echo $this->Flash->render(); ?>
  <?php echo $this->Form->create('slider', array(
    'class' => 'form-horizontal',
    'controller' => 'slider',
    'action' => 'add',
    'enctype' => 'multipart/form-data',
    'validate')); ?>

   <div class="col-lg-12">
    <div class="card">
      <div class="card-header"><strong>Add Slider Image</strong></div>
      <div class="card-body card-block">

        <div class="col-sm-4">
          <div class="form-group">
            <label><strong>Name</strong></label>
            <?php
echo $this->Form->input('name', array('class' => 'longinput form-control input-medium secrh-retail', 'placeholder' => 'Enter Image Name', 'type' => 'text', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="form-group">
            <label><strong>Image</strong></label>
            <?php
echo $this->Form->input('image', array('class' => 'longinput form-control input-medium filess', 'label' => false, 'required', 'type' => 'file', 'autocomplete' => 'off')); ?>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="form-group">
            <label><strong>Type</strong></label>
            <?php
$array = ['home slider' => 'Home Slider', 'home images' => 'Home Images'];
echo $this->Form->input('type', array('class' => 'longinput form-control input-medium filess', 'label' => false, 'required', 'type' => 'select', 'autocomplete' => 'off', 'options' => $array));?>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label><strong>Is Click</strong></label>
            <?php
$array = ['0' => 'N', '1' => 'Y'];
echo $this->Form->input('is_click', array('class' => 'longinput form-control input-medium isClick', 'label' => false, 'required', 'type' => 'select', 'autocomplete' => 'off', 'options' => $array));?>
          </div>
        </div>
        <div class="col-sm-4 action" style="display:none">
          <div class="form-group">
            <label><strong>Action</strong></label>
            <?php
            $options=[''=>'--Select Action--','category'=>'Category','product'=>'Product'];
echo $this->Form->input('action', array('class' => 'longinput form-control input-medium', 'id'=>'action', 'label' => false, 'required', 'type' => 'select', 'autocomplete' => 'off','options'=>$options)); ?>
          </div>
        </div>
        <div class="col-sm-4 categories" style="display:none">
          <div class="form-group">
            <label><strong>Categories</strong></label>
            <?php
      echo $this->Form->input('category_id', array('class' => 'longinput form-control input-medium filess', 'label' => false, 'required', 'type' => 'select', 'autocomplete' => 'off','option'=>$categories)); ?>
          </div>
        </div>
        <div class="col-sm-4 products" style="display:none">
          <div class="form-group">
            <label><strong>Products</strong></label>
            <?php
      echo $this->Form->input('product_id', array('class' => 'longinput form-control input-medium filess', 'label' => false, 'required', 'type' => 'select', 'autocomplete' => 'off','option'=>$products)); ?>
          </div>
        </div>


        <div class="col-sm-12">
          <div class="form-group">
            <div class="col-sm-1">
              <a href="<?php echo ADMIN_URL ?>slider/index" class="btn btn-primary " >Back</a>
           </div>
           <div class="col-sm-1">

                <button type="submit" class="btn btn-success">Submit</button>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <?php echo $this->Form->end(); ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.filess').change(function(){
            if($('.filess').val() != '') {
                var filevalue=$('.filess').val();
                var filnam= filevalue.split('.').pop();

                if (filnam=='jpg' || filnam=='jpeg' || filnam=='JPG' || filnam=='png' || filnam=='PNG') {
                    return true;
                }else{

                    $('.filess').val('');
                    alert("Please select only jpeg/jpg/png file");
                    return false;
                }
            }
            var action=$('#action').val();
        if(action=='category'){
          $('.categories').show();
            $('.products').hide();
        }else if(action=='product'){
          $('.categories').hide();
            $('.products').show();
        }else{
            $('.categories').hide();
            $('.products').hide();
          }
        });
        var isClick=$('.isClick').val();
        if(isClick==1){
          $('.action').show();
        }else{
          $('.action').hide();
        }
        $('.isClick').change(function(){
          var val=$(this).val();
          if(val==1){
            $('.action').show();
          }else{
            $('.action').hide();
          }
        });
     
  

    });
    $('#action').change(function(){ 
          var actionVal=$(this).val();
          if(actionVal=='category'){
            $('.categories').show();
            $('.products').hide();
          }else if(actionVal=='product'){
            $('.categories').hide();
            $('.products').show();
          }else{
            $('.categories').hide();
            $('.products').hide();
          }
        });
</script>