<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
									<th class="align-top">S.No.</th>
									<th class="align-top">Coupon Code</th>
									<th class="align-top">Expiry Date</th>
									<th class="align-top">Cashback</th>
									<th class="align-top">Max. Value</th>
									<th class="align-top">Min. Value</th>
									<th class="align-top">Created</th>
									<th class="align-top actions" ><?= __('Actions') ?></th>
								</tr>
							</thead>
							<tbody >
								<?php   //pr($this->request->params); die;
								$i=($this->request->params['paging']['Coupancode']['page']-1) * $this->request->params['paging']['Coupancode']['perPage']; 
								if(isset($coupancode) &&     !empty($coupancode)){ 
									foreach ($coupancode as $value){ $i++; //pr($value); ?>
										<tr>
											<td><?php echo  $i; ?></td>
											<td><?php echo  $value['coupan_code']; ?></td>
											<td><?php  echo date('d-m-y', strtotime($value['expirydate'])); ?></td>
											<td><?php if($value['ctype']=='F'){ echo $value['cashback'];}else{ echo $value['cashback']."%"; } ?></td>
											<td><?php  echo $value['maxvalu']; ?></td>
											<td><?php  echo $value['value']; ?></td>
											<td><?php  echo date('d-m-y', strtotime($value['created'])); ?></td>
											<td>

												<?php if($value['status']=='Y'){  ?>

													<?=  $this->Html->link('Active', ['action' => 'status',$value->id,'N'  ],['class'=>'badge badge-success']) ?>
												<?php  }else { ?>
													<?=  $this->Html->link('Inactive', ['action' => 'status',$value->id,'Y'],['class'=>'badge badge-warning']) ?>
												<?php } ?>

												<?php echo $this->Html->link(__(''), ['action' => 'edit', $value->id,],array('class'=>'fa fa-pencil','title'=>'Edit','style'=>'font-size:24px;')) ?>

												<?php
												echo $this->Html->link('', [
													'action' => 'delete',
													$value->id
												],['class'=> 'fa fa-trash','style'=>'font-size:19px; color:#FF0000;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this coupon')"]); ?>

												<?php  ?>
											</td>
										</tr>
									<?php } } else{ ?>
										<tr>
											<td colspan="12">No Data Available</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
              <?php echo $this->element('admin/pagination'); ?>
