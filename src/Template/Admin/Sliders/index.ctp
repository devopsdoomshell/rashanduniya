<style>
.anchr{margin-top: 26px; margin-bottom: 0px;}
.anchrr{margin-top: 8px; margin-bottom: 0px;}
.statuscl{
	display: block !important;
}
.statusclno{
	display: none !important;
}
.tmpprice strong{
	position: relative;
}
.tmpprice strong .bdgic {
    position: absolute;
    top: -10px;
    left: -10px;

    margin: auto;
    background-color: #f10a0a;
    width: 20px;
    height: 21px;
    color: #fff;
    font-weight: 900;
    text-align: center;
    border-radius: 50%;
    line-height: 21px;
    z-index: 99999;
}
.modal-dialog{
	max-width:800px;
}
.attribute-prices{
	border-collapse:separate;
    border-spacing:0 5px;

}
.attribute-prices tr td{
    padding:0 3px;
}
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
input[type=number] {
  -moz-appearance: textfield;
}

</style>

 <?php $role_id = $this->request->session()->read('Auth.User.role_id');
//pr($_SESSION);
?>
<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<?php echo $this->Flash->render(); ?>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Slider Manager</strong>
						<a href="<?php echo SITE_URL; ?>admin/sliders/add" ><strong class=" btn btn-info card-title pull-right" style="margin-right:10px; ">Add</strong></a>
					</div>
				

		<div id="example5" class="card-body">
			<?php //echo $this->Paginator->limitControl([10 => 10, 15 => 15,20=>20,25=>25,30=>30]);?>
			<table id="1bootstrap-data-table" class="table table-striped table-bordered ">
				<thead>
					<tr>
						<th class="align-top"><input type="checkbox" id="select_all" class="selectcheck" name="check[]" value="<?php echo $value['id']; ?>"></th>
						<th class="align-top">SNo</th>
						<th class="align-top">Image</th>
						<th class="align-top">Type</th>
						<?php if ($user['role_id'] == 1) {?>
						<th class="align-top">Added By</th>
						<?php }?>
						<th class="align-top">Status</th>
							</tr>
						</thead>

						<tbody >
								<?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Sliders']['page'] - 1) * $this->request->params['paging']['Sliders']['perPage'];
if (isset($sliders) && !empty($sliders)) {
    foreach ($sliders as $value) {$i++;?>
										<tr>
											<td><input type="checkbox" class="selectcheck mycheckbox" name="check[]" value="<?php echo $value['id']; ?>"></td>
											<td><?php echo $i; ?></td>

											<td><img src="<?php echo SITE_URL; ?>images/sliders/<?php echo $value['image']; ?>" height="75px" width="150px" style="display:block;"></td>
											<td><?php echo ucwords($value['type']); ?></td>
											<?php if ($user['role_id'] == 1) {?>
											<td><?php echo $vendors[$value['vendor_id']]; ?></td>
											<?php }?>
										<td>
										<?php if ($value['status'] == 'Y') {?>
												<a  class="st<?php echo $value['id']; ?> statuss fa fa-toggle-on statuscl" name="Active" data-statval="N" data-val="<?php echo $value['id']; ?>" style="color:green;font-size:20px"></a>

												<a  class="sts<?php echo $value['id']; ?> statuss fa fa-toggle-off statusclno" name="Inactive" data-statval="Y" data-val="<?php echo $value['id']; ?>" style="color:red;font-size:20px"></a>

											<?php } else {?>
												<a  class="sts<?php echo $value['id']; ?> statuss fa fa-toggle-off statuscl" name="Inactive" data-statval="Y" data-val="<?php echo $value['id']; ?>" style="color:red;font-size:20px"></a>

												<a class="st<?php echo $value['id']; ?> statuss fa fa-toggle-on statusclno" name="Active" data-statval="N" data-val="<?php echo $value['id']; ?>" style="color:green;font-size:20px"></a>
											<?php }?>
											<a href="<?php echo ADMIN_URL; ?>Sliders/edit/<?php echo $value['id']; ?>" class="fa fa-edit" name="Active" style="color:green;font-size:20px"></a>

										</td>
									</tr>
								<?php }} else {?>
									<tr>
										<td colspan="12">No Data Available</td>
									</tr>
								<?php }?>
							</tbody>
						</table>
						<?php echo $this->element('admin/pagination'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
      $( document ).ready(function() {
       $(".statuss").click(function() {
        var check_status = $(this).data('statval');
        var check_id = $(this).data('val');

       // alert(check_status);
        $.ajax({
          type: 'POST',
          url: '<?php echo SITE_URL; ?>admin/Sliders/status',
		  headers: {
       'X-CSRF-Token': csrfToken
   },
          data: {'status':check_status,'id':check_id},
          success: function(data){
          	//alert(data);
          	if (data==2) {
          		$('.st'+check_id).removeClass('statuscl');
          		$('.sts'+check_id).removeClass('statusclno');

          		$('.st'+check_id).addClass('statusclno');
          		$('.sts'+check_id).addClass('statuscl');
          	}else{
          		$('.sts'+check_id).removeClass('statuscl');
          		$('.st'+check_id).removeClass('statusclno');

          		$('.sts'+check_id).addClass('statusclno');
          		$('.st'+check_id).addClass('statuscl');
          	}
          },
      });
  });
});
</script>


<script src="<?php echo SITE_URL ?>datepicker/jquery-3.2.1.min.js"></script>
<script src="<?php echo SITE_URL ?>datepicker/bootstrap.min.js" ></script>

<!-- Modal -->














