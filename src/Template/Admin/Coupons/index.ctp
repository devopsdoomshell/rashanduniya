<style>
.anchr{margin-top: 26px; margin-bottom: 0px;}
.anchrr{margin-top: 8px; margin-bottom: 0px;}
.badge{font-size: 11px !important;}
</style>
<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
		</div>
	</div>
	<div class="col-sm-8">
		<div class="page-header float-right">
			<div class="page-title">
				<ol class="breadcrumb text-right">
					<li><a href="<?php echo SITE_URL; ?>admin/dashboard ">Dashboard</a></li>
					<li><a href="<?php echo ADMIN_URL; ?>product/index ">Coupon Code Manager</a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<?php echo $this->Flash->render(); ?>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Coupon Code Manager</strong>
						<a href="<?php echo SITE_URL; ?>admin/Coupons/add" ><strong class=" btn btn-info card-title pull-right">Add</strong></a>

					</div>

	<?php echo $this->Form->create('Mysubscription', array('type' => 'file', 'inputDefaults' => array('div' => false, 'label' => false), 'id' => 'Mysubscriptions', 'class' => 'form-horizontal')); ?>
          <div class="card-body card-block">
            <div class="col-sm-8">

              <script>
                $(document).ready(function () {

                  $("#Mysubscriptions").bind("submit", function (event) {

                    $.ajax({
                      async:true,
					  		  headers: {
       'X-CSRF-Token': csrfToken
   },
                      data:$("#Mysubscriptions").serialize(),
                      dataType:"html",
                      type:"POST",
                      url:"<?php echo ADMIN_URL; ?>Coupons/search",
                      success:function (data) {
                  //alert(data);
                  $("#example5").html(data); },
                });
                    return false;
                  });});
                  //]]>
                </script>

              <div class="col-sm-3">
                <input type="hidden" name="test_id" id="test_ids">
                <label for="to" class="form-control-label">Coupon code</label>
                <input type="text" name="coupan_code" placeholder ="coupon code" class="longinput form-control input-medium secrh_coupan_code" autocomplete="off" >
                <div id="myUL"><ul></ul></div>
              </div>
              <div class="col-sm-1 anchr ">
                <input type="submit"  id="Mysubscriptions" class="btn btn-info card-title " value="Search">
              </div>

            </div>

            <?php echo $this->Form->end(); ?>

					<div id="example5" class="card-body">
						<?php //echo $this->Paginator->limitControl([10 => 10, 15 => 15,20=>20,25=>25,30=>30]);?>
						<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
									<th class="align-top">S.No.</th>
									<th class="align-top">Coupon Code</th>
									<th class="align-top">Applicable To</th>
									<th class="align-top">Applicable Type</th>
									<th class="align-top">Discount Rate</th>
									<th class="align-top">Discount Type</th>
									<th class="align-top">Min Order</th>
									<th class="align-top">Max. Discount</th>
									<th class="align-top">Valid From</th>
									<th class="align-top">Valid To</th>
									<th class="align-top">Expiry Date</th>
									<th class="align-top actions" ><?=__('Actions')?></th>
								</tr>
							</thead>
							<tbody >
								<?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Coupancode']['page'] - 1) * $this->request->params['paging']['Coupancode']['perPage'];
if (isset($coupancode) && !empty($coupancode)) {
    foreach ($coupancode as $value) {$i++; //pr($value);
        $categories = null;
        $products = null;
        if ($value['applicable_to'] == 'category') {
            $categories = array_map(function ($val) {
                return $val['name'];
            }, $value['categories']);
            $categories = implode(',', $categories);
        }
        if ($value['applicable_to'] == 'product') {
            $products = array_map(function ($val) {
                return $val['productnameen'];
            }, $value['products']);
            $products = implode(',', $products);
        }

        ?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo $value['code']; ?></td>
											<td><?php echo $value['applicable_to'];
        if ($value['applicable_to'] == 'category') {
            echo "<br>(" . $categories . ')';
        }
        if ($value['applicable_to'] == 'product') {
            echo "<br>(" . $products . ')';
        }
        ?></td>
											<td><?php echo $value['applicable_type']; ?></td>
											<td><?php echo $value['discount_rate']; ?></td>
											<td><?php echo $value['discount_type']; ?></td>
											<td><?php echo $value['minimum_order_value']; ?></td>
											<td><?php echo $value['maximum_discount']; ?></td>
											<td><?php echo date('d-m-y', strtotime($value['valid_from'])); ?></td>
											<td><?php echo date('d-m-y', strtotime($value['valid_to'])); ?></td>
											<td><?php echo date('d-m-y', strtotime($value['cashback_expiry_date'])); ?></td>
											<td>
												<?php if ($value['status'] == '1') {?>
													<?=$this->Html->link('', ['action' => 'status', $value->id, '0'], ['class' => 'fa fa-toggle-off', 'style' => 'color:green;font-size:20px;margin-right:5px'])?>
												<?php } else {?>
													<?=$this->Html->link('', ['action' => 'status', $value->id, '1'], ['class' => 'fa fa-toggle-off', 'style' => 'color:red;font-size:20px;margin-right:5px'])?>
												<?php }?>

												<?php echo $this->Html->link(__(''), ['action' => 'edit', $value->id], array('class' => 'fa fa-pencil', 'title' => 'Edit', 'style' => 'font-size:24px;')) ?>
												<?php ?>
											</td>
										</tr>
									<?php }} else {?>
										<tr>
											<td colspan="12">No Data Available</td>
										</tr>
									<?php }?>
								</tbody>
							</table>
							<?php echo $this->element('admin/pagination'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

