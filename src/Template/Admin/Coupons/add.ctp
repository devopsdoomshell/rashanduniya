<style>
.showbox{
	display:none;
	}
</style>
<div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li><a href="<?php echo ADMIN_URL; ?>dashboard ">Dashboard</a></li>
          <li><a href="<?php echo ADMIN_URL; ?>coupancode/index ">Coupon Code Manager</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>

  <?php echo $this->Flash->render(); ?>
   <?php

echo $this->Form->create('coupon', array(
    'class' => 'form-horizontal',
    'controller' => 'Coupons',
    'action' => 'add',
    'enctype' => 'multipart/form-data',
    'validate'));
?>

   <div class="col-lg-12">
    <div class="card">
      <div class="card-header"><strong>Add Coupon Code</strong></div>
      <div class="card-body card-block" id="date">

        <div class="col-sm-4" style="min-height:80px">
          <div class="form-group">
            <label><strong>Coupon Code</strong></label>
            <?php
echo $this->Form->input('code', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Coupon Code', 'type' => 'text', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
        </div>
	      <div class="col-sm-4" style="min-height:80px">
          <div class="form-group">
            <label><strong>Applicable To</strong></label>
            <?php
echo $this->Form->input('applicable_to', array('class' => 'longinput form-control input-medium', 'id' => 'applicable-to', 'type' => 'select', 'options' => $applicableTo, 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
        </div>


<div class="col-sm-4" style="min-height:80px">
<div class="row">

        <div class="col-sm-12">
          <div class="form-group hide" id="categories-box">
            <label><strong>Applicable Categories</strong></label>
            <?php
echo $this->Form->input('categories[]', array('class' => 'longinput form-control input-medium', 'id' => 'categories', 'type' => 'select', 'multiple', 'options' => $categories, 'empty' => 'Select Category', 'label' => false, 'autocomplete' => 'off', 'style' => "width:100%")); ?>
          </div>
        </div>
	      <div class="col-sm-12 hide" id="products-box">
          <div class="form-group">
            <label><strong>Applicable Products</strong></label>
            <?php
echo $this->Form->input('products[]', array('class' => 'longinput form-control input-medium', 'id' => 'products', 'type' => 'select', 'multiple', 'options' => $products, 'empty' => 'Select Products', 'label' => false, 'autocomplete' => 'off', 'style' => "width:100%")); ?>
          </div>
        </div>
</div>
</div>



	      <div class="col-sm-4" style="min-height:80px">
          <div class="form-group">
            <label><strong>Applicable Type</strong></label><br>
            <div class="form-check form-check-inline">

            <?php
echo $this->Form->radio(
    'applicable_type',
    [
        ['value' => 'amount', 'text' => 'Amount', 'label' => false, 'checked', 'class' => 'custom-radio applicatble-type', 'required', 'autocomplete' => 'off'],
        ['value' => 'percentage', 'text' => 'Percentage', 'label' => false, 'class' => 'custom-radio applicatble-type', 'required', 'autocomplete' => 'off'],
    ]
);
?>
</div>
          </div>
        </div>

	      <div class="col-sm-4" style="min-height:80px">
          <div class="form-group">
            <label><strong>Discount Rate</strong></label>
            <?php
echo $this->Form->input('discount_rate', array('class' => 'longinput form-control input-medium', 'type' => 'text', 'palceholder' => 'Discount Rate', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
        </div>

	      <div class="col-sm-4" style="min-height:80px">
          <div class="form-group">
            <label><strong>Discount Type</strong></label><br>
            <div class="form-check form-check-inline">

            <?php
echo $this->Form->radio(
    'discount_type',
    [
        ['value' => 'flat', 'text' => 'Flat Discount', 'label' => false, 'checked', 'class' => 'custom-radio discount-type', 'required', 'autocomplete' => 'off'],
        ['value' => 'cashback', 'text' => 'CashBack', 'label' => false, 'class' => 'custom-radio discount-type', 'required', 'autocomplete' => 'off'],
    ]
);
?>
</div>

          </div>
        </div>

        <div class="col-sm-4 max-redeem" style="display:none; min-height:80px">
          <div class="form-group">
            <label><strong>Maximum Redeem type</strong></label>
            <?php
echo $this->Form->input('max_redeem_type', array('class' => 'longinput form-control input-medium', 'type' => 'select', 'options' => ['amount' => 'Amount', 'percentage' => 'Percentage'], 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>

	      <div class="col-sm-4 max-redeem" style="display:none; min-height:80px">
          <div class="form-group">
            <label><strong>Maximum Redeem Rate</strong></label>
            <?php
echo $this->Form->input('max_redeem_rate', array('class' => 'longinput form-control input-medium', 'type' => 'text', 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>


	      <div class="col-sm-4" style="min-height:80px">
          <div class="form-group">
            <label><strong>Minimum Order value</strong></label>
            <?php
echo $this->Form->input('minimum_order_value', array('class' => 'longinput form-control input-medium', 'type' => 'number', 'palceholder' => 'Minimum Order Value', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
        </div>
	      <div class="col-sm-4" style="min-height:80px">
          <div class="form-group">
            <label><strong>Maximum Discount</strong></label>
            <?php
echo $this->Form->input('maximum_discount', array('class' => 'longinput form-control input-medium', 'type' => 'number', 'palceholder' => 'Maximum Discount', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
        </div>
	      <div class="col-sm-4 input-daterange" id="datePicker" style="min-height:80px">
          <div class="col-sm-6">
            <label><strong>Coupon Valid From</strong></label>
            <?php
echo $this->Form->input('valid_from', array('class' => 'longinput form-control input-medium', 'type' => 'text', 'palceholder' => 'Valid From Date', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
        <!-- </div>
	      <div class="col-sm-4" style="min-height:80px"> -->
          <div class="col-sm-6">
            <label><strong>Coupon Valid To</strong></label>
            <?php
echo $this->Form->input('valid_to', array('class' => 'longinput form-control input-medium ', 'type' => 'text', 'palceholder' => 'Valid To Date', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
        </div>
	      <div class="col-sm-4" style="min-height:80px">
          <div class="form-group">
            <label><strong>Description</strong></label>
            <?php
echo $this->Form->input('description', array('class' => 'longinput form-control input-medium', 'type' => 'text', 'palceholder' => 'Valid To Date', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
        </div>
	      <div class="col-sm-4" style="min-height:80px">
          <div class="form-group">
            <label><strong>CashBack Expiry Date</strong></label>
            <?php
echo $this->Form->input('cashback_expiry_date', array('class' => 'longinput form-control input-medium cash-back', 'type' => 'text', 'palceholder' => 'Valid To Date', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
        </div>



        <div class="col-sm-12">
          <div class="form-group">
            <div class="col-sm-1">
              <a href="<?php echo ADMIN_URL ?>coupancode/index" class="btn btn-primary " >Back</a>
           </div>
           <div class="col-sm-1">

                <button type="submit" id="submit" class="btn btn-success">Submit</button>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <?php echo $this->Form->end(); ?>
  <script>
			 $(document).ready(function(){

        $('.input-daterange input').each(function() {
          $(this).datepicker({
            format: 'dd-mm-yyyy',
            container:'#date',
        autoclose: true,
        todayHighlight: true
  }).datepicker('update', new Date());
});
$('.cash-back').datepicker({
            format: 'dd-mm-yyyy',
            container:'#date',
        autoclose: true,
        todayHighlight: true
  }).datepicker('update', new Date());
       });

        $(document).ready(function(){
        $('#categories').select2({
      placeholder: "Select Categories",
      allowClear: true
    });
        $('#products').select2({
      placeholder: "Select Products",
      allowClear: true
    });
    $('.discount-type').click(function(){
      let type=$(this).val();
      if(type=='cashback'){
        $('.max-redeem').show();
      }else{
        $('.max-redeem').hide();
      }
    });
    $('#applicable-to').change(function(){
      let type=$(this).val();
      if(type=='category'){
        $('#categories-box').removeClass('hide');
        $('#products-box').addClass('hide');
      }else if(type=='product'){
        $('#products-box').removeClass('hide');
        $('#categories-box').addClass('hide');
      }else{
        $('#products-box').addClass('hide');
        $('#categories-box').addClass('hide');
      }
    });
			});
              </script>


<script>
$( document ).ready(function() {

  $(".minv").change(function(){
  var maxvalues= parseInt($(".maxv").val());
  var mvalues= parseInt($(".minv").val());

  if(mvalues > maxvalues ){
    $(".minv").val('');
    }
  });
});
</script>
<script>
 $("#maxvalue").keyup(function(){
	var mvalue= $("#maxvalue").val();
	if(mvalue=='0'){
		var mvalue= $("#maxvalue").val('');
		}
 });

  $("#minvalue").keyup(function(){
	var mvalue= $("#minvalue").val();
	if(mvalue=='0'){
		var mvalue= $("#minvalue").val('');
		}
 });

</script>
 <script>
function isNumber(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
     alert("Please Enter Only Numeric Characters!!!!");
      return false;
  }
  return true;

}
</script>


  <script>
    $( function() {

      var dateFormat = 'dd-mm-yy',
      from = $( "#datepicker1" )
      .datepicker({
		   minDate: 0,
        dateFormat: 'dd-mm-yy',


        changeMonth: true,
        numberOfMonths: 1
      })
      .on( "change", function() {
        to.datepicker( "option", "minDate", getDate( this ) );
      }),
      to = $( "#datepicker2" ).datepicker({
        dateFormat: 'dd-mm-yy',

        changeMonth: true,
        numberOfMonths: 1
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });

      function getDate( element ) {
        var date;
        try {
          date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
          date = null;
        }

        return date;
      }
    } );
  </script>







