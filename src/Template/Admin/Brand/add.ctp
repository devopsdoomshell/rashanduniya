<style>
.chngpass{ margin-left:15px}
</style>
<?php echo $this->Form->create($brand,array('class'=>'form-horizontal','controller'=>'brand','action'=>'add','enctype' => 'multipart/form-data','validate' )); ?>
<?php echo $this->Flash->render(); ?>
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<strong>Brand Manager</strong>
<a href="<?php echo SITE_URL; ?>admin/brand/add">
<strong class=" btn btn-info card-title pull-right">Add</strong>
</a>
</div>
<div class="card-body card-block">
<div class="row">


<div class="col-sm-6">
<div class="form-group">
<label for="company" class=" form-control-label">Select Category</label>
<?php echo $this->Form->input('parent_id', array('class' => 'form-control','label'=>false,'empty'=>'Main Category','options' => $parentBrand)); ?>

</div>
</div>

<div class="col-sm-6">
<div class="form-group">
<label for="company" class=" form-control-label">Brand Name</label>
<?php echo $this->Form->input('name',array('class'=>'form-control','placeholder'=>'Brand Name', 'id'=>'first_name','label' =>false,'required')); ?>
</div>
</div>

<div class="col-sm-6">
<div class="form-group" id="categoryslug_display">
<label for="company" class=" form-control-label">Category-slug</label>
<?php echo $this->Form->input('slug',array('class'=>'form-control','placeholder'=>'Category slug', 'id'=>'slug_data','label' =>false,'required')); ?>
</div>
</div>

<div class="col-sm-6" id="dicount_display">
<div class="form-group">
<label for="company" class=" form-control-label">Discount</label>
<?php echo $this->Form->input('discount',array('class'=>'form-control','placeholder'=>'Discount', 'id'=>'discount','label' =>false,'type'=>'number')); ?>
</div>
</div>

<div class="col-sm-6">
<div class="form-group">
<label for="company" class=" form-control-label">Image</label>
<?php echo $this->Form->input('image',array('class'=>'form-control','id'=>'discount','label' =>false,'type'=>'file')); ?>
</div>
</div>

<div class="col-sm-12">
<strong><u>SEO</u></strong>
</div>
<div class="col-sm-6">
<div class="form-group" id="categoryslug_display">
<label for="company" class=" form-control-label">Seo-Title</label>
<?php echo $this->Form->input('seotitle',array('class'=>'form-control','placeholder'=>'Seo Title','label' =>false,'required')); ?>
</div>
</div>
<div class="col-sm-6">

</div>
<div class="col-sm-6">
<div class="form-group" id="categoryslug_display">
<label for="company" class=" form-control-label">Seo-Keyword</label>
<?php echo $this->Form->input('seokeyword',array('class'=>'form-control','type'=>'textarea','placeholder'=>'Seo Keyword','label' =>false)); ?>
</div>
</div>
<div class="col-sm-6">
<div class="form-group" id="categoryslug_display">
<label for="company" class=" form-control-label">Seo-Descripation</label>
<?php echo $this->Form->input('seodescripation',array('class'=>'form-control','type'=>'textarea','placeholder'=>'Seo Descripation','label' =>false)); ?>
</div>
</div>

<div class="col-sm-12">
          <div class="form-group">
            <div class="col-sm-1">
             <a href="<?php echo SITE_URL; ?>admin/brand" class="btn btn-primary ">Back</a>
           </div>
           <div class="col-sm-1">
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>
          </div>
        </div>


</div>
</div>
</div>

</div>
</div>
</div>



<?php echo $this->Form->end(); ?>

</div>


<script>
$( document ).ready(function() { //alert('test');
$('#first_name').keyup(function(){
$('#slug_data').val('');

});
$('#slug_data').slugger({
source: '#first_name',
prefix: '',
suffix:'',
});
});
</script>


<script>
$('#cat_parent').change(function() {
    if ($(this).val()) {
      $("#dicount_display").css("display", "block");
      $("#categoryslug_display").css("display", "block");
        // Do something for option "b"
    }else{
      $("#categoryslug_display").css("display", "none");
      $("#dicount_display").css("display", "none");
    }
});
</script>