<!-- https://www.youtube.com/watch?v=OK_JCtrrv-c -->
<style type="text/css">
.are {
    margin-top: -6px;
}

.bfh-timepicker-popover table {
    width: 280px;
    margin: 0
}

select.form-control:not([size]):not([multiple]) {
    height: calc(1.99rem + 2px) !important;
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

input[type=number] {
    -moz-appearance: textfield;
}
</style>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="<?php echo ADMIN_URL; ?>dashboard ">Dashboard</a></li>
                    <li><a href="<?php echo ADMIN_URL; ?>product	/index ">Product Manager</a></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!--<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">-->
<?php echo $this->Flash->render(); ?>
<?php echo $this->Form->create('products', array(
    'class' => 'form-horizontal',
    'controller' => 'products',
    'action' => 'add',
    'enctype' => 'multipart/form-data',
    'validate')); ?>

<div class="col-lg-12">
    <div class="card">
        <div class="card-header"><strong>Product</strong></div>
        <div class="card-body card-block">
            <div class="row">
                <div class="col-sm-3">
                    <label><strong>Notification Type</strong></label>
                    <?php
echo $this->Form->input('type', array('class' => 'longinput form-control input-medium', 'type' => 'select', 'label' => false, 'required', 'id' => 'select-type', 'options' => ['offers' => 'Offers', 'others' => 'General'], 'empty' => 'Select Notification Type')); ?>
                </div>
                <div class="col-sm-3 users" style="display:none">
                    <label><strong>Vendors</strong></label>
                    <?php
echo $this->Form->input('vendor_id', array('class' => 'longinput form-control input-medium', 'type' => 'select', 'label' => false, 'id' => 'user_select', 'empty' => 'Select Vendor', 'options' => $vendors)); ?>
                </div>
                <div class="col-sm-3 users" style="display:none">
                    <label><strong>Offer Image</strong></label>
                    <?php
echo $this->Form->input('image', array('class' => 'longinput form-control input-medium', 'type' => 'file', 'label' => false, 'id' => 'offer_image', 'onChange' => 'ValidateFileUpload()')); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label><strong>Title</strong></label>
                        <?php
echo $this->Form->input('title', array('class' => 'longinput form-control input-medium', 'type' => 'text', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label><strong>Message</strong></label>
                        <?php
echo $this->Form->input('message', array('class' => 'longinput form-control input-medium', 'type' => 'textarea', 'label' => false, 'required', 'autocomplete' => 'off', 'id' => 'sorting_check', 'rows' => 3)); ?>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="col-sm-1">
                            <a href="<?php echo ADMIN_URL ?>product/index" class="btn btn-primary ">Back</a>
                        </div>
                        <div class="col-sm-1">
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>

</script> -->
<script type="text/javascript">
function ValidateFileUpload() {
    var fuData = document.getElementById('fileChooser');
    var FileUploadPath = fuData.value;

    //To check if user upload any file
    if (FileUploadPath == '') {
        alert("Please upload an image");
    } else {
        var Extension = FileUploadPath.substring(
            FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
        //The file uploaded is an image
        if (Extension == "png" || Extension == "jpeg" || Extension == "jpg") {
            // To Display
            var img = document.getElementById("fileChooser");
            if (img.files[0].size < 1048576) // validation according to file size
            {
                //alert(img.files[0].size);
                document.getElementById("showfeatimage").style.display = "none";
                document.getElementById("showfeatsize").style.display = "none";
                return true;
            } else {
                document.getElementById("showfeatimage").style.display = "none";
                document.getElementById("showfeatsize").style.display = "block";
                document.getElementById("fileChooser").value = "";
                return false;
            }
        }
        //The file upload is NOT an image
        else {
            document.getElementById("showfeatimage").style.display = "block";
            document.getElementById("fileChooser").value = "";
            return false;
        }
    }
}
</script>
<script>
$(document).ready(function() {
    $(document).on('change', '#select-type', function() {
        var val = $(this).val();
        if (val == "offers") {
            $('.users').show();
            $('#user_select').attr('required', true);
            $('#offer_image').attr('required', true);
        } else {
            $('.users').hide();
            $('#user_select').attr('required', false);
            $('#offer_image').attr('required', false);
        }
    });
});
</script>