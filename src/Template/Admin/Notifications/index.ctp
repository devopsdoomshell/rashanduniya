<style>
.anchr {
    margin-top: 26px;
    margin-bottom: 0px;
}

.anchrr {
    margin-top: 8px;
    margin-bottom: 0px;
}

.statuscl {
    display: block !important;
}

.statusclno {
    display: none !important;
}

.tmpprice strong {
    position: relative;
}

.tmpprice strong .bdgic {
    position: absolute;
    top: -10px;
    left: -10px;

    margin: auto;
    background-color: #f10a0a;
    width: 20px;
    height: 21px;
    color: #fff;
    font-weight: 900;
    text-align: center;
    border-radius: 50%;
    line-height: 21px;
    z-index: 99999;
}

.modal-dialog {
    max-width: 800px;
}

.attribute-prices {
    border-collapse: separate;
    border-spacing: 0 5px;

}

.attribute-prices tr td {
    padding: 0 3px;
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

input[type=number] {
    -moz-appearance: textfield;
}
</style>

<?php $role_id = $this->request->session()->read('Auth.User.role_id');
//pr($_SESSION);
?>
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">
            <?php echo $this->Flash->render(); ?>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Notifications Manager</strong>
                        <a href="<?php echo SITE_URL; ?>admin/Notifications/add"><strong
                                class=" btn btn-info card-title pull-right">Add</strong></a>
                    </div>

                    <div id="example5" class="card-body">
                        <?php //echo $this->Paginator->limitControl([10 => 10, 15 => 15,20=>20,25=>25,30=>30]);?>
                        <table id="1bootstrap-data-table" class="table table-striped table-bordered ">
                            <thead>
                                <tr>
                                    <th class="align-top">SNo</th>
                                    <th class="align-top">Type</th>
                                    <th class="align-top">Vendor</th>
                                    <th class="align-top">Title</th>
                                    <th class="align-top">Description</th>
                                    <th class="align-top">Offer Image</th>
                                    <th class="align-top">Status</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Products']['page'] - 1) * $this->request->params['paging']['Products']['perPage'];
if (isset($notifications) && !empty($notifications)) {
    foreach ($notifications as $value) {$i++;?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $value['type'] == "offers" ? 'Offer' : 'General'; ?></td>
                                    <td><?php echo $value['type'] == "offers" ? $vendors[$value['vendor_id']] : '-'; ?>
                                    </td>
                                    <td><?php echo $value['title']; ?></td>
                                    <td><?php echo $value['message']; ?></td>
                                    <td>
                                        <?php if ($value['type'] == "offers") {?>
                                        <img src="<?php echo SITE_URL; ?>/images/<?php echo $value['image']; ?>"
                                            height="75px" width="150px" style="display:block;">
                                    </td>
                                    <?php }?>
                                    <td>
                                        <?php if ($value['status'] == Y) {?>
                                        <?=$this->Html->link('', ['action' => 'status', $value->id, 'N'], ['class' => 'fa fa-toggle-on', 'style' => 'color:green;font-size:20px;margin-right:5px'])?>
                                        <?php } else {?>
                                        <?=$this->Html->link('', ['action' => 'status', $value->id, 'Y'], ['class' => 'fa fa-toggle-off', 'style' => 'color:red;font-size:20px;margin-right:5px'])?>
                                        <?php }?>
                                    </td>
                                </tr>
                                <?php }} else {?>
                                <tr>
                                    <td colspan="12">No Data Available</td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                        <?php echo $this->element('admin/pagination'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo SITE_URL ?>datepicker/jquery-3.2.1.min.js"></script>
<script src="<?php echo SITE_URL ?>datepicker/bootstrap.min.js"></script>