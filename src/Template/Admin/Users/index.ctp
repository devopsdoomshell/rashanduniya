<style>

.chngpassword{ margin-left:15px}

</style>
<?php echo $this->Flash->render(); ?>
 <?php $role_id=$this->request->session()->read('Auth.User.role_id'); ?> 
<?php echo $this->Form->create($Users, array('class'=>'form-horizontal','id' => 'sevice_form', 'enctype' => 'multipart/form-data')); ?>

<div class="col-lg-12">
  <div class="card">
    <div class="card-header"><strong>Profile Setting</strong></div>


    <div class="card-body card-block">
      <div class="row">

        <div class="col-sm-6">
          <div class="form-group">
            <label for="company" class=" form-control-label">Name</label>
            <?php echo $this->Form->input('name',array('class'=>'form-control','placeholder'=>'Name', 'id'=>'first_name','label' =>false,'readonly'=>true)); ?>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="form-group">
            <label for="company" class=" form-control-label">Mobile</label>
            <?php echo $this->Form->input('mobile',array('class'=>'form-control','placeholder'=>'Mobile Number', 'id'=>'Mobile','label' =>false,'readonly'=>true)); ?>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="form-group">
            <label for="company" class=" form-control-label">Contact Email</label>
            <?php echo $this->Form->input('email',array('class'=>'form-control','placeholder'=>'Email Address', 'id'=>'Mobile','label' =>false,'readonly'=>true)); ?>
          </div>
        </div>
        <?php if ($role_id=='1') { ?>
        <div class="col-sm-4">
          <div class="form-group">
            <label for="company" class=" form-control-label">COD Order Limit</label>
            <?php echo $this->Form->input('cod_order_limit',array('class'=>'form-control','placeholder'=>'Email Address', 'id'=>'Mobile', 'type'=>'text', 'onkeypress'=>'return isNumber(event);', 'label' =>false)); ?>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="form-group">
            <label for="company" class=" form-control-label">Minmum Order Limit</label>
            <?php echo $this->Form->input('min_order_limit',array('class'=>'form-control','placeholder'=>'Email Address', 'id'=>'Mobile', 'type'=>'text', 'onkeypress'=>'return isNumber(event);', 'label' =>false)); ?>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="form-group">
            <label for="company" class=" form-control-label">Delivery Charge</label>
            <?php echo $this->Form->input('delivery_charge',array('class'=>'form-control','placeholder'=>'Email Address', 'id'=>'Mobile', 'type'=>'text', 'onkeypress'=>'return isNumber(event);','label' =>false)); ?>
          </div>
        </div>

  <div class="col-sm-4">
          <div class="form-group">
            <label for="company" class=" form-control-label">Wallet percentage(%)</label>
            <?php echo $this->Form->input('wallet_percentage',array('class'=>'form-control','placeholder'=>'Wallet Percentage', 'id'=>'wallet', 'type'=>'text', 'onkeypress'=>'return isNumber(event);','label' =>false)); ?>
          </div>
        </div>

<?php } ?>


      </div>

    </div>



    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">

          <a href="javascript:void(0)" class="chngpassword"  >Do you want to change password ?</a>

        </div>
      </div>

      <div class="col-sm-12" style="padding-bottom: 15px;">
        <div class="passdata" style="display:none;">
          <div class="form-group">
            <div class="col-sm-4">
              <label for="inputEmail3" class="form-control-label">Old Password</label>
              <?php echo $this->Form->input('oldpassword',array('class'=>'form-control','placeholder'=>'Old Password','type'=>'password', 'id'=>'old_pass','autocomplete' => 'off', 'value'=>'','label' =>false)); ?>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-4">
              <label for="inputEmail3" class="form-control-label">New Password</label>
              <?php echo $this->Form->input('cpasswords',array('class'=>'form-control','placeholder'=>'New Password','type'=>'password', 'id'=>'confirm_pass','autocomplete' => 'off', 'label' =>false)); ?>
            </div>
          </div>
          
          
          <div class="form-group">
            <div class="col-sm-4">
              <label for="inputEmail3" class="form-control-label">Confirm Password</label>
              <?php echo $this->Form->input('spassword',array('class'=>'form-control','placeholder'=>'Confirm Password', 'type'=>'password','id'=>'password','label' =>false)); ?>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>
  <div class="col-lg-1">
    <div class="card-body">

      <?php if(isset($Users['id'])){
        echo $this->Form->submit('Update', array('title' => 'Update','div'=>false,
        'class'=>array('btn btn-primary btn-sm'))); }else{  ?>
          <button type="submit" class="btn btn-success">Submit</button>
        <?php  } ?>
      </div>
    </div>

  </div>
</div>
</div>

<?php echo $this->Form->end(); ?>

</div>

<script>
  $(document).ready(function(){
    $(".chngpassword").click(function(){
      $(".passdata").toggle();
    });
  });

</script>

<script>
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
     alert("Please Enter Only Numeric Characters!!!!");
     return false;
   }
   return true;

 }
</script>





