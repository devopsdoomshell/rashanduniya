<style type="text/css">
  .earningbox{
    color: #fff;
  }

  .totlerning p{
    display: inline;
  }.totlerning h6{
    color: #fff;
  }
  .earningbox span{
    padding-left: 5px;
    display: block;
    margin-bottom: 3px;
  }

  .fullbox{
    padding-top: 2px;
    background-color: #00a040;
    height: 76px;
  }

  .fullbox2{
    background-color: #ec4b49;
  }
  select{
  	color: #fff;
    border: 0px;
    font-weight: 900;
    padding: 2px 12px;
    border-radius: 3px;
    box-shadow: 1px 1px 7px 0px #04380673;
    background-color: #353833;
  }
  .yearlyrev, .monthrev, .dailyrev{
    color: white;
    border: 0px;
    width: 60px;
    background: transparent;
  }
</style>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Dashboard</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active"></li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3" style="
min-height: 100px;
">
<?php echo $this->Flash->render(); ?>


    <div class="col-sm-6 col-lg-2">
        <div class="card text-white bg-flat-color-1">
            <div class="card-body pb-0">
                <h4 class="mb-0">
                    <span class="count"><?php echo $orderscount; ?></span>
                </h4>
                <p class="text-light">Total Orders</p>
            </div>

        </div>
    </div>
    <!--/.col-->

    <div class="col-sm-6 col-lg-2">
        <div class="card text-white bg-flat-color-2">
            <div class="card-body pb-0">

                <h4 class="mb-0">
                    <span class="count"><?php echo $retailercount; ?></span>
                </h4>
                <p class="text-light">Total Retailer</p>
            </div>
        </div>
    </div>
    <!--/.col-->

    <div class="col-sm-6 col-lg-2">
        <div class="card text-white bg-flat-color-3">
            <div class="card-body pb-0">

                <h4 class="mb-0">
                    <!--<span class="count"><?php echo $totaltest; ?></span>-->
                    <span class="count"><?php echo $productcount; ?></span>
                </h4>
                <p class="text-light">Total Products</p>

            </div>

        </div>
    </div>
    <!--/.col-->

    <div class="col-sm-6 col-lg-3">
        <div class="card text-white fullbox2">
            <div class="card-body pb-0">
            	<?php foreach ($totalearing as $value) {
    $sumoftotl += $value['totalamount'];
}?>
                <h6><?php echo "Rs." . $sumoftotl; ?></h6>
                 <p class="text-light">Total Revenue</p>

            </div>

        </div>
    </div>
    <!--/.col-->

    <div class="col-sm-6 col-lg-3 fullbox">
    	<?php
$m = date('Y-m-d');

$nmonth = date("m", strtotime($m));
$year = date("Y", strtotime($m));
$day = date("d", strtotime($m));
?>
<!-- yearly revenue -->
               <div class="earningbox">
               <span class="count">
                <?php foreach ($yearlyearning as $values) {
    $sumoftotls += $values['totalamount'];
}?>
                <b style="margin-right: 13px;">Yearly Revenue</b> ||
                <select class="year" name="Yearly">
                <?php foreach ($years as $key => $yearss) {?>
                <option selected="<?php if ($year == $yearss) {echo "selected";}?>" value="<?php echo date('Y', strtotime($yearss['created'])); ?>"><?php echo date('Y', strtotime($yearss['created'])); ?>
                </option>
            <?php }?>
                </select>
              <input type="text" name="rev" readonly="readonly" class="yearlyrev" value="<?php if ($sumoftotls) {echo $sumoftotls;} else {echo "0";}?>">
               </span>


<!-- Monthly revenue -->
               <span class="count">
                <?php foreach ($monthlyearning as $valuesm) {
    $sumoftotlsm += $valuesm['totalamount'];
}?>
                <b>Monthly Revenue</b> ||
                <select class="month" name="Monthly">
						<option <?php if ($nmonth == '01') {echo "selected";}?> value="01">Jan</option>
						<option <?php if ($nmonth == '02') {echo "selected";}?> value="02">Feb</option>
						<option <?php if ($nmonth == '03') {echo "selected";}?> value="03">Mar</option>
						<option <?php if ($nmonth == '04') {echo "selected";}?> value="04">Apr</option>
						<option <?php if ($nmonth == '05') {echo "selected";}?> value="05">May </option>
						<option <?php if ($nmonth == '06') {echo "selected";}?> value="06">June</option>
						<option <?php if ($nmonth == '07') {echo "selected";}?> value="07">July</option>
						<option <?php if ($nmonth == '08') {echo "selected";}?> value="08">Aug</option>
						<option <?php if ($nmonth == '09') {echo "selected";}?> value="09">Sept</option>
						<option <?php if ($nmonth == '10') {echo "selected";}?> value="10">Oct </option>
						<option <?php if ($nmonth == '11') {echo "selected";}?> value="11">Nov</option>
						<option <?php if ($nmonth == '12') {echo "selected";}?> value="12">Dec</option>
                </select>
               <input type="text" name="rev" readonly="readonly" class="monthrev" value="<?php if ($sumoftotlsm) {echo $sumoftotlsm;} else {echo "0";}?>">
               </span>


<!-- daily revenue -->
               <span class="count">
                <?php foreach ($dailyearning as $valuesmd) {
    $sumoftotlsmd += $valuesmd['totalamount'];
}?>
                <b style="margin-right: 20px;">Daily Revenue</b> ||
                <select class="day" name="Monthly">
                    <?php for ($i = 0; $i < 31; $i++) {?>
						<option <?php if ($day == "1"+$i) {echo "selected";}?> value="<?php echo "1"+$i ?>"><?php echo "1"+$i ?></option>
					<?php }?>
                </select>
               <input type="text" name="rev" readonly="readonly" class="dailyrev" value="<?php if ($sumoftotlsmd) {echo $sumoftotlsmd;} else {echo "0";}?>">

               </span>
               </div>

   </div>
   <!--/.col-->

<!-- year revenue searching -->
<script type="text/javascript">
$(document).ready(function(){
    $("select.year").change(function(){
        var selectedyear = $(this).children("option:selected").val();
        //alert(selectedyear);
        //var email = $('#email').val();

$.ajax({
        type: 'POST',
        url: '<?php echo SITE_URL; ?>admin/dashboard/yearrevenue',
        data: {'selectedyear':selectedyear},
        success: function(data){
          //alert(data);
        if (data) {
        			$('.yearlyrev').val(data);
        			$('.monthrev').val('0');
					$('.dailyrev').val('0');
        		}

        },

    });

    });
});
</script>


<!-- Monthly revenue searching -->
<script type="text/javascript">
$(document).ready(function(){
    $("select.month").change(function(){
        var selectedmonth = $(this).children("option:selected").val();
        var selectedyear = $('.year').children("option:selected").val();
        //alert(selectedyear);
        //var email = $('#email').val();

$.ajax({
        type: 'POST',
        url: '<?php echo SITE_URL; ?>admin/dashboard/monthrevenue',
        data: {'selectedyear':selectedyear,'selectedmonth':selectedmonth},
        success: function(data){
         // alert(data);
        if (data > 0) {
        			$('.monthrev').val(data);
        		}else{
        		    $('.monthrev').val('0');
        		}
        },
    });
    });
});
</script>


<!-- Daily revenue searching -->
<script type="text/javascript">
$(document).ready(function(){
    $("select.day").change(function(){
        var selecteday = $(this).children("option:selected").val();
        var selectedmonth = $('.month').children("option:selected").val();
        var selectedyear = $('.year').children("option:selected").val();

$.ajax({
        type: 'POST',
        url: '<?php echo SITE_URL; ?>admin/dashboard/dayrevenue',
        data: {'selectedyear':selectedyear,'selectedmonth':selectedmonth,'selecteday':selecteday},
        success: function(data){
         // alert(data);
        if (data > 0) {
        			$('.dailyrev').val(data);
        		}else{
        		    $('.dailyrev').val('0');
        		}
        },
    });
    });
});
</script>



<!------------------------------------------------>


<div class="content mt-3">
   <div class="animated fadeIn">
      <div class="row">

         <?php echo $this->Flash->render(); ?>
         <div class="col-md-12">
            <div class="card">
               <div class="card-header">
                  <strong class="card-title">Latest </strong><span> Order</span>
              </div>
              <div class="card-body">
                        <style type="text/css">
                          .tbltr {
    background-color: rgba(242, 255, 38, 0.32) !important;
}
 .tbltr2 {
    background-color: rgba(34, 236, 61, 0.43) !important;
}
                        </style>
                          <table id="bootstrap-data-table" class="table table-striped table-bordered ">
                            <thead>
                               <tr>
                                    <th class="align-top">S.no</th>
                                    <th class="align-top">Vendor Name</th>
                                    <th class="align-top">Order No</th>
                                    <th class="align-top">Order Date</th>
                                    <th class="align-top">Order Amount</th>
                                    <th class="align-top">Mobile</th>
                                    <th class="align-top">Payment Status</th>
                                    <th class="align-top">Order Status</th>
                                    <!-- <th class="align-top actions" ><?=__('Actions')?></th>
                                    <th class="align-top actions" ><?=__('Actions')?></th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Users']['page'] - 1) * $this->request->params['paging']['Users']['perPage'];
if (isset($userlatest) && !empty($userlatest)) {
    foreach ($userlatest as $value) {$i++; // pr($value);
        $userimage = explode(',', $value['user']['vegimgs']);
        ?>
                                <tr class="<?php if ($value['status'] == 'D') {echo "tbltr";} elseif ($value['status'] == 'DL') {echo "tbltr2";}?>">

                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $value['vendor']['name']; ?></td>
                                    <td><a href="<?php echo ADMIN_URL ?>orders/details/<?php echo $value['id']; ?>" data-toggle="modal" class="ordersview">
                                      <?php echo $value['id']; // $value->user->boothno        ?> </a>
                                    </td>
                                    <td><?php echo date('d-m-Y H:i A', strtotime($value['created'])); ?> </td>
                                    <td><?php echo $value['total_amount'] . " Rs."; ?> </td>
                                    <td><?=$value->user->mobile?></td>
                                    <td><?=$value->payment_status?></td>

                    <?php
$options = $value->order_status;

        ?>
              			 <td><?php echo $this->Form->input('status', array('class' => 'longinput form-control input-medium orderst', 'type' => 'text', 'readonly', 'value' => $options, 'data-val' => $value['id'], 'label' => false)); ?></td>

                                </tr>
                              <?php }} else {?>
                              <tr>
                              <td colspan="16">No Data Available</td>
                              </tr>
                              <?php }?>
                            </tbody>
                        </table>
                <div class="paginator">
                 <div class="row">
                    <div class="col-sm-8">

                    </div>
                    <div class="col-sm-4">
                       <a href="<?php echo SITE_URL; ?>admin/orders" class="btn btn-success btn-sm pull-right">View All</a>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
</div>
</div>
</div>
<div class="content mt-3">
   <div class="animated fadeIn">
      <div class="row">

         <?php echo $this->Flash->render(); ?>
         <div class="col-md-12">
            <div class="card">
               <div class="card-header">
                  <strong class="card-title">Latest </strong><span> Retailer</span>
              </div>
              <div class="card-body">


                      <table id="bootstrap-data-table" class="table table-striped table-bordered ">
                          <thead>
                              <tr>
                    <th class="align-top">S.no</th>
                    <th class="align-top">Retailer Name</th>
                    <th class="align-top">Contact Person</th>
                    <th class="align-top">Mobile</th>
                    <th class="align-top">Vendor Commision</th>
                    <th class="align-top">Completed Orders</th>
                    <th class="align-top">Active Orders</th>
                    <th class="align-top">Paid Amount</th>

                  </tr>
                          </thead>
                            <tbody>
                                <?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Users']['page'] - 1) * $this->request->params['paging']['Users']['perPage'];
if (isset($retailerlatest) && !empty($retailerlatest)) {
    foreach ($retailerlatest as $value) {$i++;
        $totalorders = $this->Comman->orders($value->id);
        $totalpayment = $this->Comman->totalVendorPayment($value->id);
        $userimage = explode(',', $value['vegimgs']);
        ?>
                                  <tr>
                  <td><?php echo $i; ?></td>
                      <td><?=$value->name?></td>
                      <td><?=$value->contact_person?></td>
                      <td><?=$value->mobile_1?></td>
                      <td><?=$value->commision?></td>
                    <td><?php echo date('d-m-y H:i', strtotime($value['created'])); ?></td>
                 <td class="font-weight-bold">
                     <?php if ($totalorders == 0) {
            echo $totalorders;
        } else {?>
                      <a href="<?php echo ADMIN_URL ?>retailer/orderdetail/<?php echo $value['id']; ?>" target="_blank">
                      <?php echo $totalorders; ?>
                      </a>
                  <?php }?>
                </td>
                <td><?=$totalpayment;?></td>

                      </tr>
                                <?php }} else {?>
                                <tr>
                                <td colspan="16">No Data Available</td>
                                </tr>
                                <?php }?>
                            </tbody>
                        </table>
                <div class="paginator">
                 <div class="row">
                    <div class="col-sm-8">

                    </div>
                    <div class="col-sm-4">
                       <a href="<?php echo SITE_URL; ?>admin/retailer" class="btn btn-success btn-sm pull-right">View All</a>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
</div>
</div>
</div><!-- Content mt -3-->


<div class="content mt-3">
   <div class="animated fadeIn">
      <div class="row">

         <?php echo $this->Flash->render(); ?>
         <div class="col-md-12">
            <div class="card">
               <div class="card-header">
                  <strong class="card-title">Latest </strong><span> Products</span>
              </div>
              <div class="card-body">
<table id="bootstrap-data-table" class="table table-striped table-bordered ">
            <thead>
              <tr>

              <th class="align-top">SNo</th>
              <th class="align-top">Image</th>
              <th class="align-top">Product Category</th>
              <th class="align-top">Product Name</th>
              <th class="align-top">Added By</th>
              <th class="align-top">Created</th>

              </tr>
                </thead>


                <tbody >
                <?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Products']['page'] - 1) * $this->request->params['paging']['Products']['perPage'];
if (isset($latestproduct) && !empty($latestproduct)) {
    foreach ($latestproduct as $value) {$i++; //pr($value); ?>
                    <tr>

                      <td><?php echo $i; ?></td>

                      <td><img src="http://dc84viddw6ggp.cloudfront.net/products/<?php echo $value['image']; ?>" height="75px" width="150px" style="display:block;"></td>
                      <td><?php echo $value['category']['name']; ?></td>
                      <td><?php echo ucfirst($value['productnameen']); ?>(<?php echo ucfirst($value['productnamehi']); ?>)</td>
                      <td><?php echo $value['vendor_id'] == 0 ? 'Admin' : $value['vendor']['name']; ?></td>
                    <td><?php echo date('d-m-y', strtotime($value['created'])); ?></td>

                <?php }} else {?>
                  <tr>
                    <td colspan="12">No Data Available</td>
                  </tr>
                <?php }?>
              </tbody>
            </table>
                <div class="paginator">
                 <div class="row">
                    <div class="col-sm-8">

                    </div>
                    <div class="col-sm-4">
                       <a href="<?php echo SITE_URL; ?>admin/product" class="btn btn-success btn-sm pull-right">View All</a>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>
</div>
</div>
</div>






</div> <!-- .content -->
</div><!-- /#right-panel -->


<script src="<?php echo SITE_URL ?>datepicker/jquery-3.2.1.min.js"></script>
<script src="<?php echo SITE_URL ?>datepicker/bootstrap.min.js" ></script>

  <!-- Modal -->
  <!-- The Modal -->
  <div class="modal fade" id="myModals">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Order Detail</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">


        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>


  <script>
   $('.ordersview').click(function(e){
    e.preventDefault();
    $('#myModals').modal('show').find('.modal-body').load($(this).attr('href'));
  });
</script>



    <div class="modal fade" id="myModalsimg">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Images</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">


        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>

  <script>
   $('.imgview').click(function(e){
    e.preventDefault();
    $('#myModalsimg').modal('show').find('.modal-body').load($(this).attr('href'));
  });
</script>



<script src="<?php echo SITE_URL ?>datepicker/jquery-3.2.1.min.js"></script>
<script src="<?php echo SITE_URL ?>datepicker/bootstrap.min.js" ></script>
 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>-->
  <!-- Modal -->
  <!-- The Modal -->
  <div class="modal fade" id="myModals">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Images</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">


        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>

  <script>
   $('.addlangcat').click(function(e){
    e.preventDefault();
    $('#myModals').modal('show').find('.modal-body').load($(this).attr('href'));
  });
</script>


