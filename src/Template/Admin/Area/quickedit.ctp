 <div class="modal-header">
  <h6 class="modal-title">Edit Products</h6>
  <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<div class="modal-body">    

  <?php echo $this->Flash->render(); ?>
  <?php echo $this->Form->create($prodata,array(
   'class'=>'form-horizontal',
   'controller'=>'products',
   'action'=>'quickedit',
   'enctype' => 'multipart/form-data',
   'validate' )); ?>

   <div class="col-lg-12">
    <div class="card">
     
      <div class="card-header"><strong>Quick edit</strong></div>
      <div class="card-body card-block">
       <?php $i=0; foreach ($productid as $product_id) { $i++; ?>
        <div class="col-sm-4">
          <div class="form-group">
            <?php echo $this->Form->input('id[]', array('type'=>'hidden','value'=>$product_id['id'])); ?>
            <?php echo $this->Form->input('ptype[]', array('type'=>'hidden','value'=>$product_id['ptype'])); ?>
            <label><strong>Product Name </strong></label>
            <?php
            echo $this->Form->input('productnameen[]', array('class' => 'longinput form-control input-medium','placeholder'=>'Product Name' ,'type'=>'text','label'=>false,'required','autocomplete'=>'off','id'=>'seatsize',
            'value'=>$product_id['productnameen'],'readonly')); ?>
          </div>
        </div>


        <div class="col-sm-4">
          <div class="form-group">
            <label><strong>Product Price</strong></label>
            <?php
            echo $this->Form->input('price[]', array('class' => 'longinput form-control input-medium','placeholder'=>'Product Price' ,'type'=>'text','label'=>false,'required','autocomplete'=>'off','onkeypress'=>'return isNumberKey(event)','id'=>'amountsize','value'=>$product_id['price'])); ?>
          </div>
        </div>





        <div class="col-sm-4">
          <div class="form-group">
            <label><strong>Sorting Number</strong></label>
            <?php
            echo $this->Form->input('sortnumber[]', array('class' => 'longinput form-control input-medium','type'=>'number','label'=>false,'required','autocomplete'=>'off','onkeypress'=>'return isNumberKey(event)',
            'id'=>'sorting_check'.$i,'value'=>$product_id['sortnumber'])); ?>
            <span id="sortingmessage<?php echo $i ; ?>" style="color:red; display:none">Sorting number already exist select other number !</span>
          </div>
        </div>


        <script type="text/javascript">

          function delay(callback, ms) {
            var timer = 0;
            return function() {
              var context = this, args = arguments;
              clearTimeout(timer);
              timer = setTimeout(function () {
                callback.apply(context, args);
              }, ms || 0);
            };
          }

  $( "#sorting_check<?php echo $i ; ?>" ).keyup(delay(function() { //alert();  
    var txte = $('#sorting_check<?php echo $i ; ?>').val();
    var radioValue = $("input[name='ptype']"). val();
    $.ajax({ 
     type: 'POST', 
     url: '<?php echo SITE_URL; ?>admin/product/check_shortingquick',
     data: {'sortno':txte, 'radioValue':radioValue },
     success: function(data){  
       if(data=='1'){
        $('#sortingmessage<?php echo $i ; ?>').css('display','block');   
        $('#sorting_check<?php echo $i ; ?>').val('');  


      }else{
        $('#sortingmessage<?php echo $i ; ?>').css('display','none');   
        sorting_check
      }
    },    
  });   

  }, 900));

</script>

<?php } ?>

<div class="col-sm-12">
  <?php 
  echo $this->Form->submit('Update', array('title' => 'Update','div'=>false,
  'class'=>array('btn btn-primary btn-sm'))); ?>
  
</div>

</div>
</div>
</div>
<?php echo $this->Form->end(); ?>


</div>






