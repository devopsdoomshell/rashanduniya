<table id="bootstrap-data-table" class="table table-striped table-bordered ">
            <thead>
              <tr>
              <th class="align-top"><input type="checkbox" id="select_all" class="selectcheck" name="check[]" value="<?php echo $value['id']; ?>"></th>
              <th class="align-top">SNo</th>
              <th class="align-top">Image</th>
              <th class="align-top">Procuct Type</th>
              <th class="align-top">Product Name</th>
              <th class="align-top">Price</th>
              <!--<th class="align-top"><?= $this->Paginator->sort('Quantity') ?></th>
              <th class="align-top">Remaining  Quantity') ?></th>-->
              <th class="align-top">Created</th>
              <th class="align-top">Short order</th>
              <th class="align-top">Status</th>
              <th class="align-top actions" ><?= __('Actions') ?></th>
              </tr>
                </thead>
                <script>  
                  $(document).ready(function () { 
                    $("#select_all").change(function(){ 
                      if($(this).prop("checked")==true) { 
                        $(".mycheckbox").prop('checked', true);
                      }
                      else{
                        $(".mycheckbox").prop('checked', false);
                      }
                    });

                    $('.mycheckbox').click(function(){ 
                      $(this).attr('checked',true);
                      $("#select_all").attr('checked',false);
                      if ($(".mycheckbox:checked").length == $(".mycheckbox").length ){ 
                        $("#select_all").prop('checked', true);
                      }
                    });
                  });
                </script> 

                <tbody >
                <?php   //pr($this->request->params); die;
                $i=($this->request->params['paging']['Products']['page']-1) * $this->request->params['paging']['Products']['perPage']; 
                if(isset($veggsearch) &&     !empty($veggsearch)){ 
                  foreach ($veggsearch as $value){ $i++; //pr($value); ?>
                    <tr>
                      <td><input type="checkbox" class="selectcheck mycheckbox" name="check[]" value="<?php echo $value['id']; ?>"></td>
                      <td><?php echo  $i; ?></td>

                      <td><img src="<?php echo SITE_URL;?>images/<?php echo $value['image'];?>" height="75px" width="150px" style="display:block;"></td>
                      <td><?php echo  $value['ptype']; ?></td>
                      <td><?php echo  ucfirst($value['productnameen']); ?>(<?php echo  ucfirst($value['productnamehi']); ?>)</td>
                      <td><?php echo  $value['price']; ?></td>
                  <!--<td><?php echo  $value['qunatity']; ?></td>
                    <td><?php echo  "1"; ?></td>-->
                    <td><?php echo date('d-m-y', strtotime($value['created'])); ?></td>
                    <td><?php  echo $value['sortnumber']; ?></td>


                    <td>

                      <?php if($value['status']=='Y'){  ?>

                        <?=  $this->Html->link('Active', ['action' => 'status',$value->id,'N'  ],['class'=>'badge badge-success']) ?>
                      <?php  }else { ?>
                        <?=  $this->Html->link('Inactive', ['action' => 'status',$value->id,'Y'],['class'=>'badge badge-warning']) ?>
                      <?php } ?>

                    </td>


                    <td class="actions">
                      <!--<?= $this->Html->link(__('View'), ['action' => 'view', $user->id]) ?> -->
                      <?php // echo $this->Html->link(__(''), ['action' => 'edit', $value->id,],array('class'=>'fa fa-pencil','title'=>'Edit','style'=>'font-size:24px;')) ?>

                      <?php  ?>
                      <?= $this->Form->postLink(__(''), ['action' => 'delete', $value->id,'Y'],array('class'=>'fa fa-trash','title'=>'Delete','style'=>'font-size:24px;color:red'), 
                      ['confirm' => __('Are you sure you want to delete # {0}?', $value->id)]) ?>

                      <?php  ?>
                    </td>
                  </tr>
                <?php } } else{ ?>
                  <tr>
                    <td colspan="12">No Data Available</td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
<?php echo $this->element('admin/pagination'); ?>
<script>
   //prepare the dialog

   //respond to click event on anything with 'overlay' class
   $(".quik").click(function(event){

    var t_length=$('.mycheckbox:checked').length;

    if(t_length >0)
    {
      var favorite = [];
      $.each($('.mycheckbox:checked'), function(){            
        favorite.push($(this).val());
      });
      var gr_id=favorite.join(",");
      var url='<?php echo SITE_URL."admin/product/quickedit/" ?>'+encodeURIComponent(gr_id);
        //alert(url);
        $('.modal-content').load(url);
    }
    else{
     // alert('Select at least one checkbox');  
      return false;  
    }

});
</script> 