
<div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li><a href="<?php echo ADMIN_URL; ?>dashboard ">Dashboard</a></li>
          <li><a href="<?php echo ADMIN_URL; ?>product	/index ">Product Manager</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>  

  <?php echo $this->Flash->render(); ?>
  <?php echo $this->Form->create('area',array(
   'class'=>'form-horizontal',
   'controller'=>'area',
   'action'=>'add',
   'enctype' => 'multipart/form-data',
   'validate' )); ?>

   <div class="col-lg-12">
    <div class="card">
      <div class="card-header"><strong>Area</strong></div>
      <div class="card-body card-block">

        <div class="col-sm-12">
          <div class="form-group">
            <label><strong>Area</strong></label>
            <?php
            echo $this->Form->input('area', array('class' => 'longinput form-control input-medium','placeholder'=>'Area' ,'type'=>'text','label'=>false,'required','autocomplete'=>'off','id'=>'areaid')); ?>
            <span id="areamsg" style="color:red; display:none">Area already exist !</span>
          </div>
        </div>
        
        <div class="col-sm-12">
          <div class="form-group">
            <label><strong>Sorting Number </strong></label>
            <?php
            echo $this->Form->input('sortnumber', array('class' => 'longinput form-control input-medium','type'=>'number','label'=>false,'required','autocomplete'=>'off','id'=>'sorting_check')); ?>
            <span id="sortingmessage" style="color:red; display:none">Sorting number already exist selcet other number !</span>
          </div>
        </div>




        <div class="col-sm-12">
          <div class="form-group">
            <div class="col-sm-1">
              <a href="<?php echo ADMIN_URL ?>area/index" class="btn btn-primary " >Back</a>
           </div>
           <div class="col-sm-1">

                <button type="submit" class="btn btn-success">Submit</button>

            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <?php echo $this->Form->end(); ?>
  <script type="text/javascript">
$( "#areaid" ).change(function() {
    var areaValue = $("#areaid").val();
    
$.ajax({ 
 type: 'POST', 
 url: '<?php echo SITE_URL; ?>admin/area/sortingcheck',
 data: {'areaValue':areaValue},
 success: function(data){  
   if(data=='1'){
    $('#areamsg').css('display','block');   
    $('#areaid').val('');  


  }else{
    $('#areamsg').css('display','none');   

  }
},    
});
});



var txte = $('#sorting_check').val();
  $( "#sorting_check" ).keyup(function() { //alert();  
    var txte = $('#sorting_check').val();

$.ajax({ 
 type: 'POST', 
 url: '<?php echo SITE_URL; ?>admin/area/sortingcheck',
 data: {'sortno':txte},
 success: function(data){  
   if(data=='1'){
    $('#sortingmessage').css('display','block');   
    $('#sorting_check').val('');  


  }else{
    $('#sortingmessage').css('display','none');   

  }
},    
});   

});

</script>







