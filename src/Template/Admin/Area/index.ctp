<style>
.anchr{margin-top: 26px; margin-bottom: 0px;}
.anchrr{margin-top: 8px; margin-bottom: 0px;}
</style>
<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
		</div>
	</div>
	<div class="col-sm-8">
		<div class="page-header float-right">
			<div class="page-title">
				<ol class="breadcrumb text-right">
					<li><a href="<?php echo SITE_URL; ?>admin/dashboard ">Dashboard</a></li>
					<li><a href="<?php echo ADMIN_URL; ?>product/index ">Area Manager</a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<?php echo $this->Flash->render(); ?>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Area Manager</strong>
						<a href="<?php echo SITE_URL; ?>admin/area/add" ><strong class=" btn btn-info card-title pull-right">Add</strong></a> 
						
					</div>

		<div id="example5" class="card-body">
			<?php //echo $this->Paginator->limitControl([10 => 10, 15 => 15,20=>20,25=>25,30=>30]);?> 
			<table id="bootstrap-data-table" class="table table-striped table-bordered ">
				<thead>
						<tr>
						<th class="align-top">SNo</th>
						<th class="align-top">Area</th>
						<th class="align-top">Short order</th>
						<th class="align-top">Status</th>
						<th class="align-top actions" ><?= __('Actions') ?></th>
						</tr>
						</thead>
						<tbody >
								<?php   //pr($this->request->params); die;
								$i=($this->request->params['paging']['Area']['page']-1) * $this->request->params['paging']['Area']['perPage']; 
								if(isset($areas) &&     !empty($areas)){ 
									foreach ($areas as $value){ $i++; //pr($value); ?>
										<tr>
											<td><?php echo  $i; ?></td>
											<td><?php echo  $value['area']; ?></td>
											<td><?php  echo $value['sortnumber']; ?></td>
											<td>

											<?php if($value['status']=='Y'){  ?>

												<?=  $this->Html->link('Active', ['action' => 'status',$value->id,'N'  ],['class'=>'badge badge-success']) ?>
											<?php  }else { ?>
												<?=  $this->Html->link('Inactive', ['action' => 'status',$value->id,'Y'],['class'=>'badge badge-warning']) ?>
											<?php } ?>

										</td>
										<td class="actions">
											<!--<?= $this->Html->link(__('View'), ['action' => 'view', $user->id]) ?> -->
											<?php // echo $this->Html->link(__(''), ['action' => 'edit', $value->id,],array('class'=>'fa fa-pencil','title'=>'Edit','style'=>'font-size:24px;')) ?>

											<?php
											echo $this->Html->link('', [
												'action' => 'delete',
												$value->id
											],['class'=> 'fa fa-trash','style'=>'font-size:19px; color:#FF0000;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this area')"]); ?>

											<?php  ?>
										</td>
									</tr>
								<?php } } else{ ?>
									<tr>
										<td colspan="12">No Data Available</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
						<?php echo $this->element('admin/pagination'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

