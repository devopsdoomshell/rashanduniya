<style>

#myUL, #testUL, #retail{
    position: relative;
    z-index: 999;
  }
  #myUL ul, #testUL ul, #retail ul {
    list-style-type: none;
    overflow-y: scroll;
    margin: 0;
    padding: 0;
    max-height:100px;
    position: absolute;
    background-color: #fff;
  }

  #myUL li, #testUL li, #retail li {
    font: 200 20px/1.5 Helvetica, Verdana, sans-serif;
    border-bottom: 1px solid #ccc;
  }

  #myUL li  li:last-child {
    border: none;
  }

  #testUL li  li:last-child {
    border: none;
  }

  #retail li  li:last-child {
    border: none;
  }
  
  #myUL li  a, #testUL li a, #retail li a {
    text-decoration: none;
    color: #000;
    display: block;
    width: 200px;

    -webkit-transition: font-size 0.3s ease, background-color 0.3s ease;
    -moz-transition: font-size 0.3s ease, background-color 0.3s ease;
    -o-transition: font-size 0.3s ease, background-color 0.3s ease;
    -ms-transition: font-size 0.3s ease, background-color 0.3s ease;
    transition: font-size 0.3s ease, background-color 0.3s ease;
  }
.showmsg{
  display: none;
  color: red;
}

.anchr{margin-top: 26px; margin-bottom: 0px;}
.anchrr{margin-top: 8px; margin-bottom: 0px;}

</style>
<div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li><a href="<?php echo ADMIN_URL; ?>dashboard ">Dashboard</a></li>
          <li><a href="<?php echo ADMIN_URL; ?>wallet/index ">Wallet Manager</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>  

  <?php echo $this->Flash->render(); ?>
  <?php echo $this->Form->create('wallet',array(
   'class'=>'form-horizontal',
   'controller'=>'wallet',
   'action'=>'add',
   'enctype' => 'multipart/form-data',
   'validate' )); ?>

   <div class="col-lg-12">
    <div class="card">
      <div class="card-header"><strong>Add Wallet Money</strong></div>
      <div class="card-body card-block">

        <div class="col-sm-2">
          <div class="form-group">
            <label><strong>Name</strong></label>
            <?php
            echo $this->Form->input('name', array('class' => 'longinput form-control input-medium','placeholder'=>'Enter Promo Name' ,'type'=>'text','label'=>false,'required','autocomplete'=>'off')); ?>
          </div>
        </div>

         <div class="col-sm-2">
          <div class="form-group">
            <input type="hidden" name="mobile_id" id="mobile_ids">
                <label for="to" class="form-control-label">Mobile Number</label>
                <input type="text"  name="mobile" placeholder ="Mobile " class="longinput form-control input-medium secrh-mobile" autocomplete="off" >
                <div id="retail"><ul></ul></div>
          </div>
        </div>

        <div class="col-sm-2">
          <div class="form-group">
            <input type="hidden" name="user_id" id="retail_ids">
            <label><strong>Retailer Name</strong></label>
            <?php
            echo $this->Form->input('retailername', array('class' => 'longinput form-control input-medium secrh-retail','placeholder'=>'Retailers Name ','readonly' ,'type'=>'text','label'=>false,'autocomplete'=>'off')); ?>
            <span class="showmsg">"Please enter correct name"</span>
            <div id="myUL"><ul></ul></div>
          </div>
        </div>
       

        <div class="col-sm-1">
          <div class="form-group">
            <label><strong>Booth No.</strong></label>
            <?php
            echo $this->Form->input('boothno', array('class' => 'longinput form-control input-medium booth','placeholder'=>'Booth No.','readonly','label'=>false,'required','autocomplete'=>'off')); ?>
          </div>
        </div>

        <div class="col-sm-2">
          <div class="form-group">
            <label><strong>Wallet Amount</strong></label>
            <?php
            echo $this->Form->input('walletamount', array('class' => 'longinput form-control input-medium','placeholder'=>'value','id'=>'amount','type'=>'text','onkeypress'=>'return isNumber(event);','label'=>false,'required','autocomplete'=>'off')); ?>
          </div>
        </div>
<?php /* ?>
        <div class="col-sm-4" style="clear: both;">
          <div class="form-group">
            <label><strong>Wallet Percent</strong></label>
            <?php
            echo $this->Form->input('walletpercent', array('class' => 'longinput form-control input-medium','placeholder'=>'Enter wallet use percent','type'=>'text','onkeypress'=>'return isNumber(event);','label'=>false,'required','autocomplete'=>'off')); ?>
          </div>
        </div>
<?php */ ?>
        
        <div class="col-sm-2">
          <div class="form-group">
            <label><strong>Effective Date</strong></label>
            <input type="text" id="datepicker2" name="effectivedate" placeholder ="Date From" class="longinput form-control input-medium" readonly>
          </div>
        </div>

        <div class="col-sm-2">
          <div class="form-group">
            <label><strong>Expiry Date</strong></label>
            <input type="text" id="datepicker1" name="expirydate" placeholder ="Date From" class="longinput form-control input-medium" readonly>
          </div>
        </div>
        <?php /* ?>
        <div class="col-sm-4">
          <div class="form-group">
            <label><strong>Wallet Description</strong></label>
            <?php
            echo $this->Form->input('description', array('class' => 'longinput form-control input-medium','placeholder'=>'Description' ,'type'=>'textarea','label'=>false,'required','autocomplete'=>'off')); ?>
          </div>
        </div>
<?php */ ?>
        <div class="col-sm-2 anchr">
          <div class="form-group">
            <!-- <div class="col-sm-1">
              <a href="<?php echo ADMIN_URL ?>wallet/index" class="btn btn-primary " >Back</a>
           </div> -->
                <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>

      </div>
    </div>
  </div>
  <?php echo $this->Form->end(); ?>

<script type="text/javascript">

function cllbckretailnum(id,cid) { 
  $('.secrh-mobile').val(id);
  $('#mobile_ids').val(cid);
  $('#retail' ).hide();
}

  $( function() {
  $('.secrh-mobile').bind('keyup',function(){  
  var pos=$(this).val();
//alert(pos);
  $( '#retail' ).show();
  $( '#mobile_ids' ).val('');
var count=pos.length;
if(count > 0)
  {
  $.ajax({ 
    type: 'POST', 
    url: '<?php echo SITE_URL; ?>app/retailmobileFilter',
    data: {'fetch':pos},
    success: function(data){  console.log(data);
    $('#retail ul').html(data);
    },    
  }); 
    }else{
    $( '#retail' ).hide();  
    }   
  });     
});



$('#retail').click(function(){
  var relnum= $('.secrh-mobile').val();
 // alert(relname);
  $.ajax({ 
    type: 'POST', 
    url: '<?php echo SITE_URL; ?>admin/Wallet/searchbooth',
    data: {'relnum':relnum},
      success: function(data){ 
        var obj = JSON.parse(data);
      $('.booth').val(obj.boothno);
      $('.secrh-retail').val(obj.names);
      $('#retail_ids').val(obj.userid);
    },    
  });
});

 $("#amount").keyup(function(){
  var mvalue= $("#amount").val(); 
  if(mvalue=='0'){
    var mvalue= $("#amount").val('');
    }
 });
</script>
 <script>
function isNumber(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
     alert("Please Enter Only Numeric Characters!!!!");
      return false;
  }
  return true;

}
</script>


  <script src="https://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
  <script>
    $( function() {

      var dateFormat = 'dd-mm-yy',
      from = $( "#datepicker2" )
      .datepicker({
        minDate:0,
        dateFormat: 'dd-mm-yy',

        changeMonth: true,
        numberOfMonths: 1
      })
      .on( "change", function() {
        to.datepicker( "option", "minDate", getDate( this ) );
      }),
      to = $( "#datepicker1" ).datepicker({
        dateFormat: 'dd-mm-yy',

        changeMonth: true,
        numberOfMonths: 1
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });

      function getDate( element ) {
        var date;
        try {
          date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
          date = null;
        }

        return date;
      }
    } );
  </script>







