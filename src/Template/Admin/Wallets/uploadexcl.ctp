 <div class="modal-header">
  <h6 class="modal-title">Upload Wallet CSV</h6>
  <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<div class="modal-body">    

  <?php echo $this->Flash->render(); ?>
   
  <?php echo $this->Form->create('pricechange',array(
   'class'=>'form-horizontal',
   'controller'=>'products',
   'action'=>'uploadexcl',
   'enctype' => 'multipart/form-data',
   'validate' )); ?>

   <div class="col-lg-12">
    <div class="card">
     
      
      <div class="card-body card-block">
 
<div class="form-group">
  <label><strong>Upload Wallet CSV</strong></label>
  <?php
  echo $this->Form->input('uploadexcl', array('class' => 'longinput form-control input-medium' ,'type'=>'File','label'=>false)); ?>
</div>

<div class="col-sm-12">
  <?php 
  echo $this->Form->submit('Upload', array('action'=>'uploadexcl','title' => 'Update','div'=>false,
  'class'=>array('btn btn-primary btn-sm'))); ?>
  
</div>



</div>
</div>
</div>
<?php echo $this->Form->end(); ?>


</div>






