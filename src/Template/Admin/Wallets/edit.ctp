<style>

  #myUL{
    position: relative;
    z-index: 999;
  }
  #myUL ul {
    list-style-type: none;
    overflow-y: scroll;
    margin: 0;
    padding: 0;
    max-height:100px;
    position: absolute;
    background-color: #fff;
  }

  #myUL li {
    font: 200 20px/1.5 Helvetica, Verdana, sans-serif;
    border-bottom: 1px solid #ccc;
  }

  #myUL li  li:last-child {
    border: none;
  }

  
  #myUL li  a {
    text-decoration: none;
    color: #000;
    display: block;
    width: 200px;

    -webkit-transition: font-size 0.3s ease, background-color 0.3s ease;
    -moz-transition: font-size 0.3s ease, background-color 0.3s ease;
    -o-transition: font-size 0.3s ease, background-color 0.3s ease;
    -ms-transition: font-size 0.3s ease, background-color 0.3s ease;
    transition: font-size 0.3s ease, background-color 0.3s ease;
  }
.showmsg{
  display: none;
  color: red;
}
.anchr{margin-top: 26px; margin-bottom: 0px;}
</style>
<div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li><a href="<?php echo ADMIN_URL; ?>dashboard ">Dashboard</a></li>
          <li><a href="<?php echo ADMIN_URL; ?>wallet/index ">Wallet Manager</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>  

  <?php echo $this->Flash->render(); ?>
  <?php echo $this->Form->create($walletadd,array(
   'class'=>'form-horizontal',
   'controller'=>'wallet',
   'action'=>'edit',
   'enctype' => 'multipart/form-data',
   'validate' )); ?>

   <div class="col-lg-12">
    <div class="card">
      <div class="card-header"><strong>Edit Wallet Money</strong></div>
      <div class="card-body card-block">

        <div class="col-sm-2">
          <div class="form-group">
            <label><strong>Name</strong></label>
            <?php
            echo $this->Form->input('name', array('class' => 'longinput form-control input-medium','placeholder'=>'Enter Wallet Name' ,'type'=>'text','label'=>false,'required','autocomplete'=>'off')); ?>
          </div>
        </div>

        <div class="col-sm-2">
          <div class="form-group">
            <input type="hidden" name="user_id" id="retail_ids">
            <label><strong>Retailer Name</strong></label>
            <?php
            echo $this->Form->input('retailername', array('class' => 'longinput form-control input-medium secrh-retail','placeholder'=>'Enter Retailers Name Keyword' ,'type'=>'text','label'=>false,'required','autocomplete'=>'off')); ?>
            <span class="showmsg">"Please enter correct name"</span>
            <div id="myUL"><ul></ul></div>
          </div>
        </div>

        <div class="col-sm-1">
          <div class="form-group">
            <label><strong>Booth No.</strong></label>
            <?php
            echo $this->Form->input('boothno', array('class' => 'longinput form-control input-medium booth','placeholder'=>'Booth No.','readonly','label'=>false,'required','autocomplete'=>'off')); ?>
          </div>
        </div>

        <div class="col-sm-2">
          <div class="form-group">
            <label><strong>Wallet Amount</strong></label>
            <?php
            echo $this->Form->input('walletamount', array('class' => 'longinput form-control input-medium','placeholder'=>'value' ,'id'=>'amount','type'=>'text','onkeypress'=>'return isNumber(event);','label'=>false,'required','autocomplete'=>'off')); ?>
          </div>
        </div>
<?php /* ?>
        <div class="col-sm-4" style="clear: both;">
          <div class="form-group">
            <label><strong>Wallet Percent</strong></label>
            <?php
            echo $this->Form->input('walletpercent', array('class' => 'longinput form-control input-medium','placeholder'=>'Enter wallet use percent','type'=>'text','onkeypress'=>'return isNumber(event);','label'=>false,'required','autocomplete'=>'off')); ?>
          </div>
        </div>
<?php */ ?>
        <div class="col-sm-2" >
          <div class="form-group">
            <label><strong>Expiry Date</strong></label>
            <input type="text" id="datepicker1" name="expirydate" placeholder ="Date From" class="longinput form-control input-medium"value="<?php  echo date('d-m-Y', strtotime($walletadd['expirydate'])); ?>" readonly>
          </div>
        </div>
        <?php /* ?>
        <div class="col-sm-4">
          <div class="form-group">
            <label><strong>Wallet Description</strong></label>
            <?php
            echo $this->Form->input('description', array('class' => 'longinput form-control input-medium','placeholder'=>'Description' ,'type'=>'textarea','value'=>$walletadd['description'],'label'=>false,'required','autocomplete'=>'off')); ?>
          </div>
        </div>
<?php */ ?>
        <div class="col-sm-2 anchr">
          <div class="form-group">
           <!--  <div class="col-sm-1">
              <a href="<?php echo ADMIN_URL ?>wallet/index" class="btn btn-primary " >Back</a>
           </div> -->
                <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>

      </div>
    </div>
  </div>
  <?php echo $this->Form->end(); ?>

<script type="text/javascript">

function cllbckretail(id,cid) { 
  $('.secrh-retail').val(id);
  $('#retail_ids').val(cid);
  $('#myUL' ).hide();
}
$( function() {
  $('.secrh-retail').bind('keyup',function(){  
      var pos=$(this).val();
    $( '#myUL' ).show();  
    $( '#retail_ids' ).val('');
      var count=pos.length;
    if(count > 0)
      {
        $.ajax({ 
          type: 'POST', 
          url: '<?php echo SITE_URL; ?>app/retailFilter',
          data: {'fetch':pos},
            success: function(data){  
              console.log(data);
              if(data==''){
               // alert("please enter correct name");
               $( '#myUL' ).hide();
               $('.showmsg').css('display','block')
                $('.secrh-retail').val('');
              }else{
            $('#myUL ul').html(data);
             $('.showmsg').css('display','none'); }

            },    
        }); 
      }else{
          $( '#myUL' ).hide();  
      }   
  });     
});


$('#myUL').click(function(){
  var relname= $('.secrh-retail').val();
 // alert(relname);
  $.ajax({ 
          type: 'POST', 
          url: '<?php echo SITE_URL; ?>admin/Wallet/searchbooth',
          data: {'relname':relname},
            success: function(data){ 
            $('.booth').val(data);
          },    
  });
});



 $("#amount").keyup(function(){
  var mvalue= $("#amount").val(); 
  if(mvalue=='0'){
    var mvalue= $("#amount").val('');
    }
 });
</script>

 <script>
function isNumber(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
     alert("Please Enter Only Numeric Characters!!!!");
      return false;
  }
  return true;

}
</script>
  <script src="https://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
  <script>
    $( function() {

      var dateFormat = 'dd-mm-yy',
      from = $( "#datepicker1" )
      .datepicker({
        minDate:0,
        dateFormat: 'dd-mm-yy',

        changeMonth: true,
        numberOfMonths: 1
      })
      .on( "change", function() {
        to.datepicker( "option", "minDate", getDate( this ) );
      }),
      to = $( "#datepicker2" ).datepicker({
        dateFormat: 'dd-mm-yy',

        changeMonth: true,
        numberOfMonths: 1
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });

      function getDate( element ) {
        var date;
        try {
          date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
          date = null;
        }

        return date;
      }
    } );
  </script>







