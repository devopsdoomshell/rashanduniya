<p class="pull-right"><?php echo !empty($total) ? $total : null;
?></p>
<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
									<th class="align-top">S.No.</th>
									<th class="align-top">Date</th>
									<th class="align-top">Mobile</th>
									<th class="align-top">Amount</th>
									<th class="align-top">Source</th>
									<th class="align-top">Expiry Date</th>
									<th class="align-top">Transaction Type</th>
								</tr>
							</thead>
							<tbody >
								<?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Coupancode']['page'] - 1) * $this->request->params['paging']['Coupancode']['perPage'];
if (isset($wallets) && !empty($wallets)) {
    foreach ($wallets as $value) {$i++;
        $adid = $value['adminuser_id'];
        $adminid = $this->Comman->walletadmin($adid);
        //pr($adminid);
        ?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo date('d M Y', strtotime($value['created'])); ?></td>
											<td><?php echo $value['user']['mobile']; ?></td>
											<td><?php echo $value['amount']; ?></td>
											<td><?php if ($value['amount_type'] == 'payment') {
            echo "Self";
        } else if ($value['amount_type'] == 'promotional') {
            echo 'Coupon: ' . $this->Comman->findWalletCoupon($value['id']);
        } else if ($value['amount_type'] == 'expense') {
            echo 'Order: ' . $value['order_id'];
        } else if ($value['amount_type'] == 'admin') {
            echo 'Admin';
        }

        ?></td>
		<td><?php if ($value['is_expiry'] == 1) {
            echo date('d M Y', strtotime($value['expiry_date']));
        } else {
            echo '--';
        }?></td>
											<td><?php if ($value['amount_type'] == 'payment') {?>
            <span style="color:green;">Credit</span>
        <?php } else if ($value['amount_type'] == 'promotional' && $value['wallet_id'] == 0) {?>
			<span style="color:green">Credit</span>
			<?php } else if ($value['amount_type'] == 'promotional' && $value['wallet_id'] == 0) {?>
				<span style="color:red;">Debit</i></span>
        <?php } else if ($value['amount_type'] == 'expense') {?>
			<span style="color:red;">Debit</i></span>
        <?php } else if ($value['amount_type'] == 'admin') {?>
			<span style="color:green;">Credit</span>
        <?php }

        ?></td>
										</tr>
									<?php }} else {?>
										<tr>
											<td colspan="12">No Data Available</td>
										</tr>
									<?php }?>
								</tbody>
							</table>
							<?php echo $this->element('admin/pagination'); ?>