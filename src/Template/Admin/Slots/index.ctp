<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<?php echo $this->Flash->render(); ?>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Slot Manager</strong>
                    </div>
<?php echo $this->Form->create('slots', array(
    'class' => 'form-horizontal',
    'controller' => 'Slots',
    'action' => 'add',
    'enctype' => 'multipart/form-data',
    'validate')); ?>
    <?php echo $this->Form->input('update', ['class' => 'btn btn-primary pull-right', 'type' => 'submit', 'style' => 'margin:5px 5px 5px 0px;']) ?>
<table id="1bootstrap-data-table" class="table table-striped table-bordered ">
<thead>
<th>Weekdays</th>
<?php foreach ($slotTimings as $slotTiming) {?>
<td><?php echo $slotTiming ?></td>
<?php }?>
</thead>
<tbody>
<tr>
<td>Monday</td>
<?php foreach ($slotTimings as $slotTiming) {?>
<td><input type="checkbox" name="slot[Mon][]" value="<?php echo $slotTiming ?>" <?php if (in_array($slotTiming, $slotsInfo['Mon'])) {?> checked <?php }?>></td>
<?php }?>
</tr>
<tr>
<td>Tuesday</td>
<?php foreach ($slotTimings as $slotTiming) {?>
<td><input type="checkbox" name="slot[Tue][]" value="<?php echo $slotTiming ?>" <?php if (in_array($slotTiming, $slotsInfo['Tue'])) {?> checked <?php }?>></td>
<?php }?>
</tr>
<tr>
<td>Wednesday</td>
<?php foreach ($slotTimings as $slotTiming) {?>
<td><input type="checkbox" name="slot[Wed][]" value="<?php echo $slotTiming ?>" <?php if (in_array($slotTiming, $slotsInfo['Wed'])) {?> checked <?php }?>></td>
<?php }?>
</tr>
<tr>
<td>Thursday</td>
<?php foreach ($slotTimings as $slotTiming) {?>
<td><input type="checkbox" name="slot[Thu][]" value="<?php echo $slotTiming ?>" <?php if (in_array($slotTiming, $slotsInfo['Thu'])) {?> checked <?php }?>></td>
<?php }?>
</tr>
<tr>
<td>Friday</td>
<?php foreach ($slotTimings as $slotTiming) {?>
<td><input type="checkbox" name="slot[Fri][]" value="<?php echo $slotTiming ?>" <?php if (in_array($slotTiming, $slotsInfo['Fri'])) {?> checked <?php }?>></td>
<?php }?>
</tr>
<tr>
<td>Saturday</td>
<?php foreach ($slotTimings as $slotTiming) {?>
<td><input type="checkbox" name="slot[Sat][]" value="<?php echo $slotTiming ?>" <?php if (in_array($slotTiming, $slotsInfo['Sat'])) {?> checked <?php }?>></td>
<?php }?>
</tr>
<tr>
<td>Sunday</td>
<?php foreach ($slotTimings as $slotTiming) {?>
<td><input type="checkbox" name="slot[Sun][]" value="<?php echo $slotTiming ?>" <?php if (in_array($slotTiming, $slotsInfo['Sun'])) {?> checked <?php }?>></td>
<?php }?>
</tr>
</tbody>
</table>
<?php $this->Form->end();?>
                </div>
            </div>
        </div>
    </div>
</div>