<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
								<th class="align-top">S.No.</th>
								<th scope="col">Name</th>
								<th scope="col">Mobile</th>
								<th scope="col">Default Address</th>
								<th scope="col">City</th>
								<th scope="col">Active Order</th>
								<th scope="col">Completed Order</th>
								<th scope="col">Completed Order</th>
								<th scope="col">Add date</th>
								<th scope="col" class="actions">Action</th>
								</tr>
							</thead>
							<tbody >
								<?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Category']['page'] - 1) * $this->request->params['paging']['Category']['perPage'];
if (isset($customers) && !empty($customers)) {
    foreach ($customers as $customer) {$i++;
        ?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo $customer['name']; ?></td>
											<td><?php echo $customer->mobile; ?></td>
											<td><?php echo $customer['customer_addresses'][0]['flat_no'] . ',' . $customer['customer_addresses'][0]['building_name'] . '<br>' . $customer['customer_addresses'][0]['address_1'] . '<br>' . $customer['customer_addresses'][0]['address_2'] . '<br>' . $customer['customer_addresses'][0]['city']; ?></td>
											<td><?php echo $cities[$customer->city_id]; ?></td>
                                            <td><a href="#">0</a></td>
                                            <td><a href="#">0</a></td>
                                            <td><a href="#">0</a></td>
                                            <td><?php echo date('d-m-Y', strtotime($customer['created'])); ?> </td>
											<td>
												<?php if ($customer['status'] == 1) {?>
													<?=$this->Html->link('', ['action' => 'status', $customer->id, '0'], ['class' => 'fa fa-toggle-on', 'style' => 'color:green;font-size:20px;margin-right:5px'])?>
												<?php } else {?>
													<?=$this->Html->link('', ['action' => 'status', $customer->id, '1'], ['class' => 'fa fa-toggle-off', 'style' => 'color:red;font-size:20px;margin-right:5px'])?>
												<?php }?>
												<?php echo $this->Html->link(__(''), ['action' => 'add', $customer->id], array('class' => 'fa fa-pencil', 'title' => 'Edit', 'style' => 'font-size:20px;margin-right:5px')) ?>
											</td>
										</tr>
									<?php }} else {?>
										<tr><td colspan="10" style="text-align:center;">No Data Available</td></tr>
									<?php }?>
								</tbody>
							</table>
							</table>
							<?php echo $this->element('admin/pagination'); ?>