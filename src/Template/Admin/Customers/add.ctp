<!-- https://www.youtube.com/watch?v=OK_JCtrrv-c -->
<style type="text/css">
.are{margin-top: -6px;}
.bfh-timepicker-popover table{width:280px;margin:0}
select.form-control:not([size]):not([multiple]) {
  height: calc(1.99rem + 2px) !important;
}
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
</style>
<div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li><a href="<?php echo ADMIN_URL; ?>dashboard ">Dashboard</a></li>
          <li><a href="<?php echo ADMIN_URL; ?>customer/index ">Customer Manager</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!--<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">-->
  <?php echo $this->Flash->render(); ?>
  <?php
if (isset($customer)) {
    echo $this->Form->create($customer, array(
        'class' => 'form-horizontal',
        'controller' => 'customers',
        'action' => 'edit/' . $customer['id'],
        'enctype' => 'multipart/form-data',
        'validate', 'autocomplete' => 'off'));
} else {
    echo $this->Form->create('customers', array(
        'class' => 'form-horizontal',
        'controller' => 'customers',
        'action' => 'add',
        'enctype' => 'multipart/form-data',
        'validate', 'autocomplete' => 'off'));
}
?>

   <div class="col-lg-12">
    <div class="card">
      <div class="card-header"><strong>Add Customer</strong></div>
      <div class="card-body card-block">
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Customer Name</strong></label>
            <?php
echo $this->Form->input('name', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Customer Name', 'type' => 'text', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Mobile Number</strong></label>
            <?php
echo $this->Form->input('mobile_1', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Mobile Number 1', 'type' => 'number', 'label' => false, 'autocomplete' => 'off', 'required')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Email</strong></label>
            <?php
echo $this->Form->input('email', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Email', 'type' => 'email', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
          </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Flat No.</strong></label>
            <?php
echo $this->Form->input('flat_no', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Flat No', 'type' => 'number', 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Building Name</strong></label>
            <?php
echo $this->Form->input('building_name', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Building Name', 'type' => 'text', 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Address Line 1</strong></label>
            <?php
echo $this->Form->input('address_1', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Address Line 1', 'type' => 'text', 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Address Line 2</strong></label>
            <?php
echo $this->Form->input('address_2', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Address Line 2', 'type' => 'text', 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>City</strong></label>
            <?php
echo $this->Form->input('city_id', array('class' => 'longinput form-control input-medium', 'required', 'type' => 'select', 'empty' => 'Select City', 'options' => $cities, 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
        </div>
        <div class="col-sm-12">
          <div class="form-group">
            <div class="col-sm-1">
              <a href="<?php echo ADMIN_URL ?>customers/index" class="btn btn-primary " >Back</a>
           </div>
           <div class="col-sm-1">
            <?php if (isset($customer['id'])) {
    echo $this->Form->submit('Update', array('title' => 'Update', 'div' => false,
        'class' => array('btn btn-primary btn-sm')));} else {?>
                <button type="submit" class="btn btn-success">Submit</button>
              <?php }?>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <?php echo $this->Form->end(); ?>

<script type="text/javascript">
$(document).ready(function() {
    $('#categories').select2({
      placeholder: "Select Categories",
      allowClear: true
    });
});
  function ValidateFileUpload() {
    var fuData = document.getElementById('fileChooser');
    var FileUploadPath = fuData.value;

//To check if user upload any file
if (FileUploadPath == '') {
  alert("Please upload an image");
} else {
  var Extension = FileUploadPath.substring(
    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
//The file uploaded is an image
if (Extension == "png" || Extension == "jpeg" || Extension == "jpg") {
// To Display
var img = document.getElementById("fileChooser");
 if(img.files[0].size <  1048576)  // validation according to file size
 {
       //alert(img.files[0].size);
       document.getElementById("showfeatimage").style.display = "none";
       document.getElementById("showfeatsize").style.display = "none";
       return true;
     }
     else{
      document.getElementById("showfeatimage").style.display = "none";
      document.getElementById("showfeatsize").style.display = "block";
      document.getElementById("fileChooser").value = "";
      return false;
    }
  }
//The file upload is NOT an image
else {
 document.getElementById("showfeatimage").style.display = "block";
 document.getElementById("fileChooser").value = "";
 return false;
}
}
}
</script>
<script>
$(document).ready(function(){
  $(document).on('keyup','.commision',function(){
    let value=$(this).val();
    if(parseInt(value) > 100){
      alert('Commision Cannot be greater than 100');
      $(this).val('');
      return false;
    }
  })
});
</script>






