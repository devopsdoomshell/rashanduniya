	<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
								<th class="align-top">S.No.</th>
								<th scope="col">Icon</th>
								<th scope="col">Image</th>
								<th scope="col">Name</th>
								<th scope="col">Discount</th>
								<th scope="col">GST Code</th>
								<th scope="col">HSN Code</th>
								<th scope="col">Created</th>
								<th scope="col" class="actions"><?=__('Actions')?></th>
								</tr>
							</thead>
							<tbody >
								<?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Category']['page'] - 1) * $this->request->params['paging']['Category']['perPage'];
if (isset($categories) && !empty($categories)) {
    foreach ($categories as $category) {$i++; //pr($value); ?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><img src="<?php echo SITE_URL; ?>categoryicon/thumb/<?php echo $category['cat_icon']; ?>" height="100px" width="100px"></td>
											<td><img src="<?php echo SITE_URL; ?>categoryimage/thumb/<?php echo $category['cat_image']; ?>" height="100px" width="100px"></td>
                <?php /* ?> <td><?= $category->has('parent_category') ? $this->Html->link($category->parent_category->name, ['controller' => 'Categories', 'action' => 'view', $category->parent_category->id]) : '' ?></td> <?php */?>
                <td>
                <?php if ($category['parent_id'] == null) {?>
				<strong><?php echo $category->name; ?></strong>
											<?php } else {?>
							<?php echo $category->name; ?>

											<?php }?>

                </td>
				<?php if ($category->discount) {?>
				<td><?php echo $category->discount; ?></td>
				<?php } else {?>
				<td>--</td>
				<?php }?>

				<?php if ($category->gstcode) {?>
				<td><?php echo $category->gstcode; ?></td>
				<?php } else {?>
				<td>--</td>
				<?php }?>
					<?php if ($category->hsncode) {?>
				<td><?php echo $category->hsncode; ?></td>
				<?php } else {?>
				<td>--</td>
				<?php }?>
              <td><?php echo date('d-m-Y', strtotime($category->created)); ?></td>
											<td>
											<?php if ($category['status'] == 'Y') {?>
													<?=$this->Html->link('', ['action' => 'status', $category->id, 'N'], ['class' => 'fa fa-toggle-off', 'style' => 'color:green;font-size:20px;margin-right:5px'])?>
												<?php } else {?>
													<?=$this->Html->link('', ['action' => 'status', $category->id, 'Y'], ['class' => 'fa fa-toggle-off', 'style' => 'color:red;font-size:20px;margin-right:5px'])?>
												<?php }?>
												<?php echo $this->Html->link(__(''), ['action' => 'edit', $category->id], array('class' => 'fa fa-pencil', 'title' => 'Edit', 'style' => 'font-size:20px;margin-right:5px')) ?>
												<?php
echo $this->Html->link('', [
        'action' => 'delete',
        $category->id,
    ], ['class' => 'fa fa-trash', 'style' => 'font-size:20px; color:#FF0000;', "onClick" => "javascript: return confirm('Are you sure do you want to delete this category')"]); ?>
												<?php ?>
											</td>
										</tr>
									<?php }} else {?>
										<tr><td colspan="6" style="text-align:center;">No Data Available</td></tr>
									<?php }?>
								</tbody>
							</table>
							<?php echo $this->element('admin/pagination'); ?>