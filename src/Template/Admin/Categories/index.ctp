<style>
.anchr{margin-top: 26px; margin-bottom: 0px;}
.anchrr{margin-top: 8px; margin-bottom: 0px;}
.badge{font-size: 11px !important;}
</style>
<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
		</div>
	</div>
	<div class="col-sm-8">
		<div class="page-header float-right">
			<div class="page-title">
				<ol class="breadcrumb text-right">
					<li><a href="<?php echo SITE_URL; ?>admin/dashboard ">Dashboard</a></li>
					<li><a href="<?php echo ADMIN_URL; ?>categories/index ">Category Manager</a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<?php echo $this->Flash->render(); ?>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Category Manager</strong>
						<a href="<?php echo SITE_URL; ?>admin/categories/add" ><strong class=" btn btn-info card-title pull-right">Add</strong></a>

					</div>


<div class="row" style="padding: 10px;">
    <script>
    $(document).ready(function () {
        $("#category_search").bind("submit", function (event)
        {
             var param = $("#category_search").serialize();
             var siteurl = '<?php echo SITE_URL; ?>';
           window.history.pushState("test", "Title", siteurl+'admin/categories?'+param);
            //alert(this.href);
           //window.history.pushState("test", "Title", "/tets");
            $.ajax({
                async:true,
                data:$("#category_search").serialize(),
                dataType: "html",
                type:"get",

                url:"<?php echo ADMIN_URL; ?>product/search",
                success:function (data) {
                //alert(data);
                $("#search_data").html(data); },
            });
            return false;
        });

    });
    //]]>
    $(document).on('click', '.pagination a', function() {
var target = $(this).attr('href');
if(!target)
return false;
$.get(target, function(data) {
$('#search_data').html( data );
}, 'html');
return false;
});
    </script>


    	<?php echo $this->Form->create('Mysubscription', array('type' => 'get', 'inputDefaults' => array('div' => false, 'label' => false), 'id' => 'category_search', 'class' => 'form-horizontal', 'method' => 'get')); ?>
            <div class="col-sm-6">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Category Name</label>
            		<?php echo $this->Form->input('categorydata', array('class' => 'longinput form-control input-medium secrh-loc', 'placeholder' => 'Category Name', 'autocomplete' => 'off', 'type' => 'search', 'label' => false)); ?>
            		<div id="myUL"><ul></ul></div>
            	</div>
            </div>

  <div class="col-sm-1" style="padding-top: 32px;">
            	<?php if (isset($ticket['id'])) {
    echo $this->Form->submit('Update', array('title' => 'Update', 'div' => false,
        'class' => array('btn btn-primary btn-sm')));} else {?>
            			<button type="submit" class="btn btn-success" id="Mysubscriptionevent">Search</button>
            		<?php }?>
            	</div>
            	<?php echo $this->Form->end(); ?>
</div>
					<div id="search_data" class="card-body">
						<?php //echo $this->Paginator->limitControl([10 => 10, 15 => 15,20=>20,25=>25,30=>30]);?>
						<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
								<th class="align-top">S.No.</th>
								<th scope="col">Icon</th>
								<th scope="col">Image</th>
								<th scope="col">Name</th>
								<th scope="col">Discount</th>
								<th scope="col">GST Code</th>
								<th scope="col">HSN Code</th>
								<th scope="col">Created</th>
								<th scope="col" class="actions"><?=__('Actions')?></th>
								</tr>
							</thead>
							<tbody >
								<?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Category']['page'] - 1) * $this->request->params['paging']['Category']['perPage'];
if (isset($categories) && !empty($categories)) {
    foreach ($categories as $category) {$i++; //pr($value); ?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><img src="<?php echo SITE_URL; ?>categoryicon/thumb/<?php echo $category['cat_icon']; ?>" height="100px" width="100px"></td>
											<td><img src="<?php echo SITE_URL; ?>categoryimage/thumb/<?php echo $category['cat_image']; ?>" height="100px" width="100px"></td>
                <?php /* ?> <td><?= $category->has('parent_category') ? $this->Html->link($category->parent_category->name, ['controller' => 'Categories', 'action' => 'view', $category->parent_category->id]) : '' ?></td> <?php */?>
                <td>
                <?php if ($category['parent_id'] == null) {?>
				<strong><?php echo $category->name; ?></strong>
											<?php } else {?>
							<?php echo $category->name; ?>

											<?php }?>

                </td>
				<?php if ($category->discount) {?>
				<td><?php echo $category->discount; ?></td>
				<?php } else {?>
				<td>--</td>
				<?php }?>

				<?php if ($category->gstcode) {?>
				<td><?php echo $category->gstcode; ?></td>
				<?php } else {?>
				<td>--</td>
				<?php }?>
					<?php if ($category->hsncode) {?>
				<td><?php echo $category->hsncode; ?></td>
				<?php } else {?>
				<td>--</td>
				<?php }?>
              <td><?php echo date('d-m-Y', strtotime($category->created)); ?></td>
											<td>
												<?php if ($category['status'] == 'Y') {?>
													<?=$this->Html->link('', ['action' => 'status', $category->id, 'N'], ['class' => 'fa fa-toggle-off', 'style' => 'color:green;font-size:20px;margin-right:5px'])?>
												<?php } else {?>
													<?=$this->Html->link('', ['action' => 'status', $category->id, 'Y'], ['class' => 'fa fa-toggle-off', 'style' => 'color:red;font-size:20px;margin-right:5px'])?>
												<?php }?>
												<?php echo $this->Html->link(__(''), ['action' => 'edit', $category->id], array('class' => 'fa fa-pencil', 'title' => 'Edit', 'style' => 'font-size:20px;margin-right:5px')) ?>
												<?php
echo $this->Html->link('', [
        'action' => 'delete',
        $category->id,
    ], ['class' => 'fa fa-trash', 'style' => 'font-size:20px; color:#FF0000;', "onClick" => "javascript: return confirm('Are you sure do you want to delete this category')"]); ?>
												<?php ?>
											</td>
										</tr>
									<?php }} else {?>
										<tr><td colspan="6" style="text-align:center;">No Data Available</td></tr>
									<?php }?>
								</tbody>
							</table>
							<?php echo $this->element('admin/pagination'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>











