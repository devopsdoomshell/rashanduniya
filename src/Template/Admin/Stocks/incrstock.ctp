<style type="text/css">
.borderss{
    vertical-align: sub;
    border:none;
}
</style>
<div class="modal-header">
          <h4 class="modal-title">Increase Stock</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
<div class="modal-body">
  <?php echo $this->Flash->render(); ?>
  <?php echo $this->Form->create($prod,array(
   'class'=>'form-horizontal',
   'controller'=>'products',
   'action'=>'incrstock',
   'enctype' => 'multipart/form-data',
   'validate' )); ?>

   <div class="col-lg-12">
    <div class="card">
      <div class="card-header"><strong>Product Manager</strong></div>
      <div class="card-body card-block">
        <div class="col-sm-6">
        <div class="form-group">
          <label><strong>Exist Quantity</strong></label>
          <?php 
          echo $this->Form->input('qunatity', array('class' => 'longinput form-control input-medium','placeholder'=>'Product Quantity','type'=>'text','value'=>$totlqty,'readonly','label'=>false)); ?>
          <?php 
          echo $this->Form->input('qunatity', array('placeholder'=>'Product Quantity','type'=>'hidden','label'=>false)); ?>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
          <label><strong>Add Quantity</strong></label>
          <?php
          echo $this->Form->input('newqunatity', array('class' => 'longinput form-control input-medium','placeholder'=>'Product Quantity' ,'type'=>'text','label'=>false,'required','onkeypress'=>'return isNumber(event);','id'=>'amountsize','value'=>'')); ?>
        </div>
      </div>


      <div class="col-sm-12">
        <div class="form-group">

         <div class="col-sm-1">
          <?php 
          echo $this->Form->submit('Update', array('title' => 'Update','div'=>false,
          'class'=>array('btn btn-primary btn-sm'))); ?>
        </div>
      </div>
    </div>

  </div>
</div>
</div>
<?php echo $this->Form->end(); ?>
 <script>
function isNumber(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
     alert("Please Enter Only Numeric Characters!!!!");
      return false;
  }
  return true;

}
</script>

<script>
   $("#amountsize").keyup(function(){
  var mvalue= $("#amountsize").val(); 
  if(mvalue=='0'){
    var mvalue= $("#amountsize").val('');
    }
 });
</script>