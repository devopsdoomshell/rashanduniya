<style type="text/css">
.borderss{
    vertical-align: sub;
    border:none;
}
</style>
<div class="modal-header">
          <h4 class="modal-title">Stock Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">


<?php $role_id=$this->request->session()->read('Auth.User.role_id');  ?> 
    <table id="bootstrap-data-table" class="table table-striped table-bordered ">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('User Name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Product') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Added Stock') ?></th>
                <!-- <th scope="col"><?= $this->Paginator->sort('Old Stock') ?></th> -->
                <th scope="col"><?= $this->Paginator->sort('Date') ?></th>
                <?php if ($role_id=='1') { ?>
                <th scope="col"><?= $this->Paginator->sort('Action') ?></th>
            <?php } ?>
            </tr>
        </thead>
        <tbody>
             
            <?php  //pr($this->request->params); die;
           // pr($stcklastid['new_qty']);
           if(isset($stck) &&     !empty($stck)){ 
            foreach ($stck as $value){ //pr($value);
                     ?>
            <tr>
                <td><?php if ($value['user']['name']) {
                  echo $value['user']['name'];  
                }else{
                    echo "---";
                }  ?></td>
                <td><?php echo $value['product']['productnameen']; ?></td>
            
                <td>
                    <input type="text" class="stockval" name="new_qty" data-val="<?php echo $value['id']; ?>"  value="<?php echo $value['new_qty']; ?>">
                    
                </td>
                <!-- <td><?php echo $value['exist_qty']; ?></td> -->
            
                
                <td><?php echo date('d-M-Y',strtotime($value['created'])); ?></td>
                <?php if ($role_id=='1') { ?>
                <td>
                <?php
                //echo $this->Html->link('', [
                    //'action' => 'delete', $value->id, $value->new_qty],['class'=> 'fa fa-trash','style'=>'font-size:19px; color:#FF0000;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this  Detail')"]); ?>
                <?php
               // echo $this->Html->link('', ['action' => 'edit', $value->id],['class'=> 'fa fa-pencil','style'=>'font-size:19px; color:#FF0000;']); ?>

               <button class="submits fa fa-pencil" name="sub"> </button> 
               
                </td>
            <?php } ?>
            </tr>
        <?php } }else{ ?>
          <tr><td colspan="5">No Data Available</td></tr>
        <?php } ?>
        </tbody>
    </table>
<script>
$('.submits').click(function(){
var stval=$('.stockval').val();
var stockid= $('.stockval').data('val');

    $.ajax({ 
    type: 'POST', 
    url: '<?php echo SITE_URL; ?>admin/stocks/edit',
    data: {'stockval':stval,'stkid':stockid},
    success: function(data){  
       //alert(data);
        if(data=='1'){
             location.reload();
        }
    },    
    });
});
</script> 

