<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">


        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                 <li><a href="<?php echo SITE_URL; ?>admin/dashboard ">Dashboard</a></li>
                 <li>Stock Manager</li>
             </ol>
         </div>
     </div>
 </div>
</div>

<!-- <?php //echo $this->Paginator->limitControl([10 => 10, 15 => 15,20=>20,25=>25,30=>30]);?> -->

<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="row">

            <?php echo $this->Flash->render(); ?>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Stock Detail</strong>
                        <!--<a href="<?php echo SITE_URL; ?>admin/static/add"><strong class=" btn btn-info card-title pull-right">Add</strong></a>   -->
                    </div>

                    <div class="card-body">

                        <table id="bootstrap-data-table" class="table table-striped table-bordered ">
                            <thead>
                                <tr>
                                    <th class="align-top">S.no</th>
                                    <th class="align-top">Product Type</th>
                                    <th class="align-top">Product Name</th>
                                    <th class="align-top">Instock</th>
                                    <th class="align-top">Orders</th>
                                    <th class="align-top">Balance</th>
                                    <th class="align-top">Increase Stock</th>
                                </tr>
                            </thead>
                           <tbody>
                                <?php  //pr($this->request->params); die;
                                $i=($this->request->params['paging']['Products']['page']-1) * $this->request->params['paging']['Products']['perPage'];
                                if(isset($product) &&     !empty($product)){ 
                                 	foreach ($product as $value){ $i++;
                                        $totalorders=$this->Comman->ordersof_product($value->id);
                                        $totalstock=$this->Comman->totalstockqty($value->id);
                                       // pr($totalstock);
                                     ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo  $value['ptype']; ?></td>
                                    <td><?php echo  ucfirst($value['productnameen']); ?>(<?php echo  ucfirst($value['productnamehi']); ?>)</td>
                                    <td><a href="<?php echo ADMIN_URL ?>stocks/stockdetails/<?php echo $value['id']; ?>" data-toggle="modal" class="addstock" style="font-weight: bolder;">
                                      <?php echo  $value['qunatity']+$totalstock['Sum']; ?></a></td>
                                    <td><?php 
                                    if ($totalorders=='') {
                                       echo "--";
                                    }else{
                                     echo $totalorders; } ?></td>
                                    
                                    <td><?php $balance=$value['qunatity']+$totalstock['Sum']-$totalorders;
                                    echo $balance; ?></td>
                                    <td><a href="<?php echo ADMIN_URL ?>stocks/incrstock/<?php echo $value['id']; ?>" data-toggle="modal" class="addstock" style="font-size: 20px; color: green"><i class="fa fa-cart-plus"></i></a></td>
                                </tr>
                                <?php } } else{ ?>
                                <tr>
                                <td colspan="12">No Data Available</td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                     <?php echo $this->element('admin/pagination'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo SITE_URL ?>datepicker/jquery-3.2.1.min.js"></script>
<script src="<?php echo SITE_URL ?>datepicker/bootstrap.min.js" ></script>
 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>-->
  <!-- Modal -->
  <!-- The Modal -->
  <div class="modal fade" id="myModals">
    <div class="modal-dialog">
      <div class="modal-content">
        
        <!-- Modal Header -->
        
        
        <!-- Modal body -->
        <div class="modal-body">

         
        </div>
        
        <!-- Modal footer -->

        
      </div>
    </div>
  </div>

  <script>
   $('.addstock').click(function(e){ 
    e.preventDefault();
    $('#myModals').modal('show').find('.modal-body').load($(this).attr('href'));
  });
</script>
<style type="text/css">
  .modal-dialog {
    max-width: 650px !important;
  }
</style>