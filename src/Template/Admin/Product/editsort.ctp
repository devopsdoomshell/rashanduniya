 <div class="modal-header">
  <h6 class="modal-title">Edit Sort order</h6>
  <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<div class="modal-body">    

  <?php echo $this->Flash->render(); ?>
  <?php echo $this->Form->create($prodata,array(
   'class'=>'form-horizontal',
   'controller'=>'products',
   'action'=>'editsort',
   'enctype' => 'multipart/form-data',
   'validate' )); ?>

   <div class="col-lg-12">
    <div class="card">
     
      <div class="card-header"><strong>Edit Sort order</strong></div>
      <div class="card-body card-block">
       <?php $i=0; foreach ($productid as $product_id) { $i++; ?>
        <div class="col-sm-4">
          <div class="form-group">
            <?php echo $this->Form->input('id', array('type'=>'hidden','value'=>$product_id['id'])); ?>

            <?php echo $this->Form->input('ptype', array('type'=>'hidden','value'=>$product_id['ptype'])); ?>
            <label><strong>Product Name </strong></label>
            <?php
            echo $this->Form->input('productnameen', array('class' => 'longinput form-control input-medium','placeholder'=>'Product Name' ,'type'=>'text','label'=>false,'required','autocomplete'=>'off','id'=>'seatsize',
            'value'=>$product_id['productnameen'],'readonly')); ?>
          </div>
        </div>


        <div class="col-sm-4">
          <div class="form-group">
            <label><strong>Sorting Number</strong></label>
            <?php
            echo $this->Form->input('sortnumber', array('class' => 'longinput form-control input-medium','type'=>'number','label'=>false,'required','autocomplete'=>'off','onkeypress'=>'return isNumber(event)',
            'id'=>'sorting_check'.$i,'value'=>$product_id['sortnumber'])); ?>
            <span id="sortingmessage<?php echo $i ; ?>" style="color:red; display:none">Sorting number already exist select other number !</span>
          </div>
        </div>
<input type="hidden" name="prtype" value="<?php echo $productid[0]['ptype']; ?>">

<script type="text/javascript">
 function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
     alert("Please Enter Only Numeric Characters!!!!");
     return false;
   }
   return true;

 }
</script>

<?php } ?>

<div class="col-sm-12">
  <?php 
  echo $this->Form->submit('Update', array('title' => 'Update','div'=>false,
  'class'=>array('btn btn-primary btn-sm'))); ?>
  
</div>

</div>
</div>
</div>
<?php echo $this->Form->end(); ?>


</div>


<script>
  
</script>





