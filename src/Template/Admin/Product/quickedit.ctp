 <div class="modal-header">
  <h6 class="modal-title">Edit Products</h6>
  <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<div class="modal-body">

  <?php echo $this->Flash->render(); ?>
  <?php echo $this->Form->create($prodata, array(
    'class' => 'form-horizontal',
    'controller' => 'products',
    'action' => 'quickedit',
    'enctype' => 'multipart/form-data',
    'validate')); ?>

   <div class="col-lg-12">
    <div class="card">

      <div class="card-header"><strong>Quick edit</strong></div>
      <div class="card-body card-block">
      <table class="attribute-prices">
      <thead>
        <th style="width:22%">Product Name</th>
        <th style="width:15%">Attribute</th>
        <th>Price</th>
        <th>Disocunt Price</th>
        <th style="width:15%">In Stock</th>
      </thead>
       <?php $i = 0;
if ($user['role_id'] == '1') {
    foreach ($products as $product_id) {
        foreach ($product_id['attributes'] as $attribute) {
            $i++;?>
        <tr>
        <td>
            <?php echo $product_id['productnameen']; ?>
         </td>
         <td>
            <?php echo $attributes[$attribute['id']] . ' ' . $attributes[$attribute['parent_id']]; ?>
          </td><td>
            <?php
echo $this->Form->input('data[' . $attribute['_joinData']['id'] . '][price]', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Product Price', 'type' => 'number', 'label' => false, 'required', 'autocomplete' => 'off', 'onkeypress' => 'return isNumberKey(event)', 'id' => 'amount', 'value' => $attribute['_joinData']['price'])); ?>
          </td><td>
            <?php
echo $this->Form->input('data[' . $attribute['_joinData']['id'] . '][discount_price]', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Product Price', 'type' => 'number', 'label' => false, 'required', 'autocomplete' => 'off', 'onkeypress' => 'return isNumberKey(event)', 'value' => $attribute['_joinData']['discount_price'])); ?>
          </td><td>
            <?php
echo $this->Form->input('data[' . $attribute['_joinData']['id'] . '][is_stock]', array('class' => 'longinput form-control input-medium', 'options' => ['Y' => 'Y', 'N' => 'N'], 'type' => 'select', 'label' => false, 'required', 'autocomplete' => 'off', 'value' => $attribute['_joinData']['is_stock'])); ?>
         </td>
</tr>
<?php }}} else if ($user['role_id'] == 2) {
    foreach ($products as $product_id) {
        foreach ($product_id['attributes'] as $attribute) {
            $i++;
            if ($attribute['_joinData']['vendor_id'] == 0) {
                $vendorAttrExist = $this->Comman->vendorAttrExist($vendorId, $attribute['parent_id'], $attribute['id'], $product_id['id']);
                if ($vendorAttrExist) {
                    continue;
                }
            }?>
        <tr>
        <td>
            <?php echo $product_id['productnameen']; ?>
         </td>
         <td>
            <?php echo $attributes[$attribute['id']] . ' ' . $attributes[$attribute['parent_id']]; ?>
          </td><td>
            <?php
echo $this->Form->input('data[' . $attribute['_joinData']['id'] . '][vendor_id]', array('type' => 'hidden', 'label' => false, 'value' => $attribute['_joinData']['vendor_id']));
            echo $this->Form->input('data[' . $attribute['_joinData']['id'] . '][price]', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Product Price', 'type' => 'number', 'label' => false, 'required', 'autocomplete' => 'off', 'onkeypress' => 'return isNumberKey(event)', 'id' => 'amount', 'value' => $attribute['_joinData']['price'])); ?>
          </td><td>
            <?php
echo $this->Form->input('data[' . $attribute['_joinData']['id'] . '][discount_price]', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Product Price', 'type' => 'number', 'label' => false, 'required', 'autocomplete' => 'off', 'onkeypress' => 'return isNumberKey(event)', 'value' => $attribute['_joinData']['discount_price'])); ?>
          </td><td>
            <?php
echo $this->Form->input('data[' . $attribute['_joinData']['id'] . '][is_stock]', array('class' => 'longinput form-control input-medium', 'options' => ['Y' => 'Y', 'N' => 'N'], 'type' => 'select', 'label' => false, 'required', 'autocomplete' => 'off', 'value' => $attribute['_joinData']['is_stock'])); ?>
         </td>
</tr>
   <?php }}
}?>
</table>

<div class="col-sm-12">
  <?php
echo $this->Form->submit('Update', array('title' => 'Update', 'div' => false,
    'class' => array('btn btn-primary btn-sm'))); ?>

</div>

</div>
</div>
</div>
<?php echo $this->Form->end(); ?>


</div>






