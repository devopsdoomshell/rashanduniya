<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
		</div>
	</div>
	<div class="col-sm-8">
		<div class="page-header float-right">
			<div class="page-title">
				<ol class="breadcrumb text-right">
					<li><a href="<?php echo SITE_URL; ?>admin/dashboard ">Dashboard</a></li>
					<li><a href="<?php echo ADMIN_URL; ?>product/index ">Product Manager</a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
					<strong class="card-title">Import/Export Product Excel</strong>
					</div>
					<div class="card-body">
						<div class="form-group">
						<a href="<?php echo ADMIN_URL; ?>Product/downloadexcl" title="Export Excel" style="color:blue;font-weight:600">click here to download Excel</a>
						</div>
						<?php if ($user['role_id'] == 1) {
    echo $this->Form->create('', ['url' => ['controller' => 'Product', 'action' => 'add_excel'], 'method' => 'POST', 'enctype' => 'multipart/form-data']);
} else {
    echo $this->Form->create('', ['url' => ['controller' => 'Product', 'action' => 'add_vendor_excel'], 'method' => 'POST', 'enctype' => 'multipart/form-data']);
}?>
						<div class="form-group">
						<input type="file" name="product_excel" id="">
						</div>
						<div class="form-group">
						<input type="submit" value="submit" class="btn btn-primary">
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>