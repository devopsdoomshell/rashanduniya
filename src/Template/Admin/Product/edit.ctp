<!-- https://www.youtube.com/watch?v=OK_JCtrrv-c -->
<style type="text/css">
.are{margin-top: -6px;}
.bfh-timepicker-popover table{width:280px;margin:0}
select.form-control:not([size]):not([multiple]) {
  height: calc(1.99rem + 2px) !important;
}
</style>
<div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li><a href="<?php echo ADMIN_URL; ?>dashboard ">Dashboard</a></li>
          <li><a href="<?php echo ADMIN_URL; ?>product	/index ">Product Manager</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>  
<!--<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">-->
  <?php echo $this->Flash->render(); ?>
  <?php echo $this->Form->create($product,array(
   'class'=>'form-horizontal',
   'controller'=>'products',
   'action'=>'edit',
   'enctype' => 'multipart/form-data',
   'validate' )); ?>

   <div class="col-lg-12">
    <div class="card">
      <div class="card-header"><strong>Product Manager</strong></div>
      <div class="card-body card-block">
       <div class="col-sm-6">
        <div class="form-group">
          <label><strong>Product Type</strong></label>
          <br>
          <div class="form-check form-check-inline">
            <input class="form-check-input" <?php if($product['ptype']=='Fruits'){ echo "checked"; } ?> type="radio" name="ptype" id="inlineRadio1" value="Fruits">
            <label class="form-check-label" for="inlineRadio1">Fruits</label>
          </div>
          <div class="form-check form-check-inline">
            <input class="form-check-input" <?php if($product['ptype']=='Vegetables'){ echo "checked"; } ?> type="radio" name="ptype" id="inlineRadio2" value="Vegetables">
            <label class="form-check-label" for="inlineRadio2">Vegetables</label>
          </div>
        </div>
      </div>
      


      <div class="col-sm-12">
        <div class="form-group">
          <label><strong>Product Name English</strong></label>
          <?php
          echo $this->Form->input('productnameen', array('class' => 'longinput form-control input-medium','placeholder'=>'Product Name English' ,'type'=>'text','label'=>false,'required','autocomplete'=>'off','onkeypress'=>'return isNumberKey(event)','id'=>'seatsize','value'=>$product['productnameen'])); ?>
        </div>
      </div>



      <div class="col-sm-12">
        <div class="form-group">
          <label><strong>Product Name Hindi</strong></label>
          <?php
          echo $this->Form->input('productnamehi', array('class' => 'longinput form-control input-medium','placeholder'=>'Product Name Hindi' ,'type'=>'text','label'=>false,'required','autocomplete'=>'off','onkeypress'=>'return isNumberKey(event)','id'=>'seatsize','value'=>$product['productnamehi'])); ?>
        </div>
      </div>


      <div class="col-sm-6">
        <div class="form-group">
          <label><strong>Product Quantity</strong></label>
          <?php
          echo $this->Form->input('qunatity', array('class' => 'longinput form-control input-medium','placeholder'=>'Product Quantity' ,'type'=>'text','label'=>false,'required','autocomplete'=>'off','onkeypress'=>'return isNumberKey(event)','id'=>'amountsize','value'=>$product['qunatity'])); ?>
        </div>
      </div>

      <div class="col-sm-4">
        <div class="form-group">
          <label><strong>Product  Price in Rs</strong></label>
          <?php
          echo $this->Form->input('price', array('class' => 'longinput form-control input-medium','placeholder'=>'Product Price' ,'type'=>'text','label'=>false,'required','autocomplete'=>'off','onkeypress'=>'return isNumberKey(event)','id'=>'amountsize','value'=>$product['price'])); ?>
        </div>
      </div>


      <div class="col-sm-2">
          <div class="form-group">
            <label><strong>Product  Price in</strong></label>
            <?php $list= array('kg' =>'kg', 'unit'=>'unit');  
            echo $this->Form->input('price_in', array('class' => 'longinput form-control input-medium','placeholder'=>'Product Price' ,'type'=>'select','empty'=>'Select','label'=>false,'required','value'=>$product['price_in'] ,'options'=>$list)); ?>
          </div>
        </div>


      <div class="col-sm-12">
        <div class="form-group">
          <label><strong>Product Image</strong></label>
          <?php
          echo $this->Form->input('image', array('class' => 'longinput form-control input-medium','type'=>'file','label'=>false,'autocomplete'=>'off','onkeypress'=>'return isNumberKey(event)','id'=>'seatsize',)); ?>
        </div>
      </div>



      <div class="col-sm-12">
        <div class="form-group">
          <label><strong>Sorting Number </strong></label>
          <?php
          echo $this->Form->input('sortnumber', array('class' => 'longinput form-control input-medium','type'=>'number','label'=>false,'required','autocomplete'=>'off','onkeypress'=>'return isNumberKey(event)','id'=>'sorting_check','value'=>$product['sortnumber'])); ?>
          <span id="sortingmessage" style="color:red; display:none">Sorting number already exist selcet other number !</span>
        </div>
      </div>



      <div class="col-sm-12">
        <div class="form-group">

         <div class="col-sm-1">
          <?php 
          echo $this->Form->submit('Update', array('title' => 'Update','div'=>false,
          'class'=>array('btn btn-primary btn-sm'))); ?>
        </div>
      </div>
    </div>

  </div>
</div>
</div>
<?php echo $this->Form->end(); ?>


<script type="text/javascript">

  $( "#sorting_check" ).change(function() { //alert();  
    var txte = $('#sorting_check').val();
    var radioValue = $("input[name='ptype']:checked"). val();
//alert(txte); 
$.ajax({ 
 type: 'POST', 
 url: '<?php echo SITE_URL; ?>admin/product/sortingcheck',
 data: {'sortno':txte, 'radioValue':radioValue },
 success: function(data){  
   if(data=='1'){
    $('#sortingmessage').css('display','block');   
    $('#sorting_check').val('');  


  }else{
    $('#sortingmessage').css('display','none');   
    sorting_check
  }
},    
});   

});

</script>

<script type="text/javascript">
  function ValidateFileUpload() {
    var fuData = document.getElementById('fileChooser');
    var FileUploadPath = fuData.value;

//To check if user upload any file
if (FileUploadPath == '') {
  alert("Please upload an image");
} else {
  var Extension = FileUploadPath.substring(
    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
//The file uploaded is an image
if (Extension == "png" || Extension == "jpeg" || Extension == "jpg") {
// To Display
var img = document.getElementById("fileChooser"); 
 if(img.files[0].size <  1048576)  // validation according to file size
 {
       //alert(img.files[0].size);
       document.getElementById("showfeatimage").style.display = "none";
       document.getElementById("showfeatsize").style.display = "none";
       return true;
     } 
     else{
      document.getElementById("showfeatimage").style.display = "none";
      document.getElementById("showfeatsize").style.display = "block";
      document.getElementById("fileChooser").value = "";
      return false;
    }
  }   
//The file upload is NOT an image
else {
 document.getElementById("showfeatimage").style.display = "block";
 document.getElementById("fileChooser").value = "";
 return false;
}
}
}
</script>






