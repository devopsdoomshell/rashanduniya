<style>
.anchr{margin-top: 26px; margin-bottom: 0px;}
.anchrr{margin-top: 8px; margin-bottom: 0px;}
.statuscl{
	display: block !important;
}
.statusclno{
	display: none !important;
}
.tmpprice strong{
	position: relative;
}
.tmpprice strong .bdgic {
    position: absolute;
    top: -10px;
    left: -10px;

    margin: auto;
    background-color: #f10a0a;
    width: 20px;
    height: 21px;
    color: #fff;
    font-weight: 900;
    text-align: center;
    border-radius: 50%;
    line-height: 21px;
    z-index: 99999;
}
.modal-dialog{
	max-width:800px;
}
.attribute-prices{
	border-collapse:separate;
    border-spacing:0 5px;

}
.attribute-prices tr td{
    padding:0 3px;
}
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
input[type=number] {
  -moz-appearance: textfield;
}

</style>

 <?php $role_id = $this->request->session()->read('Auth.User.role_id');
//pr($_SESSION);
?>
<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<?php echo $this->Flash->render(); ?>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Product Manager</strong>
						<a href="<?php echo SITE_URL; ?>admin/product/Import" ><strong class=" btn btn-info card-title pull-right">Import</strong></a>
						<a href="<?php echo SITE_URL; ?>admin/product/downloadexcl" ><strong class=" btn btn-info card-title pull-right" style="margin-right:10px; ">Export Excel</strong></a>
						<a href="<?php echo SITE_URL; ?>admin/product/add" ><strong class=" btn btn-info card-title pull-right" style="margin-right:10px; ">Add</strong></a>
						<a href="javascript:void(0)" data-toggle="modal" class="quik" ><strong class=" btn btn-info card-title pull-right" id="save_value" style="margin-right:10px; " >Change Price</strong></a>

					</div>
					<div class="row" style="padding: 10px;">
					<?php echo $this->Form->create('Mysubscription', array('url' => ['action' => 'excel_search'], 'inputDefaults' => array('div' => false, 'label' => false), 'id' => 'category_search', 'class' => 'form-horizontal', 'method' => 'post', 'style' => 'width:100%')); ?>
            <div class="col-sm-3">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Product Name</label>
            		<?php echo $this->Form->input('product_name', array('class' => 'longinput form-control input-medium secrh-loc', 'placeholder' => 'Product Name', 'autocomplete' => 'off', 'type' => 'text', 'label' => false)); ?>
            		<div id="myUL"><ul></ul></div>
            	</div>
            </div>
            <div class="col-sm-2">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Brand</label>
            		<?php echo $this->Form->input('brand_id', array('class' => 'longinput form-control input-medium secrh-loc', 'type' => 'select', 'empty' => 'Select Brand', 'options' => $brands, 'label' => false)); ?>
            		<div id="myUL"><ul></ul></div>
            	</div>
            </div>
			    <div class="col-sm-2">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Product Category</label>
            		<?php echo $this->Form->input('category_id', array('class' => 'longinput form-control input-medium secrh-loc', 'autocomplete' => 'off', 'type' => 'select', 'empty' => 'Select Category', 'options' => $categoriesSearch, 'label' => false)); ?>
            		<div id="myUL"><ul></ul></div>
            	</div>
            </div>
			<?php if ($user['role_id'] == 1) {
    $vendors = ['admin_product' => 'All Admin Products', 'vendors_product' => 'All Vendor Products'] + $vendors;
    ?>
            <div class="col-sm-3">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Products Added By</label>
            		<?php echo $this->Form->input('product_type', array('class' => 'longinput form-control input-medium secrh-loc', 'empty' => 'All Products', 'autocomplete' => 'off', 'type' => 'select', 'options' => $vendors, 'label' => false)); ?>
            		<div id="myUL"><ul></ul></div>
            	</div>
            </div>

			<?php }?>
  <div class="col-sm-2" style="padding-top: 32px;">
            			<button type="search" class="btn btn-success" id="product-search" value="search">Search</button>
						<?php if ($user['role_id'] == 1) {?>
            			<button type="submit" class="btn btn-success" id="excel-search" value="Excel">Excel</button>
						<?php }?>
            	</div>
            	<?php echo $this->Form->end(); ?>
</div>
								<script>
									$(document).ready(function () {
									    	// var radioValue = $("input[name='ptype']:checked"). val();
										$("#product-search").click(function (event) {
										//alert(radioValue);
										 var param = $("#category_search").serialize();
             var siteurl = '<?php echo SITE_URL; ?>';
           window.history.pushState("test", "Title", siteurl+'admin/product?'+param);
										$.ajax({
											async:true,
											type:"GET",
													  headers: {
       'X-CSRF-Token': csrfToken
   },
											dataType: "html",
											url:"<?php echo ADMIN_URL; ?>product/search",
											data:  $("#category_search").serialize(),
											success:function (data) {
												$("#example5").html(data); },
											});
											return false;
										});
										$("#excel-search").click(function (event) {
										$("#category_search").submit();
										});
									});
									 $(document).on('click', '.pagination a', function(e) {
var target = $(this).attr('href');
 var res = target.replace("/search", "/");
 //alert(res);
var siteurl = '<?php echo SITE_URL; ?>';
//alert(target);
window.location = siteurl+res;

return false;
});

								</script>

		<div id="example5" class="card-body">
			<?php //echo $this->Paginator->limitControl([10 => 10, 15 => 15,20=>20,25=>25,30=>30]);?>
			<table id="1bootstrap-data-table" class="table table-striped table-bordered ">
				<thead>
					<tr>
						<th class="align-top"><input type="checkbox" id="select_all" class="selectcheck" name="check[]" value="<?php echo $value['id']; ?>"></th>
						<th class="align-top">SNo</th>
						<th class="align-top">Image</th>
						<th class="align-top">Category</th>
						<th class="align-top">Sub Category</th>
						<th class="align-top">Brand</th>
						<th class="align-top">Product Name</th>
						<th class="align-top">Attribute-qty-price</th>
						<th class="align-top">Created <?php if ($user['role_id'] == '1') {?> <br> (Added By) <?php }?></th>
						<th class="align-top">Status</th>
						<!-- <th class="align-top actions" ><?=__('Actions')?></th> -->
							</tr>
						</thead>

						<script>
							//for quick edit....
							$(document).ready(function () {
								$("#select_all").change(function(){
									if($(this).prop("checked")==true) {
										$(".mycheckbox").prop('checked', true);
									}
									else{
										$(".mycheckbox").prop('checked', false);
									}
								});

								$('.mycheckbox').click(function(){
									$(this).attr('checked',true);
									$("#select_all").attr('checked',false);
									if ($(".mycheckbox:checked").length == $(".mycheckbox").length ){
										$("#select_all").prop('checked', true);
									}
								});
							});
						</script>

						<tbody >
								<?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Products']['page'] - 1) * $this->request->params['paging']['Products']['perPage'];
if (isset($product) && !empty($product)) {
    foreach ($product as $value) {$i++;?>
										<tr>
											<td><input type="checkbox" class="selectcheck mycheckbox" name="check[]" value="<?php echo $value['id']; ?>"></td>
											<td><?php echo $i; ?></td>

											<td><img src="http://dc84viddw6ggp.cloudfront.net/products/<?php echo $value['image']; ?>" height="75px" width="150px" style="display:block;"></td>
											<td><?php echo $categories[$value['category_id']]; ?></td>
											<td><?php echo $categories[$value['subcategory_id']]; ?></td>
											<td><?php echo $brands[$value['brand_id']]; ?></td>
											<td><?php echo ucfirst($value['productnameen']); ?>
											<p>(<?php echo ucfirst($value['productnamehi']); ?>)</p></td>
											<td><table style="width:100%"><tr>
                      <td>Attribute</td>
                      <td>Price</td>
                      <td>Dis. Price</td>
                      <td>is Stock</td>
                      </tr>
					  <?php if ($user['role_id'] == '1') {
        foreach ($value['attributes'] as $attribute) {
            if ($attribute['_joinData']['vendor_id'] == 0) {?>
		 				<tr>
						 <td><?php echo $attribute['name'] . ' ' . $attributes[$attribute['parent_id']]; ?></td>
                        <td><?php echo $attribute['_joinData']['price']; ?></td>
                        <td><?php echo $attribute['_joinData']['discount_price'] == 0 ? '-' : $attribute['_joinData']['discount_price']; ?></td>
                        <td><?php echo $attribute['_joinData']['is_stock']; ?></td>
        				<?php }}?>
						<tr>
							<td colspan="4" style="text-align:center;"><a href="javascript:void(0)"  data-toggle="modal" class="vendor-attr" data-id="<?php echo $value['id'] ?>">Vendor Attributes</a></td>
						</tr>
						<?php
} else {
        foreach ($value['attributes'] as $attribute) {
            if ($attribute['_joinData']['vendor_id'] == 0) {
                $vendorAttrExist = $this->Comman->vendorAttrExist($vendorId, $attribute['parent_id'], $attribute['id'], $value['id']);
                if ($vendorAttrExist) {
                    continue;
                }
            }?>
					 <tr>
					<td><?php echo $attribute['name'] . ' ' . $attributes[$attribute['parent_id']]; ?></td>
					<td><?php echo $attribute['_joinData']['price']; ?></td>
                    <td><?php echo $attribute['_joinData']['discount_price'] == 0 ? '-' : $attribute['_joinData']['discount_price']; ?></td>
                    <td><?php echo $attribute['_joinData']['is_stock']; ?></td>
					</tr>
					<?php }
    }
        ?>

		</table></td>
										<td><?php echo date('d-m-Y', strtotime($value['created']));if ($user['role_id'] == '1') { ?> <br>
										( <?php
echo $value['vendor_id'] != '0' ? $vendors[$value['vendor_id']] : 'Admin'; ?> ) <?php
}?></td>

										<td>

										<?php if ($value['status'] == 'Y') {?>
												<a  class="st<?php echo $value['id']; ?> statuss fa fa-toggle-on statuscl" name="Active" data-value="N" data-val="<?php echo $value['id']; ?>" style="color:green;font-size:20px"></a>

												<a  class="sts<?php echo $value['id']; ?> statuss fa fa-toggle-off statusclno" name="Inactive" data-value="Y" data-val="<?php echo $value['id']; ?>" style="color:red;font-size:20px"></a>

											<?php } else {?>
												<a  class="sts<?php echo $value['id']; ?> statuss fa fa-toggle-off statuscl" name="Inactive" data-value="Y" data-val="<?php echo $value['id']; ?>" style="color:red;font-size:20px"></a>

												<a class="st<?php echo $value['id']; ?> statuss fa fa-toggle-on statusclno" name="Active" data-value="N" data-val="<?php echo $value['id']; ?>" style="color:green;font-size:20px"></a>
											<?php }?>
											<!-- <a href="<?php echo ADMIN_URL; ?>Product/delete/<?php echo $value['id']; ?>" class="fa fa-trash" name="Active" style="color:green;font-size:20px"></a> -->

										</td>
										<?php /* ?>
        <td class="actions">

        <?php echo $this->Html->link(__(''), ['action' => 'edit', $value->id,],array('class'=>'fa fa-pencil','title'=>'Edit','style'=>'font-size:24px;')) ?>

        <?php
        echo $this->Html->link('', ['action' => 'delete',$value->id],['class'=> 'fa fa-trash','style'=>'font-size:19px; color:#FF0000;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this Product')"]); ?>

        </td>
        <?php */?>
									</tr>
								<?php }} else {?>
									<tr>
										<td colspan="12">No Data Available</td>
									</tr>
								<?php }?>
							</tbody>
						</table>
						<?php echo $this->element('admin/pagination'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
      $( document ).ready(function() {
       $(".statuss").click(function() {
		var check_status = $(this).data('value');
        var check_id = $(this).data('val');

       // alert(check_status);
        $.ajax({
          type: 'POST',
		  headers: {
       'X-CSRF-Token': csrfToken
   },
          url: '<?php echo SITE_URL; ?>admin/Product/status',
          data: {'check_status':check_status,'check_id':check_id},
          success: function(data){
          	//alert(data);
          	if (data==2) {
          		$('.st'+check_id).removeClass('statuscl');
          		$('.sts'+check_id).removeClass('statusclno');

          		$('.st'+check_id).addClass('statusclno');
          		$('.sts'+check_id).addClass('statuscl');
          	}else{
          		$('.sts'+check_id).removeClass('statuscl');
          		$('.st'+check_id).removeClass('statusclno');

          		$('.sts'+check_id).addClass('statusclno');
          		$('.st'+check_id).addClass('statuscl');
          	}
          },
      });
  });
});
</script>

<script>
   //prepare the dialog

   //respond to click event on anything with 'overlay' class
   $(".quik").click(function(event){
	event.preventDefault();

   	var t_length=$('.mycheckbox:checked').length;

   	if(t_length >0)
   	{
   		var favorite = [];
   		$.each($('.mycheckbox:checked'), function(){
   			favorite.push($(this).val());
   		});
   		var gr_id=favorite.join(",");
   		var url='<?php echo SITE_URL . "admin/product/quickedit/" ?>'+encodeURIComponent(gr_id);
        //alert(url);
        $('.modal-content').load(url);
		$('#myModals').modal('show');
    }
    else{
		$('#myModals').modal('hide');
    	toastr.error('Select at least one checkbox');
    	return false;
    }

});
</script>

<script src="<?php echo SITE_URL ?>datepicker/jquery-3.2.1.min.js"></script>
<script src="<?php echo SITE_URL ?>datepicker/bootstrap.min.js" ></script>

<!-- Modal -->
<!-- The Modal -->
<div class="modal fade" id="myModals">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<!-- Modal body -->
			<div class="modal-body">
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="editsorts">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<!-- Modal body -->
			<div class="modal-body">
			</div>
		</div>
	</div>
</div>


<!-- <script>
	$('.quik').click(function(e){
		e.preventDefault();
		$('#myModals').modal('show').find('.modal-body').load($(this).attr('href'));
	});
</script> -->

<script>
	$('.editsort').click(function(e){
		e.preventDefault();
		$('#editsorts').modal('show').find('.modal-body').load($(this).attr('href'));
	});
</script>

<div class="modal fade" id="productprice">
	<div class="modal-dialog" style="max-width: 1000px !important;">
		<div class="modal-content">
			<div class="modal-header">
  <h6 class="modal-title">Products Price Request</h6>
  <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
			<div class="modal-body">
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="rejetedprice" >
	<div class="modal-dialog"  style="max-width: 1000px !important;">
		<div class="modal-content">



			<div class="modal-body">
			</div>
		</div>
	</div>
</div>


<script>
	$('.tmpprice').click(function(e){
		e.preventDefault();
		$('#productprice').modal('show').find('.modal-body').load($(this).attr('href'));
	});
</script>

<script>
	$('.rejeted').click(function(e){
		e.preventDefault();
		$('#rejetedprice').modal('show').find('.modal-body').load($(this).attr('href'));
	});
</script>


<div class="modal fade" id="productprices">
  <div class="modal-dialog" style="max-width: 950px !important;">
    <div class="modal-content">
     	 <div class="modal-header">
  <h6 class="modal-title">Approve Products Price</h6>
  <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<script>
 $(document).ready(function(){
	$(document).on('click','a.vendor-attr',function(){
		let id=$(this).data('id');
		var url='<?php echo SITE_URL . "admin/product/vendor_attributes/" ?>'+id;
        $('.modal-content').load(url);
		$('#myModals').modal('show')
	})
 });
</script>






