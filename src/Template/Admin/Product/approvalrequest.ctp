 


  <?php echo $this->Flash->render(); ?>
  <?php  echo $this->Form->create($productnews,array(
   'class'=>'form-horizontal',
   'id'=>'form-my',
   'enctype' => 'multipart/form-data',
   'validate' ));  ?>

   <div class="col-lg-12">
    <div class="card">
     
      <div class="card-header"><strong>Quick edit</strong></div>
      <div class="card-body card-block">
       <?php $i=0; foreach ($productnewprices as $product_id) { $i++; 
        $approvalreq=$this->Comman->pricerequests($product_id['id']);
        $approvaled=$this->Comman->pricerequestsapproved($product_id['id']);
        //pr($approvalreq);
        if ($approvalreq) {
        ?>
        
        
  <?php echo $this->Form->input('id[]', array('type'=>'hidden','id'=>'tmpids','value'=>$product_id['id'])); ?>
  
          <div id="rejd"></div>

          <div class="col-sm-5">
          <div class="form-group">
            <label><strong>Request Date</strong></label>
            <?php $datere=date('d-m-Y h:i A', strtotime($product_id['created']));
            echo $this->Form->input('created', array('class' => 'longinput form-control input-medium','placeholder'=>'Request Date' ,'type'=>'text', 'readonly'=>true,'label'=>false,'value'=>$datere)); ?>
          </div>
        </div>

        <div class="col-sm-5">
          <div class="form-group">
            <label><strong>Request ID</strong></label>
            <?php
            echo $this->Form->input('created', array('class' => 'longinput form-control input-medium','placeholder'=>'Product Price' ,'type'=>'text','readonly'=>true,'label'=>false,'value'=>$product_id['id'])); ?>
          </div>
        </div>

<div class="col-sm-2" style="margin-top: 25px;">
  <?php if ($approvaled > 0) { ?>
  <a href="<?php echo SITE_URL; ?>admin/product/approve/<?php echo $product_id['id']; ?>" data-toggle="modal" class="tmpprices" ><strong class=" btn btn-danger card-title pull-right" id="save_price" style="margin-right:10px; " >Pending</strong></a> 
   <?php }else{ ?>
   <a href="<?php echo SITE_URL; ?>admin/product/approve/<?php echo $product_id['id']; ?>" data-toggle="modal" class="tmpprices" ><strong class=" btn btn-success card-title pull-right" id="save_price" style="margin-right:10px; " >Approved</strong></a> 
  <?php } ?> 
</div>

<?php } } ?>

<div class="col-sm-2">
  <?php 
  //echo $this->Form->submit('Approve', array('title' => 'Update','div'=>false,
  //'class'=>array('btn btn-success btn-sm accepted'))); ?>
  
</div>
<!-- <div class="col-sm-2">
  <?php 
 // echo $this->Form->submit('Reject', array('title' => 'rejetc','div'=>false,
  //'class'=>array('btn btn-primary btn-sm rejectclss'))); ?>
  
</div> -->

</div>
</div>
</div>
<?php  echo $this->Form->end(); ?>


<script>
$(document).ready(function(){
$('.rejectclss').click(function(){

$('#rejd').html('<input type="hidden" name="rejects" value="1">');
 //event.preventDefault();
 $("#form-my").submit();
})
  });
</script>
<script>
  $('.tmpprices').click(function(e){ 
    e.preventDefault();
    $('#productprices').modal('show').find('.modal-body').load($(this).attr('href'));
  });
</script>

