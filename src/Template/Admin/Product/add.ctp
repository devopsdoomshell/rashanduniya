<!-- https://www.youtube.com/watch?v=OK_JCtrrv-c -->
<style type="text/css">
.are {
    margin-top: -6px;
}

.bfh-timepicker-popover table {
    width: 280px;
    margin: 0
}

select.form-control:not([size]):not([multiple]) {
    height: calc(1.99rem + 2px) !important;
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

input[type=number] {
    -moz-appearance: textfield;
}
</style>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="<?php echo ADMIN_URL; ?>dashboard ">Dashboard</a></li>
                    <li><a href="<?php echo ADMIN_URL; ?>product	/index ">Product Manager</a></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!--<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">-->
<?php echo $this->Flash->render(); ?>
<?php echo $this->Form->create('products', array(
    'class' => 'form-horizontal',
    'controller' => 'products',
    'action' => 'add',
    'enctype' => 'multipart/form-data',
    'validate')); ?>

<div class="col-lg-12">
    <div class="card">
        <div class="card-header"><strong>Product</strong></div>
        <div class="card-body card-block">
            <div class="row">
                <div class="col-sm-3">
                    <label><strong>Category</strong></label>
                    <?php
echo $this->Form->input('category_id', array('class' => 'longinput form-control input-medium', 'type' => 'select', 'label' => false, 'required', 'id' => 'select-category', 'options' => $categories, 'empty' => 'Select Category')); ?>
                </div>
                <div class="col-sm-3">
                    <label><strong>Sub Category</strong></label>
                    <?php
echo $this->Form->input('subcategory_id', array('class' => 'longinput form-control input-medium', 'type' => 'select', 'label' => false, 'required', 'id' => 'select-subcategory', 'empty' => 'Select Subcategory', 'disabled')); ?>
                </div>
                <div class="col-sm-3">
                    <label><strong>Brand</strong></label>
                    <?php
echo $this->Form->input('brand_id', array('class' => 'longinput form-control input-medium', 'type' => 'select', 'label' => false, 'required', 'empty' => 'Select Brand')); ?>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label><strong>Is Top Saving </strong></label>
                        <?php
echo $this->Form->input('is_top_saving', array('class' => 'longinput form-control input-medium', 'type' => 'select', 'options' => ['N' => 'N', 'Y' => 'Y'], 'label' => false, 'required', 'autocomplete' => 'off', 'id' => 'sorting_check')); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label><strong>Product Name English</strong></label>
                        <?php
echo $this->Form->input('productnameen', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Product Name English', 'type' => 'text', 'label' => false, 'required', 'autocomplete' => 'off', 'onkeypress' => 'return isNumberKey(event)', 'id' => 'seatsize')); ?>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label><strong>Product Name Hindi</strong></label>
                        <?php
echo $this->Form->input('productnamehi', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Product Name Hindi', 'type' => 'text', 'label' => false, 'required', 'autocomplete' => 'off', 'onkeypress' => 'return isNumberKey(event)', 'id' => 'seatsize')); ?>
                    </div>
                </div>
                <div class="col-sm-2">
                    <label><strong>Attribute Type</strong></label>
                    <?php
echo $this->Form->input('attribute[0][attribute_type]', array('class' => 'form-control attr-type', 'type' => 'select', 'label' => false, 'required', 'options' => $attributes, 'empty' => 'Select Attibute Type', 'id' => 'attribute-type', 'data-id' => "0"));
?>
                </div>
                <div class="col-sm-2">
                    <label><strong>Attribute</strong></label>
                    <?php
echo $this->Form->input('attribute[0][attribute_id]', array('class' => 'form-control', 'type' => 'select', 'label' => false, 'required', 'empty' => 'Select Attribute', 'id' => 'attr-id0', 'disabled'));
?>
                </div>

                <div class="col-sm-1">
                    <label><strong>In Stock</strong></label>
                    <?php
echo $this->Form->input('attribute[0][is_stock]', array('class' => 'longinput form-control input-medium', 'options' => ['Y' => 'Y', 'N' => 'N'], 'type' => 'select', 'label' => false, 'required')); ?>
                </div>

                <div class="col-sm-2">
                    <label><strong>Product Price</strong></label>
                    <?php
echo $this->Form->input('attribute[0][price]', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Product Price', 'type' => 'number', 'label' => false, 'required', 'autocomplete' => 'off', 'onkeypress' => 'return isNumberKey(event)', 'id' => 'amountsize')); ?>
                </div>
                <div class="col-sm-2">
                    <label><strong>Discount Price</strong></label>
                    <?php
echo $this->Form->input('attribute[0][discount_price]', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Discount Price', 'type' => 'number', 'label' => false, 'required', 'autocomplete' => 'off', 'onkeypress' => 'return isNumberKey(event)', 'id' => 'amountsize')); ?>
                </div>
                <div class="col-sm-2">
                    <label><strong>Sorting</strong></label>
                    <?php
echo $this->Form->input('attribute[0][sort]', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Sort', 'type' => 'number', 'label' => false, 'required', 'autocomplete' => 'off', 'onkeypress' => 'return isNumberKey(event)', 'id' => 'amountsize')); ?>
                </div>
                <div class="col-sm-1">
                    <label></label>
                    <a href="javascript:void(0);" class="add_field_butto"
                        style="font-weight: bold; font-size: 21px;margin-top: 23px;"><i
                            class="fa fa-plus-circle"></i></a>
                </div>
            </div>
            <div id="add-attr" class="row">

            </div>
            <!-- <div class="col-sm-2">
          <div class="form-group">
            <label><strong>Product  Price in</strong></label>
            <?php $list = array('kg' => 'kg', 'Unit' => 'unit');
echo $this->Form->input('price_in', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Product Price', 'type' => 'select', 'empty' => 'Select', 'label' => false, 'required', 'options' => $list));?>
          </div>
        </div> -->

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label><strong>Product Image</strong></label>
                        <?php
echo $this->Form->input('image', array('class' => 'longinput form-control input-medium', 'type' => 'text', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
                    </div>
                </div>


                <div class="col-sm-6">
                    <div class="form-group">
                        <label><strong>Product Description</strong></label>
                        <?php
echo $this->Form->input('description', array('class' => 'longinput form-control input-medium', 'type' => 'textarea', 'label' => false, 'required', 'autocomplete' => 'off', 'id' => 'sorting_check', 'rows' => 3)); ?>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="col-sm-1">
                            <a href="<?php echo ADMIN_URL ?>product/index" class="btn btn-primary ">Back</a>
                        </div>
                        <div class="col-sm-1">
                            <?php if (isset($event['id'])) {
    echo $this->Form->submit('Update', array('title' => 'Update', 'div' => false,
        'class' => array('btn btn-primary btn-sm')));} else {?>
                            <button type="submit" class="btn btn-success">Submit</button>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>
<!--   <script type="text/javascript">
var txte = $('#sorting_check').val();
  $( "#sorting_check" ).keyup(function() { //alert();
    var txte = $('#sorting_check').val();
    var radioValue = $("input[name='ptype']:checked"). val();
 //alert(txte);
$.ajax({
 type: 'POST',
 url: '<?php echo SITE_URL; ?>admin/product/sortingcheck',
 data: {'sortno':txte, 'radioValue':radioValue },
 success: function(data){
   if(data=='1'){
    $('#sortingmessage').css('display','block');
    $('#sorting_check').val('');


  }else{
    $('#sortingmessage').css('display','none');

  }
},
});

});

</script> -->
<script type="text/javascript">
function ValidateFileUpload() {
    var fuData = document.getElementById('fileChooser');
    var FileUploadPath = fuData.value;

    //To check if user upload any file
    if (FileUploadPath == '') {
        alert("Please upload an image");
    } else {
        var Extension = FileUploadPath.substring(
            FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
        //The file uploaded is an image
        if (Extension == "png" || Extension == "jpeg" || Extension == "jpg") {
            // To Display
            var img = document.getElementById("fileChooser");
            if (img.files[0].size < 1048576) // validation according to file size
            {
                //alert(img.files[0].size);
                document.getElementById("showfeatimage").style.display = "none";
                document.getElementById("showfeatsize").style.display = "none";
                return true;
            } else {
                document.getElementById("showfeatimage").style.display = "none";
                document.getElementById("showfeatsize").style.display = "block";
                document.getElementById("fileChooser").value = "";
                return false;
            }
        }
        //The file upload is NOT an image
        else {
            document.getElementById("showfeatimage").style.display = "block";
            document.getElementById("fileChooser").value = "";
            return false;
        }
    }
}
</script>
<script>
$(document).ready(function() {
    $(document).on('change', 'select.attr-type', function() {
        let dataId = $(this).data('id');
        let id = $(this).val();
        $.ajax({
            async: false,
            headers: {
                'X-CSRF-Token': csrfToken
            },
            type: 'POST',
            data: {
                id
            },
            url: '<?php echo ADMIN_URL; ?>Attributes/search_attribute',
            success: function(data) {
                var obj = JSON.parse(data);
                $('#attr-id' + dataId + ' option').remove();
                $('#attr-id' + dataId).append('<option value="">Select Attribute</option>')
                $.each(obj, function(id, value) {
                    $('#attr-id' + dataId).append('<option value="' + id + '">' +
                        value + '</option>')
                });
                $('#attr-id' + dataId).attr('disabled', false);

            }

        });
    });
    $(document).on('change', '#select-category', function() {
        let id = $(this).val();
        $.ajax({
            async: false,
            type: 'POST',
            data: {
                id
            },
            headers: {
                'X-CSRF-Token': csrfToken
            },
            url: '<?php echo ADMIN_URL; ?>Categories/search_category',
            success: function(data) {
                var obj = JSON.parse(data);
                $('#select-subcategory option').remove();
                $('#select-subcategory').append(
                    '<option value="">Select Attribute</option>')
                $.each(obj, function(id, value) {
                    $('#select-subcategory').append('<option value="' + id + '">' +
                        value + '</option>')
                });
                $('#select-subcategory').attr('disabled', false);

            }

        });
    });
});
</script>
<script>
let count = 1;
$(document).ready(function() {
    $('.add_field_butto').click(function() {
        $('#add-attr').append('<div id="add-attr' + count +
            '"><div class="col-sm-2"><label><strong>Attribute Type</strong></label><select name="attribute[' +
            count + '][attribute_type]" class="form-control attr-type" data-id="' + count +
            '"><option value="">Select Attribute Type</option><?php foreach ($attributes as $key => $value) {echo "<option value=" . $key . ">" . $value . "</option>";}?></select></div><div class="col-sm-2"><label><strong>Attribute</strong></label><select name="attribute[' +
            count + '][attribute_id]" class="form-control" id="attr-id' + count +
            '" disabled><option value="">Select Attribute</option></select></div><div class="col-sm-1"><label><strong>In Stock</strong></label><select name="attribute[' +
            count +
            '][is_stock]" class="longinput form-control input-medium"><option value="Y">Y</option><option value="N">N</option></select></div><div class="col-sm-2"><label><strong>Product Price</strong></label><input type="number" name="attribute[' +
            count +
            '][price]" class="amountsize form-control" placeholder="Product Price",required="required" onkeypress="return isNumberKey(event)" /></div><div class="col-sm-2"><label><strong>Discount Price</strong></label><input type="number" name="attribute[' +
            count +
            '][discount_price]" class="amountsize form-control" placeholder="Discount Price",required="required" /></div><div class="col-sm-2"><label><strong>Sorting</strong></label><input type="number" name="attribute[' +
            count +
            '][sort]" class="amountsize form-control" placeholder="Sort",required="required" /></div><div class="col-sm-1"><label></label><a href="javascript:void(0);" class="sub_field_butto" style="font-weight: bold; font-size: 21px;margin-top: 23px;" data-id="' +
            count + '"><i class="fa fa-minus-circle"></i></a></div></div>');
        count++;
    });
});
</script>
<script>
$(document).ready(function() {
    $(document).on('click', 'a.sub_field_butto', function() {
        let dataId = $(this).data('id');
        $('#add-attr' + dataId).remove();
    });
});
</script>