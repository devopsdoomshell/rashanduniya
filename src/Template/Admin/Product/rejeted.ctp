 <div class="modal-header">
  <h6 class="modal-title">Rejected Price</h6>
  <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<div class="modal-body">    

  <?php echo $this->Flash->render(); ?>
  <?php  echo $this->Form->create($productnew,array(
   'class'=>'form-horizontal',
   'id'=>'form-my',
   'enctype' => 'multipart/form-data',
   'validate' ));  ?>

   <div class="col-lg-12">
    <div class="card">
     
      
      <div class="card-body card-block">
       <?php $i=0; foreach ($productnewprice as $product_id) { $i++; ?>
        <div class="col-sm-4">
          <div class="form-group">
            <?php echo $this->Form->input('id[]', array('type'=>'hidden','id'=>'tmpids','value'=>$product_id['id'])); ?>
            <?php echo $this->Form->input('ptype[]', array('type'=>'hidden','value'=>$product_id['ptype'])); ?>
            
            <label>
              <?php if ($product_id['price_approval']==1) { echo " <strong> Approved price, </strong>"; }else{ ?>
              <td><input type="checkbox" class="selectcheck mycheckbox" disabled name="check[]"></td>
              <?php } ?>
              <strong>Product Name </strong></label>
            <?php
            echo $this->Form->input('productnameen[]', array('class' => 'longinput form-control input-medium','placeholder'=>'Product Name' ,'type'=>'text','label'=>false,'required','autocomplete'=>'off','id'=>'seatsize',
            'value'=>$product_id['productnameen'],'readonly')); ?>
          </div>
        </div>


        <div class="col-sm-4">
          <div class="form-group">
            <label><strong>Old Price</strong></label>
            <?php
            echo $this->Form->input('proprice[]', array('class' => 'longinput form-control input-medium','placeholder'=>'Product Price' ,'type'=>'text','label'=>false,'id'=>'tmpid','required','autocomplete'=>'off','onkeypress'=>'return isNumberKey(event)','readonly'=>true,'value'=>$product_id['price'])); ?>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="form-group">
            <label><strong>New Price</strong></label>
            <?php
            echo $this->Form->input('price[]', array('class' => 'longinput form-control input-medium','placeholder'=>'Product Price' ,'type'=>'text','label'=>false,'readonly'=>true,'id'=>'amount','value'=>$product_id['tmp_price'])); ?>
          </div>
        </div>
<?php } ?>


</div>
</div>
</div>
<?php  echo $this->Form->end(); ?>


</div>

