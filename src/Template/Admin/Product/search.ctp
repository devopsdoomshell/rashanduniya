<table id="1bootstrap-data-table" class="table table-striped table-bordered ">
				<thead>
					<tr>
						<th class="align-top"><input type="checkbox" id="select_all" class="selectcheck" name="check[]" value="<?php echo $value['id']; ?>"></th>
						<th class="align-top">SNo</th>
						<th class="align-top">Image</th>
						<th class="align-top">Category</th>
						<th class="align-top">Sub Category</th>
						<th class="align-top">Brand</th>
						<th class="align-top">Product Name</th>
						<th class="align-top">Attribute-qty-price</th>
						<th class="align-top">Created <?php if ($user['role_id'] == '1') {?> <br> (Added By) <?php }?></th>
						<th class="align-top">Short order</th>
						<th class="align-top">Status</th>
						<!-- <th class="align-top actions" ><?=__('Actions')?></th> -->
							</tr>
						</thead>

						<tbody >
								<?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Products']['page'] - 1) * $this->request->params['paging']['Products']['perPage'];
if (isset($product) && !empty($product)) {
    foreach ($product as $value) {$i++;?>
										<tr>
											<td><input type="checkbox" class="selectcheck mycheckbox" name="check[]" value="<?php echo $value['id']; ?>"></td>
											<td><?php echo $i; ?></td>

											<td><img src="http://dc84viddw6ggp.cloudfront.net/products/<?php echo $value['image']; ?>" height="75px" width="150px" style="display:block;"></td>
											<td><?php echo $categories[$value['category_id']]; ?></td>
											<td><?php echo $categories[$value['subcategory_id']]; ?></td>
											<td><?php echo $brands[$value['brand_id']]; ?></td>
											<td><?php echo ucfirst($value['productnameen']); ?>
											<p>(<?php echo ucfirst($value['productnamehi']); ?>)</p></td>
											<td><table style="width:100%"><tr>
                      <td>Attribute</td>
                      <td>Price</td>
                      <td>Dis. Price</td>
                      <td>is Stock</td>
                      </tr>
					  <?php if ($user['role_id'] == '1') {
        foreach ($value['attributes'] as $attribute) {
            if ($attribute['_joinData']['vendor_id'] == 0) {?>
		 				<tr>
						 <td><?php echo $attribute['name'] . ' ' . $attributes[$attribute['parent_id']]; ?></td>
                        <td><?php echo $attribute['_joinData']['price']; ?></td>
                        <td><?php echo $attribute['_joinData']['discount_price'] == 0 ? '-' : $attribute['_joinData']['discount_price']; ?></td>
                        <td><?php echo $attribute['_joinData']['is_stock']; ?></td>
        				<?php }}?>
						<tr>
							<td colspan="4" style="text-align:center;"><a href="javascript:void(0)"  data-toggle="modal" class="vendor-attr" data-id="<?php echo $value['id'] ?>">Vendor Attributes</a></td>
						</tr>
						<?php
} else {
        foreach ($value['attributes'] as $attribute) {
            if ($attribute['_joinData']['vendor_id'] == 0) {
                $vendorAttrExist = $this->Comman->vendorAttrExist($vendorId, $attribute['parent_id'], $attribute['id'], $value['id']);
                if ($vendorAttrExist) {
                    continue;
                }
            }?>
					 <tr>
					<td><?php echo $attribute['name'] . ' ' . $attributes[$attribute['parent_id']]; ?></td>
					<td><?php echo $attribute['_joinData']['price']; ?></td>
                    <td><?php echo $attribute['_joinData']['discount_price'] == 0 ? '-' : $attribute['_joinData']['discount_price']; ?></td>
                    <td><?php echo $attribute['_joinData']['is_stock']; ?></td>
					</tr>
					<?php }
    }
        ?>

		</table></td>
										<td><?php echo date('d-m-Y', strtotime($value['created']));if ($user['role_id'] == '1') { ?> <br>
										( <?php
echo $value['vendor_id'] != '0' ? $vendors[$value['vendor_id']] : 'Admin'; ?> ) <?php
}?></td>
										<td style="font-size: 16px;"><?php echo $value['sortnumber']; ?>
											<a  href="<?php echo SITE_URL; ?>admin/product/editsort/<?php echo $value['id'] ?>" data-toggle="modal" class="editsort  fa fa-edit" >
												</a>
										</td>


										<td>

										<?php if ($value['status'] == 'Y') {?>
												<a  class="st<?php echo $value['id']; ?> statuss fa fa-toggle-on statuscl" name="Active" value="N" data-val="<?php echo $value['id']; ?>" style="color:green;font-size:20px"></a>

												<a  class="sts<?php echo $value['id']; ?> statuss fa fa-toggle-off statusclno" name="Inactive" value="Y" data-val="<?php echo $value['id']; ?>" style="color:red;font-size:20px"></a>

											<?php } else {?>
												<a  class="sts<?php echo $value['id']; ?> statuss fa fa-toggle-off statuscl" name="Inactive" value="Y" data-val="<?php echo $value['id']; ?>" style="color:red;font-size:20px"></a>

												<a class="st<?php echo $value['id']; ?> statuss fa fa-toggle-on statusclno" name="Active" value="N" data-val="<?php echo $value['id']; ?>" style="color:green;font-size:20px"></a>
											<?php }?>
											<!-- <a href="<?php echo ADMIN_URL; ?>Product/delete/<?php echo $value['id']; ?>" class="fa fa-trash" name="Active" style="color:green;font-size:20px"></a> -->

										</td>
										<?php /* ?>
        <td class="actions">

        <?php echo $this->Html->link(__(''), ['action' => 'edit', $value->id,],array('class'=>'fa fa-pencil','title'=>'Edit','style'=>'font-size:24px;')) ?>

        <?php
        echo $this->Html->link('', ['action' => 'delete',$value->id],['class'=> 'fa fa-trash','style'=>'font-size:19px; color:#FF0000;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this Product')"]); ?>

        </td>
        <?php */?>
									</tr>
								<?php }} else {?>
									<tr>
										<td colspan="12">No Data Available</td>
									</tr>
								<?php }?>
							</tbody>
						</table>
						<?php echo $this->element('admin/pagination'); ?>