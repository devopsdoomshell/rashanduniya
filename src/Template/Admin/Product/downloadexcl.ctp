<?php
$objPHPExcel = new PHPExcel();
// Set properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");
// Miscellaneous glyphs, UTF-8
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Product Id')
    ->setCellValue('B1', 'Product Name In English')
    ->setCellValue('C1', 'Product Name in Hindi')
    ->setCellValue('D1', 'Category')
    ->setCellValue('E1', 'Subcategory')
    ->setCellValue('F1', 'Attribute')
    ->setCellValue('G1', 'Added By')
    ->setCellValue('H1', 'Image Name')
    ->setCellValue('I1', 'Brand Name')
    ->setCellValue('J1', 'Description')
    ->setCellValue('K1', 'Price')
    ->setCellValue('L1', 'Discount Price')
    ->setCellValue('M1', 'in Stock')
    ->setCellValue('N1', 'Sort')
    ->setCellValue('O1', 'Is Top Selling')
;
$i = 0;
$ii = $i + 2;
$attributes = '"' . implode(',', $attributes) . '"';
$allbrands = $brands;
$brands = '"' . implode(',', $brands) . '"';
$childCategories = '"' . implode(',', $childCategories) . '"';
$parentCategories = '"' . implode(',', $parentCategories) . '"';
$string = '"Y,N"';
if (!empty($products)) {
    foreach ($products as $product) {
        if ($user['role_id'] != 1 && $product['vendor_id'] == 0) {
            $vendorAttrExist = $this->Comman->vendorAttrExist($vendorId, $product['attribute']['parent_attribute']['id'], $product['attribute']['id'], $product['product']['id']);
            if ($vendorAttrExist) {
                continue;
            }
        }
        //echo date("d-m-Y", strtotime($user[$i]['user']['workstartday'])); die;
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $ii, $product['product']['id']);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $ii, $product['product']['productnameen']);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $ii, $product['product']['productnamehi']);
        $objValidation = $objPHPExcel->getActiveSheet()->getCell('D' . $ii)->getDataValidation();
        $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
        $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
        $objValidation->setShowErrorMessage(true);
        $objValidation->setAllowBlank(false);
        $objValidation->setShowInputMessage(true);
        $objValidation->setShowDropDown(true);
        $objValidation->setPromptTitle('Pick from list');
        $objValidation->setPrompt('Please pick a value from the drop-down list.');
        $objValidation->setErrorTitle('Input error');
        $objValidation->setError('Value is not in list');
        $objValidation->setFormula1($parentCategories);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $ii, $categories[$product['product']['category_id']]);
        $objValidation1 = $objPHPExcel->getActiveSheet()->getCell('E' . $ii)->getDataValidation();
        $objValidation1->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
        $objValidation1->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
        $objValidation1->setShowErrorMessage(true);
        $objValidation1->setAllowBlank(false);
        $objValidation1->setShowInputMessage(true);
        $objValidation1->setShowDropDown(true);
        $objValidation1->setPromptTitle('Pick from list');
        $objValidation1->setPrompt('Please pick a value from the drop-down list.');
        $objValidation1->setErrorTitle('Input error');
        $objValidation1->setError('Value is not in list');
        $objValidation1->setFormula1($childCategories);
        $objPHPExcel->getActiveSheet()->setCellValue('E' . $ii, $categories[$product['product']['subcategory_id']]);
        $objValidation2 = $objPHPExcel->getActiveSheet()->getCell('F' . $ii)->getDataValidation();
        $objValidation2->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
        $objValidation2->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
        $objValidation2->setShowErrorMessage(true);
        $objValidation2->setAllowBlank(false);
        $objValidation2->setShowInputMessage(true);
        $objValidation2->setShowDropDown(true);
        $objValidation2->setPromptTitle('Pick from list');
        $objValidation2->setPrompt('Please pick a value from the drop-down list.');
        $objValidation2->setErrorTitle('Input error');
        $objValidation2->setError('Value is not in list');
        $objValidation2->setFormula1($attributes);
        $objPHPExcel->getActiveSheet()->setCellValue('F' . $ii, $product['attribute']['name'] . ' ' . $product['attribute']['parent_attribute']['name']);
        $objPHPExcel->getActiveSheet()->setCellValue('G' . $ii, ($product['vendor_id'] == "" || $product['vendor_id'] == 0) ? 'Admin' : $vendors[$product['vendor_id']]);
        $objPHPExcel->getActiveSheet()->setCellValue('H' . $ii, $product['image']);
        $objValidation4 = $objPHPExcel->getActiveSheet()->getCell('I' . $ii)->getDataValidation();
        $objValidation4->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
        $objValidation4->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
        $objValidation4->setShowErrorMessage(true);
        $objValidation4->setAllowBlank(false);
        $objValidation4->setShowInputMessage(true);
        $objValidation4->setShowDropDown(true);
        $objValidation4->setPromptTitle('Pick from list');
        $objValidation4->setPrompt('Please pick a value from the drop-down list.');
        $objValidation4->setErrorTitle('Input error');
        $objValidation4->setError('Value is not in list');
        $objValidation4->setFormula1($brands);
        $objPHPExcel->getActiveSheet()->setCellValue('I' . $ii, ($product['product']['brand_id'] == "" || $product['product']['brand_id'] == 0) ? 0 : $allbrands[$product['product']['brand_id']]);
        $objPHPExcel->getActiveSheet()->setCellValue('J' . $ii, $product['product']['description']);
        $objPHPExcel->getActiveSheet()->setCellValue('K' . $ii, $product['price']);
        $objPHPExcel->getActiveSheet()->setCellValue('L' . $ii, $product['discount_price']);
        $objValidation4 = $objPHPExcel->getActiveSheet()->getCell('M' . $ii)->getDataValidation();
        $objValidation4->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
        $objValidation4->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
        $objValidation4->setShowErrorMessage(true);
        $objValidation4->setAllowBlank(false);
        $objValidation4->setShowInputMessage(true);
        $objValidation4->setShowDropDown(true);
        $objValidation4->setPromptTitle('Pick from list');
        $objValidation4->setPrompt('Please pick a value from the drop-down list.');
        $objValidation4->setErrorTitle('Input error');
        $objValidation4->setError('Value is not in list');
        $objValidation4->setFormula1($string);
        $objPHPExcel->getActiveSheet()->setCellValue('M' . $ii, $product['is_stock']);
        $objPHPExcel->getActiveSheet()->setCellValue('N' . $ii, $product['sort']);
        $objValidation4 = $objPHPExcel->getActiveSheet()->getCell('O' . $ii)->getDataValidation();
        $objValidation4->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
        $objValidation4->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
        $objValidation4->setShowErrorMessage(true);
        $objValidation4->setAllowBlank(false);
        $objValidation4->setShowInputMessage(true);
        $objValidation4->setShowDropDown(true);
        $objValidation4->setPromptTitle('Pick from list');
        $objValidation4->setPrompt('Please pick a value from the drop-down list.');
        $objValidation4->setErrorTitle('Input error');
        $objValidation4->setError('Value is not in list');
        $objValidation4->setFormula1($string);
        $objPHPExcel->getActiveSheet()->setCellValue('O' . $ii, $product['product']['is_top_saving']);
        if ($user['role_id'] != 1) {
            $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
            $objPHPExcel->getActiveSheet()->getStyle('A' . $ii . ':J' . $ii)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);
            $objPHPExcel->getActiveSheet()->getStyle('K' . $ii . ':O' . $ii)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
        }
        $ii++;
    }
    if ($user['role_id'] != 1) {
        $objPHPExcel->getActiveSheet()->getStyle('A' . $ii . ':K5000')->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
    }
}
for ($i = $ii; $i <= 1000; $i++) {
    $objValidation = $objPHPExcel->getActiveSheet()->getCell('D' . $i)->getDataValidation();
    $objValidation->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
    $objValidation->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
    $objValidation->setShowErrorMessage(true);
    $objValidation->setAllowBlank(false);
    $objValidation->setShowInputMessage(true);
    $objValidation->setShowDropDown(true);
    $objValidation->setPromptTitle('Pick from list');
    $objValidation->setPrompt('Please pick a value from the drop-down list.');
    $objValidation->setErrorTitle('Input error');
    $objValidation->setError('Value is not in list');
    $objValidation->setFormula1($parentCategories);
    $objValidation1 = $objPHPExcel->getActiveSheet()->getCell('E' . $i)->getDataValidation();
    $objValidation1->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
    $objValidation1->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
    $objValidation1->setShowErrorMessage(true);
    $objValidation1->setAllowBlank(false);
    $objValidation1->setShowInputMessage(true);
    $objValidation1->setShowDropDown(true);
    $objValidation1->setPromptTitle('Pick from list');
    $objValidation1->setPrompt('Please pick a value from the drop-down list.');
    $objValidation1->setErrorTitle('Input error');
    $objValidation1->setError('Value is not in list');
    $objValidation1->setFormula1($childCategories);
    $objValidation2 = $objPHPExcel->getActiveSheet()->getCell('F' . $i)->getDataValidation();
    $objValidation2->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
    $objValidation2->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
    $objValidation2->setShowErrorMessage(true);
    $objValidation2->setAllowBlank(false);
    $objValidation2->setShowInputMessage(true);
    $objValidation2->setShowDropDown(true);
    $objValidation2->setPromptTitle('Pick from list');
    $objValidation2->setPrompt('Please pick a value from the drop-down list.');
    $objValidation2->setErrorTitle('Input error');
    $objValidation2->setError('Value is not in list');
    $objValidation2->setFormula1($attributes);
    $objValidation4 = $objPHPExcel->getActiveSheet()->getCell('I' . $i)->getDataValidation();
    $objValidation4->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
    $objValidation4->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
    $objValidation4->setShowErrorMessage(true);
    $objValidation4->setAllowBlank(false);
    $objValidation4->setShowInputMessage(true);
    $objValidation4->setShowDropDown(true);
    $objValidation4->setPromptTitle('Pick from list');
    $objValidation4->setPrompt('Please pick a value from the drop-down list.');
    $objValidation4->setErrorTitle('Input error');
    $objValidation4->setError('Value is not in list');
    $objValidation4->setFormula1($brands);
    $objValidation4 = $objPHPExcel->getActiveSheet()->getCell('L' . $i)->getDataValidation();
    $objValidation4->setType(PHPExcel_Cell_DataValidation::TYPE_LIST);
    $objValidation4->setErrorStyle(PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
    $objValidation4->setShowErrorMessage(true);
    $objValidation4->setAllowBlank(false);
    $objValidation4->setShowInputMessage(true);
    $objValidation4->setShowDropDown(true);
    $objValidation4->setPromptTitle('Pick from list');
    $objValidation4->setPrompt('Please pick a value from the drop-down list.');
    $objValidation4->setErrorTitle('Input error');
    $objValidation4->setError('Value is not in list');
    $objValidation4->setFormula1($string);
}
// Rename sheet
//$objPHPExcel->getActiveSheet()->setTitle('Simple');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel2007)
$filename = "productprice.xlsx";
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename=' . $filename);
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
