<style>
.anchr{margin-top: 26px; margin-bottom: 0px;}
.anchrr{margin-top: 8px; margin-bottom: 0px;}
.badge{font-size: 11px !important;}
</style>
<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
		</div>
	</div>
	<div class="col-sm-8">
		<div class="page-header float-right">
			<div class="page-title">
				<ol class="breadcrumb text-right">
					<li><a href="<?php echo SITE_URL; ?>admin/dashboard ">Dashboard</a></li>
					<li><a href="<?php echo ADMIN_URL; ?>roles/index ">Roles Manager</a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<?php echo $this->Flash->render(); ?>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Roles Manager</strong>
						<a href="<?php echo SITE_URL; ?>admin/roles/add" ><strong class=" btn btn-info card-title pull-right">Add</strong></a> 
						
					</div>
					<div id="example5" class="card-body">
						<?php //echo $this->Paginator->limitControl([10 => 10, 15 => 15,20=>20,25=>25,30=>30]);?> 
						<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
									<th class="align-top">S.No.</th>
									<th class="align-top">name</th>
									<th class="align-top">Role</th>
									<th class="align-top">Username</th>
									<th class="align-top">Created</th>
									<th class="align-top actions" ><?= __('Actions') ?></th>
								</tr>
							</thead>
							<tbody >
								<?php   //pr($this->request->params); die;
								$i=($this->request->params['paging']['Coupancode']['page']-1) * $this->request->params['paging']['Coupancode']['perPage']; 
								if(isset($wallets) &&     !empty($wallets)){ 
									foreach ($wallets as $value){ $i++; //pr($value); ?>
										<tr>
											<td><?php echo  $i; ?></td>
											<td><?php echo  $value['name']; ?></td>
											<td><?php 
											if ($value['role_id']=='1') {
												echo "Admin";
											}elseif($value['role_id']=='2'){
												echo "Product Manager";
											}elseif($value['role_id']=='3'){
												echo "Stock Manager";
											}elseif($value['role_id']=='4'){
												echo "Order Manager";
											}
											?></td>
											<td><?php echo $value['email']; ?></td>
											<td><?php echo date('d-m-y', strtotime($value['created'])); ?></td>
											<td>

												<?php if($value['status']=='Y'){  ?>

													<?=  $this->Html->link('Active', ['action' => 'status',$value->id,'N'  ],['class'=>'badge badge-success']) ?>
												<?php  }else { ?>
													<?=  $this->Html->link('Inactive', ['action' => 'status',$value->id,'Y'],['class'=>'badge badge-warning']) ?>
												<?php } ?>

												<?php // echo $this->Html->link(__(''), ['action' => 'edit', $value->id,],array('class'=>'fa fa-pencil','title'=>'Edit','style'=>'font-size:24px;')) ?>

												<?php
												echo $this->Html->link('', [
													'action' => 'delete',
													$value->id
												],['class'=> 'fa fa-trash','style'=>'font-size:19px; color:#FF0000;',"onClick"=>"javascript: return confirm('Are you sure do you want to delete this Wallet Detail')"]); ?>

												<?php  ?>
											</td>
										</tr>
									<?php } } else{ ?>
										<tr>
											<td colspan="12">No Data Available</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
							<?php echo $this->element('admin/pagination'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

