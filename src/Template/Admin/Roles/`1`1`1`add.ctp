
 <?php echo $this->Flash->render(); ?>

<?php echo $this->Form->create($Coursecategory, array('class'=>'form-horizontal','id' => 'sevice_form', 'enctype' => 'multipart/form-data')); ?>

                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header"><strong>Add Test Center Detail</strong></div>
                      <div class="card-header">
                        
                      </div>

                      <div class="card-body card-block">
                      <div class="col-sm-6">
                        <div class="form-group">
                            <label for="company" class=" form-control-label">Name</label>
                            <?php echo $this->Form->input('name', array('class' => 
                    'longinput form-control','placeholder'=>'Enter Name','required','label'=>false)); ?>
                        </div>
                        </div>
                        <div class="col-sm-6">
                        <div class="form-group">
                            <label for="company" class=" form-control-label">Email ID</label>
                            <?php echo $this->Form->input('email', array('class' => 
                    'longinput form-control', 'type'=>'email','id'=>'myemail', 'placeholder'=>'Enter Mail ID','required','label'=>false, 'autocomplete' => 'off')); ?>
                    <span id="mailvalid" style="font-size: 11px;color:red; display:none">Invalid email !!</span>
                        </div>
                        
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="company" class=" form-control-label">Password</label>
                            <?php echo $this->Form->input('pass', array('class' => 
                    'longinput form-control', 'type'=>'Password' ,'placeholder'=>'Password','required','label'=>false)); ?>
                        </div>
</div>

 
                    
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="company" class=" form-control-label">Mobile Number</label>
                            <?php echo $this->Form->input('mobile', array('class' => 
                    'longinput form-control' ,'placeholder'=>'Enter Mobile Number','required','onkeypress'=>'return isNumber(event);', 'onchange'=>'checkLength()', 'maxlength'=>'12','autocomplete' => 'off','id'=>'addnum0','label'=>false)); ?>
                        </div>
                        <span id="nummessag" style="font-size: 11px;color:red; display:none">Mobile Number is already exist !!</span>
                    </div>
</div> 

        <div class="content mt-3">
                   <div class="row">
                    <div class="col-sm-1">
                     <a href="<?php echo SITE_URL ?>admin/testcenters" class="btn btn-primary " >Back</a>
                    </div>
                    <div class="col-sm-1">
                    <?php if(isset($transports['id'])){
                    echo $this->Form->submit('Update', array('title' => 'Update','div'=>false,
                    'class'=>array('btn btn-primary btn-sm'))); }else{  ?>
                    <button type="submit"  class="btn btn-success">Submit</button>
                    <?php  } ?>
                    </div>
                    </div>
        </div>
                  </div>
                </div>
                <?php echo $this->Form->end(); ?>

 <script>
  //Duplicate number validation add more...
$( document ).ready(function() {
     $( "#addnum0" ).change(function() { 
        var addnumber = $('#addnum0').val(); 
        
        $.ajax({ 
          type: 'POST', 
          url: '<?php echo SITE_URL; ?>admin/testcenters/chek_number',
          data: {'addnumber':addnumber},
          success: function(data){ 

       console.log(data);  
           if(data==0){
          $('#nummessag').css('display','none');   
       }
       else{
         
          $('#nummessag').css('display','block');   
          $('#addnum0').val('');  
       }  
          },    
      });   
	});
  });
 </script>
 
 
 <script>
function isNumber(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
     alert("Please Enter Only Numeric Characters!!!!");
      return false;
  }
  return true;

}
function checkLength() {

 var textbox = document.getElementById("addnum0");
    if(textbox.value.length >= 10 && textbox.value.length <= 12){
 return true;
    }
    else{
        alert("Enter minimum 10 digits of your mobile number");
         $('#addnum0').val('');
        return false;
    }
  }
</script>
<script type="text/javascript">
  $("#myemail").change(function() {
var txt = $('#myemail').val();
var testCases = [txt];
    var test = testCases; 
    if(isValidEmailAddress(test)!=true){
    $('#mailvalid').css('display','block');
    $('#myemail').val('');
    }
    else{
   $('#mailvalid').css('display','none');     
  }
});
</script>

<script>
 function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};
</script>