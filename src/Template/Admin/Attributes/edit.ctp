<style>
.chngpass{ margin-left:15px}
</style>
<?php echo $this->Form->create($attribute); ?>
<?php echo $this->Flash->render(); ?>
<div class="col-lg-12">
<div class="card">
<div class="card-header">
<strong>Attribute Manager</strong>
<a href="<?php echo SITE_URL; ?>admin/attributes/add">
<strong class=" btn btn-info card-title pull-right">Add</strong>
</a>
</div>
<div class="card-body card-block">
<div class="row">

<div class="col-sm-6">
<div class="form-group">
<label for="company" class=" form-control-label">Select Category</label>
<?php echo $this->Form->input('parent_id', array('class' => 'form-control','label'=>false,'empty'=>'Main Category','options' => $parentAttributes)); ?>

</div>
</div>


<div class="col-sm-6">
<div class="form-group">
<label for="company" class=" form-control-label">Name</label>
<?php echo $this->Form->input('name', array('class' => 'form-control','type'=>'text','label'=>false,'placeholder'=>'Name')); ?>
</div>
</div>

<div class="col-sm-12">
          <div class="form-group">
            <div class="col-sm-1">
             <a href="<?php echo SITE_URL; ?>admin/attributes" class="btn btn-primary ">Back</a>
           </div>
           <div class="col-sm-1">
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>
          </div>
        </div>


</div>
</div>
</div>

</div>
</div>
</div>



<?php echo $this->Form->end(); ?>

</div>







