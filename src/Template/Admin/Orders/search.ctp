	<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
									<th class="align-top">S.No.</th>
									<th class="align-top">Order ID</th>
									<th class="align-top">Order Date</th>
									<th class="align-top">Order Amount</th>
									<th class="align-top">Custome Name</th>
									<th class="align-top">Address</th>
									<th class="align-top">Mobile</th>
									<!--<th class="align-top">Invoice</th>-->
									<th class="align-top">Order Status</th>
								
								</tr>
							</thead>
							<tbody >
								<?php   //pr($this->request->params); die;
								$i=($this->request->params['paging']['Orders']['page']-1) * $this->request->params['paging']['Orders']['perPage']; 
								if(isset($order) &&     !empty($order)){ 
									foreach ($order as $value){ $i++; //pr($value); ?>
										<tr class="<?php  if ($value['status']=='dl') { echo "tbltrgreen"; }elseif($value['status']=='c'){ echo "tbltrred"; }elseif($value['status']=='dp'){echo "tbltrorange";} ?>">
											<td><?php echo  $i; ?></td>
											<td><a href="<?php echo SITE_URL; ?>admin/orders/orderdetail/<?php echo $value['id']; ?>" target="_blank"><?php echo  "#".$value['id']; ?></a></td>
										<td>	<?php  echo date('d-m-Y', strtotime($value->created)); ?>
										</td>
								<td>	<?php echo $value['totalamount']." Rs."; ?>
										</td>
										
											</td>
								<td>	<?php echo $value['user']['name']; ?>
								
									</td>
								<td>	<?php echo $value['user']['address']; ?>
								
									</td>
								<td>	<?php echo $value['user']['mobile']; ?>
										
										</td>
									
                                            
<!--											<td>-->
<!--Invoice-->
<!--											</td>-->
												<td>
<?php $options=array('p'=>'Pending', 'dp'=>'Dispatched', 'dl'=>'Delivered','c'=>'Cancelled'); ?>

                <?php echo $this->Form->input('status', array('class' => 'longinput form-control input-medium orderst','type'=>'select','value'=>$value['status'],'options'=>$options, 'data-val'=>$value['id'],'label'=>false, )); ?>
											</td>
										</tr>
									<?php } } else{ ?>
										<tr>
											<td colspan="9" style="text-align:center;">No Orders Available</td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
							<?php echo $this->element('admin/pagination'); ?>