<style>

.tbltrgreen {
  background-color: #31c731 !important;
}
.tbltrred {
  background-color: #d01515 !important;
}
.tbltrorange {
  background-color: #13c2ca !important;
}
.anchr{margin-top: 26px; margin-bottom: 0px;}
.anchrr{margin-top: 8px; margin-bottom: 0px;}
.badge{font-size: 11px !important;}
</style>
<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
		</div>
	</div>
	<div class="col-sm-8">
		<div class="page-header float-right">
			<div class="page-title">
				<ol class="breadcrumb text-right">
					<li><a href="<?php echo SITE_URL; ?>admin/dashboard">Dashboard</a></li>
					<li><a href="<?php echo ADMIN_URL; ?>orders ">Order Manager</a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<?php echo $this->Flash->render(); ?>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Order Manager</strong>
					</div>

<div class="row" style="padding: 14px;">
    	<?php echo $this->Form->create('orderSearch', array('type' => 'get', 'inputDefaults' => array('div' => false, 'label' => false), 'id' => 'category_search', 'class' => 'form-horizontal', 'method' => 'get')); ?>
		<?php if ($user['role_id'] == 1) {?>
			<div class="col-sm-2">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Vendor</label>
             <?php echo $this->Form->input('vendorId', array('class' => 'longinput form-control input-medium ', 'type' => 'select', 'options' => $vendors, 'empty' => 'Select Vendor', 'label' => false, 'value' => $orderSearch['vendorId'])); ?>

            	</div>
            </div>
		<?php }?>
            <div class="col-sm-2">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label"> Date From</label>
            		<?php echo $this->Form->input('date_from', array('class' => 'longinput form-control input-medium secrh-loc', 'placeholder' => 'Date From', 'autocomplete' => 'off', 'label' => false, 'readonly', 'id' => 'datepicker1', 'value' => $orderSearch['date_from'])); ?>
            	</div>
            </div>

              <div class="col-sm-2">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Date To</label>
            		<?php echo $this->Form->input('date_to', array('class' => 'longinput form-control input-medium secrh-loc', 'placeholder' => 'Date To', 'autocomplete' => 'off', 'label' => false, 'readonly', 'id' => 'datepicker2', 'value' => $orderSearch['date_to'])); ?>
            	</div>
            </div>
            <div class="col-sm-2">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Mobile</label>
            		<?php echo $this->Form->input('mobile', array('class' => 'longinput form-control input-medium secrh-loc', 'placeholder' => 'Mobile', 'autocomplete' => 'off', 'type' => 'number', 'label' => false, 'value' => $orderSearch['mobile'])); ?>
            	</div>
            </div>

<div class="col-sm-2">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Order Status</label>
            	   <?php $optionss = array('pending' => 'Pending', 'dispatched' => 'Dispatched', 'delivered' => 'Delivered', 'cancelled' => 'Cancelled');?>
             <?php echo $this->Form->input('orderStatus', array('class' => 'longinput form-control input-medium ', 'type' => 'select', 'options' => $optionss, 'empty' => 'Select Status', 'label' => false, 'value' => $orderSearch['orderStatus'])); ?>

            	</div>
            </div>


  <div class="col-sm-1">

            	<label></label>
            			<button type="submit" class="btn btn-info" id="Mysubscriptionevent" style="margin-top: 7px;">Search</button>

            	</div>
            	<?php echo $this->Form->end(); ?>
</div>

					<div id="search_data" class="card-body">
						<?php //echo $this->Paginator->limitControl([10 => 10, 15 => 15,20=>20,25=>25,30=>30]);?>
						<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
									<th class="align-top">S.No.</th>
									<?php if ($user['role_id'] == 1) {?>
									<th class="align-top">Vendor Name</th>
									<?php }?>
									<th class="align-top">Order ID</th>
									<th class="align-top">Order Date</th>
									<th class="align-top">Order Amount</th>
									<th class="align-top">Address</th>
									<th class="align-top">Mobile</th>
									<th class="align-top">Order Status</th>

								</tr>
							</thead>
							<tbody >
								<?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Orders']['page'] - 1) * $this->request->params['paging']['Orders']['perPage'];
if (isset($orders) && !empty($orders)) {
    foreach ($orders as $value) {$i++; //pr($value); ?>
										<tr class="<?php if ($value['order_status'] == 'delivered') {echo "tbltrgreen";} elseif ($value['status'] == 'c') {echo "tbltrred";} elseif ($value['status'] == 'dispatched') {echo "tbltrorange";}?>">
											<td><?php echo $i; ?></td>
											<?php if ($user['role_id'] == 1) {?>
											<td><?php echo $value['vendor']['name']; ?></td>
											<?php }?>
											<td><a href="<?php echo SITE_URL; ?>admin/orders/orderdetail/<?php echo $value['id']; ?>" target="_blank"><?php echo "#" . $value['id']; ?></a></td>
										<td>	<?php echo date('d-m-Y', strtotime($value->created)); ?>
										</td>
								<td>	<?php echo $value['total_amount'] . " Rs."; ?>
										</td>

											</td>
								<td>	<?php echo $value['user_address']['name'] . ',' . $value['user_address']['flat_no'] . ',' . $value['user_address']['address_1']; ?>

									</td>
								<td>	<?php echo $value['user']['mobile']; ?>

										</td>


<!--											<td>-->
<!--Invoice-->
<!--											</td>-->
												<td>
<?php $options = array('pending' => 'Pending', 'dispatched' => 'Dispatched', 'delivered' => 'Delivered', 'cancelled' => 'Cancelled');?>

                <?php echo $this->Form->input('status', array('class' => 'longinput form-control input-medium orderst', 'type' => 'select', 'value' => $value['order_status'], 'options' => $options, 'data-val' => $value['id'], 'label' => false)); ?>
											</td>
										</tr>
									<?php }} else {?>
										<tr>
											<td colspan="9" style="text-align:center;">No Orders Available</td>
										</tr>
									<?php }?>
								</tbody>
							</table>
							<?php echo $this->element('admin/pagination'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script>
$(document).ready(function(){
$(".orderst").on('change',function() {
var stval=$(this).val();
var orderid=$(this).data('val');
// alert(stval);
$.ajax({
type: 'POST',
url: '<?php echo SITE_URL; ?>admin/orders/orderstatus',
data: {'status':stval,'orderid':orderid},
success: function(data){
var obj = JSON.parse(data);
if (obj=='1') {
location.reload();
}else{
alert('Not able to change');
location.reload();
}
},
});
});
});
</script>



<script>
  $( function() {

    var dateFormat = 'dd-mm-yy',
    from = $( "#datepicker1" )
    .datepicker({
      dateFormat: 'dd-mm-yy',

      changeMonth: true,
      numberOfMonths: 1
    })
    .on( "change", function() {
      to.datepicker( "option", "minDate", getDate( this ) );
    }),
    to = jQuery( "#datepicker2" ).datepicker({
      dateFormat: 'dd-mm-yy',

      changeMonth: true,
      numberOfMonths: 1
    })
    .on( "change", function() {
      from.datepicker( "option", "maxDate", getDate( this ) );
    });

    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }

      return date;
    }
  } );
</script>