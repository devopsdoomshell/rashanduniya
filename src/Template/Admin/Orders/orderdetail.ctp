<?php
class xtcpdf extends TCPDF
{

}

//$subject=$this->Comman->findexamsubjectsresult($students['id'],$students['section']['id'],$students['acedmicyear']);

$this->set('pdf', new TCPDF('1', 'mm', 'A4'));
$pdf = new TCPDF("H", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, true);

// set document information

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(false);
$pdf->AddPage();
$pdf->setHeaderMargin(0);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetAutoPageBreak(true, 0);
//$pdf->SetMargins(5, 0, 5, true);

$pdf->SetFont('', '', 6, '', 'true');
TCPDF_FONTS::addTTFfont('../Devanagari/Devanagari.ttf', 'TrueTypeUnicode', "", 32);

//$studetails=$this->Comman->findstudetails($sid);
//pr($totlorderpdf['id']); die;
$ordercreate = date('d-m-Y', strtotime($order['created']));
$invoicedate = date('d-m-Y');

function numberTowords($num)
{

    $ones = array(
        0 => "ZERO",
        1 => "ONE",
        2 => "TWO",
        3 => "THREE",
        4 => "FOUR",
        5 => "FIVE",
        6 => "SIX",
        7 => "SEVEN",
        8 => "EIGHT",
        9 => "NINE",
        10 => "TEN",
        11 => "ELEVEN",
        12 => "TWELVE",
        13 => "THIRTEEN",
        14 => "FOURTEEN",
        15 => "FIFTEEN",
        16 => "SIXTEEN",
        17 => "SEVENTEEN",
        18 => "EIGHTEEN",
        19 => "NINETEEN",
        "014" => "FOURTEEN",
    );
    $tens = array(
        0 => "ZERO",
        1 => "TEN",
        2 => "TWENTY",
        3 => "THIRTY",
        4 => "FORTY",
        5 => "FIFTY",
        6 => "SIXTY",
        7 => "SEVENTY",
        8 => "EIGHTY",
        9 => "NINETY",
    );
    $hundreds = array(
        "HUNDRED",
        "THOUSAND",
        "MILLION",
        "BILLION",
        "TRILLION",
        "QUARDRILLION",
    ); /*limit t quadrillion */
    $num = number_format($num, 2, ".", ",");
    $num_arr = explode(".", $num);
    $wholenum = $num_arr[0];
    $decnum = $num_arr[1];
    $whole_arr = array_reverse(explode(",", $wholenum));
    krsort($whole_arr, 1);
    $rettxt = "";
    foreach ($whole_arr as $key => $i) {

        while (substr($i, 0, 1) == "0") {
            $i = substr($i, 1, 5);
        }

        if ($i < 20) {
/* echo "getting:".$i; */
            $rettxt .= $ones[$i];
        } elseif ($i < 100) {
            if (substr($i, 0, 1) != "0") {
                $rettxt .= $tens[substr($i, 0, 1)];
            }

            if (substr($i, 1, 1) != "0") {
                $rettxt .= " " . $ones[substr($i, 1, 1)];
            }

        } else {
            if (substr($i, 0, 1) != "0") {
                $rettxt .= $ones[substr($i, 0, 1)] . " " . $hundreds[0];
            }

            if (substr($i, 1, 1) != "0") {
                $rettxt .= " " . $tens[substr($i, 1, 1)];
            }

            if (substr($i, 2, 1) != "0") {
                $rettxt .= " " . $ones[substr($i, 2, 1)];
            }

        }
        if ($key > 0) {
            $rettxt .= " " . $hundreds[$key] . " ";
        }
    }
    if ($decnum > 0) {
        $rettxt .= " and ";
        if ($decnum < 20) {
            $rettxt .= $ones[$decnum];
        } elseif ($decnum < 100) {
            $rettxt .= $tens[substr($decnum, 0, 1)];
            $rettxt .= " " . $ones[substr($decnum, 1, 1)];
        }
    }
    return $rettxt;
}

$html .= '
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title><link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">';
$html .= '
<table width="100%">
<tr>
<td width="50%">
<img src="' . SITE_URL . 'img/logo_new.png" alt="logo" style="height:60px">
</td>

<td width="50%">
<h5 style="text-align:right; font-size:14px; font-weight:bold;">Tax Invoice/Bill of Supply/Cash Memo</h5>
<h6 style="text-align:right; font-size:12px; font-weight:normal;">(Original for Recipient)</h6>
</td>
</tr>
</table>


<br>
<br>
<br>

<table width="100%">
<tr>
<td width="49%">


<p style="text-align:left; color:#000; font-size:10px; height:8px; line-height:8px;"> &nbsp; <strong>Order Number:</strong> #' . $order['id'] . ' </p>

<p style="text-align:left; color:#000; font-size:10px; height:8px; line-height:8px;"> &nbsp; <strong>Order Date:</strong> ' . $ordercreate . '</p>
<!--<h4 style="background-color:#f3f3f3;  color:#000; text-align:left; height:25px; line-height:28px; font-size:12px; margin:0px; verticale-align:30px;"> &nbsp; Sold By</h4>
<p></p>
<p style="text-align:left; color:#000; font-size:10px; height:8px; line-height:8px;"> &nbsp; GHK Enterprises</p>
<p style="text-align:left; color:#000; font-size:10px;height:8px; line-height:8px;"> &nbsp; *
 F-28/4 BASEMENT, OKHLA PHASE-2</p>
 <p style="text-align:left; color:#000; font-size:10px;height:8px; line-height:8px;"> &nbsp; Delhi, Delhi, 110020</p>
 <p style="text-align:left; color:#000; font-size:10px;height:8px; line-height:8px;"> &nbsp; India</p>-->
</td>

<td width="2%"></td>
<td width="49%">
<h4 style="background-color:#f3f3f3; padding:5px; color:#000; text-align:right; height:25px; line-height:25px; font-size:12px;"> &nbsp; Shipping Address  &nbsp; </h4>
<p></p>
<p style="text-align:right; color:#000; font-size:10px; height:8px; line-height:8px;">' . $order['user_address']['name'] . ' &nbsp; </p>
<p style="text-align:right; color:#000; font-size:10px; height:8px; line-height:8px;">' . $order['user_address']['flat_no'] . ' &nbsp; </p>
 <p style="text-align:right; color:#000; font-size:10px; height:8px; line-height:8px;">' . $order['address_1'] . ' &nbsp; </p>
 <p style="text-align:right; color:#000; font-size:10px; height:8px; line-height:8px;">India &nbsp; </p>
</td>
</tr>
</table>


<br>
<br>
<br>
<table width="100%">
<tr>
<td width="49%">
<!--<p style="text-align:left; color:#000; font-size:10px; height:8px; line-height:8px;"> &nbsp; <strong>Order Number:</strong> #' . $order['id'] . ' </p>

<p style="text-align:left; color:#000; font-size:10px; height:8px; line-height:8px;"> &nbsp; <strong>Order Date:</strong> ' . $ordercreate . '</p>-->
</td>


<td width="2%"></td>
<td width="49%">
<p style="text-align:right; color:#000; font-size:10px; height:8px; line-height:8px;">  <strong>GST Registration No:</strong> ' . $order['gstno'] . ' &nbsp; </p>

<p style="text-align:right; color:#000; font-size:10px; height:8px; line-height:8px;">  <strong>Invoice Number:</strong> YNHA-' . $order['id'] . '&nbsp; </p>

<p style="text-align:right; color:#000; font-size:10px; height:8px; line-height:8px;">  <strong>Invoice Date:</strong> ' . $invoicedate . ' &nbsp; </p>



</td>
</tr>
</table>

<br><br>
<table width="100%">
<tr>
<td width="5%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-left:1px solid #000; border-bottom:1px solid #000; background-color:#ddd; border-top:1px solid #000;"> Sl.
No</td>
<td width="50%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000; background-color:#ddd; border-top:1px solid #000;"> Description </td>
<td width="20%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000; background-color:#ddd; border-top:1px solid #000;"> Unit
Price</td>
<td width="10%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000; background-color:#ddd; border-top:1px solid #000;"> Qty</td>
<td width="15%" style="font-size:10px; text-align:center; border-right:1px solid #000;border-top:1px solid #000; border-bottom:1px solid #000; background-color:#ddd;"> Total
Amount</td>
</tr>';
$totalQuantiy = 0;
$i = 1;foreach ($order['order_details'] as $value) {
    $totalQuantiy += $value['quantity'];
    $singlePrice = $value['price'] / $value['quantity'];
    $final_price_amount += $value['price'];
    $html .= '<tr>
<td width="5%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-left:1px solid #000; border-bottom:1px solid #000;  border-top:1px solid #000;">' . $i . '</td>
<td width="50%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000; border-top:1px solid #000;">' . $value['product']['productnameen'] . '</td>
<td width="20%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000; border-top:1px solid #000;"> Rs. ' . $singlePrice . '</td>
<td width="10%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000; border-top:1px solid #000;"> ' . $value['quantity'] . '</td>
<td width="15%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000;  border-top:1px solid #000;"> Rs. ' . round($value['price']) . '</td>
</tr>';
    $i++;
}

/*
$html.= '<tr>
<td width="5%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-left:1px solid #000; border-bottom:1px solid #000;  border-top:1px solid #000;"> </td>
<td width="37%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000; border-top:1px solid #000;"> Shipping Charges

</td>

<td width="10%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000; border-top:1px solid #000;"> Rs 35.71</td>
<td width="5%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000; border-top:1px solid #000;"> </td>
<td width="10%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000;  border-top:1px solid #000;"> Rs 35.71 </td>
<td width="5%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000; border-top:1px solid #000;"> 12%</td>
<td width="8%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000;  border-top:1px solid #000;"> GST</td>
<td width="10%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000;  border-top:1px solid #000;">Rs 4.29 </td>
<td width="10%" style="font-size:10px; text-align:center; border-right:1px solid #000;border-top:1px solid #000; border-bottom:1px solid #000; ">Rs 40.00</td>
</tr>';
 */

$html .= '<tr>
<td width="75%" style="font-size:10px; text-align:left; border-right:1px solid #000; border-bottom:1px solid #000; border-top:1px solid #000; border-left:1px solid #000;"> &nbsp; TOTAL</td>


<td width="10%" style="font-size:10px; text-align:center; border-right:1px solid #000; border-bottom:1px solid #000; background-color:#ddd; border-top:1px solid #000;">' . round($totalQuantiy) . '</td>
<td width="15%" style="font-size:10px; text-align:center; border-right:1px solid #000;border-top:1px solid #000; border-bottom:1px solid #000; background-color:#ddd;"> Rs. ' . round($final_price_amount) . '</td>
</tr>




<tr>

<td width="100%" style="font-size:10px; text-align:left; font-weight:bold; border-right:1px solid #000;border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; "> &nbsp; Amount in Words:<br>
 &nbsp; ' . numberTowords($final_price_amount) . '</td>
</tr>


<tr>

<td width="100%" style="font-size:10px; text-align:right; font-weight:bold; border-right:1px solid #000;border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; "> For Rashan Duniya: &nbsp; <br><br><br><br>
 Authorized Signatory  &nbsp; </td>
</tr>
</table>
';

$pdf->writeHTMLCell(0, 0, '', '', utf8_encode($html), 0, 1, 0, true, '', true);
//$pdf->WriteHTML($html, true, false, true, false, '');
ob_end_clean();
echo $pdf->Output('orderlist');
exit;
