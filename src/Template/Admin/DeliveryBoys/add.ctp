<!-- https://www.youtube.com/watch?v=OK_JCtrrv-c -->
<style type="text/css">
.are{margin-top: -6px;}
.bfh-timepicker-popover table{width:280px;margin:0}
select.form-control:not([size]):not([multiple]) {
  height: calc(1.99rem + 2px) !important;
}
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
</style>
<div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li><a href="<?php echo ADMIN_URL; ?>dashboard ">Dashboard</a></li>
          <li><a href="<?php echo ADMIN_URL; ?>customer/index ">Customer Manager</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!--<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">-->
  <?php echo $this->Flash->render(); ?>
  <?php
if (isset($deliveryBoys)) {
    echo $this->Form->create($deliveryBoys, array(
        'class' => 'form-horizontal',
        'controller' => 'customers',
        'action' => 'edit/' . $deliveryBoys['id'],
        'enctype' => 'multipart/form-data',
        'validate', 'autocomplete' => 'off'));
} else {
    echo $this->Form->create('customers', array(
        'class' => 'form-horizontal',
        'controller' => 'customers',
        'action' => 'add',
        'enctype' => 'multipart/form-data',
        'validate', 'autocomplete' => 'off'));
}
?>

   <div class="col-lg-12">
    <div class="card">
      <div class="card-header"><strong>Add Delivery Boy</strong></div>
      <div class="card-body card-block">
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Name</strong></label>
            <?php
echo $this->Form->input('name', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Name', 'type' => 'text', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Mobile Number</strong></label>
            <?php
echo $this->Form->input('mobile', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Mobile Number ', 'type' => 'number', 'label' => false, 'autocomplete' => 'off', 'required')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Id Card Type</strong></label>
            <?php
echo $this->Form->input('id_card_type', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Id Card Type', 'type' => 'text', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
          </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Id Card Number</strong></label>
            <?php
echo $this->Form->input('id_card_number', array('class' => 'longinput form-control input-medium', 'placeholder' => 'ID Card Number', 'type' => 'text', 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Image</strong></label>
            <?php
echo $this->Form->input('image', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Building Name', 'type' => 'file', 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="form-group">
            <div class="col-sm-1">
              <a href="<?php echo ADMIN_URL ?>DeliveryBoys/index" class="btn btn-primary " >Back</a>
           </div>
           <div class="col-sm-1">
            <?php if (isset($deliveryBoys['id'])) {
    echo $this->Form->submit('Update', array('title' => 'Update', 'div' => false,
        'class' => array('btn btn-primary btn-sm')));} else {?>
                <button type="submit" class="btn btn-success">Submit</button>
              <?php }?>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <?php echo $this->Form->end(); ?>








