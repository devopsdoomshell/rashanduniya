<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
		</div>
	</div>
	<div class="col-sm-8">
		<div class="page-header float-right">
			<div class="page-title">
				<ol class="breadcrumb text-right">
					<li><a href="<?php echo SITE_URL; ?>admin/dashboard ">Dashboard</a></li>
					<li><a href="<?php echo ADMIN_URL; ?>vendors/index ">Vendor Manager</a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="card">
<div class="card-body">
<table  class="table table-striped table-bordered ">
	<tr>
		<td style="width:15%">Name</td>
		<td><?php echo $vendor->name; ?></td>
		<td style="width:15%">Contact Person</td>
		<td><?php echo $vendor->contact_person; ?></td>
	</tr>
	<tr>
		<td style="width:15%">Mobile 1</td>
		<td><?php echo $vendor->mobile_1; ?></td>
		<td style="width:15%">Mobile 2</td>
		<td><?php echo $vendor->mobile_2; ?></td>
	</tr>
	<tr>
		<td style="width:15%">Bank Name</td>
		<td><?php echo $vendor->bank_name; ?></td>
		<td style="width:15%">Account Number</td>
		<td><?php echo $vendor->account_number; ?></td>
	</tr>
	<tr>
		<td style="width:15%">IFSC Code</td>
		<td><?php echo $vendor->ifsc_code; ?></td>
		<td style="width:15%">PAN card</td>
		<td><?php echo $vendor->pan_card; ?></td>
	</tr>
	<tr>
		<td style="width:15%">GST Number</td>
		<td><?php echo $vendor->gst_number; ?></td>
		<td style="width:15%">Address</td>
		<td><?php echo $vendor->address; ?></td>
	</tr>
	<tr>
		<td style="width:15%">City</td>
		<td><?php echo $vendor->city; ?></td>
		<td style="width:15%">Rating</td>
		<td><?php echo $vendor->rating; ?></td>
	</tr>
	<tr>
		<td style="width:15%">Commission</td>
		<td><?php echo $vendor->commision . ' %'; ?></td>
		<td style="width:15%"></td>
		<td></td>
	</tr>
	<tr>
		<td style="width:15%">Banner Image</td>
		<td colspan="3"><img src="<?php echo SITE_URL ?>vendors/<?php echo $vendor->banner_images ?>" height="100px" width="150px">
</td>
	</tr>
</table>
</div>
</div>

