<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
								<th class="align-top">S.No.</th>
								<th scope="col">Name</th>
								<th scope="col">Contact Person</th>
								<th scope="col">Mobile</th>
								<th scope="col">City</th>
								<th scope="col">Vendor Commision</th>
								<th scope="col">Rating</th>
								<th scope="col">Completed Orders</th>
								<th scope="col">Payment</th>
								<th scope="col" class="actions">Action</th>
								</tr>
							</thead>
							<tbody >
								<?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Category']['page'] - 1) * $this->request->params['paging']['Category']['perPage'];
if (isset($vendors) && !empty($vendors)) {
    foreach ($vendors as $vendor) {$i++;
        ?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><a href="<?php echo ADMIN_URL; ?>vendors/detail/<?php echo $vendor['id']; ?>"> <?php echo $vendor['name']; ?></a></td>
											<td><?php echo $vendor->contact_person; ?></td>
											<td><?php echo $vendor->mobile_1; ?></td>
											<td><?php echo $vendor->city; ?></td>
											<td><?php echo $vendor->commision; ?></td>
			                                <?php if ($category->rating) {?>
				                            <td><?php echo $category->rating; ?></td>
				                            <?php } else {?>
				                            <td>--</td>
				                            <?php }?>
                                            <td><a href="#">0</a></td>
                                            <td><a href="#">0</a></td>
											<td>
											<?php if ($vendor['status'] == 1) {?>
													<?=$this->Html->link('', ['action' => 'status', $vendor->id, '0'], ['class' => 'fa fa-toggle-on', 'style' => 'color:green;font-size:20px;margin-right:5px'])?>
												<?php } else {?>
													<?=$this->Html->link('', ['action' => 'status', $vendor->id, '1'], ['class' => 'fa fa-toggle-off', 'style' => 'color:red;font-size:20px;margin-right:5px'])?>
												<?php }?>
												<?php echo $this->Html->link(__(''), ['action' => 'add', $vendor->id], array('class' => 'fa fa-pencil', 'title' => 'Edit', 'style' => 'font-size:20px;margin-right:5px')) ?>
												<?php
echo $this->Html->link('', [
            'action' => 'delete',
            $vendor->id,
        ], ['class' => 'fa fa-trash', 'style' => 'font-size:19px; color:#FF0000;margin-right:5px', "onClick" => "javascript: return confirm('Are you sure do you want to delete this category')"]); ?>
				<?php
echo $this->Html->link('', [
            'action' => 'area',
            $vendor->id,
        ], ['class' => 'fa fa-map-marker', 'style' => 'font-size:19px; color:#FF0000;']); ?>
												<?php ?>
											</td>
										</tr>
									<?php }} else {?>
										<tr><td colspan="10" style="text-align:center;">No Data Available</td></tr>
									<?php }?>
								</tbody>
							</table>
							<?php echo $this->element('admin/pagination'); ?>