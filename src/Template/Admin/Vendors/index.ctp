<style>
.anchr{margin-top: 26px; margin-bottom: 0px;}
.anchrr{margin-top: 8px; margin-bottom: 0px;}
.badge{font-size: 11px !important;}
</style>
<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
		</div>
	</div>
	<div class="col-sm-8">
		<div class="page-header float-right">
			<div class="page-title">
				<ol class="breadcrumb text-right">
					<li><a href="<?php echo SITE_URL; ?>admin/dashboard ">Dashboard</a></li>
					<li><a href="<?php echo ADMIN_URL; ?>vendors/index ">Vendor Manager</a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<?php echo $this->Flash->render(); ?>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Vendor Manager</strong>
						<a href="<?php echo SITE_URL; ?>admin/Vendors/add" ><strong class=" btn btn-info card-title pull-right">Add</strong></a>

					</div>


<div class="row" style="padding: 10px;">
    <script>
    $(document).ready(function () {
        $("#category_search").bind("submit", function (event)
        {
            $.ajax({
                async:true,
                data:$("#category_search").serialize(),
                dataType: "html",
                type:"POST",
                url:"<?php echo ADMIN_URL; ?>vendors/search",
                success:function (data) {
                //alert(data);
                $("#search_data").html(data); },
            });
            return false;
        });

    });
    //]]>
    $(document).on('click', '.pagination a', function() {
var target = $(this).attr('href');
if(!target)
return false;
$.get(target, function(data) {
$('#search_data').html( data );
}, 'html');
return false;
});
    </script>


    	<?php echo $this->Form->create('Mysubscription', array('type' => 'get', 'inputDefaults' => array('div' => false, 'label' => false), 'id' => 'category_search', 'class' => 'form-horizontal', 'method' => 'get', 'style' => 'width:100%')); ?>
            <div class="col-sm-3">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Vendor Name</label>
            		<?php echo $this->Form->input('name', array('class' => 'longinput form-control input-medium secrh-loc', 'placeholder' => 'Vendor Name', 'autocomplete' => 'off', 'type' => 'text', 'label' => false)); ?>
            		<div id="myUL"><ul></ul></div>
            	</div>
            </div>
            <div class="col-sm-3">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Vendor Mobile</label>
            		<?php echo $this->Form->input('mobile', array('class' => 'longinput form-control input-medium secrh-loc', 'placeholder' => 'Vendor Mobile', 'autocomplete' => 'off', 'type' => 'text', 'label' => false)); ?>
            		<div id="myUL"><ul></ul></div>
            	</div>
            </div>
            <div class="col-sm-3">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Vendor City</label>
            		<?php echo $this->Form->input('city', array('class' => 'longinput form-control input-medium secrh-loc', 'placeholder' => 'Vendor Mobile', 'autocomplete' => 'off', 'type' => 'select', 'empty' => 'Select City', 'options' => $city, 'label' => false)); ?>
            		<div id="myUL"><ul></ul></div>
            	</div>
            </div>

  <div class="col-sm-1" style="padding-top: 32px;">
            			<button type="submit" class="btn btn-success" id="Mysubscriptionevent">Search</button>
            	</div>
            	<?php echo $this->Form->end(); ?>
</div>
					<div id="search_data" class="card-body">
					<b class="pull-right">Total Vendor:<?php echo count($vendors); ?></b>
						<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
								<th class="align-top">S.No.</th>
								<th scope="col">Name</th>
								<th scope="col">Contact Person</th>
								<th scope="col">Mobile</th>
								<th scope="col">City</th>
								<th scope="col">Vendor Commision</th>
								<th scope="col">Rating</th>
								<th scope="col">Completed Orders</th>
								<th scope="col">Active Orders</th>
								<th scope="col">Paid Amount</th>
								<th scope="col" class="actions">Action</th>
								</tr>
							</thead>
							<tbody >
								<?php //pr($this->request->params); die;
$i = ($this->request->params['paging']['Category']['page'] - 1) * $this->request->params['paging']['Category']['perPage'];
if (isset($vendors) && !empty($vendors)) {
    foreach ($vendors as $vendor) {$i++;
        ?>
										<tr>
											<td><?php echo $i; ?></td>
											<td><a href="<?php echo ADMIN_URL; ?>vendors/detail/<?php echo $vendor['id']; ?>"> <?php echo $vendor['name']; ?></a></td>
											<td><?php echo $vendor->contact_person; ?></td>
											<td><?php echo $vendor->mobile_1; ?></td>
											<td><?php echo $vendor->city; ?></td>
											<td><?php echo $vendor->commision; ?></td>
			                                <?php if ($category->rating) {?>
				                            <td><?php echo $category->rating; ?></td>
				                            <?php } else {?>
				                            <td>--</td>
				                            <?php }?>
                                            <td><a href="<?php echo ADMIN_URL; ?>Orders/index/?vendorId=<?php echo $vendor->id; ?>&orderStatus=delivered" target="_blank"><?php echo $this->Comman->getVendorCompletedOrders($vendor->id); ?></a></td>
                                            <td><a href="<?php echo ADMIN_URL; ?>Orders/index/?vendorId=<?php echo $vendor->id; ?>&orderStatus=pending" target="_blank"><?php echo $this->Comman->getVendorPendingOrders($vendor->id); ?></a></td>
                                            <td><?php echo $this->Comman->totalVendorPayment($vendor->id); ?></td>
											<td>
												<?php if ($vendor['status'] == 1) {?>
													<?=$this->Html->link('', ['action' => 'status', $vendor->id, '0'], ['class' => 'fa fa-toggle-on', 'style' => 'color:green;font-size:20px;margin-right:5px'])?>
												<?php } else {?>
													<?=$this->Html->link('', ['action' => 'status', $vendor->id, '1'], ['class' => 'fa fa-toggle-off', 'style' => 'color:red;font-size:20px;margin-right:5px'])?>
												<?php }?>
												<?php echo $this->Html->link(__(''), ['action' => 'add', $vendor->id], array('class' => 'fa fa-pencil', 'title' => 'Edit', 'style' => 'font-size:20px;margin-right:5px')) ?>
												<?php
echo $this->Html->link('', [
            'action' => 'delete',
            $vendor->id,
        ], ['class' => 'fa fa-trash', 'style' => 'font-size:19px; color:#FF0000;margin-right:5px', "onClick" => "javascript: return confirm('Are you sure do you want to delete this category')"]); ?>
				<?php
echo $this->Html->link('', [
            'action' => 'area',
            $vendor->id,
        ], ['class' => 'fa fa-map-marker', 'style' => 'font-size:19px; color:#FF0000;', 'target' => '_blank']); ?>
												<?php ?>
											</td>
										</tr>
									<?php }} else {?>
										<tr><td colspan="10" style="text-align:center;">No Data Available</td></tr>
									<?php }?>
								</tbody>
							</table>
							<?php echo $this->element('admin/pagination'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>











