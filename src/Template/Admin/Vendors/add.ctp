<!-- https://www.youtube.com/watch?v=OK_JCtrrv-c -->
<style type="text/css">
.are{margin-top: -6px;}
.bfh-timepicker-popover table{width:280px;margin:0}
select.form-control:not([size]):not([multiple]) {
  height: calc(1.99rem + 2px) !important;
}
input[type=number]::-webkit-inner-spin-button,
input[type=number]::-webkit-outer-spin-button {
  -webkit-appearance: none;
  margin: 0;
}
</style>
<div class="breadcrumbs">
  <div class="col-sm-4">
    <div class="page-header float-left">
    </div>
  </div>
  <div class="col-sm-8">
    <div class="page-header float-right">
      <div class="page-title">
        <ol class="breadcrumb text-right">
          <li><a href="<?php echo ADMIN_URL; ?>dashboard ">Dashboard</a></li>
          <li><a href="<?php echo ADMIN_URL; ?>vendor/index ">Vendor Manager</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!--<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">-->
  <?php echo $this->Flash->render(); ?>
  <?php
if (isset($vendor)) {
    echo $this->Form->create($vendor, array(
        'class' => 'form-horizontal',
        'controller' => 'vendors',
        'action' => 'edit/' . $vendor['id'],
        'enctype' => 'multipart/form-data',
        'validate', 'autocomplete' => 'off'));
} else {
    echo $this->Form->create('products', array(
        'class' => 'form-horizontal',
        'controller' => 'vendors',
        'action' => 'add',
        'enctype' => 'multipart/form-data',
        'validate', 'autocomplete' => 'off'));
}
?>

   <div class="col-lg-12">
    <div class="card">
      <div class="card-header"><strong>Vendor</strong></div>
      <div class="card-body card-block">
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Vendor Name</strong></label>
            <?php
echo $this->Form->input('name', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Vendor Name', 'type' => 'text', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Contact Person</strong></label>
            <?php
echo $this->Form->input('contact_person', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Contact Person Name', 'type' => 'text', 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Mobile Number 1</strong></label>
            <?php
echo $this->Form->input('mobile_1', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Mobile Number 1', 'type' => 'number', 'label' => false, 'autocomplete' => 'off', 'onkeypress' => 'return isNumberKey(event)', 'required')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Mobile Number 2</strong></label>
            <?php
echo $this->Form->input('mobile_2', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Mobile Number 2', 'type' => 'number', 'label' => false, 'autocomplete' => 'off', 'onkeypress' => 'return isNumberKey(event)')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Bank Name</strong></label>
            <?php
echo $this->Form->input('bank_name', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Bank Name', 'type' => 'text', 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Account Number</strong></label>
            <?php
echo $this->Form->input('account_number', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Account Number', 'type' => 'number', 'label' => false, 'autocomplete' => 'off', 'onkeypress' => 'return isNumberKey(event)')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>IFSC Code</strong></label>
            <?php
echo $this->Form->input('ifsc_code', array('class' => 'longinput form-control input-medium', 'placeholder' => 'IFSC Code', 'type' => 'text', 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Pan Card</strong></label>
            <?php
echo $this->Form->input('pan_card', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Pan Card', 'type' => 'text', 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>GST Number</strong></label>
            <?php
echo $this->Form->input('gst_number', array('class' => 'longinput form-control input-medium', 'placeholder' => 'GST Number', 'type' => 'text', 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Address</strong></label>
            <?php
echo $this->Form->input('address', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Address', 'type' => 'text', 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>City</strong></label>
            <?php
echo $this->Form->input('city_id', array('class' => 'longinput form-control input-medium', 'required', 'type' => 'select', 'empty' => 'Select City', 'options' => $cities, 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
          <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Commision</strong></label>
            <?php
echo $this->Form->input('commision', array('class' => 'longinput form-control input-medium commision', 'placeholder' => 'Commision', 'type' => 'text', 'label' => false, 'autocomplete' => 'off')); ?>%
          </div>
        </div>
          <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Categories</strong></label>
            <?php if (isset($vendor)) {
    echo $this->Form->input('categories[]', array('class' => 'longinput form-control input-medium', 'type' => 'select', 'multiple', 'options' => $categories, 'label' => false, 'autocomplete' => 'off', 'id' => 'categories', 'value' => $categoriesSelected));
} else {
    echo $this->Form->input('categories[]', array('class' => 'longinput form-control input-medium', 'type' => 'select', 'multiple', 'options' => $categories, 'label' => false, 'autocomplete' => 'off', 'id' => 'categories'));
}
?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Email</strong></label>
            <?php
echo $this->Form->input('email', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Email', 'type' => 'text', 'label' => false, 'required', 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
            <label><strong>Banner Image</strong></label>
            <?php
echo $this->Form->input('banner_images', array('class' => 'longinput form-control input-medium', 'placeholder' => 'Address', 'type' => 'file', 'label' => false, 'autocomplete' => 'off')); ?>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="form-group">
            <div class="col-sm-1">
              <a href="<?php echo ADMIN_URL ?>vendors/index" class="btn btn-primary " >Back</a>
           </div>
           <div class="col-sm-1">
            <?php if (isset($event['id'])) {
    echo $this->Form->submit('Update', array('title' => 'Update', 'div' => false,
        'class' => array('btn btn-primary btn-sm')));} else {?>
                <button type="submit" class="btn btn-success">Submit</button>
              <?php }?>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <?php echo $this->Form->end(); ?>
<!--   <script type="text/javascript">
var txte = $('#sorting_check').val();
  $( "#sorting_check" ).keyup(function() { //alert();
    var txte = $('#sorting_check').val();
    var radioValue = $("input[name='ptype']:checked"). val();
 //alert(txte);
$.ajax({
 type: 'POST',
 url: '<?php echo SITE_URL; ?>admin/product/sortingcheck',
 data: {'sortno':txte, 'radioValue':radioValue },
 success: function(data){
   if(data=='1'){
    $('#sortingmessage').css('display','block');
    $('#sorting_check').val('');


  }else{
    $('#sortingmessage').css('display','none');

  }
},
});

});

</script> -->
<script type="text/javascript">
$(document).ready(function() {
    $('#categories').select2({
      placeholder: "Select Categories",
      allowClear: true
    });
});
  function ValidateFileUpload() {
    var fuData = document.getElementById('fileChooser');
    var FileUploadPath = fuData.value;

//To check if user upload any file
if (FileUploadPath == '') {
  alert("Please upload an image");
} else {
  var Extension = FileUploadPath.substring(
    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
//The file uploaded is an image
if (Extension == "png" || Extension == "jpeg" || Extension == "jpg") {
// To Display
var img = document.getElementById("fileChooser");
 if(img.files[0].size <  1048576)  // validation according to file size
 {
       //alert(img.files[0].size);
       document.getElementById("showfeatimage").style.display = "none";
       document.getElementById("showfeatsize").style.display = "none";
       return true;
     }
     else{
      document.getElementById("showfeatimage").style.display = "none";
      document.getElementById("showfeatsize").style.display = "block";
      document.getElementById("fileChooser").value = "";
      return false;
    }
  }
//The file upload is NOT an image
else {
 document.getElementById("showfeatimage").style.display = "block";
 document.getElementById("fileChooser").value = "";
 return false;
}
}
}
</script>
<script>
$(document).ready(function(){
  $(document).on('keyup','.commision',function(){
    let value=$(this).val();
    if(parseInt(value) > 100){
      alert('Commision Cannot be greater than 100');
      $(this).val('');
      return false;
    }
  })
});
</script>






