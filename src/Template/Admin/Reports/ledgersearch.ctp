<p class="pull-right"><?php echo !empty($total) ? $total : null;
?></p>
<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
									<th class="align-top">S.No.</th>
									<th class="align-top">Date</th>
									<th class="align-top">Order Id</th>
									<th class="align-top">Description</th>
									<th class="align-top">Order Total</th>
									<th class="align-top">Commission</th>
									<th class="align-top">Credit</th>
									<th class="align-top">Debit</th>
									<th class="align-top">Balance</th>
								</tr>
							</thead>
							<tbody >
					<?php
if ($response['success'] == true) {?>
	<tr>
		<td colspan="8" style="text-align:right"><b>Opening Balance:</b></td>
		<td><?php echo round($openingBalance, 2); ?></td>
	</tr>
	<?php
$i = 1;foreach ($response['output']['transactions'] as $value) { //pr($value);
    ?>

										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo $value['date']; ?></td>
											<td><?php echo $value['order_id']; ?></td>
											<td><?php echo $value['description'] ?></td>
											<td><?php echo $value['orderTotal']; ?></td>
											<td><?php echo $value['commission']; ?></td>
											<td><?php if ($value['credit'] == 0) {echo "";} else {echo $value['credit'];}?></td>
											<td><?php if ($value['debit'] == 0) {echo "";} else {echo $value['debit'];}?></td>
											<td><?php echo round($value['balance'], 2); ?></td>
<?php $totalamount += $value['total'];?>
<?php $totalcomission += $value['comission'];?>

										</tr>
									<?php $i++;

}?>

<tr>
<td colspan="8" style="text-align:right"><b>Total Order:</b></td>
<td><?php echo round($response['output']['totalOrder'], 2); ?></td>
</tr>
<tr>
<td colspan="8" style="text-align:right"><b>Total Commission:</b></td>
<td><?php echo round($response['output']['totalComission'], 2); ?></td>
</tr>
<tr>
<td colspan="8" style="text-align:right"><b>Total Payment:</b></td>
<td><?php echo round($response['output']['totalPayment'], 2); ?></td>
</tr>
<tr>
<td colspan="8" style="text-align:right"><b>Total Balance:</b></td>
<td><?php echo round($response['output']['totalBalance'], 2); ?></td>
</tr>

									  <?php } else {?>
										<tr>

											<td colspan="12" style="text-align":center>No Data Available</td>
										</tr>
									<?php }?>
								</tbody>
							</table>
							<?php echo $this->element('admin/pagination'); ?>