<style>
.anchr{margin-top: 26px; margin-bottom: 0px;}
.anchrr{margin-top: 8px; margin-bottom: 0px;}
.badge{font-size: 11px !important;}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
		</div>
	</div>
	<div class="col-sm-8">
		<div class="page-header float-right">
			<div class="page-title">
				<ol class="breadcrumb text-right">
					<li><a href="<?php echo SITE_URL; ?>admin/dashboard ">Dashboard</a></li>
					<li><a href="<?php echo ADMIN_URL; ?>vendorledger/index ">Vendor Ledger</a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<?php echo $this->Flash->render(); ?>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Vendor Ledger</strong>
						<a href="<?php echo SITE_URL; ?>admin/wallet/add" ><strong class=" btn btn-info card-title pull-right">Add</strong></a>

						<a href="<?php echo SITE_URL; ?>admin/wallet/downloadexcl" ><strong title="Download excel file" class="fa fa-cloud-download btn btn-primary card-title pull-right"  style="margin-right:10px; font-size: 23px; " ></strong></a>

						<a href="<?php echo SITE_URL; ?>admin/wallet/uploadexcl" data-toggle="modal" class="uploadpr" ><strong title="Upload excel file" class="fa fa-cloud-upload btn btn-info card-title pull-right" id="save_value" style="margin-right:10px; font-size: 23px; " ></strong></a>
						<div class="row" style="margin-top:25px;width:100%">
					<?php echo $this->Form->create('Mysubscription', array('inputDefaults' => array('div' => false, 'label' => false), 'id' => 'category_search', 'class' => 'form-horizontal', 'method' => 'post', 'style' => 'width:100%', 'id' => 'wallet-search', 'autocomplete' => 'off')); ?>
            <div class="col-sm-2">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Customer Mobile</label>
            		<?php echo $this->Form->input('customer_mobile', array('class' => 'longinput form-control input-medium secrh-loc', 'placeholder' => 'Customer Mobile', 'autocomplete' => 'off', 'type' => 'text', 'label' => false)); ?>
            		<div id="myUL"><ul></ul></div>
            	</div>
            </div>
            <div class="col-sm-2">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Amount Type</label>
            		<?php echo $this->Form->input('type', array('class' => 'longinput form-control input-medium amount-type', 'empty' => 'All', 'autocomplete' => 'off', 'type' => 'select', 'options' => ['promotional' => 'CashBacks', 'cashback_used' => 'Cashback Used', 'income' => 'Wallet Cash', 'expense' => 'Wallet Used'], 'label' => false)); ?>
            		<div id="myUL"><ul></ul></div>
            	</div>
            </div>
			<div class="col-sm-2">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Cashbacks Type</label>
            		<?php echo $this->Form->input('cashback_type', array('class' => 'longinput form-control input-medium cashback-type', 'empty' => 'All', 'autocomplete' => 'off', 'type' => 'select', 'options' => ['expired' => 'Expired', 'active' => 'Active'], 'label' => false, 'disabled')); ?>
            		<div id="myUL"><ul></ul></div>
            	</div>
            </div>
            <div class="col-sm-3">
            	<div class="form-group input-daterange">
            	    <label for="company" class=" form-control-label">Date</label>
					<input type="text" name="from_date" id="date-range" class="longinput form-control input-medium secrh-loc" placeholder="Select Date Range" autocomplete="off" value="">

            	</div>
            </div>

  				<div class="col-sm-2" style="padding-top: 32px;">
            			<button type="search" class="btn btn-success" id="product-search" value="search">Search</button>
            			<button type="reset" class="btn btn-success" id="product-search" value="search">Reset</button>

            	</div>
            	<?php echo $this->Form->end(); ?>
				</div>
					</div>
					<div id="example5" class="card-body">
						<?php //echo $this->Paginator->limitControl([10 => 10, 15 => 15,20=>20,25=>25,30=>30]);?>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<div class="modal fade" id="myModalss">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal Header -->
			<!-- Modal body -->
			<div class="modal-body">
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$('.amount-type').change(function(){
		let val=$(this).val();
		if(val=='promotional' || val=="cashback_used"){
			$('.cashback-type').attr('disabled',false);
		}else{
			$('.cashback-type').attr('disabled',true);
			$('.cashback-type').val('');
		}
	});
	$('#date-range').daterangepicker({
		autoApply:true,
		locale: {
      format: 'DD/MM/YYYY',
	  cancelLabel: 'Clear'
    }
	});
	 $('#date-range').val('');
	 $('.cancelBtn').click(function(){
		 $('#date-range').val('');
	 })
});
	$('.uploadpr').click(function(e){
		e.preventDefault();
		$('#myModalss').modal('show').find('.modal-body').load($(this).attr('href'));
	});
</script>
<script>
	$(document).ready(function () {
	    	// var radioValue = $("input[name='ptype']:checked"). val();
		$("#wallet-search").submit(function (event) {
		//alert(radioValue);
		$.ajax({
			async:true,
			type:"POST",
			dataType: "html",
			url:"<?php echo ADMIN_URL; ?>wallets/search",
			data:  $("#wallet-search").serialize(),
			success:function (data) {
				$("#example5").html(data); },
			});
			return false;
		});
	});
</script>