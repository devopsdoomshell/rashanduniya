<?php
$objPHPExcel = new PHPExcel();
// Set properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");
// Miscellaneous glyphs, UTF-8
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1', 'S.No.')
    ->setCellValue('B1', 'Date')
    ->setCellValue('C1', 'Order Id')
    ->setCellValue('D1', 'Description')
    ->setCellValue('E1', 'Order Total')
    ->setCellValue('F1', 'Commission')
    ->setCellValue('G1', 'Credit')
    ->setCellValue('H1', 'Debit')
    ->setCellValue('I1', 'Balance')
;
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('i1')->getFont()->setBold(true);
$i = 0;
$ii = $i + 2;
$cnt = 1;
if ($response['success'] == true) {
    extract($response['output']);
    $objPHPExcel->getActiveSheet()->mergeCells('A' . $ii . ':H' . $ii);
    $objPHPExcel->getActiveSheet()->setCellValue('A' . $ii, 'Opening Balance');
    $objPHPExcel->getActiveSheet()->getStyle('A' . $ii . ':H' . $ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('A' . $ii)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->setCellValue('I' . $ii, round($openingBalance, 2));
    $ii++;
    foreach ($transactions as $value) {
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $ii, $cnt);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $ii, date('d M Y', strtotime($value['date'])));
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $ii, $value['order_id']);
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $ii, $value['description']);
        $objPHPExcel->getActiveSheet()->setCellValue('E' . $ii, $value['orderTotal']);
        $objPHPExcel->getActiveSheet()->setCellValue('F' . $ii, $value['commission']);
        $objPHPExcel->getActiveSheet()->setCellValue('G' . $ii, $value['credit']);
        $objPHPExcel->getActiveSheet()->setCellValue('H' . $ii, $value['debit']);
        $objPHPExcel->getActiveSheet()->setCellValue('I' . $ii, round($value['balance'], 2));
        $ii++;
        $cnt++;
    }
}
$objPHPExcel->getActiveSheet()->mergeCells('A' . $ii . ':H' . $ii);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $ii, 'Total Order');
$objPHPExcel->getActiveSheet()->getStyle('A' . $ii . ':H' . $ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('A' . $ii)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('I' . $ii, round($totalOrder, 2));
$ii++;
$objPHPExcel->getActiveSheet()->mergeCells('A' . $ii . ':H' . $ii);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $ii, 'Total Commission');
$objPHPExcel->getActiveSheet()->getStyle('A' . $ii . ':H' . $ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('A' . $ii)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('I' . $ii, round($totalComission, 2));
$ii++;
$objPHPExcel->getActiveSheet()->mergeCells('A' . $ii . ':H' . $ii);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $ii, 'Total payment');
$objPHPExcel->getActiveSheet()->getStyle('A' . $ii . ':H' . $ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('A' . $ii)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('I' . $ii, round($totalPayment, 2));
$ii++;
$objPHPExcel->getActiveSheet()->mergeCells('A' . $ii . ':H' . $ii);
$objPHPExcel->getActiveSheet()->setCellValue('A' . $ii, 'Total Balance');
$objPHPExcel->getActiveSheet()->getStyle('A' . $ii . ':H' . $ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('A' . $ii)->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->setCellValue('I' . $ii, round($totalBalance, 2));
$ii++;

// Rename sheet
//$objPHPExcel->getActiveSheet()->setTitle('Simple');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
// Redirect output to a client’s web browser (Excel2007)
$filename = "ledger.xlsx";
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename=' . $filename);
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
