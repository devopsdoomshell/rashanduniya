<style>
.anchr{margin-top: 26px; margin-bottom: 0px;}
.anchrr{margin-top: 8px; margin-bottom: 0px;}
.badge{font-size: 11px !important;}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<div class="breadcrumbs">
	<div class="col-sm-4">
		<div class="page-header float-left">
		</div>
	</div>
	<div class="col-sm-8">
		<div class="page-header float-right">
			<div class="page-title">
				<ol class="breadcrumb text-right">
					<li><a href="<?php echo SITE_URL; ?>admin/dashboard ">Dashboard</a></li>
					<li><a href="<?php echo ADMIN_URL; ?>reports/paymenthistory ">Payment History</a></li>
				</ol>
			</div>
		</div>
	</div>
</div>
<div class="content mt-3">
	<div class="animated fadeIn">
		<div class="row">
			<?php echo $this->Flash->render(); ?>
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">Payment History</strong>
<a href="#"  class="" data-toggle="modal" data-target="#myModal" ><strong title="Pay" class="fa fa-money btn btn-primary card-title pull-right"  style="margin-right:10px; font-size: 23px; " ></strong></a>
						<div class="row" style="margin-top:25px;width:100%">
					<?php echo $this->Form->create('Mysubscription', array('inputDefaults' => array('div' => false, 'label' => false), 'id' => 'category_search', 'class' => 'form-horizontal', 'method' => 'post', 'style' => 'width:100%', 'id' => 'wallet-search', 'autocomplete' => 'off')); ?>
         
            <div class="col-sm-2">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Vendors</label>
            		<?php echo $this->Form->input('vendor_id', array('class' => 'longinput form-control input-medium amount-type', 'empty' => '--Select--', 'autocomplete' => 'off', 'type' => 'select', 'options' =>$vendor, 'label' => false)); ?>
            		<div id="myUL"><ul></ul></div>
            	</div>
            </div>
			
            <div class="col-sm-3">
            	<div class="form-group input-daterange">
            	    <label for="company" class=" form-control-label">Date</label>
					<input type="text" name="from_date" id="date-range" class="longinput form-control input-medium secrh-loc" placeholder="Select Date Range" autocomplete="off" value="">

            	</div>
            </div> 

  				<div class="col-sm-2" style="padding-top: 32px;">
            			<button type="search" class="btn btn-success" id="product-search" value="search">Search</button>
            			<button type="reset" class="btn btn-success" id="product-search" value="search">Reset</button>

            	</div>
            	<?php echo $this->Form->end(); ?>
				</div>
					</div>
					<div id="example5" class="card-body">
<table id="bootstrap-data-table" class="table table-striped table-bordered ">
							<thead>
								<tr>
									<th class="align-top">S.No.</th>
									<th class="align-top">Tran. Date</th>
									<th class="align-top">Description</th>
									<th class="align-top">Amount</th>
									<th class="align-top">Vendor Name</th>
									<th class="align-top">Vendor Mobile</th>
								</tr>
							</thead>
							<tbody >
				
<?php
					if (isset($vendor_payments) && !empty($vendor_payments)) {
					$i = 1; foreach ($vendor_payments as $value) { //pr($value);
					?>
					
										<tr>
											<td><?php echo $i; ?></td>
											<td><?php echo  date('d M Y', strtotime($value['created']));; ?></td>
											<td><?php echo $value['descri'] ?></td>
											<td><?php echo $value['amount']; ?></td>
											<td><?php echo $value['vendor']['name']; ?></td>
											<td><?php echo $value['vendor']['mobile_1']; ?></td>
											


										</tr>
									<?php $i++;  }} else {?>
										<tr>
											<td colspan="12" style="text-align":center>No Data Available</td>
										</tr>
									<?php }?>
								</tbody>
							</table>
						<?php //echo $this->Paginator->limitControl([10 => 10, 15 => 15,20=>20,25=>25,30=>30]);?>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        
        <div class="modal-body">
         <?php echo $this->Form->create('vendorpayment', array('inputDefaults' => array('div' => false, 'label' => false), 'id' => 'vendorpayment', 'class' => 'form-horizontal', 'method' => 'post', 'style' => 'width:100%', 'autocomplete' => 'off')); ?>
         <div class="row">
<div class="col-sm-6">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Vendors</label>
            		<?php echo $this->Form->input('vendor_rec', array('class' => 'longinput form-control input-medium amount-type', 'empty' => '--Select--', 'autocomplete' => 'off', 'type' => 'select', 'options' =>$vendor, 'label' => false)); ?>
            	</div>
            </div>

<div class="col-sm-6">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Vendors Number</label>
            		<?php echo $this->Form->input('vendor_mobile', array('class' => 'longinput form-control input-medium amount-type', 'autocomplete' => 'off', 'label' => false,'readonly')); ?>
            	</div>
            </div>


            <div class="col-sm-6">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Vendors Balance</label>
            		<?php echo $this->Form->input('vendor_balance', array('class' => 'longinput form-control input-medium amount-type','autocomplete' => 'off', 'type' => 'text','label' => false,'readonly')); ?>
            	</div>
            </div>


              <div class="col-sm-6">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Pay Amount</label>
            		<?php echo $this->Form->input('amount', array('class' => 'longinput form-control input-medium amount-type','autocomplete' => 'off', 'type' => 'text','label' => false)); ?>
            	</div>
            </div>


             <div class="col-sm-12">
            	<div class="form-group">
            	    <label for="company" class=" form-control-label">Description</label>
            		<?php echo $this->Form->input('descri', array('class' => 'longinput form-control input-medium amount-type','autocomplete' => 'off', 'type' => 'textarea','label' => false)); ?>
            	</div>
            </div>
 <div class="col-sm-12">
          <div class="form-group">
            <div class="col-sm-2">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
           <div class="col-sm-1">
                            <button type="submit" class="btn btn-success">Submit</button>
                          </div>
                         
          </div>
        </div>
        </div>
         <?php echo $this->Form->end(); ?>
        </div>
     
      </div>
      
    </div>
  </div>
  

<script src="https://code.jquery.com/jquery-3.5.1.min.js"
  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
$(document).ready(function(){
	$('.amount-type').change(function(){
		let val=$(this).val();
		if(val=='promotional' || val=="cashback_used"){
			$('.cashback-type').attr('disabled',false);
		}else{
			$('.cashback-type').attr('disabled',true);
			$('.cashback-type').val('');
		}
	});
	$('#date-range').daterangepicker({
		autoApply:true,
		locale: {
      format: 'DD/MM/YYYY',
	  cancelLabel: 'Clear'
    }
	});
	 $('#date-range').val('');
	 $('.cancelBtn').click(function(){
		 $('#date-range').val('');
	 })
});
	$('.uploadpr').click(function(e){
		e.preventDefault();
		$('#myModalss').modal('show').find('.modal-body').load($(this).attr('href'));
	});
</script>



<script>
	$(document).ready(function () {
$('#vendor-rec').on('change',function() { //alert('test');
 let rec=$(this).val();
  	$.ajax({
        type: "POST",
        headers: {'X-CSRF-Token': csrfToken},
        url: "<?php echo SITE_URL ;?>admin/reports/vendordetail",
        data: {vendor_id: rec},
        cache:false,
        success:function(data){ 
        	obj = JSON.parse(data);
        	var user_address = $("#vendor-mobile").val(obj.vendormobile);
        var user_landmark = $("#vendor-balance").val(obj.vendorbalance);
           
        }
        });
		});


$("#vendorpayment").submit(function (event) {
	$(".error").hide();


     var amount = $('#amount').val();
     var vendorbalance = $('#vendor-balance').val();
      var hasError = false;
var vendorselect = $('#vendor-rec').val();
var descri = $('#descri').val();
      if(vendorselect == '') 
    {
        $("#vendor-rec").after('<span class="error" style="color:red;">Select Vendor</span>');
        hasError = true;
    }

    if(descri == '') 
    {
        $("#descri").after('<span class="error" style="color:red;">Description is required.</span>');
        hasError = true;
    }
      if(amount == '') 
    {
        $("#amount").after('<span class="error" style="color:red;">Amount is required.</span>');
        hasError = true;
    }

    if(vendorbalance < amount) 
    {
        $("#amount").after('<span class="error" style="color:red;">input amount is big according to  vendor balance amount.</span>');
        hasError = true;
    }
        if(hasError == true) 
    {
    return false; 
    } 
		$.ajax({
			async:true,
			type:"POST",
			headers: {'X-CSRF-Token': csrfToken},
			dataType: "html",
			url:"<?php echo ADMIN_URL; ?>reports/vendorpayment",
			data:  $("#vendorpayment").serialize(),
			success:function (data) {
				if(data==0){
					location.reload();
				}

			},
			});
			return false;
		});

	    	// var radioValue = $("input[name='ptype']:checked"). val();
		$("#wallet-search").submit(function (event) {
		//alert(radioValue);
		$.ajax({
			async:true,
			type:"POST",
			headers: {'X-CSRF-Token': csrfToken},
			dataType: "html",
			url:"<?php echo ADMIN_URL; ?>reports/paymenthistorysearch",
			data:  $("#wallet-search").serialize(),
			success:function (data) {
				$("#example5").html(data); },
			});
			return false;
		});
	});
</script>