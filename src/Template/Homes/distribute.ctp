<section class="pagebanner">
    <div class="page-distribute-banner"> <h2 class="sub-headings">The Way We Distribute</h2>  </div>
</section>

<section id="consume-sec">
  <div class="consume-sec-bg">
    
     <div class="container-fluid">
        <div class="row m-top-30">
           <div class="col-md-4 col-sm-6 col-xs-12"> <div class="consume-block">
                <div class="consume-icon green">
                <img src="<?php echo SITE_URL ?>frontfile/images/pricing.png" alt="image"></div>
                <h3>Fair Pricing With Quality Produce</h3>
                <p>We create a wide supplier base which allows our retailers (supermarket, vendor of farm produce, restaurant, bulk consumer, food or other business) to procure quality produce at the fair and lowest price directly from the farmers.</p>
            </div></div>
            
            
            <div class="col-md-4 col-sm-6 col-xs-12"><div class="consume-block">
                <div class="consume-icon red"><img src="<?php echo SITE_URL ?>frontfile/images/trackorder.png " alt="image"></div>
                <h3>Order Tracking</h3>
                <p>Our retailers can easily track the order from warehouse-to-shop ensuring complete transparency from our end. </p>
                
            </div></div>
            
            <div class="col-md-4 col-sm-6 col-xs-12"><div class="consume-block">
                <div class="consume-icon blue"><img src="<?php echo SITE_URL ?>frontfile/images/freshfood.png" alt="image"></div>
                <h3> F & F Delivery</h3>
                <p>Our digital agri-supply chain ensures Fresh & Fast delivery of produce from Farm to retailer within 12 to 15 hours.</p>
                
                
            </div></div>
            <div class="col-md-6 col-sm-6 col-xs-12"><div class="consume-block">
                <div class="consume-icon orange"><img src="<?php echo SITE_URL ?>frontfile/images/truck.png " alt="image"></div>
                <h3>Delivery at Shopstep</h3>
                <p>No more pains of wasting time in the market for purchase as Orders are placed on our mobile app and the delivery is made to the doorstep at the desired time.</p>
                
                
                
            </div></div>
            
            
            <div class="col-md-6 col-sm-12 col-xs-12 "><div class="consume-block">
                <div class="consume-icon red"><img src="<?php echo SITE_URL ?>frontfile/images/no_extra_cost.png " alt="image"></div>
                <h3>No Extra Costs of Logistics</h3>
                <p>Our retailers do not suffer any logistical cost like that of storing or transporting the fruits and veggies. We completely ensure that our buyers do not have to pay any extra cost other than the original price of the fruits and veggies they purchase.</p>
               
            </div></div>
            
        </div>
        
        
        
    </div> 
</div>




</section> 



</div><!--o-wrapper-->

<script>
$('.slid-carousal').owlCarousel({
	
    loop:true,
    autoplay:true,
	
	nav:false,
    animateOut: 'fadeOut',
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:false,
            
        }
    }
})


</script>
</body>
</html>
