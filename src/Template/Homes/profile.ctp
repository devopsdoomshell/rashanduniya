        <section id="cartPage" class="orderDetailPage">
            <div class="productD">
                <div class="container">
                    <h3>Placed on Sat 06 Feb 2021, 4:00 PM</h3>
                    <div class="row">
                        <div class="col-md-8 col-sm-7">

                            <div class="orderDetailTime d-flex justify-content-between p-3">
                                <p><span>Delivery Time</span> <b>Tue. 6 Feb 2021</b></p>
                                <p>Order Status: <b>Delivered</b></p>
                            </div>

                            <div class="orderDetailTime d-flex justify-content-between p-3">
                                <p><b>Pay on Delivery:</b> You have to pay ₹ 2450 to the delivery Executive</p>
                            </div>

                            <p class="mb-2">3 Item(s)</p>

                            <div class="cartList">
                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt="" class="img-fluid" />
                                <div class="dtl flex-fill">
                                    <h5>Fortune Health Refined Rice Bran Oil (Jar)</h5>
                                    <p class="cartPrice">₹ 800.00</p>
                                    <div class="d-flex align-items-center justify-content-between">
                                        <p>5 Ltr</p>

                                    </div>
                                </div>
                            </div>

                            <div class="cartList">
                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt="" class="img-fluid" />
                                <div class="dtl flex-fill">
                                    <h5>Fortune Health Refined Rice Bran Oil (Jar)</h5>
                                    <p class="cartPrice">₹ 800.00</p>
                                    <div class="d-flex align-items-center justify-content-between">
                                        <p>5 Ltr</p>

                                    </div>
                                </div>
                            </div>

                            <div class="cartList">
                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt="" class="img-fluid" />
                                <div class="dtl flex-fill">
                                    <h5>Fortune Health Refined Rice Bran Oil (Jar)</h5>
                                    <p class="cartPrice">₹ 800.00</p>
                                    <div class="d-flex align-items-center justify-content-between">
                                        <p>5 Ltr</p>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-5">
                            <div class="subTOtal">
                                <h5>Order Summery</h5>
                                <ul>
                                    <li><span>M.R.P</span><span>₹ 2400.00</span></li>
                                    <li><span>Product Discount</span><span>₹ 00.00</span></li>
                                    <li><span>Delivery Charges</span><span>₹ 50.00</span></li>
                                    <li class="border-top mt-2">
                                        <strong>Sub Total</strong>
                                        <strong>₹ 2450</strong>
                                    </li>
                                </ul>
                                <!-- <a href="<?php echo SITE_URL; ?>checkout.html">Checkout</a> -->
                            </div>
                            <div class="subTOtal mt-3">
                                <h5>Delivery Address</h5>
                                <ul>
                                    <li>Office</li>
                                    <li>Ritesh Chhipa</li>
                                    <li>DAAC Institute A3 Mall Road Vidhyadhar Nagar jaipur Raj.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="topSavers">
                <div class="container-fluid">
                    <h4>Top Savers Today</h4>
                    <div class="owl-carousel owl-theme owl-loaded savers-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>product_detail.html" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>cart.html"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>product_detail.html" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>cart.html"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>product_detail.html" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>cart.html"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>