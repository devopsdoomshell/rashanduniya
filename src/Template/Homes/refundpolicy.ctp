
<section id="privacy_pg">
<div class="container">
<h2 class="text-center">Refund Policy</h2>



<div class="privacy_div">
<p class="text-justify">Customer can cancel his/her order until it is not dispatched from the warehouse. We will refund 100% amount. Once if order has been dispatched from ware house, customer cannot cancel or modify the order and we are not liable to refund the amount.</p></div>


</div>
</section>