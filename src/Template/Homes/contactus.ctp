
<section id="career-contact">
<h2 class="sub-heading">Contact Us</h2>


<div class="cntct_us"> 
<div class="container">

<div class="row">
<div class="col-sm-7">

<div class="career">
<h2>Write to Us</h2>
<p>Send your queries by updating below details</p>


</div>



<section class="cntct-form">
     <?php echo $this->Form->create($ds,array(
   'class'=>'form-horizontal',
   'controller'=>'homes',
   'action'=>'contactus',
   'enctype' => 'multipart/form-data',
   'validate' )); ?>
  <div class="form-group">

    <div class="col-sm-6">
      <input type="text" name="name" class="form-control" id="name" placeholder="Name" required="required">
    </div>
    
    <div class="col-sm-6">
    <input type="email"  name="email" class="form-control" id="email" placeholder="Email" required="required">
    </div>
    
    </div>
    
    <div class="form-group">
    <div class="col-sm-6">
    <input type="text"  name="phn" class="form-control" id="phone" onkeypress="return isNumber(event);" placeholder="Mobile No." minlength="10" maxlength="10" required="required">
    </div>
    
    <div class="col-sm-6">
        
<?php $sector=array('Farmer'=>'Farmer','Retailer'=>'Retailer','Consumer'=>'Consumer');
echo $this->Form->input('openings', array('class' => 'form-control','options'=>$sector,'required','label'=>false)); ?>
        
    </div>
  </div>
  
  <div class="form-group">
  <div class="col-sm-12">
  <?php 
echo $this->Form->input('description', array('class' => 'form-control','type'=>'textarea','placeholder'=>'Description...' ,'label'=>false)); ?>
  </div>
  </div>
  <div class="form-group">
  <div class="col-sm-12">
  <?php 
echo $this->Form->input('file', array('class' => 'form-control filess','accept'=>'.doc,.docx,.pdf','type'=>'file','label'=>false)); ?>
  </div>
  </div>
  <div class="text-center"><input type="submit" id="resume_link" value="Submit"></div>
 
 <?php echo $this->Form->end(); ?>
  <?php echo $this->Flash->render(); ?>
</section>

</div>

<div class="col-sm-5 contct-link">





<div class="col-sm-12">
<ul>
<li>
<a href="mailto:xyz@xyz.com" class=""><i class="fa fa-2x fa-envelope" aria-hidden="true"></i> customer.support@veggiegreenhouse.com</a>
</li>
<!-- <li>
<a href="#" class=""><i class="fa fa-2x fa-phone" aria-hidden="true"></i>
9610002020 call us</a>
</li> -->
</ul>
</div>


<img src="<?php echo SITE_URL ?>frontfile/images/content-image-4.png" alt="image">

</div>

</div>



</div>



</div><!--cntct_us-->



</section>

<script type="text/javascript">
    $(document).ready(function(){

        $('.filess').change(function(){
 
            if($('.filess').val() != '') { 
             
                var filevalue=$('.filess').val();
                var filnam= filevalue.split('.').pop();
                
                if (filnam=='doc' || filnam=='docx' || filnam=='pdf' || filnam=='PDF') {
                    return true;
                }else{
                    
                    $('.filess').val('');
                    alert("Please select only pdf/doc/docx file");
                    return false;
                }       
            }
        });
    });   

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
     alert("Please Enter Only Numeric Characters!!!!");
     return false;
   }
   return true;

 }
</script>