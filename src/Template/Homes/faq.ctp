
<section id="faq_pg">






<div class="container">
<h2 class="">FAQ</h2>


<div class="panel-group" id="accordion">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" class="collapsed" class="collapsed" data-parent="#accordion" href="#collapse1">Is there a minimum order value for delivery?</a>
        </h4>
      </div>
      <div id="collapse1" class="panel-collapse collapse">
        <div class="panel-body">Yes, if the order is placed before 10 AM then the minimum order value should be of  Rs. 400 and an extra of rs. 50 will be added to the final bill as delivery charges.

 but if the order is placed  after 10 AM then the minimum order limit is Rs. 700 and the delivery charges will be applied as per the company policy.

 </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse2">What is the delivery process of VGH ?</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">
        <div class="panel-body">Once your order is placed online it is forwarded to our warehouse where it is carefully picked, processed and packed. Your order once goes through a final quality check to check freshness, physical condition, etc. by a dedicated team of trained employees before sending it to your doorstep. Once the order leaves for delivery from the warehouse a notification will be sent on your registered email address and a message will be sent on your mobile number. In case of any delay in your delivery time, you will be notified through a mail and a message.

 </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse3">What if I'm not available to receive my order ?</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">
        <div class="panel-body">Once the order is dispatched for delivery from our warehouse an email  and a message will be sent on your registered email address and  mobile number. In case of non-availability at your residence, our Delivery Expert will try reaching you on the registered mobile number. The order will accordingly be rescheduled only after getting a confirmation from your end.</div>
      </div>
    </div>
    
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse4">Can I change my order once it is placed ?</a>
        </h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse">
        <div class="panel-body">Yes, you can change your order before it is dispatched, however you cannot change your order once it is dispatched from our warehouse.</div>
      </div>
    </div>
    
    
    
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse5">Can I place my order online and pick it from your warehouse ?</a>
        </h4>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
        <div class="panel-body">Currently we do not offer this facility.</div>
      </div>
    </div>
    
    
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse6">What if I didn't like the quality of the product delivered to me ?</a>
        </h4>
      </div>
      <div id="collapse6" class="panel-collapse collapse">
        <div class="panel-body">Our delivery boy will get the quality of all the products checked by you before you make the payment. If you do not like the quality of any product them you right away return it to the delivery boy without any hassle. </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse7">Can I cancel my order once it has been placed ?</a>
        </h4>
      </div>
      <div id="collapse7" class="panel-collapse collapse">
        <div class="panel-body">Yes you can cancel your order before it has been dispatched. If you do not want to accept the order due to any reason then you will have to pay Rs. 50 delivery charges after it reaches you.

 </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse8">What if the products ordered by me are out of stock ?</a>
        </h4>
      </div>
      <div id="collapse8" class="panel-collapse collapse">
        <div class="panel-body">If any product is visible to you on our website or app when you place your order then it is our responsibility to arrange and deliver it to you anyhow.</div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse9">Will I be compensated in case of order delay ?</a>
        </h4>
      </div>
      <div id="collapse9" class="panel-collapse collapse">
        <div class="panel-body">Yes, in case of delay in your order delivery by more than one hour then we are liable to pay 10% of your total  order value.


.</div>
      </div>
    </div>
    
    
    
    
  </div>  
</div>
</section>