
<section id="privacy_pg">
<div class="container">
<h2 class="text-center">
TERMS & Condition</h2>

<div class="privacy_div">
<p class="text-justify">Except as otherwise expressly stated with respect to our products, all contents of the site are offered on an "as is" basis without any warranty whatsoever either express or implied. VGH makes no representations, express or implied, including without limitation implied warranties of merchantability and fitness for a particular purpose. 
VGH does not guarantee the functions contained in the site will be uninterrupted or error-free, that this site or its server will be free of viruses or other harmful components, or defects will be corrected even if VGH is aware of them.</p></div>

<div class="privacy_div"><h5 class="text-left">Copyright and Trademark</h5>
<p class="text-justify">Unless otherwise noted, all materials on this site are protected as the copyrights, trade dress, trademarks and/ or other intellectual properties owned by VGH or by other parties that have licensed their material to VGH .
<br>
All rights not expressly granted are reserved.</p></div>

<div class="privacy_div"><h5 class="text-left">Personal Use</h5>
<p class="text-justify">Your use of the materials included on this site is for informational and shopping purposes only. You agree you will not distribute, publish, transmit, modify, display or create derivative works from or exploit the contents of this site in any way. You agree to indemnify, defend and hold harmless VGH for any and all unauthorized uses you may make of any material on the site. You acknowledge the unauthorized use of the contents could cause irreparable harm to VGH and that in the event of an unauthorized use, VGH shall be entitled to an injunction in addition to any other remedies available at law or in equity.</p></div>

<div class="privacy_div"><h5 class="text-left">Feedback and Submissions </h5>
<p class="text-justify">You agree you are and shall remain solely responsible for the contents of any submissions you make, and you will not submit material that is unlawful, defamatory, abusive or obscene. You agree that you will not submit anything to the site that will violate any right of any third party, including copyright, trademark, privacy or other personal or proprietary right(s).

While we appreciate your interest in VGH, we do not want and cannot accept any ideas you consider to be proprietary regarding designs, product technology or other suggestions you may have developed. Consequently, any material you submit to this site will be deemed a grant of a royalty free non-exclusive right and license to use, reproduce, modify, display, transmit, adapt, publish, translate, create derivative works from and distribute these materials throughout the universe in any medium and through any methods of distribution, transmission and display whether now known or hereafter devised. In addition, you warrant that all so-called "moral rights" have been waived.</p></div>



</div>
</section>