        <section id="cartPage" class="orderDetailPage">
            <div class="productD">
                <div class="container">
                    <h3>offers</h3>
                    <div class="row">
                        <div class="col-md-3">
                            <a href="#" class="offerTile">
                                <div class="imgContainer">
                                    <img src="<?php echo SITE_URL; ?>images/baby_pet_care.png" alt="" class="img-fluid">
                                </div>
                                <h6>10% off on all baby care Products</h6>
                                <p>Up to 10% off on Colgate and Palmolive range!</p>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="#" class="offerTile">
                                <div class="imgContainer">
                                    <img src="<?php echo SITE_URL; ?>images/baby_pet_care.png" alt="" class="img-fluid">
                                </div>
                                <h6>10% off on all baby care Products</h6>
                                <p>Up to 10% off on Colgate and Palmolive range!</p>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="#" class="offerTile">
                                <div class="imgContainer">
                                    <img src="<?php echo SITE_URL; ?>images/baby_pet_care.png" alt="" class="img-fluid">
                                </div>
                                <h6>10% off on all baby care Products</h6>
                                <p>Up to 10% off on Colgate and Palmolive range!</p>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="#" class="offerTile">
                                <div class="imgContainer">
                                    <img src="<?php echo SITE_URL; ?>images/baby_pet_care.png" alt="" class="img-fluid">
                                </div>
                                <h6>10% off on all baby care Products</h6>
                                <p>Up to 10% off on Colgate and Palmolive range!</p>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="topSavers">
                <div class="container-fluid">
                    <h4>Top Savers Today</h4>
                    <div class="owl-carousel owl-theme owl-loaded savers-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>Home/productDetail" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>Home/productDetail" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>Home/productDetail" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>