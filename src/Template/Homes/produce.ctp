<section class="pagebanner">
    <div class="page-produce-banner"> <h2 class="sub-headings">The Way We Produce</h2> </div>
</section>

<section id="produce-sec">
  <div class="consume-sec-bg">
    
     <div class="container-fluid">
        <div class="row">
           <div class="col-md-4 col-sm-6 col-xs-12 "> <div class="produce-block">
                <div class="consume-icon white"><img src="<?php echo SITE_URL ?>frontfile/images/p-1.png" alt="image"></div>
                <h3>All-In-One Platform</h3>
                <p>We provide a single-stop platform for our farmers, be it related to procurement or payment.</p>
            </div></div>
            <div class="col-md-4 col-sm-6 col-xs-12"><div class="produce-block">
                <div class="consume-icon white"><img src="<?php echo SITE_URL ?>frontfile/images/p-2.png" alt="image"></div>
                <h3>Produce Risk-Free</h3>
                <p>We guide our farmers to produce veggies and fruits with advanced agricultural practices to make the produce risk-free with improved productivity and less wastage.</p>
                
            </div></div>
            <div class="col-md-4 col-sm-6 col-xs-12 "><div class="produce-block">
                <div class="consume-icon white"><img src="<?php echo SITE_URL ?>frontfile/images/p-3.png" alt="image"></div>
                <h3> Better Earnings</h3>
                <p>We ensure our farmers enjoy increased earnings as we sell their produce directly to the end businesses over fair prices comparing to existing market.</p>
                
                
            </div></div>
            
            
        
        
        <div class="col-md-6  col-sm-6 col-xs-12 "><div class="produce-block">
                <div class="consume-icon white"><img src="<?php echo SITE_URL ?>frontfile/images/p-4.png" alt="image"></div>
                <h3>  No Hassle Over Transportation</h3>
                <p>Our farmers get support at every turn as they simply drop off their produce at our hub points and never worry about getting it further which leads to remove their logistic costs.</p>
                
                
                
            </div></div>
            
           <div class="col-md-6 col-sm-12 col-xs-12 "><div class="produce-block">
                <div class="consume-icon white"><img src="<?php echo SITE_URL ?>frontfile/images/p-5.png" alt="image"></div>
                <h3>Get Paid Fast</h3>
                <p>No more headaches of spending time in the market to sell their produce and get payment within 12 hours.</p>
                
                
                
            </div></div> 
            
            
            </div>
        
        
    </div> 
</div>




</section> 



</div><!--o-wrapper-->

<script>
$('.slid-carousal').owlCarousel({
	
    loop:true,
    autoplay:true,
	
	nav:false,
    animateOut: 'fadeOut',
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:false,
            
        }
    }
})


</script>
</body>
</html>
