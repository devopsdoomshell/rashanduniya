        <section id="myOrderPage">
            <div class="container">
                <div class="d-flex align-items-start myOrdersSec">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a href="#">
                            <img src="<?php echo SITE_URL; ?>images/logo_black.png" class="sideLogo" />
                        </a>
                        <a class="nav-link active" id="v-pills-home-tab" data-bs-toggle="pill" href="#v-pills-home"
                            role="tab" aria-controls="v-pills-home" aria-selected="true">My Orders</a>
                        <a class="nav-link" id="v-pills-profile-tab" data-bs-toggle="pill" href="#v-pills-profile"
                            role="tab" aria-controls="v-pills-profile" aria-selected="false">My Addresses</a>
                        <a class="nav-link" id="v-pills-profile-tab" data-bs-toggle="pill" href="#v-pills-wallet"
                            role="tab" aria-controls="v-pills-profile" aria-selected="false">My Wallet</a>
                        <a class="nav-link" id="v-pills-profile-tab" data-bs-toggle="pill" href="#v-pills-myprofile"
                            role="tab" aria-controls="v-pills-profile" aria-selected="false">My Profile</a>
                    </div>
                    <div class="tab-content flex-fill" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel"
                            aria-labelledby="v-pills-home-tab">
                            <h4>My Orders</h4>
                            <a href="<?php echo SITE_URL; ?>Home/orderDetail" class="cartList">
                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt="" class="img-fluid" />
                                <div class="dtl flex-fill">
                                    <h5>Fortune Health Refined Rice Bran Oil (Jar) <span>5 Ltr</span></h5>
                                    <p class="cartPrice">₹ 800.00</p>
                                    <div class="d-flex align-items-center justify-content-between">
                                        <p>Order ID: 321</p>
                                    </div>
                                    <ul class="d-flex justify-content-between">
                                        <li>Delivery Date: <b>5 Fab 2021</b></li>
                                        <li>Status: <b class="text-success">Delivered</b></li>
                                    </ul>
                                </div>
                            </a>

                            <a href="<?php echo SITE_URL; ?>Home/orderDetail" class="cartList">
                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt="" class="img-fluid" />
                                <div class="dtl flex-fill">
                                    <h5>Fortune Health Refined Rice Bran Oil (Jar) <span>5 Ltr</span></h5>
                                    <p class="cartPrice">₹ 800.00</p>
                                    <div class="d-flex align-items-center justify-content-between">
                                        <p>Order ID: 321</p>
                                    </div>
                                    <ul class="d-flex justify-content-between">
                                        <li>Delivery Date: <b>5 Fab 2021</b></li>
                                        <li>Status: <b class="text-success">Delivered</b></li>
                                    </ul>
                                </div>
                            </a>

                            <a href="<?php echo SITE_URL; ?>Home/orderDetail" class="cartList">
                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt="" class="img-fluid" />
                                <div class="dtl flex-fill">
                                    <h5>Fortune Health Refined Rice Bran Oil (Jar) <span>5 Ltr</span></h5>
                                    <p class="cartPrice">₹ 800.00</p>
                                    <!-- <div class="d-flex align-items-center justify-content-between">
                                        <p>5 Ltr</p>
                                    </div> -->
                                    <ul class="d-flex justify-content-between">
                                        <li>Delivery Date: <b>5 Fab 2021</b></li>
                                        <li>Status: <b class="text-success">Delivered</b></li>
                                    </ul>
                                </div>
                            </a>


                        </div>
                        <div class="tab-pane fade" id="v-pills-profile" role="tabpanel"
                            aria-labelledby="v-pills-profile-tab">
                            <h4>My Orders</h4>
                            <a href="#" class="addNewAddress"><i class="fas fa-plus me-1"></i> Add New Address</a>
                            <div class="addressForm">
                                <form>
                                    <div class="mb-3">
                                        <input type="test" class="form-control" placeholder="Name">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control" placeholder="Flat/House/Office No.">
                                    </div>
                                    <div class="mb-3">
                                        <input type="text" class="form-control"
                                            placeholder="Street/Society/Office Name">
                                    </div>
                                    <div class="mb-3 form-check">
                                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                        <label class="form-check-label" for="exampleCheck1">Set as default
                                            Address</label>
                                    </div>
                                    <div class="mb-3 nicknames">
                                        <label class=" form-label ">Nickname of your Address</label>
                                        <div class="" role="group" aria-label="Basic radio toggle button group">
                                            <input type="radio" class="btn-check" name="btnradio" id="btnradio1"
                                                autocomplete="off" checked>
                                            <label class="btn btn-outline-secondary rounded-0"
                                                for="btnradio1">Office</label>

                                            <input type="radio" class="btn-check" name="btnradio" id="btnradio2"
                                                autocomplete="off">
                                            <label class="btn btn-outline-secondary rounded-0"
                                                for="btnradio2">Home</label>

                                            <input type="radio" class="btn-check" name="btnradio" id="btnradio3"
                                                autocomplete="off">
                                            <label class="btn btn-outline-secondary rounded-0"
                                                for="btnradio3">Others</label>
                                        </div>
                                    </div>

                                    <button type="submit " class="btn rounded-0 text-white"
                                        style="background: #6ba20d;">Save Address</button>
                                </form>
                            </div>
                            <div class="row mt-4">
                                <div class="col-md-6 ">
                                    <a href="# " class="addressList ">
                                        <h6><i class="fas fa-briefcase "></i> Office</h6>
                                        <p>DAAC - a3 mall road, Doomshell, Vidyadhar Nagar, Jaipur, Rajasthan</p>
                                    </a>
                                </div>

                                <div class="col-md-6 ">
                                    <a href="# " class="addressList ">
                                        <h6><i class="fas fa-home "></i> Home</h6>
                                        <p>DAAC - a3 mall road, Doomshell, Vidyadhar Nagar, Jaipur, Rajasthan</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-wallet" role="tabpanel"
                            aria-labelledby="v-pills-profile-tab">
                            <div class="walletBalance">
                                <i class="fas fa-wallet"></i>
                                <p>Wallet Balance</p>
                                <h5><b>₹ 1000</b></h5>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="v-pills-myprofile" role="tabpanel"
                            aria-labelledby="v-pills-profile-tab">
                            <h6>My Profile</h6>
                            <div class="proContainer">
                                <div class="profilePage">
                                    <ul>
                                        <li><i class="fas fa-mobile-alt"></i> 9694401207</li>
                                        <li><i class="fas fa-user"></i> Ritesh Chhipa</li>
                                        <li><i class="fas fa-map-marker-alt"></i> DAAC - A3 mall road, Doomshell,
                                            Vidyadhar Nagar, Jaipur, Rajasthan</li>
                                    </ul>
                                    <a href="#" class="proButton pedit">Edit Profile</a>
                                </div>
                                <div class="editProfilePage">
                                    <form>
                                        <input type="text" placeholder="" value="9694401207" class="form-control" />
                                        <input type="text" placeholder="" value="Ritesh Chhipa" class="form-control" />
                                        <input type="text" placeholder=""
                                            value="DAAC - A3 mall road, Doomshell, Vidyadhar Nagar, Jaipur, Rajasthan"
                                            class="form-control" />
                                    </form>
                                    <a href="#" class="proButton pupdate">Update Profile</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="topSavers ">
            <div class="container-fluid ">
                <h4>Top Savers Today</h4>
                <div class="owl-carousel owl-theme owl-loaded savers-carousel ">
                    <div class="owl-stage-outer ">
                        <div class="owl-stage ">
                            <div class="owl-item ">
                                <div class="productTile ">
                                    <a href="<?php echo SITE_URL; ?>Home/productDetail" class=" ">
                                        <div class="imgContainer ">
                                            <p class="productOffer ">-10%</p>
                                            <img src="<?php echo SITE_URL; ?>images/product_1.png " alt=" "
                                                class="img-fluid " />
                                        </div>
                                        <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                        <ul class="tilePrice ">
                                            <li class="newPrice ">₹ 770</li>
                                            <li class="oldPrice ">₹ 800</li>
                                        </ul>
                                    </a>
                                    <ul class="tileButton ">
                                        <li class="buyButton "><a href="javascript:void(0) " data-bs-toggle="modal "
                                                data-bs-target="#quantityModalPicker ">5 Ltr. <i
                                                    class="fas fa-sort-down "></i></a></li>
                                        <li class="cartButton "><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                    class="fas fa-shopping-cart "></i> Add to Cart</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="owl-item ">
                                <div class="productTile ">
                                    <a href="<?php echo SITE_URL; ?>Home/productDetail" class=" ">
                                        <div class="imgContainer ">
                                            <p class="productOffer ">-10%</p>
                                            <img src="<?php echo SITE_URL; ?>images/product_1.png " alt=" "
                                                class="img-fluid " />
                                        </div>
                                        <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                        <ul class="tilePrice ">
                                            <li class="newPrice ">₹770</li>
                                            <li class="oldPrice ">₹800</li>
                                        </ul>
                                    </a>
                                    <ul class="tileButton ">
                                        <li class="buyButton "><a href="javascript:void(0) " data-bs-toggle="modal "
                                                data-bs-target="#quantityModalPicker ">5 Ltr. <i
                                                    class="fas fa-sort-down "></i></a></li>
                                        <li class="cartButton "><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                    class="fas fa-shopping-cart "></i> Add to Cart</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="owl-item ">
                                <div class="productTile ">
                                    <a href="<?php echo SITE_URL; ?>Home/productDetail" class=" ">
                                        <div class="imgContainer ">
                                            <p class="productOffer ">-10%</p>
                                            <img src="<?php echo SITE_URL; ?>images/product_1.png " alt=" "
                                                class="img-fluid " />
                                        </div>
                                        <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                        <ul class="tilePrice ">
                                            <li class="newPrice ">₹770</li>
                                            <li class="oldPrice ">₹800</li>
                                        </ul>
                                    </a>
                                    <ul class="tileButton ">
                                        <li class="buyButton "><a href="javascript:void(0) " data-bs-toggle="modal "
                                                data-bs-target="#quantityModalPicker ">5 Ltr. <i
                                                    class="fas fa-sort-down "></i></a></li>
                                        <li class="cartButton "><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                    class="fas fa-shopping-cart "></i> Add to Cart</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>