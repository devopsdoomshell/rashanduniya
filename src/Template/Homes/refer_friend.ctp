        <section id="referPage">
            <div class="productD">
                <div class="container">
                    <img src="<?php echo SITE_URL; ?>images/refer.jpg" alt="" class="img-fluid" />
                    <h5>Start Buying Grocery Online</h5>
                    <p>Refer Rashan Duniya to your friends and family</p>
                    <p>You will get ₹ 50 when your friend first order completed on Rashan Duniya</p>
                    <p>Referral Code: <b class="text-success">k5JzBDCWrw</b></p>
                    <p>Referral Amount: <b class="text-success">₹ 50</b></p>
                    <a href="#">Share Rashan Duniya with Friends</a>
                </div>
            </div>

            <div class="topSavers">
                <div class="container-fluid">
                    <h4>Top Savers Today</h4>
                    <div class="owl-carousel owl-theme owl-loaded savers-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>Home/productDetail" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>Home/productDetail" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>Home/productDetail" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>