        <section id="cartPage">
            <div class="productD">
                <div class="container">
                    <h3>Cart Items (3)</h3>
                    <div class="row">
                        <div class="col-md-8 col-sm-7">
                            <div class="cartList">
                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt="" class="img-fluid" />
                                <div class="dtl flex-fill">`
                                    <h5>Fortune Health Refined Rice Bran Oil (Jar)</h5>
                                    <p class="cartPrice">₹ 800.00</p>
                                    <div class="d-flex align-items-center justify-content-between">
                                        <p>5 Ltr</p>
                                        <div class="wrap">
                                            <button type="button" id="sub" class="sub">-</button>
                                            <input class="count" type="text" id="1" value="1" min="1" max="100" />
                                            <button type="button" id="add" class="add">+</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cartList">
                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt="" class="img-fluid" />
                                <div class="dtl flex-fill">
                                    <h5>Fortune Health Refined Rice Bran Oil (Jar)</h5>
                                    <p class="cartPrice">₹ 800.00</p>
                                    <div class="d-flex align-items-center justify-content-between">
                                        <p>5 Ltr</p>
                                        <div class="wrap">
                                            <button type="button" id="sub" class="sub">-</button>
                                            <input class="count" type="text" id="1" value="1" min="1" max="100" />
                                            <button type="button" id="add" class="add">+</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="cartList">
                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt="" class="img-fluid" />
                                <div class="dtl flex-fill">
                                    <h5>Fortune Health Refined Rice Bran Oil (Jar)</h5>
                                    <p class="cartPrice">₹ 800.00</p>
                                    <div class="d-flex align-items-center justify-content-between">
                                        <p>5 Ltr</p>
                                        <div class="wrap">
                                            <button type="button" id="sub" class="sub">-</button>
                                            <input class="count" type="text" id="1" value="1" min="1" max="100" />
                                            <button type="button" id="add" class="add">+</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-5">
                            <div class="subTOtal">
                                <h5>Order Summary</h5>
                                <ul>
                                    <li><span>M.R.P</span><span>₹ 2400.00</span></li>
                                    <li><span>Product Discount</span><span>₹ 00.00</span></li>
                                    <li><span>Delivery Charges</span><span>₹ 50.00</span></li>
                                    <li class="border-top mt-2">
                                        <strong>Sub Total</strong>
                                        <strong>₹ 2450</strong>
                                    </li>
                                </ul>
                                <a href="<?php echo SITE_URL; ?>Home/checkout">Checkout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="topSavers">
                <div class="container-fluid">
                    <h4>Top Savers Today</h4>
                    <div class="owl-carousel owl-theme owl-loaded savers-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>Home/productDetail" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>Home/productDetail" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>Home/productDetail" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>