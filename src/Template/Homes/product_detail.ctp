        <section id="productDetailPage">
            <div class="productD">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-sm-6">
                            <div id="js-gallery" class="gallery">
                                <!--Gallery Hero-->
                                <div class="gallery__hero">
                                    <img src="<?php echo SITE_URL; ?>images/product_1.png">
                                </div>
                                <!--Gallery Hero-->

                                <!--Gallery Thumbs-->
                                <div class="gallery__thumbs">
                                    <a href="<?php echo SITE_URL; ?>images/product_1.png" data-gallery="thumb"
                                        class="is-active">
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png">
                                    </a>
                                    <a href="<?php echo SITE_URL; ?>images/product_2.png" data-gallery="thumb">
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png">
                                    </a>
                                    <a href="<?php echo SITE_URL; ?>images/product_3.png" data-gallery="thumb">
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png">
                                    </a>
                                    <a href="<?php echo SITE_URL; ?>images/product_4.png" data-gallery="thumb">
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png">
                                    </a>
                                    <a href="<?php echo SITE_URL; ?>images/product_5.png" data-gallery="thumb">
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png">
                                    </a>
                                </div>
                                <!--Gallery Thumbs-->

                            </div>
                            <!--.gallery-->
                            <!-- Gallery -->
                        </div>
                        <div class="col-md-7 col-sm-6">
                            <div class="productDetail">
                                <h3>Fortune Health Refined Rice Bran Oil (Jar)</h3>
                                <ul class="productPricing">
                                    <li class="pOld">Product MRP: <del>₹800.00</del></li>
                                    <li class="pNew">Selling Price:<span> ₹675.00</span></li>
                                </ul>
                                <p class="mb-2">Available in:</p>
                                <!-- <ul class="availability">
                                    <li><a href="#" class="active">1 Ltr.</a></li>
                                    <li><a href="#">5 Ltr.</a></li>
                                    <li><a href="#">10 Ltr.</a></li>
                                </ul> -->
                                <div class="btn-group" role="group" aria-label="Basic radio toggle button group">
                                    <input type="radio" class="btn-check" name="btnradio" id="btnradio1"
                                        autocomplete="off" checked>
                                    <label class="btn btn-outline-secondary rounded-0" for="btnradio1">1 Ltr.</label>

                                    <input type="radio" class="btn-check" name="btnradio" id="btnradio2"
                                        autocomplete="off">
                                    <label class="btn btn-outline-secondary rounded-0" for="btnradio2">5 Ltr.</label>

                                    <input type="radio" class="btn-check" name="btnradio" id="btnradio3"
                                        autocomplete="off">
                                    <label class="btn btn-outline-secondary rounded-0" for="btnradio3">10 Ltr.</label>
                                </div>
                                <a href="<?php echo SITE_URL; ?>Home/cart" class="cartButton">Add to Cart</a>

                                <div class="sapretorHorizontal"></div>

                                <h5>Product Description</h5>
                                <p>Fortune Sunlite Refined Sunflower Oil is a healthy and nutritious cooking aid that
                                    keeps the natural flavour and nutrients of the food intact. The lightweight and
                                    healthy formula is ideal for deep frying and doesn’t affect
                                    the heart adversely. Enriched with vitamins A and D to keep your body healthy and
                                    fit. Has a lightweight formulation that is odourless, thus keeping the natural
                                    flavours intact. The nutritious formula helps keep the
                                    heart and bones healthy. Also has minerals that are good for the eyes.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="topSavers">
                <div class="container-fluid">
                    <h4>Top Savers Today</h4>
                    <div class="owl-carousel owl-theme owl-loaded savers-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>Home/productDetail" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>Home/product_detail" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="owl-item">
                                    <div class="productTile">
                                        <a href="<?php echo SITE_URL; ?>Home/productDetail" class="">
                                            <div class="imgContainer">
                                                <p class="productOffer">-10%</p>
                                                <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                                    class="img-fluid" />
                                            </div>
                                            <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                            <ul class="tilePrice">
                                                <li class="newPrice">₹770</li>
                                                <li class="oldPrice">₹800</li>
                                            </ul>
                                        </a>
                                        <ul class="tileButton">
                                            <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                                    data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                        class="fas fa-sort-down"></i></a></li>
                                            <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                        class="fas fa-shopping-cart"></i> Add to Cart</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>