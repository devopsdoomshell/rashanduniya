<section class="pagebanner">
    <div class="page-consume-banner">  <h2 class="sub-headings">The Way You Consume</h2> </div>
</section>

<section id="consume-sec">
  <div class="consume-sec-bg">
    
     <div class="container-fluid">
        <div class="row  m-top-30">
           <div class="col-md-4 col-sm-6 col-xs-12 "> <div class="consume-block">
                <div class="consume-icon green"><img src="<?php echo SITE_URL ?>frontfile/images/fresh_farm.png" alt="image"></div>
                <h3> Farm Fresh For You!</h3>
                <p>Our interface is also beneficial to end consumers as we bring your fruits and veggies directly from the farms in the quickest possible time keeping them truly farm fresh.</p>
            </div></div>
            <div class="col-md-4 col-sm-6 col-xs-12"><div class="consume-block">
                <div class="consume-icon red"><img src="<?php echo SITE_URL ?>frontfile/images/bestquality.png" alt="image"></div>
                <h3> Best Quality</h3>
                <p>We ensure that nothing less than the best quality fruits  and veggies reach you. We send our fruits and veggies ahead only after careful inspection and a strict quality check.</p>
                
            </div></div>
            <?php/* ?><div class="col-md-4 col-sm-6 col-xs-12"><div class="consume-block">
                <div class="consume-icon blue"><img src="<?php echo SITE_URL ?>frontfile/images/trackorder.png" alt="image"></div>
                <h3> Easy To Track</h3>
                <p>Our customers can track down their orders right to their delivery from the time they are picked from us.</p>
                
                
            </div></div> <?php */ ?>
            <div class="col-md-4 col-sm-6 col-xs-12"><div class="consume-block">
                <div class="consume-icon orange"><img src="<?php echo SITE_URL ?>frontfile/images/quality.png" alt="image"></div>
                <h3> Food Safety</h3>
                <p>We truly believe that food safety is the most important thing and we ensure that the fruits and veggies you consume are completely hygienic and unadulterated by chemicals of any kind.</p>
                
                
                
            </div></div>
            
            
            <div class="col-md-6 col-sm-6 col-xs-12 "><div class="consume-block">
                <div class="consume-icon orange2"><img src="<?php echo SITE_URL ?>frontfile/images/no_inflation.png" alt="image"></div>
                <h3> No Inflation Rates</h3>
                <p>We play an important role in ensuring that the final consumer gets only the fair prices without any kind of inflation. We ensure the proper supply of fruits and veggies so that there is no instability in their prices due to over or under production.</p>
                
                
                
            </div></div>
            
            
            <div class="col-md-6 col-sm-12 col-xs-12 "><div class="consume-block">
                <div class="consume-icon green2"><img src="<?php echo SITE_URL ?>frontfile/images/eat_good.png" alt="image"></div>
                <h3>Eat Good! Feel Good!</h3>
                <p>With advanced technology at every step, we ensure that your fruits and vegetables stay as natural as nature intended them to be and make them ready for your final consumption by keeping them completely nutritional and hygienic.</p>
                
                
                
            </div></div>
            
            
            
        </div>
        
        
        
    </div> 
</div>




</section> 



</div><!--o-wrapper-->

<script>
$('.slid-carousal').owlCarousel({
	
    loop:true,
    autoplay:true,
	
	nav:false,
    animateOut: 'fadeOut',
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:false,
            
        }
    }
})


</script>
</body>
</html>
