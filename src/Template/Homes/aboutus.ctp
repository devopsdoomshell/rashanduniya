<style type="text/css">
	.about-block h3 {
    text-transform: capitalize;
}
</style>

<section id="page-about">
<div class="page-about-banner"><h2 class="sub-heading">About Us</h2></div>
<div class="page-about-bg">
<div class="container">
<div class="about-block"><div class="row">
<div class="col-sm-4"></div>
<div class="col-sm-8 col-xs-12">
  <h3 class="text-center">A House of fresh <span style="display: block">fruits and vegetables</span></h3>  
  <div class="align-center border-choose">
                            <div class="images">
                                <img src="<?php echo SITE_URL ?>frontfile/images/leaf.png" alt="image">
                            </div>
                         </div>
</div>
</div></div>


<div class="row"> 
 <div class="col-sm-4"> 
 	<ul class="list-unstyled">
    	 <li style="margin-bottom: 16px;"><img src="<?php echo SITE_URL ?>frontfile/images/vegetabl.png" alt="image"></li>
    	<li ><img src="<?php echo SITE_URL ?>frontfile/images/Farm.png" alt="image"></li>
       
    </ul>
 	</div>
  <div class="col-sm-8 about-block">

       <p class="m-top-40">Veggie Green House, is one such initiative that has evolved into a meaningful B2B platform for providing a solution to one of the biggest inefficiencies in the existing agricultural ecosystem of India in distribution and selling of farm fresh produce.</p> 
       <p>By building a digital agri-supply chain for fruits and vegetables that is efficient and has zero-wastage, we are here to increase the incomes of both farmers and retailers.
       	It is achieved through demand based production where farmers produce to a future demand from buyers such as supermarkets, restaurants, retailers and other large wholesalers.
        This enables better price realizations for the farmer while at the same time more cash flows to the retailer.  A win-win situation for everyone.



   </p>
              <p>We integrate the food production and consumption with just in time logistics and real time transactions which optimizes the huge wastage of food in the supply chain from farm to the shop.
</p>


	   
	 <!--   <div class="row m-top-40">
	   <div class="col-sm-3">
	   <div class="about-cnt">
	   <img src="<?php echo SITE_URL ?>frontfile/images/truck1.png" alt="image">
	   <h5>Doorstep Delivery</h5>
	   </div> 
	   </div>
	    <div class="col-sm-3">
		<div class="about-cnt">
	   <img src="<?php echo SITE_URL ?>frontfile/images/freshfood1.png" alt="image">
	   <h5>Best Quality</h5>
	   </div>
		
		</div>
		 <div class="col-sm-3">
		 <div class="about-cnt">
	   <img src="<?php echo SITE_URL ?>frontfile/images/pricing1.png" alt="image">
	   <h5>Fair Pricing</h5>
	   </div>
		 
		 </div>
		  <div class="col-sm-3">
		  <div class="about-cnt">
	   <img src="<?php echo SITE_URL ?>frontfile/images/quality1.png" alt="image">
	   <h5>Food Safety</h5>
	   </div>
		  
		  
		  
		  </div>
	   
	   </div>               
   -->
  
  </div>
 <div class="col-sm-12">
 



<p>We also simplify the process of pricing for the farmers as well as the buyers by helping the farmers to get fair and higher prices for their farm produce along with procuring quality and natural produce at the right and lowest prices for end buyers (Retailers, hotels, restaurants, hospitals, eateries) etc.
</p>

<p>We envision to increase the knowledge of the farmers about various modern farming techniques for better land utilization and revenues to improve their potential to provide 60–100% more quality food by simply eliminating the losses, while simultaneously freeing up land, energy and water resources for other uses.

Based in Jaipur, our just-in-time supply chain model has proved to be beneficial for all as it has eradicated any extra logistic expense for both the farmers and the retailers by striking a delicate balance between supply and demand.</p>
 
 
 </div>
 </div>




</div>

</div>

</section>



</div><!--o-wrapper-->

<script>
$('.slid-carousal').owlCarousel({
	
    loop:true,
    autoplay:true,
	
	nav:false,
    animateOut: 'fadeOut',
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:1,
            nav:false,
            
        }
    }
})


</script>
</body>
</html>
