        <section id="CheckoutPage">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-7">
                        <div class="accordion" id="accordionExample">
                            <form>
                                <div class="accordion-item">
                                    <div class="itemHeading">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                            aria-expanded="true" aria-controls="collapseOne">
                                            Phone Number Verification
                                        </button>
                                    </div>
                                    <div id="collapseOne" class="accordion-collapse collapse show"
                                        aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <p>We need your phone number so that we can update you about your order.</p>
                                            <p><small>Your 10 digit mobile number</small></p>
                                            <div class="d-flex">
                                                <input type="tel" maxlength="10" class="form-control"
                                                    data-test-id="phone-no-text-box" value="" placeholder="Mobile No.">
                                                <button class="checkOutNextButton" type="button"
                                                    data-bs-toggle="collapse" data-bs-target="#collapseTwo"
                                                    aria-expanded="true" aria-controls="collapseOne">
                                                    Next
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <div class="itemHeading">
                                        <button class="accordion-button collapsed" type="button"
                                            data-bs-toggle="collapse" aria-expanded="false" aria-controls="collapseTwo">
                                            Delivery Address
                                        </button>
                                        <button class="edit" type="button" data-bs-toggle="collapse"
                                            aria-expanded="true" data-bs-target="#collapseTwo"
                                            aria-controls="collapseOne">
                                            <i class="fas fa-pen"></i>
                                        </button>
                                    </div>

                                    <div id="collapseTwo" class="accordion-collapse collapse"
                                        aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <div class="addressForm">
                                                <div class="mb-3">
                                                    <input type="test" class="form-control" placeholder="Name">
                                                </div>
                                                <div class="mb-3">
                                                    <input type="text" class="form-control"
                                                        placeholder="Flat/House/Office No.">
                                                </div>
                                                <div class="mb-3">
                                                    <input type="text" class="form-control"
                                                        placeholder="Street/Society/Office Name">
                                                </div>
                                                <div class="mb-3 form-check">
                                                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                                                    <label class="form-check-label" for="exampleCheck1">Set as default
                                                        Address</label>
                                                </div>
                                                <div class="mb-3 nicknames">
                                                    <label class=" form-label ">Nickname of your Address</label>
                                                    <div class="" role="group"
                                                        aria-label="Basic radio toggle button group">
                                                        <input type="radio" class="btn-check" name="btnradio"
                                                            id="btnradio1" autocomplete="off" checked>
                                                        <label class="btn btn-outline-secondary rounded-0"
                                                            for="btnradio1">Office</label>

                                                        <input type="radio" class="btn-check" name="btnradio"
                                                            id="btnradio2" autocomplete="off">
                                                        <label class="btn btn-outline-secondary rounded-0"
                                                            for="btnradio2">Home</label>

                                                        <input type="radio" class="btn-check" name="btnradio"
                                                            id="btnradio3" autocomplete="off">
                                                        <label class="btn btn-outline-secondary rounded-0"
                                                            for="btnradio3">Others</label>
                                                    </div>
                                                </div>

                                                <button class="checkOutNextButton" type="button"
                                                    data-bs-toggle="collapse" data-bs-target="#collapseThree"
                                                    aria-expanded="false" aria-controls="collapseTwo">
                                                    Next
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <div class="itemHeading">
                                        <button class="accordion-button collapsed" type="button"
                                            data-bs-toggle="collapse" aria-expanded="false"
                                            aria-controls="collapseThree">
                                            Choose delivery slot for the address
                                        </button>
                                        <button class="edit" type="button" data-bs-toggle="collapse"
                                            aria-expanded="true" data-bs-target="#collapseThree"
                                            aria-controls="collapseOne">
                                            <i class="fas fa-pen"></i>
                                        </button>
                                    </div>

                                    <div id="collapseThree" class="accordion-collapse collapse"
                                        aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <a class="nav-link active" id="pills-home-tab" data-bs-toggle="pill"
                                                        href="#dayOne" role="tab" aria-controls="pills-home"
                                                        aria-selected="true">Mon</a>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <a class="nav-link" id="pills-profile-tab" data-bs-toggle="pill"
                                                        href="#dayTwo" role="tab" aria-controls="pills-profile"
                                                        aria-selected="false">Tue</a>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <a class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                                                        href="#dayThree" role="tab" aria-controls="pills-contact"
                                                        aria-selected="false">Wed</a>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <a class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                                                        href="#dayFoue" role="tab" aria-controls="pills-contact"
                                                        aria-selected="false">Thu</a>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <a class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                                                        href="#dayFive" role="tab" aria-controls="pills-contact"
                                                        aria-selected="false">Fri</a>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <a class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                                                        href="#daySix" role="tab" aria-controls="pills-contact"
                                                        aria-selected="false">Sat</a>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <a class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                                                        href="#daySeven" role="tab" aria-controls="pills-contact"
                                                        aria-selected="false">Sun</a>
                                                </li>
                                            </ul>
                                            <div class="tab-content" id="pills-tabContent">
                                                <div class="tab-pane fade show active" id="dayOne" role="tabpanel"
                                                    aria-labelledby="pills-home-tab">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault1">
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            10:00 AM TO 12:00 PM
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault2" checked>
                                                        <label class="form-check-label" for="flexRadioDefault2">
                                                            01:00 PM TO 05:00 PM
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault3" checked>
                                                        <label class="form-check-label" for="flexRadioDefault3">
                                                            06:00 PM TO 09:00 PM
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="dayTwo" role="tabpanel"
                                                    aria-labelledby="pills-profile-tab">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault1">
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            10:00 AM TO 12:00 PM
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault2" checked>
                                                        <label class="form-check-label" for="flexRadioDefault2">
                                                            01:00 PM TO 05:00 PM
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault3" checked>
                                                        <label class="form-check-label" for="flexRadioDefault3">
                                                            06:00 PM TO 09:00 PM
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="dayThree" role="tabpanel"
                                                    aria-labelledby="pills-contact-tab">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault1">
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            10:00 AM TO 12:00 PM
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault2" checked>
                                                        <label class="form-check-label" for="flexRadioDefault2">
                                                            01:00 PM TO 05:00 PM
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault3" checked>
                                                        <label class="form-check-label" for="flexRadioDefault3">
                                                            06:00 PM TO 09:00 PM
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="dayFoue" role="tabpanel"
                                                    aria-labelledby="pills-contact-tab">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault1">
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            10:00 AM TO 12:00 PM
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault2" checked>
                                                        <label class="form-check-label" for="flexRadioDefault2">
                                                            01:00 PM TO 05:00 PM
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault3" checked>
                                                        <label class="form-check-label" for="flexRadioDefault3">
                                                            06:00 PM TO 09:00 PM
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="dayFive" role="tabpanel"
                                                    aria-labelledby="pills-contact-tab">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault1">
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            10:00 AM TO 12:00 PM
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault2" checked>
                                                        <label class="form-check-label" for="flexRadioDefault2">
                                                            01:00 PM TO 05:00 PM
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault3" checked>
                                                        <label class="form-check-label" for="flexRadioDefault3">
                                                            06:00 PM TO 09:00 PM
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="daySix" role="tabpanel"
                                                    aria-labelledby="pills-contact-tab">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault1">
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            10:00 AM TO 12:00 PM
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault2" checked>
                                                        <label class="form-check-label" for="flexRadioDefault2">
                                                            01:00 PM TO 05:00 PM
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault3" checked>
                                                        <label class="form-check-label" for="flexRadioDefault3">
                                                            06:00 PM TO 09:00 PM
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="daySeven" role="tabpanel"
                                                    aria-labelledby="pills-contact-tab">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault1">
                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                            10:00 AM TO 12:00 PM
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault2" checked>
                                                        <label class="form-check-label" for="flexRadioDefault2">
                                                            01:00 PM TO 05:00 PM
                                                        </label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio"
                                                            name="flexRadioDefault" id="flexRadioDefault3" checked>
                                                        <label class="form-check-label" for="flexRadioDefault3">
                                                            06:00 PM TO 09:00 PM
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="checkOutNextButton" type="button" data-bs-toggle="collapse"
                                                data-bs-target="#collapseFour" aria-expanded="false"
                                                aria-controls="collapseFour">
                                                Process to Payment
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        aria-expanded="false" aria-controls="collapseFour">
                                        Choose delivery slot for the address
                                    </button>
                                    <div id="collapseFour" class="accordion-collapse collapse"
                                        aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                        <div class="accordion-body">
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="flexRadioDefault"
                                                    id="flexRadioDefault1">
                                                <label class="form-check-label" for="flexRadioDefault1">
                                                    Cash on Delivery
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="flexRadioDefault"
                                                    id="flexRadioDefault2" checked>
                                                <label class="form-check-label" for="flexRadioDefault2">
                                                    Online Payment
                                                </label>
                                            </div>
                                            <input class="checkOutNextButton" type="submit" value="Place Order" />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-5">
                        <div class="subTOtal">
                            <h5>Order Summary</h5>
                            <ul>
                                <li><span>M.R.P</span><span>₹ 2400.00</span></li>
                                <li><span>Product Discount</span><span>₹ 00.00</span></li>
                                <li><span>Delivery Charges</span><span>₹ 50.00</span></li>
                                <li class="border-top mt-2">
                                    <strong>Sub Total</strong>
                                    <strong>₹ 2450</strong>
                                </li>
                            </ul>
                            <!-- <a href="<?php echo SITE_URL; ?>/checkout.html">Checkout</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="topSavers">
            <div class="container-fluid">
                <h4>Top Savers Today</h4>
                <div class="owl-carousel owl-theme owl-loaded savers-carousel">
                    <div class="owl-stage-outer">
                        <div class="owl-stage">
                            <div class="owl-item">
                                <div class="productTile">
                                    <a href="<?php echo SITE_URL; ?>Home//productDetail" class="">
                                        <div class="imgContainer">
                                            <p class="productOffer">-10%</p>
                                            <img src="<?php echo SITE_URL; ?>/images/product_1.png" alt=""
                                                class="img-fluid" />
                                        </div>
                                        <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                        <ul class="tilePrice">
                                            <li class="newPrice">₹770</li>
                                            <li class="oldPrice">₹800</li>
                                        </ul>
                                    </a>
                                    <ul class="tileButton">
                                        <li class="buyButton">
                                            <a href="javascript:void(0)" data-bs-toggle="modal"
                                                data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                    class="fas fa-sort-down"></i></a>
                                        </li>
                                        <li class="cartButton">
                                            <a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                    class="fas fa-shopping-cart"></i> Add to Cart</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="owl-item">
                                <div class="productTile">
                                    <a href="<?php echo SITE_URL; ?>Home/productDetail" class="">
                                        <div class="imgContainer">
                                            <p class="productOffer">-10%</p>
                                            <img src="<?php echo SITE_URL; ?>/images/product_1.png" alt=""
                                                class="img-fluid" />
                                        </div>
                                        <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                        <ul class="tilePrice">
                                            <li class="newPrice">₹770</li>
                                            <li class="oldPrice">₹800</li>
                                        </ul>
                                    </a>
                                    <ul class="tileButton">
                                        <li class="buyButton">
                                            <a href="javascript:void(0)" data-bs-toggle="modal"
                                                data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                    class="fas fa-sort-down"></i></a>
                                        </li>
                                        <li class="cartButton">
                                            <a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                    class="fas fa-shopping-cart"></i> Add to Cart</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="owl-item">
                                <div class="productTile">
                                    <a href="<?php echo SITE_URL; ?>Home/productDetail" class="">
                                        <div class="imgContainer">
                                            <p class="productOffer">-10%</p>
                                            <img src="<?php echo SITE_URL; ?>/images/product_1.png" alt=""
                                                class="img-fluid" />
                                        </div>
                                        <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                        <ul class="tilePrice">
                                            <li class="newPrice">₹770</li>
                                            <li class="oldPrice">₹800</li>
                                        </ul>
                                    </a>
                                    <ul class="tileButton">
                                        <li class="buyButton">
                                            <a href="javascript:void(0)" data-bs-toggle="modal"
                                                data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                    class="fas fa-sort-down"></i></a>
                                        </li>
                                        <li class="cartButton">
                                            <a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                    class="fas fa-shopping-cart"></i> Add to Cart</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>