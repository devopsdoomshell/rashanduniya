
<section id="privacy_pg">
<div class="container">
<h2 class="text-center">Privacy Policy</h2>

<div class="privacy_div"><h5 class="text-left"> Personal Information</h5>

<p class="text-justify">Veggie Green House Pvt Ltd ("VGH") is the licensed owner of the brand Veggie green house and the website <a href="www.veggiegreenhouse.com ">www.veggiegreenhouse.com</a>("The Site") We at VGH respect your privacy. This Privacy Policy provides the manner your data is collected and used by VGH on the Site in a brief manner. As a visitor to the Site you are advised to please read the Privacy Policy carefully. By accessing the services provided by the Site you agree to the collection and use of your data by VGH in the manner provided in this Privacy Policy.</p></div>


<div class="privacy_div"><h5 class="text-left">What information is, or may be, collected form you?</h5>
<p class="text-justify">As part of the registration process on the Site, VGH may collect the following personally identifiable information about you: Name including first and last name, alternate email address, mobile phone number and contact details, Postal code, Demographic profile (like your age, gender, occupation, education, address etc.) and the information about the pages on the site you visit/access, the links you click on the site, the number of times you access the page and any such browsing information.</p></div>

<div class="privacy_div"><h5 class="text-left">How is information used?</h5>
<p class="text-justify">VGH will use your personal information to provide personalized features to you on the Site and to provide for promotional offers to you through the Site and other channels. VGH will also provide this information to its business associates and partners so that they can get in touch with you when necessary to provide the services requested by you. VGH will use this information to preserve transaction history as governed by existing law or policy. VGH may also use contact information internally to direct its efforts for product improvement, to contact you as a survey respondent, to notify you if you win any contest; and to send you promotional materials from its contest sponsors or advertisers. VGH will also use this information to serve various promotional and advertising materials to you via display advertisements through the Google Ad network on third party websites. You can opt out of Google Analytics for Display Advertising and customize Google Display network ads using the Ads Preferences Manager. Information about Customers on an aggregate (excluding any information that may identify you specifically) covering Customer transaction data and Customer demographic and location data may be provided to partners of VGH for the purpose of creating additional features on the website, creating appropriate merchandising or creating new products and services and conducting marketing research and statistical analysis of customer behaviour and transactions.</p></div>

<div class="privacy_div"><h5 class="text-left">4.What Choice are available to you regarding collection, use and distribution of your information?</h5>
<p class="text-justify">To protect against the loss, misuse and alteration of the information under its control, VGH has in place appropriate physical, electronic and managerial procedures. For example, VGH servers are accessible only to authorized personnel and your information is shared with employees and authorised personnel on a need to know basis to complete the transaction and to provide the services requested by you. Although VGH will endeavour to safeguard the confidentiality of your personally identifiable information, transmissions made by means of the Internet cannot be made absolutely secure. By using this site, you agree that VGH will have no liability for disclosure of your information due to errors in transmission or unauthorized acts of third parties.</p></div>

<div class="privacy_div"><h5 class="text-left">5.How do we Collect the Information?</h5>
<p class="text-justify">VGH will collect personally identifiable information about you only as part of a voluntary registration process, on-line survey or any combination thereof. The Site may contain links to other Web sites. VGH is not responsible for the privacy practices of such Web sites which it does not own, manage or control. The Site and third-party vendors, including Google, use first-party cookies (such as the Google Analytics cookie) and third-party cookies (such as the DoubleClick cookie) together to inform, optimize, and serve ads based on someone's past visits to the Site.</p></div>

<div class="privacy_div"><h5 class="text-left">How can you correct inaccuracies in the information?</h5>
<p class="text-justify">To correct or update any information you have provided, the Site allows you to do it online. In the event of loss of access details you can send an e-mail to:<a href="mailto:customer.service@veggiegreenhouse.com"> customer.service@veggiegreenhouse.com</a></p></div>


<div class="privacy_div"><h5 class="text-left">Policy updates ?</h5>
<p class="text-justify">VGH reserves the right to change or update this policy at any time. Such changes shall be effective immediately upon posting to the Site.</p></div>




</div>
</section>