        <section id="categorySection">
            <div class="container-fluid">
                <div class="owl-carousel owl-theme owl-loaded category-carousel">
                    <div class="owl-stage-outer">
                        <ul class="nav nav-pills owl-stage" id="pills-tab" role="tablist">
                            <li class="owl-item" role="presentation">
                                <a class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" href="#allTab"
                                    role="tab" aria-controls="pills-home" aria-selected="true">
                                    <img src="<?php echo SITE_URL; ?>images/all.png" alt="" class="img-fluid" />
                                    <p>All</p>
                                </a>
                            </li>
                            <li class="owl-item" role="presentation">
                                <a class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" href="#groceryTab"
                                    role="tab" aria-controls="pills-profile" aria-selected="false">
                                    <img src="<?php echo SITE_URL; ?>images/grocery.png" alt="" class="img-fluid" />
                                    <p>Grocery</p>
                                </a>
                            </li>
                            <li class="owl-item" role="presentation">
                                <a class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" href="#fruitVaggiTab"
                                    role="tab" aria-controls="pills-contact" aria-selected="false">
                                    <img src="<?php echo SITE_URL; ?>images/fruits_veggi.png" alt=""
                                        class="img-fluid" />
                                    <p>Fruits and Vegetables</p>
                                </a>
                            </li>
                            <li class="owl-item" role="presentation">
                                <a class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" href="#beveragesTab"
                                    role="tab" aria-controls="pills-contact" aria-selected="false">
                                    <img src="<?php echo SITE_URL; ?>images/beverages.png" alt="" class="img-fluid" />
                                    <p>Beverages</p>
                                </a>
                            </li>
                            <!-- <li class="owl-item" role="presentation">
                                <a class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" href="#breakfastTab" role="tab" aria-controls="pills-contact" aria-selected="false">
                                    <img src="<?php echo SITE_URL; ?>images/breakfast.png" alt="" class="img-fluid" />
                                    <p>Breakfast</p>
                                </a>
                            </li> -->
                            <li class="owl-item" role="presentation">
                                <a class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" href="#householdTab"
                                    role="tab" aria-controls="pills-contact" aria-selected="false">
                                    <img src="<?php echo SITE_URL; ?>images/household.png" alt="" class="img-fluid" />
                                    <p>Household Items</p>
                                </a>
                            </li>
                            <!-- <li class="owl-item" role="presentation">
                                <a class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" href="#hygieneTab" role="tab" aria-controls="pills-contact" aria-selected="false">
                                    <img src="<?php echo SITE_URL; ?>images/hygiene.png" alt="" class="img-fluid" />
                                    <p>Hygiene</p>
                                </a>
                            </li> -->
                            <li class="owl-item" role="presentation">
                                <a class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" href="#packageTab"
                                    role="tab" aria-controls="pills-contact" aria-selected="false">
                                    <img src="<?php echo SITE_URL; ?>images/package.png" alt="" class="img-fluid" />
                                    <p>Biscuits, Snacks & Chocolates</p>
                                </a>
                            </li>
                            <li class="owl-item" role="presentation">
                                <a class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" href="#personalCareTab"
                                    role="tab" aria-controls="pills-contact" aria-selected="false">
                                    <img src="<?php echo SITE_URL; ?>images/personal_care.png" alt=""
                                        class="img-fluid" />
                                    <p>Personal Care</p>
                                </a>
                            </li>
                            <!-- <li class="owl-item" role="presentation">
                                <a class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" href="#homekitchenTab" role="tab" aria-controls="pills-contact" aria-selected="false">
                                    <img src="<?php echo SITE_URL; ?>images/home_kitchen.png" alt="" class="img-fluid" />
                                    <p>Home Kitchen</p>
                                </a>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="allTab" role="tabpanel" aria-labelledby="pills-home-tab">
                <div id="banner">
                    <div class="owl-carousel owl-theme owl-loaded banner-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_4.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row mt-5">
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="groceryTab" role="tabpanel" aria-labelledby="pills-profile-tab">
                <div id="banner">
                    <div class="owl-carousel owl-theme owl-loaded banner-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_4.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row mt-5">
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="fruitVaggiTab" role="tabpanel" aria-labelledby="pills-contact-tab">
                <div id="banner">
                    <div class="owl-carousel owl-theme owl-loaded banner-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_4.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row mt-5">
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="beveragesTab" role="tabpanel" aria-labelledby="pills-contact-tab">
                <div id="banner">
                    <div class="owl-carousel owl-theme owl-loaded banner-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_4.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row mt-5">
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="breakfastTab" role="tabpanel" aria-labelledby="pills-contact-tab">
                <div id="banner">
                    <div class="owl-carousel owl-theme owl-loaded banner-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_4.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row mt-5">
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="householdTab" role="tabpanel" aria-labelledby="pills-contact-tab">
                <div id="banner">
                    <div class="owl-carousel owl-theme owl-loaded banner-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_4.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row mt-5">
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="hygieneTab" role="tabpanel" aria-labelledby="pills-contact-tab">
                <div id="banner">
                    <div class="owl-carousel owl-theme owl-loaded banner-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_4.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row mt-5">
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="packageTab" role="tabpanel" aria-labelledby="pills-contact-tab">
                <div id="banner">
                    <div class="owl-carousel owl-theme owl-loaded banner-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_4.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row mt-5">
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="personalCareTab" role="tabpanel" aria-labelledby="pills-contact-tab">
                <div id="banner">
                    <div class="owl-carousel owl-theme owl-loaded banner-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_4.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row mt-5">
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="homekitchenTab" role="tabpanel" aria-labelledby="pills-contact-tab">
                <div id="banner">
                    <div class="owl-carousel owl-theme owl-loaded banner-carousel">
                        <div class="owl-stage-outer">
                            <div class="owl-stage">
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_4.jpg" alt="" class="img-fluid" />
                                </div>
                                <div class="owl-item">
                                    <img src="<?php echo SITE_URL; ?>images/slider_5.jpg" alt="" class="img-fluid" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container-fluid">
                    <div class="row mt-5">
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <!-- <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal" data-bs-target="#quantityModalPicker">5 Ltr. <i class="fas fa-sort-down"></i></a></li> -->
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_1.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_2.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_3.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_4.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xxl col-lg-3 col-md-4 col-sm-6">
                            <div class="productTile">
                                <a href="<?php echo SITE_URL; ?>Home/ProductDetail" class="">
                                    <div class="imgContainer">
                                        <p class="productOffer">-10%</p>
                                        <img src="<?php echo SITE_URL; ?>images/product_5.png" alt=""
                                            class="img-fluid" />
                                    </div>
                                    <h4>Fortune Rice Bran Health Oil Jar - 5 ltr</h4>
                                    <ul class="tilePrice">
                                        <li class="newPrice">₹770</li>
                                        <li class="oldPrice">₹800</li>
                                    </ul>
                                </a>
                                <ul class="tileButton">
                                    <li class="buyButton"><a href="javascript:void(0)" data-bs-toggle="modal"
                                            data-bs-target="#quantityModalPicker">5 Ltr. <i
                                                class="fas fa-sort-down"></i></a></li>
                                    <li class="cartButton"><a href="<?php echo SITE_URL; ?>Home/cart"><i
                                                class="fas fa-shopping-cart"></i>
                                            Add to Cart</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        </div>