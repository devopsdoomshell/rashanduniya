<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Login</title>
    <!-- Bootstrap CSS-->
    <link href="<?php  echo SITE_URL ?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">
    <!-- Main CSS-->
    <link href="<?php  echo SITE_URL ?>css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="#">
                                <div class="row">
                                   <div class="newddd">
                                   </div>
                                   <div class="col-sm-4"></div>
                                   <div class="col-sm-4">


                                       <img src="<?php echo SITE_URL ?>img/logo_new.png" style="
                                       width: 430px;
                                       height: 84px;
                                       ">
                                   </div> 
                                   <div class="col-sm-4"></div>
                               </div>
                           </a>
                       </div>

                       <div class="login-form">
                        <?php echo $this->Flash->render(); ?>
                       <!--<form>-->
                    <?php echo $this->Form->create('logins',array('url'=>array('controller'=>'logins','action'=>'forgotpassword'),'id'=>'loginform', 'class'=>'form-vertical'));?>
                    <div class="login-form-head">
                        <h4>Forgot Your Password</h4>
                    </div>
                    <?php echo $this->Flash->render(); ?>
                    <div class="login-form-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Username</label>
                            <input type="text" name="username" placeholder="Enter Email Address" autocomplete="off" id="exampleInputEmail1" class="form-control">

                        </div>
                        <div class="row mb-4 rmber-area">
                        </div>
                        <div class="submit-btn-area">
                            <button id="form_submit" type="submit">Submit <i class="ti-arrow-right"></i></button>
                        </div>
            <div class="row mb-4 rmber-area">
                        </div>
                    </div>
                </form>
             </div>
         </div>
     </div>
 </div>
</div>

</div>

<!-- Jquery JS-->
<script src="<?php  echo SITE_URL ?>vendor/jquery-3.2.1.min.js"></script>
<!-- Bootstrap JS-->
<script src="<?php  echo SITE_URL ?>vendor/bootstrap-4.1/popper.min.js"></script>
<script src="<?php  echo SITE_URL ?>vendor/bootstrap-4.1/bootstrap.min.js"></script>

<!-- Main JS-->
<script src="<?php  echo SITE_URL ?>js/main.js"></script>

</body>

</html>
<!-- end document-->
