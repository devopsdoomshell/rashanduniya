<?php //echo $user['confirm_pass'];die;?>
<section id="chick_tickte"><!--event_detail_page-->
<div class="container">
<hgroup class="innerpageheading">
<h2>Edit Profile</h2>
<ul>
<li><a href="#">Home</a></li>
<li><i class="fas fa-angle-double-right"></i></li>
<li>Edit Profile</li>
</ul>
</hgroup>

<div class="chick_tickte_form">
  <?php echo $this->Flash->render(); ?>
  <?php echo $this->Form->create($user,array(
                       'class'=>'form-horizontal',
                       'controller'=>'homes',
                       'action'=>'updateprofile',
                       'enctype' => 'multipart/form-data',
                       'validate' )); ?>
  <div class="form-group">
    <div class="col-sm-4">
    <label>Name</label>
    <?php echo $this->Form->input('name',array('class'=>'form-control','label' =>false,'required' => true,'placeholder'=>'Enter Name','pattern'=>'.*[^ ].*')); ?>
    <input type="hidden" id="id" name="id" value="<?php echo $user['id']?>" >
    </div>
    <div class="col-sm-4">
    <label>Mobile Number</label>
     <?php echo $this->Form->input('mobile',array('class'=>'form-control','placeholder'=>'Enter Mobile No.','maxlength'=>'20','id'=>'phone','label' =>false,'required' => true,'onkeypress'=> 'return isNumber(event);')); ?>
    </div>
    <div class="col-sm-4">
    <label>E-Mail Id</label>
    <?php echo $this->Form->input('email',array('class'=>'form-control','placeholder'=>'Enter E-Mail Id','id'=>'email','label' =>false,'required' => true,'onkeypress'=> 'return isNumber(event);','type'=>'email','readonly')); ?>
    </div>
    </div>
  
  <div class="form-group">
    <div class="col-sm-6">
    <label>Change Password</label>
       <?php echo $this->Form->input('password',array('type'=>'password','class'=>'form-control','placeholder'=>'Enter Your Password', 'id'=>'password','required'=>'true','label' =>false,'value'=>$user['confirm_pass'])); ?>
    
    </div>
    <div class="col-sm-6">
    <label>Confirm Password</label>
       <?php echo $this->Form->input('confirm_pass',array('type'=>'password','class'=>'form-control','placeholder'=>'Enter Your confirm Password','required'=>'true','id'=>'password','label' =>false,'value'=>$user['confirm_pass'])); ?>
    </div>
  </div>  


  <div class="form-group"> 
    <div class="col-sm-12">
      <!-- <?php
          echo $this->Form->submit(
              'Save', 
              array('class' => 'main_button', 'title' => 'Save','style'=>'border: 2px solid #ec118a important;')
          ); ?>-->
               <button type="submit" class="main_button">Save</button>
    </div>
  </div>
  </form>
</div>

</div> 
<div class="footer_buildings"></div>
</section><!--upcoming Event End-->
