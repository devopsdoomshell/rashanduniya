<section id="forgot_password_page"><!--event_detail_page-->
<div class="container">
<hgroup class="innerpageheading">
<h2>Password Reset</h2>
<ul>
<li><a href="#">Home</a></li>
<li><i class="fas fa-angle-double-right"></i></li>
<li>Password Reset</li>
</ul>
</hgroup>

<div class="forgot_form">
<h3>Reset Your Password</h3>

 <div class="tab-content">
    <div id="login_tab" class="tab-pane fade in active">
    <?php echo $this->Flash->render(); ?>
<?php /*echo $this->Form->create('User',array('url'=>array('class'=>'form-horizontal','controller'=>'Users','action'=>'forget_cpass/kp/'.$usrid),'type'=>'file','inputDefaults'=>array('div'=>false,'label'=>false),'class'=>'cont-form','role'=>'form','onsubmit'=>'return validate();')); */?>


 <?php echo $this->Form->create('User',array(
                       'class'=>'form-horizontal',
                       'controller'=>'Users',
                       'action'=>'forget_cpass/kp/'.$usrid,
                       'type'=>'file',
                       'enctype' => 'multipart/form-data',
                       'validate',
                       'onsubmit'=>'return validate();'
                        )); ?>


 <?php if($ftyp==1) { ?>
  <div class="form-group">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
       <?php echo $this->Form->password('password',array('required','id'=>'pass','class'=>'form-control', 'placeholder'=>($multiplelang[14]['title']) ? $multiplelang[14]['title'] : "New Password")); ?>
    </div>
    <div class="col-sm-4"></div>
  </div>
  
<div class="form-group">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
      <?php echo $this->Form->password('cpassword',array('required','id'=>'cpass','class'=>'form-control', 'placeholder'=>($multiplelang[15]['title']) ? $multiplelang[15]['title'] : "Confirm Password")); ?>
    </div>
    <div class="col-sm-4"></div>
  </div>


  <div class="form-group"> 
    <div class="col-sm-12">
       <?php
          echo $this->Form->submit(
              'Submit', 
              array('class' => 'main_button', 'id' => 'submit', 'title' => 'Submit','style'=>'border: 2px solid #ec118a;')
          ); ?>

    </div>
  </div>
  <?php } else {
 echo("<span style='color:red;align:center;margin-left:110px'>Invalid Key or Expired link</span>");    
 }
  
  ?>
<?php echo $this->Form->end(); ?>
    </div>
  </div> 

</div>

</div> 
<div class="footer_buildings"></div>
</section><!--upcoming Event End-->

<script type="text/javascript">
function validate(){
  
  if($('#pass').val()!=$('#cpass').val()){
    alert("Password and confirm password should be same");
    return false;
  }else
  return true;
}

</script>